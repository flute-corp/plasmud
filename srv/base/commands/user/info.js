/**
 * Description : Affichage des information d'un utilisateur.
 * Syntaxe : user-info <nom-d-utilisateur>
 */

async function main (context, name) {
  const { client, uc, print, check } = context
  try {
    if (!check('S', name)) {
      return
    }
    const { user } = await uc.FindUser.execute(name)
    const { clients } = await uc.FindUserClients.execute(user.id)
    const connected = clients.length > 0
    const banishment = await uc.GetUserBanishment.execute(user.id)
    print.text('user/info', {
      name,
      connected,
      email: user.email,
      dateCreation: user.dateCreation,
      dateLogin: user.dateLastUsed,
      charCount: 0,
      banishment,
      roles: user.roles
    })
  } catch (e) {
  }
}

module.exports = main

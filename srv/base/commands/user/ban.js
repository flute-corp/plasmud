const DateService = require('../../../infrastructure/frameworks/external-services/DateService')
const { parseDurationMs } = require('../../../../libs/parse-duration')

/**
 * Description : Bannissement d'un utilisateur
 * Syntaxe : user-ban <nom-d-utilisateur> <duration> <reason>
 */
async function main (context, name, duration, reason) {
  const { client, check, uc, print } = context
  try {
    if (!check('SSS', name, duration, reason)) {
      return
    }
    // : calculer la date de fin
    // La durée doit etre converti en ms
    // la durée doit etre en minutes, heures, ou jours, ou permanente
    const nDuration = parseDurationMs(duration)
    if (isNaN(nDuration)) {
      print.text('user.error.badDuration')
      print.text('user.info.durationFormat')
      return
    }
    const bForever = nDuration === Infinity
    const nTSEnd = bForever ? 0 : Date.now() + nDuration
    const { user } = await uc.FindUser.execute(name)
    // effectuer le bannissement
    const b = await uc.SetUserBanishment.execute(user.id, nTSEnd, reason, client.uid)
    // envoyer message d'adieu
    print.text.to(user.id, 'user/banned', {
      forever: bForever,
      reason,
      until: nTSEnd
    })
    // trouver les clients correspondant à l'utilisateur (pour les kicker)
    const { clients } = await uc.FindUserClients.execute(user.id)
    // ejecter les clients
    clients.forEach(c => {
      c.socket.close()
    })
    if (b.until) {
      print.log('%s banishes %s until %s because : %s', user.id, name, b.until, reason)
      print.text('user.info.banned', { name, date: DateService.getDateTimeString(b.until), reason: b.reason })
    } else {
      print.log('%s banishes %s permanently because : %s', user.id, name, reason)
      print.text('user.info.bannedForever', { name, reason: b.reason })
    }
  } catch (e) {
    switch (e.message) {
      case 'ERR_USER_NOT_FOUND':
        print.text('user.error.notFound', { name })
        break

      default:
        console.error(e)
        print.text('generic.error.command')
        break
    }
  }
}

module.exports = main

/**
 * Description : annulation du bannissement d'un utilisateur
 * Syntaxe : user-unban <nom-d-utilisateur>
 */

async function main (context, name) {
  const { check, uc, print } = context
  try {
    if (!check('S', name)) {
      return
    }
    const { user } = await uc.FindUser.execute(name)
    const b = await uc.RemoveUserBanishment.execute(user.id)
    if (b.success) {
      print.log('%s unbanishes %s', user.id, name)
      print.text('user.info.unbanned', { name })
    } else {
      print.text('user.warn.notBanned', { name })
    }
  } catch (e) {
    switch (e.message) {
      case 'ERR_USER_NOT_FOUND':
        print.text('user.error.notFound', { name })
        break

      default:
        console.error(e)
        print.text('generic.error.command')
        break
    }
  }
}

module.exports = main

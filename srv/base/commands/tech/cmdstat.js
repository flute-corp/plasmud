async function main ({ print, uc }) {
  const aStats = await uc.GetCommandStats.execute()
  aStats.sort((a, b) => b.count - a.count)
  print.text('tech/cmdstat', { stats: aStats })
}

module.exports = main

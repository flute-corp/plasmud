/**
 * Description : Authentifiacation du client
 * Cas d'utilisation : Utilisée automatiquement lors de la saisie login/mot de passe
 * Syntaxe : login <nom-d'utilisateur> <mot-de-passe>
 */

const pjson = require('../../../package.json')
const { motd } = require('../../config/motd')

/**
 * @param context {{uc: ContextUseCases, client: object, print: function}}
 * @param name
 * @param password
 * @returns {Promise<void>}
 */
async function main (context, name, password) {
  const { uc, client, print, check } = context
  if (!check('SS', name, password)) {
    return
  }
  const idClient = client.id
  try {
    const oResult = await uc.AuthenticateClient.execute(idClient, name, password)
    if (oResult.success) {
      const oUser = oResult.user
      const idUser = oUser ? oUser.id : ''
      print.log('client::%s has logged as user::%s', idClient, idUser)
      // TODO enregistrer le client dans le MUD
      // controllers.plasmud.registerPlayer(idClient)
      print.log('user::%s is now registered in game as "%s"', idUser, oUser.name)
      // TODO Enregistrer le user dans le txat
      const { user } = await uc.RegisterChatUser.execute(oUser.id, oUser.name, idClient)
      user.data.client = client
      print.text('login.info.success', { name })
      print.text('menu/main', {
        version: pjson.version,
        description: pjson.description,
        motd
      })
      await uc.JoinChannel.execute(user.id, 'lobby')
    } else {
      print.log('client::%s could not register as "%s"', idClient, name)
      if (oResult.banishment) {
        print.text('user-banned', {
          forever: oResult.banishment.forever,
          reason: oResult.banishment.reason,
          until: oResult.banishment.until
        })
        client.socket.close()
      } else {
        print.text('login.error.failure')
      }
    }
  } catch (e) {
    switch (e.message) {
      case 'ERR_AUTH_FAILED':
        print.text('login.error.failure')
        break

      case 'ERR_ALREADY_CONNECTED':
        print.text('login.error.already')
        break

      default:
        console.error(e)
        print.text('generic.error.command')
        break
    }
    if (client) {
      client.socket.close()
    }
  }
}

module.exports = main

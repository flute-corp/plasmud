/**
 * Description : changement du mot de passe utilisateur
 */

async function main ({ check, client, uc, print }, sNewPassword) {
  if (!check('S', sNewPassword)) {
    return
  }
  await uc.ChangeUserPassword.execute(client.uid, sNewPassword)
  print.text('passwd.info.success')
}

module.exports = main

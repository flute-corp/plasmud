/**
 * La commande d'aide permet d'affiche un fichier d'aide
 * exemple : help char create -> va lire templates/help/char/create
 */
function main (context, ...aArguments) {
  const sHelpFile = [
    'help',
    ...aArguments
  ].join('/')
  context.print.text(sHelpFile)
}

module.exports = main

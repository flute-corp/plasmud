/**
 * Description : Selection du personnage courant
 * Cas d'utilisation : Les client utilisent cette commande pour selectionner le personnage avec lequel ils vont jouer
 * Syntaxe : char select <nom-du-personnage>
 */

const { suggest } = require('@laboralphy/did-you-mean')

async function getCharacterNameSuggest (uc, client, name) {
  const r = await uc.GetCharacterList.execute(client.id)
  return suggest(name, r.characters.map(c => c.name))
}

async function main (context, name) {
  const { client, uc, print } = context
  try {
    if (name === undefined) {
      print.text('char.error.noParam')
      return
    }
    const r = await uc.SetClientCurrentCharacter.execute(client.id, name)
    print.text('char.info.selected', { name: r.character.name })
  } catch (e) {
    if (e.message === 'ERR_CHARACTER_NOT_FOUND') {
      print.text('char.error.notFound', { name, suggest: await getCharacterNameSuggest(uc, client, name) })
    } else {
      console.error(e)
      print.text('generic.error.command')
    }
  }
}

module.exports = main

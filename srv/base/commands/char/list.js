/**
 * Description : Affiche la liste des personnages d'un client
 * Syntaxe : char list
 */

/**
 * @param context {MUDContext}
 */
async function main (context) {
  const { uc, client, print } = context
  try {
    const r = await uc.GetCharacterList.execute(client.id)
    const characters = r.characters.map(c => ({
      name: c.name,
      current: c.selected,
      dates: {
        creation: c.dateCreation,
        lastUsed: c.dateLastUsed
      }
    }))
    if (characters.length > 0) {
      print.text('char/list', { characters })
    } else {
      print.text('char.warn.noCharacter', { characters })
    }
  } catch (e) {
    console.error(e)
    print.text('generic.error.command')
  }
}

module.exports = main

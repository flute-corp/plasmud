/**
 * Description : Suppression de l'un de ses propre personnages
 * Cas d'utilisation : Quand un client veut effacer l'un de ces personnage, il fait appel à cette commande
 * Syntaxe : char delete <nom-du-personnage>
 */

const { suggest } = require('@laboralphy/did-you-mean')

/**
 * Dans le cas où l'on orthographie mal un nom de personnage, suggérer un nom valide proche
 * @param uc {object} les use cases
 * @param client {Client}
 * @param name {string} nom du personnage mal orthographié
 * @returns {Promise<string[]>} tableau de noms proches
 */
async function getCharacterNameSuggest (uc, client, name) {
  const r = await uc.GetCharacterList.execute(client.id)
  return suggest(name, r.characters.map(c => c.name))
}

/**
 * Commande de suppression de personnage.
 * @param context {MUDContext}
 * @param name {string} nom du personnage
 * @returns {Promise<void>}
 */
async function main (context, name) {
  const { client, uc, print, check } = context
  try {
    if (name === undefined) {
      print.text('char.error.noParam')
      return
    }
    const r = await uc.DeleteCharacter.execute(client.uid, name)
    print.text('char.info.deleted', { name: r.name })
  } catch (e) {
    if (e.message === 'ERR_CHARACTER_NOT_FOUND') {
      print.text('char.error.notFound', { name, suggest: await getCharacterNameSuggest(uc, client, name) })
    } else {
      console.error(e)
      print.text('generic.error.command')
    }
  }
}

module.exports = main

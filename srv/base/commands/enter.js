async function main ({ uc, client, print }) {
  try {
    // s'enregistrer en tant que joueur dans le plasmud
    await uc.EnterGame.execute(client.uid)
    print.log('user::%s has enter plasmud', client.uid)
  } catch (e) {
    switch (e.message) {
      case 'ERR_NO_SELECTED_CHARACTER':
        print.text('enter.error.noChar')
        break

      case 'ERR_ILLEGAL_CHARACTER':
        print.text('enter.error.illegalChar')
        break

      default:
        console.error(e)
        print.text('generic.error.command')
        break
    }
  }
}

module.exports = main

const path = require('path')
const os = require('os')
const sHomeDir = path.resolve(os.homedir())

module.exports = {
  PLASMUD_SERVER_PORT_HTTP: {
    required: false,
    type: 'integer',
    default: 8082
  },
  PLASMUD_ADMIN_SERVER_PORT_HTTP: {
    required: false,
    type: 'integer',
    default: 8084
  },
  PLASMUD_SERVER_PORT_TELNET: {
    required: false,
    type: 'integer',
    default: 8023
  },
  PLASMUD_DATA_DIRECTORY: {
    required: false,
    type: 'string',
    default: path.join(sHomeDir, '.plasmud')
  },
  PLASMUD_SAVE_SLOT_DIRECTORY: {
    required: false,
    type: 'string',
    default: 'mudstate'
  },
  PLASMUD_MODULES: {
    required: true,
    type: 'string'
  },
  PLASMUD_LANG: {
    require: true,
    type: 'string'
  },
  PLASMUD_MOTD: {
    require: false,
    type: 'string',
    default: ''
  }
}

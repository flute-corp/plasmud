const fs = require('fs')
const path = require('path')

function getMOTD () {
  const dataDir = process.env.PLASMUD_DATA_DIRECTORY
  const sFilename = path.resolve(dataDir, 'motd.txt')
  try {
    return fs.readFileSync(sFilename).toString()
  } catch (e) {
    return ''
  }
}

module.exports = {
  motd: getMOTD()
}

const path = require('path')

module.exports = {
  root: {
    name: 'root',
    email: ''
  },
  path: path.resolve(process.env.PLASMUD_DATA_DIRECTORY, 'db'),
  collections: ['users', 'characters', 'banishments'],
  indexes: {
    users: {
      name: {
        caseInsensitive: true,
        unique: true
      }
    },
    characters: {
      name: {
        caseInsensitive: true,
        unique: true
      }
    },
    banishments: {
      banner: {}
    }
  }
}

const path = require('path')
const cwd = process.cwd()

/**
 * These are the locations where the server will scan for modules.
 * Any module may be located at any location, there is no special location for special type of module.
 * The ideal location for a module to be placed is in the PLASMUD_DATA_DIRECTORY folder
 *
 * location $CWD/libs/plasmud/modules : these are plasmud specific module library
 * location $CWD/modules : these are modules that can be common throughout many adventures
 * location PLASMUD_DATA_DIRECTORY : this is where adventures usually go
 */

module.exports = {
  modulesFolderList: [
    path.resolve(cwd, 'libs/plasmud/modules'), // plasmud library built-in modules
    path.resolve(cwd, 'modules'), // suggestion : server built-in module (combat, magic, ...)
    path.resolve(process.env.PLASMUD_DATA_DIRECTORY, 'modules') // suggestion : modularized adventure, map and resources
  ]
}

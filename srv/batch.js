require('dotenv').config()
const dependencies = require('./config/dependencies')

async function main () {
  // déterminer la commande principale
  const oContainer = dependencies.createContainer()
  const sCommand = process.argv[2].toLowerCase()
  const aArgv = process.argv.slice(3)
  const bc = oContainer.resolve('BatchController')
  switch (sCommand) {
    case 'show-user': {
      return bc.viewUserInfo(aArgv)
    }

    case 'admin-switches': {
      return bc.viewAdminSwitches()
    }
  }
}

main()

const envValidator = require('./config/env-validator')
const processEnvMan = require('../libs/process-env-man')

const oPackage = require('../package.json')
const Server = require('./infrastructure/frameworks/web/Server')
const dependencies = require('./config/dependencies')
const debug = require('debug')

const logServ = debug('serv:main')

logServ('%s v%s - %s', oPackage.name, oPackage.version, oPackage.description)

function checkProcessEnv () {
  logServ('checking environment variables')
  processEnvMan.checkAll(envValidator)
}

async function run () {
  checkProcessEnv()
  const oServer = new Server()

  const ports = {
    ws: process.env.PLASMUD_SERVER_PORT_HTTP,
    admin: process.env.PLASMUD_ADMIN_SERVER_PORT_HTTP,
    telnet: process.env.PLASMUD_SERVER_PORT_TELNET
  }

  await oServer.init({
    dependencies,
    ports
  })

  return oServer
}

run()
  .catch(e => console.error(e))

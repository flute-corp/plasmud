class GameInteractor {
  registerPlayer (uid) {
    throw new Error('ERR_NOT_IMPLEMENTED')
  }

  unregisterPlayer (uid) {
    throw new Error('ERR_NOT_IMPLEMENTED')
  }

  isCommandExist (sOpcode) {
    throw new Error('ERR_NOT_IMPLEMENTED')
  }

  processCommand (uid, sOpcode, args) {
    throw new Error('ERR_NOT_IMPLEMENTED')
  }

  saveGame (sNamespace) {
    throw new Error('ERR_NOT_IMPLEMENTED')
  }

  loadGame (sNamespace) {
    throw new Error('ERR_NOT_IMPLEMENTED')
  }
}

module.exports = GameInteractor

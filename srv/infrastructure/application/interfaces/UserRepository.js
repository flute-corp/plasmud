const Repository = require('./Repository')
/**
 * Opérations de base de tout repository proposant de la persistance
 */
class UserRepository extends Repository {
  /**
   * Recherche un utilisateur par son nom
   * @param name {string}
   * @returns {Promise<never>}
   */
  async findByName (name) {
    return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'))
  }

  /**
   * Renvoie le nombre d'utilisateur enregistrés
   * @returns {Promise<number>}
   */
  async getCount () {
    return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'))
  }
}

module.exports = UserRepository

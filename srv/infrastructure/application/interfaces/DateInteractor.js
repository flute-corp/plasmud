class DateInteractor {
  getDateString (oDate) {
    return new Error('ERR_NOT_IMPLEMENTED')
  }

  getTimeString (oDate, bSeconds = false) {
    return new Error('ERR_NOT_IMPLEMENTED')
  }

  getDateTimeString (oDate, bSeconds = false) {
    return new Error('ERR_NOT_IMPLEMENTED')
  }

  getDurationString (nDuration, format = {}) {
    return new Error('ERR_NOT_IMPLEMENTED')
  }
}

module.exports = DateInteractor

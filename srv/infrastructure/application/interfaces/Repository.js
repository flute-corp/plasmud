/**
 * Opérations de base de tout repository proposant de la persistance
 */
class Repository {
  /**
   * Sauvegarde de l'entité, si l'identifiant de l'entité est null ou undefined un nouvel identifiant est attribué
   * @param entity {object}
   * @returns {Promise<never>}
   */
  persist (entity) {
    return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'))
  }

  /**
   * Suppression de l'entité, l'identifiant de l'entité doit être spécifié.
   * @param entity
   * @returns {Promise<never>}
   */
  remove (entity) {
    return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'))
  }

  /**
   * Récupération d'une entité à partir de son identifiant
   * @param id {string|number}
   * @returns {Promise<never>}
   */
  get (id) {
    return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'))
  }

  /**
   * Récupération de toutes les entités
   * @returns {Promise<never>}
   */
  getAll () {
    return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'))
  }

  /**
   * Renvoie une liste d'entité satisfaisant le prédicat spécifié
   * @param pFunction {function}
   * @returns {Promise<never>}
   */
  find (pFunction) {
    return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'))
  }
}

module.exports = Repository

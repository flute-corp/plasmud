/**
 * Ce service permet de personnaliser les message de sortie a destination des clients en fonction de leur
 * moyens de connexion et de leur préférence.
 */

class ClientOutputInteractor {
  renderText (client, sTextRef, oContext) {
    throw new Error('ERR_NOT_IMPLEMENTED')
  }
}

module.exports = ClientOutputInteractor

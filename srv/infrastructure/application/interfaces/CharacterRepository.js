const Repository = require('./Repository')
/**
 * Opérations de base de tout repository proposant de la persistance
 */
class UserRepository extends Repository {
  /**
   * Recherche un utilisateur par son nom
   * @param name {string}
   * @return {Promise<never>}
   */
  findByName (name) {
    return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'))
  }

  findByOwner (owner) {
    return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'))
  }

  /**
   * Défini le personnage spécifié comme étant le personnage par défaut
   * @param id {string} identifiant du personnage
   */
  setCharacterAsCurrent (id) {
    return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'))
  }

  /**
   * Renvoie le personnage par défault de l'utilisateur spécifié
   * @param owner {string} identifiant utilisateur
   * @return {CharacterData}
   */
  getOwnerCurrentCharacter (owner) {
    return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'))
  }
}

module.exports = UserRepository

class AdminGetSwitch {
  constructor ({ AdminSwitchRepository }) {
    this.adminSwitchRepository = AdminSwitchRepository
  }

  async execute (sSwitch) {
    const value = await this.adminSwitchRepository.getSwitch(sSwitch)
    return { value }
  }
}

module.exports = AdminGetSwitch

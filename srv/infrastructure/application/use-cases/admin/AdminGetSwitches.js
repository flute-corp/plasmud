class AdminGetSwitches {
  constructor ({ AdminSwitchRepository }) {
    this.adminSwitchRepository = AdminSwitchRepository
  }

  async execute () {
    const aSwitches = await this.adminSwitchRepository.getSwitchList()
    const oSwitches = {}
    aSwitches.forEach(({ id, value }) => {
      oSwitches[id] = value
    })
    return oSwitches
  }
}

module.exports = AdminGetSwitches

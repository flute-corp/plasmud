const Message = require('../../../entities/Message')

class ParseMessage {
  parse (sMessage) {
    const aMessage = sMessage.trim().split(' ')
    const opcode = aMessage.shift()
    return {
      valid: true,
      opcode,
      arguments: aMessage
    }
  }

  /**
   * Transforme une chaine de caractère en entité message
   * @return {{success: boolean, error: string, message: Message}}
   */
  async execute (sMessage) {
    const t = typeof sMessage
    if (t !== 'string') {
      throw new TypeError('ParseMessage message should be a string... ' + t + 'given')
    }
    if (sMessage.startsWith('{')) {
      throw new Error('ERR_BAD_FORMAT')
    }
    return { message: this.parse(sMessage) }
  }
}

module.exports = ParseMessage

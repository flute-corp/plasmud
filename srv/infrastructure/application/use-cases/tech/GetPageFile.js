/**
 * Obtenir le fichier d'aide correspondant
 */
class GetPageFile {
  constructor ({ TemplateInteractor }) {
    this.template = TemplateInteractor
  }

  async execute (file) {
    return {
      content: this.template.render('menu/' + file)
    }
  }
}

module.exports = GetPageFile

class ExecuteCommand {
  constructor ({ CommandStatRepository, CommandRunnerInteractor, GameInteractor, CLIENT_STATES }) {
    this.commandRunner = CommandRunnerInteractor
    this.game = GameInteractor
    this.commandStatRepository = CommandStatRepository
    this.CLIENT_STATES = CLIENT_STATES
  }

  async commitTime (m, t) {
    const [s, ns] = process.hrtime(t)
    return this.commandStatRepository.addCommandStat(m, s + ns / 1e9)
  }

  async execute (context, message) {
    // vérifier que le client soit bien dans le MUD
    const aTime1 = process.hrtime()
    const client = context.client
    let r
    if (this.game.isCommandExist(message.opcode)) {
      if (client.state === this.CLIENT_STATES.CLIENT_STATE_IN_GAME) {
        r = await this.game.processCommand(context.client.uid, message.opcode, message.arguments)
      } else {
        throw new Error('ERR_NOT_IN_GAME')
      }
    } else {
      r = await this.commandRunner.runScript(message.opcode, context, message.arguments)
    }
    await this.commitTime(message.opcode, aTime1)
    return r
  }
}

module.exports = ExecuteCommand

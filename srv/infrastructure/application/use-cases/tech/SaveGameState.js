class SaveGameState {
  constructor ({ GameInteractor }) {
    this.game = GameInteractor
  }

  /**
   * Sauvegarde le contenu du jeu dans un fichier spécifié
   * @returns {Promise<void>}
   */
  async execute (sFileName) {
    await this.game.saveGame(sFileName)
  }
}

module.exports = SaveGameState

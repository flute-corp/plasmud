const path = require('path')

/**
 * Obtenir le fichier d'aide correspondant
 */
class InitApplication {
  constructor ({
    DatabaseInteractor,
    IntlInteractor,
    TemplateInteractor,
    ChatInteractor,
    GameInteractor,
    CommandRunnerInteractor,
    ContextBuilderInteractor,
    AdminSwitchRepository,
    ADMIN_SWITCHES
  }) {
    this.ADMIN_SWITCHES = ADMIN_SWITCHES
    this._interactors = {
      DatabaseInteractor,
      IntlInteractor,
      TemplateInteractor,
      ChatInteractor,
      GameInteractor,
      CommandRunnerInteractor,
      ContextBuilderInteractor
    }
    this._repositories = {
      AdminSwitchRepository
    }
  }

  async execute ({
    moduleLocation,
    logFunc
  }) {
    const PATH_STRINGS = path.join(moduleLocation, 'strings')
    const STRINGS = require(PATH_STRINGS)

    const int = this._interactors
    const rep = this._repositories

    logFunc('database - starting service')
    await int.DatabaseInteractor.init()

    logFunc('string - starting service')
    await int.IntlInteractor.init(STRINGS)

    logFunc('template renderer - starting service')
    await int.TemplateInteractor.init(path.join(moduleLocation, 'templates'))

    logFunc('chat - starting service')
    int.ChatInteractor.init()

    logFunc('plasmud - starting service')
    await int.GameInteractor.init()

    logFunc('command interpreter - starting service')
    await int.CommandRunnerInteractor.loadScripts(path.join(moduleLocation, 'commands'))

    logFunc('client context builder - starting service')
    int.ContextBuilderInteractor.init()

    logFunc('opening service for clients')
    await rep.AdminSwitchRepository.setSwitch(this.ADMIN_SWITCHES.OPEN, true)
  }
}

module.exports = InitApplication

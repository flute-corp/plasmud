class ShutdownService {
  constructor ({ DateInteractor, AdminSetSwitch, AdminGetSwitch, KickAllClients, AdminBroadcastMessage, ADMIN_SWITCHES }) {
    this._timer = 0
    this._interval = 0
    this._nTSShutdown = 0
    this.useCases = {
      AdminSetSwitch,
      AdminGetSwitch,
      AdminBroadcastMessage,
      KickAllClients
    }
    this.DateInteractor = DateInteractor
    this.ADMIN_SWITCHES = ADMIN_SWITCHES
    this._message = ''
  }

  _clearTimeout () {
    if (this._timer) {
      clearTimeout(this._timer)
      this._timer = 0
    }
  }

  _clearInterval () {
    if (this._interval) {
      clearInterval(this._interval)
      this._interval = 0
    }
    this._nTSShutdown = 0
  }

  _doWarn () {
    const nDateNow = Date.now()
    const nETAms = this._nTSShutdown - nDateNow
    const nETA = Math.floor(nETAms / 1000)
    if (nETA === 30) {
      this._doBroadcast()
    } else if (nETA <= 10) {
      // Ultimate message
      this._doBroadcast()
      this._clearInterval()
    } else if (nETA % 60 === 0) {
      this._doBroadcast()
    }
  }

  _doBroadcast () {
    const nDateNow = Date.now()
    const nETAms = this._nTSShutdown - nDateNow
    const sDuration = this.DateInteractor.getDurationString(nETAms)
    const sMessage = this._message + ' ' + '(T - ' + sDuration + ')'
    this.useCases.AdminBroadcastMessage.execute(sMessage)
  }

  /**
   * Déconnecte tous les clients du service
   * @returns {*}
   * @private
   */
  _doKickClients () {
    return this.useCases.KickAllClients.execute()
  }

  setShutdownTimer (nDelay = 0) {
    if (nDelay > 0) {
      // Avec delai
      const nDelayMS = nDelay * 60 * 1000
      const nDateNow = Date.now()
      this._clearInterval()
      this._clearTimeout()
      this._timer = setTimeout(() => {
        this._doKickClients()
        this._clearInterval()
        this._timer = 0
      }, nDelayMS)
      this._nTSShutdown = nDateNow + nDelayMS
      this._doBroadcast()
      this._interval = setInterval(() => this._doWarn(), 1000)
    } else {
      // Arret immédiat
      this._clearInterval()
      this._clearTimeout()
      this._doKickClients()
    }
  }

  async execute ({ cancel = false, delay = 0, message = '' } = {}) {
    const { value: bOpen } = await this.useCases.AdminGetSwitch.execute(this.ADMIN_SWITCHES.OPEN)
    if (cancel) {
      if (bOpen) {
        return {
          nop: true,
          cancel: false,
          open: true
        }
      }
      this._clearInterval()
      this._clearTimeout()
      await this.useCases.AdminSetSwitch.execute(this.ADMIN_SWITCHES.OPEN, true)
      return {
        cancel: true,
        open: true
      }
    } else {
      if (!bOpen) {
        return {
          nop: true,
          cancel: false,
          open: false
        }
      }
      this._message = message
      await this.useCases.AdminSetSwitch.execute(this.ADMIN_SWITCHES.OPEN, false)
      this.setShutdownTimer(delay)
      return {
        delay,
        message,
        cancel,
        open: false
      }
    }
  }
}

module.exports = ShutdownService

class RestoreGameState {
  constructor ({ GameInteractor }) {
    this.game = GameInteractor
  }

  /**
   * Sauvegarde le contenu du jeu dans un fichier spéciifé
   * @returns {Promise<void>}
   */
  async execute () {
    await this.game.loadGame()
  }
}

module.exports = RestoreGameState

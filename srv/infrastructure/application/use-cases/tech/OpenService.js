class OpenService {
  constructor ({ AdminSetSwitch, ADMIN_SWITCHES }) {
    this.useCases = {
      AdminSetSwitch
    }
    this.ADMIN_SWITCHES = ADMIN_SWITCHES
  }

  async execute () {
    await this.useCases.AdminSetSwitch.execute(this.ADMIN_SWITCHES.OPEN, true)
    return {
      open: true
    }
  }
}

module.exports = OpenService

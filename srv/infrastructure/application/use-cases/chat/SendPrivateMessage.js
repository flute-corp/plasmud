class SendPrivateMessage {
  constructor ({ ChatInteractor }) {
    this.chat = ChatInteractor
  }

  async execute (uid, duid, sMessage) {
    return this.chat.sendPrivateMessage(uid, duid, sMessage)
  }
}

module.exports = SendPrivateMessage

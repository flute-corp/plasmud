class LeaveChannel {
  constructor ({ ChatInteractor }) {
    this.chat = ChatInteractor
  }

  async execute (uid, sChannel) {
    return this.chat.leave(uid, sChannel)
  }
}

module.exports = LeaveChannel

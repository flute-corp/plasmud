class RegisterChatUser {
  constructor ({ ChatInteractor, ClientRepository }) {
    this.chat = ChatInteractor
    this.clientRepository = ClientRepository
  }

  async execute (uid, sName, idClient) {
    // c'est dans ChatInteractor que les évènement txat sont écoutés
    // et on peut récupérer le client dans les évènements car on le fixe au client txat ici
    const oTxatUser = this.chat.connectUser(uid, sName)
    oTxatUser.data.client = await this.clientRepository.get(idClient)
    return Promise.resolve({
      user: oTxatUser
    })
  }
}

module.exports = RegisterChatUser

const Character = require('../../../entities/Character')

class CreateCharacter {
  constructor ({ ClientRepository, CharacterRepository }) {
    this.clientRepository = ClientRepository
    this.characterRepository = CharacterRepository
  }

  isValidCharacterName (name) {
    // le nom doit etre supérieur à 2 caractères
    // ne pas comporter d'espace ou de charactère chelou
    const bCrit1 = Boolean(name.match(/^[a-z][-a-z0-9]{2,}$/i))
    const bCrit2 = !name.startsWith('-')
    const bCrit3 = !name.endsWith('-')
    const bCrit4 = !name.includes('--')
    return bCrit1 && bCrit2 && bCrit3 && bCrit4
  }

  /**
   * Efface un personnage d'un joueur
   * @param idUser {number}
   * @param name {string}
   * @returns {Promise<{character}>}
   */
  async execute (idUser, name) {
    const oChar = await this.characterRepository.findByName(name)
    if (oChar) {
      throw new Error('ERR_CHARACTER_NAME_ALREADY_USED')
    }
    if (!this.isValidCharacterName(name)) {
      throw new Error('ERR_CHARACTER_NAME_INVALID')
    }
    const d = Date.now()
    const c = new Character({
      name,
      owner: idUser,
      dateCreation: d
    })
    this.characterRepository.persist(c)
    return {
      character: c
    }
  }
}

module.exports = CreateCharacter

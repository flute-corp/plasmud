class DeleteCharacter {
  constructor ({ ClientRepository, CharacterRepository }) {
    this.clientRepository = ClientRepository
    this.characterRepository = CharacterRepository
  }

  /**
   * Efface un personnage d'un joueur
   * @param idUser {number}
   * @param name {string}
   * @returns {Promise<{character}>}
   */
  async execute (idUser, name) {
    name = name.toLowerCase()
    const oChar = await this.characterRepository.findByName(name)
    const sCharName = oChar.name
    // verifier que le personnage appartienne bien au user
    if (oChar && oChar.owner === idUser) {
      // TODO Peut etre faudrait il archiver le personnage
      await this.characterRepository.remove(oChar)
      return {
        name: sCharName
      }
    } else {
      throw new Error('ERR_CHARACTER_NOT_FOUND')
    }
  }
}

module.exports = DeleteCharacter

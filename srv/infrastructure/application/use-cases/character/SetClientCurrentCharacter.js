class SetClientCurrentCharacter {
  constructor ({ ClientRepository, CharacterRepository }) {
    this.clientRepository = ClientRepository
    this.characterRepository = CharacterRepository
  }

  /**
   * Définit le personnage courant du client.
   * Le personnage courant est celui que l'utilisateur va utiliser quand il rejoindra le jeu.
   * @param clientId {number}
   * @param name {string}
   * @returns {Promise<{character}>}
   */
  async execute (clientId, name) {
    name = name.toLowerCase()
    const client = await this.clientRepository.get(clientId)
    const oChar = await this.characterRepository.findByName(name)
    // verifier que le personnage appartienne bien au client
    if (oChar && oChar.owner === client.uid) {
      // Personnage éligible
      await this.characterRepository.setCharacterAsCurrent(oChar.id)
      return {
        character: oChar
      }
    } else {
      throw new Error('ERR_CHARACTER_NOT_FOUND')
    }
  }
}

module.exports = SetClientCurrentCharacter

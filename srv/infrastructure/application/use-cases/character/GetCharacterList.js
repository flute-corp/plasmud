class GetCharacterList {
  constructor ({ ClientRepository, CharacterRepository }) {
    this.clientRepository = ClientRepository
    this.characterRepository = CharacterRepository
  }

  async execute (clientId) {
    const client = await this.clientRepository.get(clientId)
    const uid = client.uid
    if (uid) {
      const aCharList = await this.characterRepository.findByOwner(uid)
      return {
        characters: aCharList
      }
    } else {
      throw new Error('ERR_CLIENT_HAS_NO_USER')
    }
  }
}

module.exports = GetCharacterList

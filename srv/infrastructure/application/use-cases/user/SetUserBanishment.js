const Banishment = require('../../../entities/Banishment')

class SetUserBanishment {
  constructor ({ UserRepository, BanishmentRepository }) {
    this._userRepository = UserRepository
    this._banishmentRepository = BanishmentRepository
  }

  async getBanishment (idBanishment) {
    try {
      if (idBanishment) {
        return await this._banishmentRepository.get(idBanishment)
      } else {
        return Promise.resolve(null)
      }
    } catch (e) {
      return Promise.resolve(null)
    }
  }

  async execute (idUser, nDateEnd, sReason, idBanner) {
    const oUser = await this._userRepository.get(idUser)
    const idBanishment = oUser.banishment
    const bForever = !nDateEnd
    const dateEnd = bForever ? 0 : nDateEnd
    const oBanishment = await this.getBanishment(idBanishment)
    if (oBanishment) {
      // il faut calculer le date end
      oBanishment.dateEnd = dateEnd
      oBanishment.forever = bForever
      oBanishment.reason = sReason
      oBanishment.banner = idBanner
      await this._banishmentRepository.persist(oBanishment)
      return {
        until: bForever ? false : dateEnd,
        forever: bForever,
        reason: sReason,
        banner: idBanner
      }
    } else {
      const oNewBanishment = new Banishment({
        dateBegin: Date.now(),
        dateEnd,
        forever: bForever,
        reason: sReason,
        banner: idBanner
      })
      await this._banishmentRepository.persist(oNewBanishment)
      oUser.banishment = oNewBanishment.id
      await this._userRepository.persist(oUser)
      return {
        until: bForever ? false : dateEnd,
        forever: bForever,
        reason: sReason,
        banner: idBanner
      }
    }
  }
}

module.exports = SetUserBanishment

class FindUser {
  constructor ({ UserRepository, GetUserBanishment }) {
    this.userRepository = UserRepository
    this.getUserBanishment = GetUserBanishment
  }

  getDateDisplayString (timestamp) {
    const oDate = new Date(timestamp)
    return oDate.toJSON()
  }

  async execute (sName, bFull = false) {
    const oUser = await this.userRepository.findByName(sName)
    if (oUser) {
      if (bFull) {
        // Reformater pour affichage
        const oUserDisp = {
          id: oUser.id,
          name: oUser.name,
          email: oUser.email,
          dateCreation: this.getDateDisplayString(oUser.dateCreation),
          dateLastUsed: this.getDateDisplayString(oUser.dateLastUsed),
          roles: oUser.roles
        }
        const oBanishment = await this.getUserBanishment.execute(oUser.id)
        if (oBanishment.banned) {
          oUserDisp.banishment = {
            banner: oBanishment.banner,
            when: this.getDateDisplayString(oBanishment.when),
            until: oBanishment.forever ? 'forever' : this.getDateDisplayString(oBanishment.until),
            reason: oBanishment.reason
          }
        }
        return {
          user: oUserDisp
        }
      } else {
        return {
          user: oUser
        }
      }
    } else {
      throw new Error('ERR_USER_NOT_FOUND')
    }
  }
}

module.exports = FindUser

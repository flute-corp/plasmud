const User = require('../../../entities/User')
const { isValidEMailAddress } = require('../../../frameworks/common/validator')

class CreateUser {
  constructor ({ UserRepository, USER_ROLES }) {
    this._userRepository = UserRepository
    this.USER_ROLES = USER_ROLES
  }

  async execute (sName, sEmail, sPassword) {
    // Validité du user name
    if (!sName.match(/^[-_a-z0-9]{3,24}$/i)) {
      throw new Error('ERR_USER_NAME_INVALID')
    }
    // Validité de l'email
    if (!isValidEMailAddress(sEmail)) {
      throw new Error('ERR_USER_EMAIL_INVALID')
    }

    // déterminer si le nom d'utilisateur existe déja
    const oHistoUser = await this._userRepository.findByName(sName)
    if (oHistoUser) {
      // on ne peut pas créer un nouvel utilisateur portant le nom d'un utilisateur existant
      throw new Error('ERR_USER_NAME_ALREADY_TAKEN')
    }
    const nNowTimeStamp = Date.now()
    const roles = sName === 'root' ? Object.values(this.USER_ROLES) : []
    const oUser = new User({
      id: null,
      name: sName,
      email: sEmail,
      password: sPassword,
      dateCreation: nNowTimeStamp,
      dateLastUsed: nNowTimeStamp,
      roles
    })
    await this._userRepository.persist(oUser)
    return {
      user: oUser
    }
  }
}

module.exports = CreateUser

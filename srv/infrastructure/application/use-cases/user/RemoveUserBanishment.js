class RemoveUserBanishment {
  constructor ({ UserRepository, BanishmentRepository }) {
    this._userRepository = UserRepository
    this._banishmentRepository = BanishmentRepository
  }

  async execute (idUser) {
    const oUser = await this._userRepository.get(idUser)
    const idBanishment = oUser.banishment
    oUser.banishment = ''
    await this._userRepository.persist(oUser)
    if (idBanishment) {
      const oBanishment = await this._banishmentRepository.get(idBanishment)
      oBanishment.dateEnd = Date.now()
      oBanishment.forever = false
      await this._banishmentRepository.persist(oBanishment)
      return {
        success: true
      }
    } else {
      return {
        success: false,
        hint: 'HINT_NOT_BANNED'
      }
    }
  }
}

module.exports = RemoveUserBanishment

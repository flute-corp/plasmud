class ChangeUserPassword {
  constructor ({ UserRepository }) {
    this._userRepository = UserRepository
  }

  async execute (idUser, sPassword) {
    // déterminer si le nom d'utilisateur existe déja
    const oUser = await this._userRepository.get(idUser)
    if (!oUser) {
      // on ne trouve pas l'utilisateur
      throw new Error('ERR_USER_NOT_FOUND')
    }
    oUser.password = sPassword
    await this._userRepository.persist(oUser)
    return {
      user: oUser
    }
  }
}

module.exports = ChangeUserPassword

class SendMessageToClient {
  constructor ({ ClientOutputInteractor }) {
    this.clientOutputInteractor = ClientOutputInteractor
  }

  async execute (client, content) {
    return this.clientOutputInteractor.sendToClient(client, content)
  }
}

class FindUserClients {
  constructor ({ ClientRepository }) {
    this.clientRepository = ClientRepository
  }

  /**
   * Creation d'une instance client à partir d'une socket de connexion
   */
  async execute (idUser) {
    const clients = await this.clientRepository.findByUser(idUser)
    return {
      clients
    }
  }
}

module.exports = FindUserClients

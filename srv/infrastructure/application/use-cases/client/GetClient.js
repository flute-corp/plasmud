class GetClient {
  constructor ({ ClientRepository }) {
    this.clientRepository = ClientRepository
  }

  /**
   * Creation d'une instance client à partir d'une socket de connexion
   */
  async execute (clientId) {
    const client = await this.clientRepository.get(clientId)
    return {
      client
    }
  }
}

module.exports = GetClient

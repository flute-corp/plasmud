const Client = require('../../../entities/Client')
const CLIENT_TYPES = require('../../types/client-types')

const ALLOWED_CLIENT_TYPES = new Set(Object.values(CLIENT_TYPES))

class ConnectClient {
  constructor ({ ClientRepository, ContextBuilderInteractor, AdminSwitchRepository, CLIENT_STATES, ADMIN_SWITCHES }) {
    this.clientRepository = ClientRepository
    this.contextBuilder = ContextBuilderInteractor
    this.adminSwitchRepository = AdminSwitchRepository
    this.CLIENT_STATES = CLIENT_STATES
    this.ADMIN_SWITCHES = ADMIN_SWITCHES
  }

  /**
   * Creation d'une instance client à partir d'une socket de connexion
   * Invoquée à chaque fois qu'un client se connecte.
   * @param socket {WebSocket}
   * @param type {string} 'ws'
   * @param cbTransmit {function} function (client, message)
   */
  async execute (socket, type, cbTransmit) {
    // Verifier si le flag d'admin OPEN est positionné à true
    const bServiceOpen = await this.adminSwitchRepository.getSwitch(this.ADMIN_SWITCHES.OPEN, true)
    if (!bServiceOpen) {
      // Pas de connexion client autorisé
      throw new Error('ERR_SERVICE_DOWN')
    }
    if (!ALLOWED_CLIENT_TYPES.has(type)) {
      console.error('unknown client type : "%s", client type must be [%s]', type, [...ALLOWED_CLIENT_TYPES].join(', '))
      throw new Error('ERR_CLIENT_TYPE_UNKNOWN')
    }
    const client = new Client({
      socket,
      state: this.CLIENT_STATES.CLIENT_STATE_CONNECTED,
      type
    })
    await this.clientRepository.persist(client)
    const context = this.contextBuilder.buildClientContext(client)
    context.print.to = async (idUser, sString) => {
      if (typeof idUser !== 'string') {
        return
      }
      // 1. Chercher les clients de cet utilisateur
      const aClients = await this.clientRepository.findByUser(idUser)
      // 2. Transmettre le print à tous les clients
      for (const c of aClients) {
        const prom = cbTransmit(c, sString)
        if (prom instanceof Promise) {
          await prom
        }
      }
    }
    context.print.text.to = async (idUser, sString, oVariables) => {
      const aText = context
        .text(sString, oVariables)
        .split('\n')
      for (const s of aText) {
        await context.print.to(idUser, s)
      }
    }
    return {
      client,
      context
    }
  }
}

module.exports = ConnectClient

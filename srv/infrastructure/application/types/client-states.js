module.exports = {
  CLIENT_STATE_CONNECTED: 'CLIENT_STATE_CONNECTED', // le client est juste connecté
  CLIENT_STATE_AUTHENTICATED: 'CLIENT_STATE_AUTHENTICATED', // le client a entré un mot de passe valide
  CLIENT_STATE_IN_GAME: 'CLIENT_STATE_IN_GAME', // le client est entré dans le jeu, certaines fonctions ne sont plus disponibles
  CLIENT_STATE_DISCONNECTED: 'CLIENT_STATE_DISCONNECTED' // le client n'est plus connecté
}

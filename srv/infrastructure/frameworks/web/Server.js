const express = require('express')
const { WebSocket } = require('ws')
const telnet = require('telnet')
const http = require('http')
const debug = require('debug')
const fs = require('fs')
const path = require('path')

// web server sub components
const { SOCKET_PATH } = require('../../../../libs/socket-path')
const httpPresentedResponse = require('./middlewares/httpPresentedResponse')
const apiRouter = require('./routes/api')
const adminRouter = require('./routes/admin')

const logServ = debug('serv:main')

class Server {
  constructor () {
    this._servers = {
      exp: {},
      http: null,
      httpAdmin: null,
      ws: null,
      telnet: null,
      app: null,
      appAdmin: null
    }
    this._container = null
  }

  checkDocumentRoot () {
    const sDocumentRoot = path.resolve(__dirname, '../../../../dist/spa')
    try {
      fs.statSync(sDocumentRoot)
      return sDocumentRoot
    } catch (e) {
      logServ('document root does not exist : %s - spa might not be built.', sDocumentRoot)
      return null
    }
  }

  getWebApp (port, name) {
    const sPort = port.toString()
    if (!(sPort in this._servers.exp)) {
      const app = express()
      app.use(express.json())
      app.use(httpPresentedResponse)
      const serv = http.createServer(app)
      this._servers.exp[sPort] = {
        app,
        serv,
        port,
        names: new Set()
      }
    }
    const s = this._servers.exp[sPort]
    if (name) {
      s.names.add(name)
    }
    return s
  }

  createTelnetServer (cnxHandler) {
    return telnet.createServer(function (socket) {
      // make unicode characters work properly
      socket.do.transmit_binary()

      // make the client emit 'window size' events
      socket.do.window_size()

      cnxHandler(socket)
    })
  }

  /**
   * Remplace le début du chemin par "." s'il correspond au CWD
   * Permet ainsi de créer un chemin relatif au CWD
   */
  convertPathToRelativeCWD (sPath) {
    const rCWD = new RegExp('^' + process.cwd().replace(/\/+$/, ''))
    return sPath.replace(rCWD, '.')
  }

  /**
   * Création des instance des différents servers (express, http, ws)
   */
  async init ({
    dependencies,
    autorun = true
  }) {
    try {
      // Awilix : contruction des dépendences
      logServ('building dependencies')
      const container = dependencies.createContainer() // TODO async ou pas ?
      this._container = container

      // Express : instancier le service http d'administration (qui sera sur un autre port)
      const cwd = process.cwd()
      logServ('current working directory : %s', cwd)

      const PATH_BASE_MODULE = path.resolve('./srv/base') // container.resolve('PATH_BASE_MODULE')
      logServ('base module path : %s', this.convertPathToRelativeCWD(PATH_BASE_MODULE))

      const InitApplication = container.resolve('InitApplication')
      await InitApplication.execute({
        moduleLocation: PATH_BASE_MODULE,
        logFunc: debug('serv:init')
      })

      const oUserCount = await container.resolve('GetUserCount').execute()
      logServ('registered user count : %d', oUserCount.count)

      if (autorun) {
        const ports = {
          telnet: process.env.PLASMUD_SERVER_PORT_TELNET,
          ws: process.env.PLASMUD_SERVER_PORT_HTTP,
          admin: process.env.PLASMUD_ADMIN_SERVER_PORT_HTTP
        }
        logServ('starting listening services')
        await this.listen(ports)
      }
    } catch (e) {
      logServ('server initialization failed')
      throw e
    }
  }

  defineAdminWebApp (port) {
    logServ('creating admin http service')
    const { app } = this.getWebApp(port, 'admin')
    logServ('declaring admin application routes and middlewares')
    app.use('/admin', adminRouter(this._container))
  }

  defineBaseWebApp (port) {
    logServ('creating base http service')
    const { app } = this.getWebApp(port, 'api')

    logServ('declaring base application routes and middlewares')
    app.use('/api', apiRouter(this._container))

    const sDocumentRoot = this.checkDocumentRoot()
    logServ('spa location : %s', this.convertPathToRelativeCWD(sDocumentRoot))
    app.use('/', express.static(sDocumentRoot))
  }

  /**
   * Mise à l'écoute du serveur sur le port spécifié en paramètre
   * @param port {number}
   */
  listenWS (port) {
    // Creation serveur HTTP
    logServ('creating websocket service')
    // SOCKET_PATH est la route dédiée aux websockets
    if (SOCKET_PATH) {
      logServ('socket path : %s', SOCKET_PATH)
    }

    const { serv } = this.getWebApp(port, 'ws')

    // Creation server WebSocket
    logServ('building websocket server')
    const wss = new WebSocket.Server({
      server: serv,
      path: SOCKET_PATH || null
    })

    logServ('declaring websocket client connection handler')

    // Controleur WebSocket : initialisation
    const oWSController = this._container.resolve('WebSocketController')
    wss.on('connection', ws => oWSController
      .connectClient(ws)
      .catch(e => {
        switch (e.message) {
          case 'ERR_SERVICE_DOWN': {
            break
          }
          default: {
            logServ(e.message)
            break
          }
        }
      }))
  }

  listenTelnet (port) {
    port = parseInt(port)
    if (port) {
      logServ('creating telnet service')
      // Creation serveur Telnet
      logServ('declaring telnet client connection handler')
      const oTelnetController = this._container.resolve('TelnetController')
      this._servers.telnet = this.createTelnetServer(socket => oTelnetController.connectClient(socket))
      this._servers.telnet.listen(port)
      logServ('telnet : now listening on port ' + port)
    }
  }

  openPorts () {
    const proms = Object.values(this
      ._servers
      .exp)
      .map(({ serv, port, names }) =>
        new Promise(resolve => {
          return serv.listen(port, '0.0.0.0', () => {
            const sNames = [...names].join(', ')
            logServ('%s : now listening on port %d', sNames, port)
            resolve()
          })
        })
      )
    return Promise.all(proms)
  }

  /**
   *
   * @param ports {{admin: number, ws: number, telnet: number}}
   * @returns {Promise<void>}
   */
  listen (ports) {
    if (ports.admin) {
      this.defineAdminWebApp(ports.admin)
    }
    if (ports.ws) {
      this.defineBaseWebApp(ports.ws)
      this.listenWS(ports.ws)
    }
    if (ports.telnet) {
      this.listenTelnet(ports.telnet)
    }
    logServ('opening http ports')
    return this.openPorts()
  }
}

module.exports = Server

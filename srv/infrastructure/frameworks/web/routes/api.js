const express = require('express')

function main (container) {
  const router = express.Router()
  const oHttpController = container.resolve('ApiController')

  /**
   * Création d'un utilisateur (Compte).
   * Cette route est solliscité par l'interface graphique lorsque lon appuie sur le boutton "Nouveau compte"
   */
  router.post('/user', (req, res) => {
    return oHttpController.postCreateUser(req, res)
  })

  return router
}

module.exports = main

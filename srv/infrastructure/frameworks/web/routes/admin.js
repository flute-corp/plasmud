const express = require('express')

function main (container) {
  const router = express.Router()
  const oAdminController = container.resolve('AdminController')

  router.put('/save/:name', (req, res) => {
    return oAdminController.saveGameState(req, res)
  })

  router.put('/save', (req, res) => {
    return oAdminController.saveGameState(req, res)
  })

  router.put('/restore/:name', (req, res) => {
    return oAdminController.restoreGameState(req, res)
  })

  router.put('/restore', (req, res) => {
    return oAdminController.restoreGameState(req, res)
  })

  router.put('/shutdown', (req, res) => {
    return oAdminController.shutdownService(req, res)
  })

  router.delete('/shutdown', (req, res) => {
    return oAdminController.cancelShutdowService(req, res)
  })

  router.put('/open', (req, res) => {
    return oAdminController.openService(req, res)
  })

  router.put('/message', (req, res) => {
    return oAdminController.broadcastMessage(req, res)
  })

  router.get('/users/find/:filter/:count', (req, res) => {
    return oAdminController.findUsers(req, res)
  })

  router.get('/users/find/:filter', (req, res) => {
    return oAdminController.findUsers(req, res)
  })

  router.get('/users/:count(\\d+)', (req, res) => {
    return oAdminController.getUserList(req, res)
  })

  router.get('/users', (req, res) => {
    return oAdminController.getUserList(req, res)
  })

  router.put('/user/ban/:user', (req, res) => {
    return oAdminController.banUser(req, res)
  })

  router.delete('/user/ban/:user', (req, res) => {
    return oAdminController.unbanUser(req, res)
  })

  router.put('/user/role/:user/:role', (req, res) => {
    return oAdminController.addUserRoles(req, res)
  })

  router.delete('/user/role/:user/:role', (req, res) => {
    return oAdminController.removeUserRoles(req, res)
  })

  router.get('/user/:user', (req, res) => {
    return oAdminController.getUser(req, res)
  })

  /**
   * SWITCHES
   */
  router.put('/switch/:name', (req, res) => {
    return oAdminController.putSwitch(req, res)
  })

  router.get('/switch/:name', (req, res) => {
    return oAdminController.getSwitch(req, res)
  })

  router.get('/switches', (req, res) => {
    return oAdminController.getSwitches(req, res)
  })

  return router
}

module.exports = main

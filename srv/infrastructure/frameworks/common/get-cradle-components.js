/**
 * Extraction des use-cases d'un cradle awilix
 * @param CreateCharacter
 * @param DeleteCharacter
 * @param GetCharacterList
 * @param SetClientCurrentCharacter
 * @param JoinChannel
 * @param LeaveChannel
 * @param RegisterChatUser
 * @param SendPublicMessage
 * @param SendPrivateMessage
 * @param AuthenticateClient
 * @param ConnectClient
 * @param DisconnectClient
 * @param FindUserClients
 * @param GetClient
 * @param GetCommandStats
 * @param ExecuteCommand
 * @param ParseMessage
 * @param AddUserRole
 * @param ChangeUserPassword
 * @param CreateUser
 * @param EnterGame
 * @param FindUser
 * @param GetUser
 * @param GetUserBanishment
 * @param RemoveUserBanishment
 * @param RemoveUserRole
 * @param SetUserBanishment
 * @returns {{GetCommandStats, SetUserBanishment, RegisterChatUser, AuthenticateClient, ChangeUserPassword, JoinChannel, SetClientCurrentCharacter, CreateCharacter, GetUserBanishment, RemoveUserBanishment, FindUser, LeaveChannel, GetUser, GetCharacterList, ParseMessage, DisconnectClient, SendPublicMessage, CreateUser, DeleteCharacter, EnterGame, ConnectClient, AddUserRole, ExecuteCommand, FindUserClients, GetClient, SendPrivateMessage, RemoveUserRole}}
 */
function getCradleUseCases ({
  // Use cases
  // use cases : Character
  CreateCharacter,
  DeleteCharacter,
  GetCharacterList,
  SetClientCurrentCharacter,
  // use cases : Chat
  JoinChannel,
  LeaveChannel,
  RegisterChatUser,
  SendPublicMessage,
  SendPrivateMessage,
  // use cases : Client
  AuthenticateClient,
  ConnectClient,
  DisconnectClient,
  FindUserClients,
  GetClient,
  // use cases : Tech
  GetCommandStats,
  ExecuteCommand,
  ParseMessage,
  // use cases : User
  AddUserRole,
  ChangeUserPassword,
  CreateUser,
  EnterGame,
  FindUser,
  GetUser,
  GetUserBanishment,
  RemoveUserBanishment,
  RemoveUserRole,
  SetUserBanishment
}) {
  return {
    // Use cases
    // use cases : Character
    CreateCharacter,
    DeleteCharacter,
    GetCharacterList,
    SetClientCurrentCharacter,
    // use cases : Chat
    JoinChannel,
    LeaveChannel,
    RegisterChatUser,
    SendPublicMessage,
    SendPrivateMessage,
    // use cases : Client
    AuthenticateClient,
    ConnectClient,
    DisconnectClient,
    FindUserClients,
    GetClient,
    // use cases : Tech
    GetCommandStats,
    ExecuteCommand,
    ParseMessage,
    // use cases : User
    AddUserRole,
    ChangeUserPassword,
    CreateUser,
    EnterGame,
    FindUser,
    GetUser,
    GetUserBanishment,
    RemoveUserBanishment,
    RemoveUserRole,
    SetUserBanishment
  }
}

/**
 * Extraction des services d'un cradle awilix
 * @param CommandRunnerInteractor
 * @param ChatInteractor
 * @param GameInteractor
 * @param DAteInteractor
 * @returns {{CommandRunnerInteractor, TxatInteractor, GameInteractor, DateInteractor }}
 */
function getCradleServices ({
  // Services
  CommandRunnerInteractor,
  ChatInteractor,
  GameInteractor,
  DateInteractor
}) {
  return {
    // Services
    CommandRunnerInteractor,
    ChatInteractor,
    GameInteractor,
    DateInteractor
  }
}

module.exports = {
  getCradleUseCases,
  getCradleServices
}

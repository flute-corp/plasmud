/*
 * Les roles sont attribué aux users et leur donnent accès à différente fonctionnalité dans le jeu.
 * voir : srv/infrastructure/frameworks/common/user-roles.js
 */

/**
 * Transforme un nom de role en constante (ajoute USER_ROLE_ en gros)
 * @param role {string}
 * @returns {string}
 */
function roleNameToConst (role) {
  return 'USER_ROLE_' + role.replace(/-/g, '_').toUpperCase()
}

/**
 * Transforme une constante de role en nom (retire le USER_ROLE_ de devant, en gros)
 * @param sContantName {string} nom de constante
 * @returns {string}
 */
function roleConstToName (sContantName) {
  return sContantName
    .substring(10)
    .replace(/-/g, '_')
    .toLowerCase()
}

module.exports = {
  roleConstToName,
  roleNameToConst
}

const Ansi256 = require('../../../../libs/ansi-256-colors')
const c = new Ansi256()

module.exports = {
  error: s => c.render('[#F00]\uD83D\uDCA5 ' + s),
  warn: s => c.render('[#FF0]\u26a0 ' + s),
  info: s => c.render('[#02F]❨ℹ❩ ' + s),
  room: s => c.render('[#0DF]❨ℹ❩ ' + s),
  fail: s => c.render('[#F50]\u2716 ' + s),
  succ: s => c.render('[#2F0]✔ ' + s),
  hint: s => c.render('[#999]❨?❩ ' + s),
  speak: s => c.render('[#BB0]💬 ' + s),
  shout: s => c.render('[#FF2]\uD83D\uDCE3 ' + s),
  opt: s => {
    const r = s.match(/(\[[0-9]+]) +(.*)$/)
    if (r) {
      return c.render('[#2F4]' + r[1]) + ' ' + r[2]
    } else {
      return c.render('[#2F4]' + s)
    }
  }
}

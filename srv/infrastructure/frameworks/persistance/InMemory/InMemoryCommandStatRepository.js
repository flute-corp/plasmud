const CommandStatRepository = require('../../../application/interfaces/CommandStatRepository')
const InMemoryImplementationAbstract = require('./implementations/InMemoryImplementationAutoInc')
const CommandStat = require('../../../entities/CommandStat')

class InMemoryCommandStatRepository extends CommandStatRepository {
  constructor () {
    super()
    this._implementation = new InMemoryImplementationAbstract()
  }

  async get (id) {
    return this._implementation.get(id)
  }

  async persist (entity) {
    return this._implementation.persist(entity)
  }

  async remove (entity) {
    return this._implementation.remove(entity)
  }

  async getAll () {
    return this._implementation.getAll()
  }

  async addCommandStat (opcode, time) {
    const id = opcode.toLowerCase()
    let oCommandState
    try {
      oCommandState = await this.get(id)
      oCommandState.time += time
      ++oCommandState.count
    } catch (e) {
      oCommandState = new CommandStat({
        id,
        command: id,
        time,
        count: 1
      })
    }
    return this._implementation.persist(oCommandState)
  }
}

module.exports = InMemoryCommandStatRepository

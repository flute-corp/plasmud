const UserRepository = require('../../../application/interfaces/UserRepository')
const User = require('../../../entities/User')
const InMemoryImplementationUuid = require('./implementations/InMemoryImplementationUuid')

class InMemoryUserRepository extends UserRepository {
  constructor () {
    super()
    this._implementation = new InMemoryImplementationUuid()
    this._implementation._data = {
      1: new User({
        id: 1,
        name: 'root',
        password: 'root',
        email: 'root@localhost.com',
        dateCreation: 0,
        dateLastUsed: 0
      })
    }
  }

  /**
   * Sauvegarde de l'entité, si l'identifiant de l'entité est null ou undefined un nouvel identifiant est attribué
   * @param entity {object}
   * @returns {Promise<never>}
   */
  persist (entity) {
    return this._implementation.persist(entity)
  }

  /**
   * Suppression de l'entité, l'identifiant de l'entité doit être spécifié.
   * @param entity
   * @returns {Promise<never>}
   */
  remove (entity) {
    return this._implementation.remove(entity)
  }

  /**
   * Récupération d'une entité à partir de son identifiant
   * @param id {string|number}
   * @returns {Promise<never>}
   */
  get (id) {
    return this._implementation.get(id)
  }

  /**
   * Récupération de toutes les entités
   * @returns {Promise<never>}
   */
  getAll () {
    return this._implementation.getAll()
  }

  /**
   * Renvoie une liste d'entité satisfaisant le prédicat spécifié
   * @param pFunction {function}
   * @returns {Promise<never>}
   */
  find (pFunction) {
    return this._implementation.find(pFunction)
  }

  /**
   * Recherche un utilisateur par son nom
   * @param name {string}
   * @return {Promise<User>}
   */
  async findByName (name) {
    const aUsers = await this.find(user => user.name === name)
    return aUsers.shift()
  }

  /**
   * Renvoie le nombre d'utilisateur enregistrés
   * @returns {Promise<number>}
   */
  async getCount () {
    return this._implementation.getCount()
  }
}

module.exports = InMemoryUserRepository

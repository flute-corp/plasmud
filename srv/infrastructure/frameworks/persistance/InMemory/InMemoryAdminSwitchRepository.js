const AdminSwitchRepository = require('../../../application/interfaces/AdminSwitchRepository')
const InMemoryImplementation = require('./implementations/InMemoryImplementation')
const AdminSwitch = require('../../../entities/AdminSwitch')

class InMemoryAdminSwitchRepository extends AdminSwitchRepository {
  constructor () {
    super()
    this._implementation = new InMemoryImplementation()
  }

  async getSwitch (prop, sDefault = undefined) {
    try {
      const { value } = await this._implementation.get(prop)
      return value
    } catch (e) {
      return sDefault
    }
  }

  async getSwitchList () {
    return this._implementation.getAll()
  }

  async setSwitch (prop, value) {
    const sw = new AdminSwitch({
      id: prop,
      value,
      dateChanged: Date.now()
    })
    return this._implementation.persist(sw)
  }
}

module.exports = InMemoryAdminSwitchRepository

const CharacterRepository = require('../../../application/interfaces/CharacterRepository')
const DatabaseImplementationUuid = require('./implementations/DatabaseImplementationUuid')

class DatabaseCharacterRepository extends CharacterRepository {
  constructor ({ DatabaseInteractor }) {
    super()
    this._implementation = new DatabaseImplementationUuid('characters', DatabaseInteractor)
  }

  /**
   * Sauvegarde de l'entité, si l'identifiant de l'entité est null ou undefined un nouvel identifiant est attribué
   * @param entity {object}
   * @returns {Promise<never>}
   */
  persist (entity) {
    return this._implementation.persist(entity)
  }

  /**
   * Suppression de l'entité, l'identifiant de l'entité doit être spécifié.
   * @param entity
   * @returns {Promise<never>}
   */
  remove (entity) {
    return this._implementation.remove(entity)
  }

  /**
   * Récupération d'une entité à partir de son identifiant
   * @param id {string|number}
   * @returns {Promise<never>}
   */
  get (id) {
    return this._implementation.get(id)
  }

  /**
   * Récupération de toutes les entités
   * @returns {Promise<never>}
   */
  getAll () {
    return this._implementation.getAll()
  }

  /**
   * Renvoie une liste d'entité satisfaisant le prédicat spécifié
   * @param pFunction {function}
   * @returns {Promise<never>}
   */
  find (pFunction) {
    return this._implementation.find(pFunction)
  }

  /**
   * Recherche un utilisateur par son nom
   * @param name {string}
   * @return {Promise<Character>}
   */
  async findByName (name) {
    const aCharacters = await this.find({ name: { $eq: name } })
    return aCharacters.shift()
  }

  findByOwner (idUser) {
    return this.find({ owner: idUser })
  }

  async setCharacterAsCurrent (id) {
    const oChar = await this.get(id)
    // liste des autres personnages du meme auteur
    const aChars = await this.findByOwner(oChar.owner)
    const aPromMutChars = aChars
      .map(ch => {
        const bOld = ch.selected
        ch.selected = id === ch.id
        return bOld !== ch.selected ? ch : false
      })
      .filter(ch => !!ch)
      .map(ch => this.persist(ch))
    return Promise.all(aPromMutChars)
  }

  async getOwnerCurrentCharacter (owner) {
    const aChars = await this.findByOwner(owner)
    return aChars.filter(c => c.selected).shift()
  }
}

module.exports = DatabaseCharacterRepository

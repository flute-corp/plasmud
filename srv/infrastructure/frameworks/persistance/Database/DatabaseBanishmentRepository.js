const BanishmentRepository = require('../../../application/interfaces/BanishmentRepository')
const DatabaseImplementationUuid = require('./implementations/DatabaseImplementationUuid')

class DatabaseBanishmentRepository extends BanishmentRepository {
  constructor ({ DatabaseInteractor }) {
    super()
    this._implementation = new DatabaseImplementationUuid('banishments', DatabaseInteractor)
  }

  /**
   * Sauvegarde de l'entité, si l'identifiant de l'entité est null ou undefined un nouvel identifiant est attribué
   * @param entity {object}
   * @returns {Promise<never>}
   */
  persist (entity) {
    return this._implementation.persist(entity)
  }

  /**
   * Suppression de l'entité, l'identifiant de l'entité doit être spécifié.
   * @param entity
   * @returns {Promise<never>}
   */
  remove (entity) {
    return this._implementation.remove(entity)
  }

  /**
   * Récupération d'une entité à partir de son identifiant
   * @param id {string|number}
   * @returns {Promise<never>}
   */
  get (id) {
    return this._implementation.get(id)
  }

  /**
   * Récupération de toutes les entités
   * @returns {Promise<never>}
   */
  getAll () {
    return this._implementation.getAll()
  }

  /**
   * Renvoie une liste d'entité satisfaisant le prédicat spécifié
   * @param pFunction {function}
   * @returns {Promise<never>}
   */
  find (pFunction) {
    return this._implementation.find(pFunction)
  }
}

module.exports = DatabaseBanishmentRepository

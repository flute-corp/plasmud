class DatabaseImplementationAbstract {
  constructor (sCollection, DatabaseInteractor) {
    this._collection = sCollection
    this._database = DatabaseInteractor
  }

  getCollection () {
    const db = this._database.getManager()
    return db.collections[this._collection]
  }

  getNewId () {
    return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'))
  }

  /**
   * Sauvegarde de l'entité, si l'identifiant de l'entité est null ou undefined un nouvel identifiant est attribué
   * @param entity {object}
   * @returns {Promise<object>}
   */
  async persist (entity) {
    const c = this.getCollection()
    if (entity.id === '' || entity.id === undefined || entity.id === null) {
      entity.id = await this.getNewId()
    }
    await c.save(entity)
    return Promise.resolve(entity)
  }

  /**
   * Suppression de l'entité, l'identifiant de l'entité doit être spécifié.
   * @param entity
   * @returns {Promise<*>}
   */
  remove (entity) {
    const c = this.getCollection()
    const id = entity.id
    if (id === undefined || id === null) {
      return Promise.reject(new Error('ERR_UNDEFINED_ID'))
    }
    return c.remove(id)
  }

  /**
   * Récupération d'une entité à partir de son identifiant
   * @param id {string|number}
   * @returns {Promise<never>}
   */
  get (id) {
    const c = this.getCollection()
    return c.get(id)
  }

  /**
   * Récupération de toutes les entités
   * @returns {Promise<object[]>}
   */
  getAll () {
    const c = this.getCollection()
    return c.getAll()
  }

  getPartial (nStart, nCount) {
    const c = this.getCollection()
    return c.getPartial(nStart, nCount)
  }

  /**
   * Renvoie une liste d'entité satisfaisant le prédicat spécifié
   * @param query {function|object}
   * @returns {Promise<object[]>}
   */
  find (query) {
    const c = this.getCollection()
    return c.find(query)
  }

  getCount () {
    const c = this.getCollection()
    return Promise.resolve(c.keys.length)
  }
}

module.exports = DatabaseImplementationAbstract

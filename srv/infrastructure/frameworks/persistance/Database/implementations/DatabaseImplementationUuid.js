const DatabaseImplementationAbstract = require('./DatabaseImplementationAbstract')
const smalluuid = require('../../../../../../libs/small-uuid')

class DatabaseImplementationUuid extends DatabaseImplementationAbstract {
  getNewId () {
    return Promise.resolve(smalluuid())
  }
}

module.exports = DatabaseImplementationUuid

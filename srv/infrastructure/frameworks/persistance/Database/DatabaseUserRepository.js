const UserRepository = require('../../../application/interfaces/UserRepository')
const DatabaseImplementationUuid = require('./implementations/DatabaseImplementationUuid')

class DatabaseUserRepository extends UserRepository {
  constructor ({ DatabaseInteractor }) {
    super()
    this._users = {}
    this._implementation = new DatabaseImplementationUuid('users', DatabaseInteractor)
  }

  /**
   * Sauvegarde de l'entité, si l'identifiant de l'entité est null ou undefined un nouvel identifiant est attribué
   * @param entity {object}
   * @returns {Promise<never>}
   */
  persist (entity) {
    this._users[entity.id] = entity
    return this._implementation.persist(entity)
  }

  /**
   * Suppression de l'entité, l'identifiant de l'entité doit être spécifié.
   * @param entity
   * @returns {Promise<never>}
   */
  remove (entity) {
    delete this._users[entity.id]
    return this._implementation.remove(entity)
  }

  /**
   * Récupération d'une entité à partir de son identifiant
   * @param id {string|number}
   * @returns {Promise<never>}
   */
  async get (id) {
    if (!(id in this._users)) {
      this._users[id] = await this._implementation.get(id)
    }
    return this._users[id]
  }

  /**
   * Récupération de toutes les entités
   * @returns {Promise<never>}
   */
  getAll () {
    return this._implementation.getAll()
  }

  /**
   * Renvoie une liste d'entité satisfaisant le prédicat spécifié
   * @param pFunction {function}
   * @returns {Promise<never>}
   */
  find (pFunction) {
    return this._implementation.find(pFunction)
  }

  /**
   * Recherche un utilisateur par son nom
   * @param name {string}
   * @return {Promise<User>}
   */
  async findByName (name) {
    const aUsers = await this.find({ name: { $eq: name } })
    return aUsers.shift()
  }

  async getCount () {
    return this._implementation.getCount()
  }
}

module.exports = DatabaseUserRepository

const I18n = require('../../../../libs/i18n-manager')

class I18nService {
  constructor () {
    this._oI18n = null
  }

  async init (oStrings) {
    if (this._oI18n) {
      return Promise.resolve(this._oI18n)
    }
    this._oI18n = new I18n()
    await this._oI18n.init({ default: oStrings })
    return this._oI18n
  }

  render (sKey, oParams) {
    if (this._oI18n) {
      return this._oI18n.render(sKey, oParams)
    } else {
      throw new Error('ERR_I18N_NOT_INITIALIZED')
    }
  }
}

module.exports = I18nService

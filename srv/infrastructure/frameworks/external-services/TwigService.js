const TwigManager = require('../../../../libs/twig-manager')
const TemplateInteractor = require('../../application/interfaces/TemplateInteractor')

class TwigService extends TemplateInteractor {
  constructor () {
    super()
    this._twigManager = new TwigManager()
  }

  async init (sPath) {
    if (typeof sPath !== 'string') {
      throw new TypeError('TwigService.init : Path must be a string.')
    }
    return this._twigManager.index(sPath)
  }

  hasTemplate (sKey) {
    return this._twigManager.hasTemplate(sKey)
  }

  render (sKey, oVariables) {
    return this._twigManager.render(sKey, oVariables)
  }
}

module.exports = TwigService

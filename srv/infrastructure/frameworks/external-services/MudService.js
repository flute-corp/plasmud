const EventManager = require('events')
const GameInteractor = require('../../application/interfaces/GameInteractor')
const MUD = require('../../../../libs/plasmud')
const config = require('../../../config/modules')
const PromFS = require('../../../../libs/prom-fs')
const path = require('path')

class MudService extends GameInteractor {
  constructor () {
    super()
    this._env = {
      PLASMUD_MODULES: process.env.PLASMUD_MODULES,
      PLASMUD_LANG: process.env.PLASMUD_LANG,
      PLASMUD_DATA_DIRECTORY: process.env.PLASMUD_DATA_DIRECTORY,
      PLASMUD_SAVE_SLOT_DIRECTORY: process.env.PLASMUD_SAVE_SLOT_DIRECTORY
    }
    this._events = new EventManager()
    this._system = new MUD.System()
    this._system._events.on('output', ({ id, content }) => {
      this.events.emit('output', { id, content })
    })
  }

  async init () {
    /**
     * Vérifie l'existence d'un module
     * @param sModule {string}
     * @returns {Promise<{filename:string, access:boolean}>}
     */
    const moduleExists = sModule => new Promise(resolve =>
      PromFS
        .access(sModule, 'r')
        .then(bAccess => {
          resolve({
            filename: sModule,
            access: bAccess
          })
        })
    )

    /**
     * Déterminer le nom complet d'un module susceptible de se trouver dans la liste
     * des dossiers spécifiés
     * @param sModule {string}
     * @param aPaths {string[]}
     * @returns {Promise<{filename:string, access:boolean}>[]}
     */
    const seekModuleFullPath = (sModule, aPaths) =>
      aPaths.map((p) => {
        const sFull = path.resolve(p, sModule)
        return moduleExists(sFull)
      })

    // Rechercher les modules
    const aModuleFolders = config.modulesFolderList
    const aModuleList = this._env.PLASMUD_MODULES.split(',').map(s => s.trim())
    const aResolvedModules = await Promise.all(aModuleList
      .map(m => seekModuleFullPath(m, aModuleFolders))
      .flat()
    )
    const aAccessibleModules = aResolvedModules
      .filter(m => m.access)
      .map(m => m.filename)
    this._system.config = {
      modules: aAccessibleModules,
      language: this._env.PLASMUD_LANG
    }
    return this._system.init()
  }

  get events () {
    return this._events
  }

  registerPlayer (uid, client, oCharacter) {
    const oPlayer = this._system.registerPlayer(uid) // MUD: REGISTER CLIENT
    oPlayer.client = client
    const oEntity = this._system.engine.createPlayerEntity(uid, 'player#' + oCharacter.id, oCharacter.name)
    oPlayer.context.pid = oEntity.id
    return {
      player: oPlayer,
      entity: oEntity
    }
  }

  unregisterPlayer (uid) {
    if (this._system.isPlayerRegistered(uid)) {
      this._system.unregisterPlayer(uid) // MUD: UNREGISTER CLIENT
    }
  }

  processCommand (uid, sOpcode, args) {
    return this._system.command(uid, sOpcode, args)
  }

  isCommandExist (sOpcode) {
    return this._system.isCommandExist(sOpcode)
  }

  getSaveFileName (sSlotName) {
    if (sSlotName === undefined) {
      sSlotName = this._env.PLASMUD_SAVE_SLOT_DIRECTORY
    }
    return path.resolve(this._env.PLASMUD_DATA_DIRECTORY, 'saves', sSlotName + '.json')
  }

  /**
   * @returns {Promise<unknown>}
   */
  saveGame (sSlotName = undefined) {
    const aDocuments = this._system.engine.saveState()
    // tester si le dossier de destination existe
    const sFilename = this.getSaveFileName(sSlotName)
    const sPath = path.dirname(sFilename)
    return PromFS
      .mkdir(sPath)
      .then(() => PromFS.write(sFilename, JSON.stringify(aDocuments)))
  }

  /**
   * @returns {Promise<unknown>}
   */
  loadGame (sSlotName = undefined) {
    const sFilename = this.getSaveFileName(sSlotName)
    return PromFS
      .read(sFilename)
      .then(sContent => {
        const aDocuments = JSON.parse(sContent.toString())
        this._system.engine.loadState(aDocuments)
      })
  }
}

module.exports = MudService

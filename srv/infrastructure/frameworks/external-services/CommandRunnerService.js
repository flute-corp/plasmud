const Scriptorium = require('../../../../libs/scriptorium')
const reqDirScripts = require('../../../../libs/require-dir-scripts')
const CommandRunnerInteractor = require('../../application/interfaces/CommandRunnerInteractor')

class CommandRunnerService extends CommandRunnerInteractor {
  constructor () {
    super()
    this._scriptorium = new Scriptorium()
  }

  get scriptorium () {
    return this._scriptorium
  }

  get events () {
    return this._scriptorium.events
  }

  /**
   * Défini le chemin ou se trouve les events de commandes
   * @param sPath {string}
   * @returns {Promise<{count: number}>}
   */
  async loadScripts (sPath) {
    const oScripts = await reqDirScripts(sPath)
    this._scriptorium.defineRoutes(oScripts)
    return {
      count: this._scriptorium.count
    }
  }

  /**
   * Lance un script.
   * Les script son des mini programme qui correspondent à des commandes tapées par l'utilisateur.
   * @param sScript {string}
   * @param context {object}
   * @param params {string}
   */
  runScript (sScript, context, params) {
    if (this._scriptorium.scriptExists(sScript)) {
      return this._scriptorium.runScript(sScript, context, ...params)
    } else {
      throw new Error('ERR_COMMAND_NOT_FOUND')
    }
  }
}

module.exports = CommandRunnerService

const ClientOutputInteractor = require('../../application/interfaces/ClientOutputInteractor')
const TextRenderer = require('../../../../libs/plasmud/TextRenderer')

class ClientOutputService extends ClientOutputInteractor {
  constructor ({ IntlInteractor, TemplateInteractor }) {
    super()
    this._textRenderer = new TextRenderer({
      intl: IntlInteractor,
      tpl: TemplateInteractor
    })
  }

  renderText (client, sTextRef, oContext) {
    return this._textRenderer.render(sTextRef, oContext)
  }
}

module.exports = ClientOutputService

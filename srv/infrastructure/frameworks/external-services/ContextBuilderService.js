const ContextInteractor = require('../../application/interfaces/ContextBuilderInteractor')
const debug = require('debug')
const CheckType = require('../../../../libs/check-type')

const logCommand = debug('serv:command')

class ContextBuilderService extends ContextInteractor {
  constructor ({ ClientOutputInteractor }) {
    super()
    this._services = {
      clientOutput: ClientOutputInteractor
    }
  }

  init () {
  }

  static check (print, sTypes, ...params) {
    try {
      sTypes.split('').forEach((t, i) => {
        const p = params[i]
        switch (t) {
          case 'S': { // string mandatory
            CheckType.checkString(p)
            break
          }

          case 'N': { // number mandatory
            CheckType.checkNumeric(p)
            break
          }

          case 's': { // string optional
            if (CheckType.isDefined(p)) {
              CheckType.checkString(p)
            }
            break
          }

          case 'n': { // string optional
            if (CheckType.isDefined(p)) {
              CheckType.checkNumeric(p)
            }
            break
          }
        }
      })
      return true
    } catch (e) {
      switch (e.message) {
        case 'ERR_VALUE_UNDEFINED': {
          print.text('generic.error.missingParam')
          break
        }

        case 'ERR_TYPE_MISMATCH': {
          print.text('generic.error.typeMismatch')
          break
        }
      }
      return false
    }
  }

  /**
   * Création d'un contexte pour le client
   * @param client {Client}
   * @returns {{print: {}, data: {}, client: Client, check: (function(*, ...[*]): boolean), text: ((function(*, *): *)|*)}}
   */
  buildClientContext (client) {
    const text = (sString, oVariables) => {
      return this._services.clientOutput.renderText(client, sString, oVariables)
    }
    const print = {
      to: function (idUser, sString) {
        throw new Error('ERR_PRINT_TO_UNDEFINED')
      }
    }
    print.log = (...aArguments) => {
      logCommand(...aArguments)
    }
    print.room = () => print.to(client, 'print.room function is not available in this context')
    print.text = (sString, oVariables) =>
      text(sString, oVariables)
        .split('\n')
        .map(s => print.to(client.uid, s))
    /**
     * Check est en outil permetant de tester si les paramètre correspondent aux types spécifiés
     * @example check('SSNn', param1, param2, param3, param4)
     * va verifier que param1 soint bien une chaine de caractère (pas undefined), param2 pareil, param3 doite etre un nombre
     * et param4 s'il n'est pas undefined doit être un nombre.
     * S: Chaine de caractère non-undefined
     * s: Chaine de caracter optionnel (undefined possible)
     * N: Nombre
     * n: Nombre ou undefined
     * @param sType
     * @param params
     * @returns {boolean}
     */
    const check = (sType, ...params) => ContextBuilderService.check(print, sType, ...params)
    /**
     * @typedef SRVClientContext {object}
     * @property client {Client}
     * @property check {function (string, string[]): boolean}
     * @property text {function (sString:string, oVariables: object): string}
     * @property print {object}
     * @property data {object}
     */
    return {
      client,
      check,
      text,
      print,
      data: {}
    }
  }
}

module.exports = ContextBuilderService

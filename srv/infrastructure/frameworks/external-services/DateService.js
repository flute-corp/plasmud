const DateInteractor = require('../../application/interfaces/DateInteractor')
const { getType } = require('../../../../libs/get-type')
const { renderDuration } = require('../../../../libs/date-decoration')

class DateService extends DateInteractor {
  /**
   * @param x {*}
   * @returns {Date}
   */
  parse (x) {
    switch (getType(x)) {
      case 'number':
      case 'string':
        return new Date(x)

      case 'date':
        return x

      case 'array':
        if (x.some(isNaN)) {
          throw new TypeError('ERR_EXPECTED_NUMBER')
        } else {
          return new Date()
        }

      default:
        throw new Error('ERR_DATE_PARSE_ERROR: ' + getType(x))
    }
  }

  /**
   * pad avec des '0'
   * @param n {number} nombre initial
   * @param l {number} nombre de 0 a ajouter à gauche
   * @returns {string} version affichable du nombre
   */
  static pad (n, l) {
    return n.toString().padStart(l, '0')
  }

  /**
   * Renvoie une représentation affichable de l'heure
   * @param oDate {Date}
   * @param bSeconds {boolean} true = afficher aussi les secondes
   * @returns {string}
   */
  getTimeString (oDate, bSeconds = false) {
    oDate = this.parse(oDate)
    return [
      DateService.pad(oDate.getHours(), 2),
      DateService.pad(oDate.getMinutes(), 2),
      bSeconds ? DateService.pad(oDate.getSeconds(), 2) : ''
    ].filter(s => s !== '').join(':')
  }

  /**
   * Renvoie une version affichable de la date
   * @param oDate {Date}
   * @returns {string}
   */
  getDateString (oDate) {
    oDate = this.parse(oDate)
    return [
      DateService.pad(oDate.getFullYear(), 4),
      DateService.pad(oDate.getMonth() + 1, 2),
      DateService.pad(oDate.getDate(), 2)
    ].join('-')
  }

  /**
   * Renvoie une version affichable de la Date et de l'heure
   * @param oDate  {Date}
   * @param bSeconds {boolean}
   * @returns {string}
   */
  getDateTimeString (oDate, bSeconds = false) {
    oDate = this.parse(oDate)
    return [
      this.getDateString(oDate),
      this.getTimeString(oDate, bSeconds)
    ].join(' ')
  }

  /**
   * Renvoie la durée sous forme de chaine texte
   * @param nDuration
   * @param format
   * @returns {string}
   */
  getDurationString (nDuration, format = {}) {
    return renderDuration(nDuration, format)
  }
}

module.exports = DateService

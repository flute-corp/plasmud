const db = require('../../../../libs/o876-db')
const config = require('../../../config/database')
const USER_ROLES = require('../common/user-roles')
const promFS = require('../../../../libs/prom-fs')

class DatabaseService {
  constructor () {
    this._manager = null
  }

  /**
   * Création du dossier d'accueil de la base de données s'il n'existe pas encore
   */
  async createDatabasePath () {
    const path = config.path
    const oStat = await promFS.stat(path)
    if (!oStat) {
      await promFS.mkdir(path)
    }
  }

  async init () {
    const oManager = new db.Manager()
    await this.createDatabasePath()
    await oManager.init(config)
    const users = oManager.collections.users
    // création d'un utilisateur "root"
    const aRoots = users.find({ name: config.root.name })
    if (aRoots.length === 0) {
      const r = config.root
      await users.save({
        id: '1',
        name: r.name,
        password: '',
        email: r.email,
        dateCreation: Date.now(),
        dateLastUsed: null,
        roles: USER_ROLES,
        banishment: ''
      })
    }
    this._manager = oManager
    return oManager
  }

  getManager () {
    if (!this._manager) {
      throw new Error('ERR_DB_NOT_INITIALIZED')
    }
    return this._manager
  }
}

module.exports = DatabaseService

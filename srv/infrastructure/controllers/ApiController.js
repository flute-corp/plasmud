class ApiController {
  constructor ({
    CreateUser
  }) {
    this.useCases = {
      CreateUser
    }
  }

  async postCreateUser (req, res) {
    try {
      const { name, password, email } = req.body
      await this.useCases.CreateUser.execute(name, email, password)
      res.send.ok({
        success: true
      })
    } catch (e) {
      switch (e.message) {
        case 'ERR_USER_NAME_ALREADY_TAKEN': {
          res.send.conflict()
          break
        }

        case 'ERR_USER_NAME_INVALID': {
          res.send.badRequest('username')
          break
        }

        case 'ERR_USER_EMAIL_INVALID': {
          res.send.badRequest('email')
          break
        }

        default: {
          console.error(e)
          res.send.internalError(e.message)
          break
        }
      }
    }
  }
}

module.exports = ApiController

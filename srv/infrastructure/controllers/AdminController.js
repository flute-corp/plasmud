const debug = require('debug')
const logAdmin = debug('serv:admin')

const RESULT = {
  STATE_SAVED: 'STATE_SAVED',
  STATE_RESTORED: 'STATE_RESTORED'
}

class AdminController {
  constructor ({
    AdminSetSwitch,
    AdminGetSwitches,
    AdminGetSwitch,
    KickAllClients,
    SaveGameState,
    RestoreGameState,
    OpenService,
    ShutdownService,
    AddUserRole,
    RemoveUserRole,
    GetUserList,
    AdminBroadcastMessage,
    FindUser,
    SetUserBanishment,
    RemoveUserBanishment,
    ADMIN_SWITCHES
  }) {
    this.useCases = {
      KickAllClients,
      AdminSetSwitch,
      AdminGetSwitch,
      AdminGetSwitches,
      OpenService,
      ShutdownService,
      SaveGameState,
      RestoreGameState,
      GetUserList,
      AdminBroadcastMessage,
      FindUser,
      SetUserBanishment,
      RemoveUserBanishment,
      AddUserRole,
      RemoveUserRole
    }
    this.ADMIN_SWITCHES = ADMIN_SWITCHES
  }

  /**
   * Liste des switches d'administration
   * @param req {*}
   * @param res {*}
   * @returns {Promise<void>}
   */
  async getSwitches (req, res) {
    res.send.ok(await this.useCases.AdminGetSwitches.execute())
  }

  /**
   * Obtention de la valeur d'un switch d'administration
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  async getSwitch (req, res) {
    const name = req.params.name
    const { value } = await this.useCases.AdminGetSwitch.execute(name)
    res.send.ok({
      result: { [name]: value }
    })
  }

  /**
   * Sauvegarde l'état du jeu dans un fichier sur disque
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  async saveGameState (req, res) {
    // value: undefined
    // Sauvegarde du module
    const sFileName = req.params.name
    if (sFileName === undefined) {
      logAdmin('saving game state')
    } else {
      logAdmin('saving game state in file %s', sFileName)
    }
    await this.useCases.SaveGameState.execute(sFileName)
    logAdmin('game state saved')
    res.send.ok({
      result: RESULT.STATE_SAVED
    })
  }

  /**
   * Restaure l'état d'un jeu précédemment enregistré
   * avec saveGameState
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  async restoreGameState (req, res) {
    // value: undefined
    // Chargement du fichier de sauvegarde du module
    const sFileName = req.params.name
    if (sFileName === undefined) {
      logAdmin('restoring game state')
    } else {
      logAdmin('restoring game state from file %s', sFileName)
    }
    const { value: bOpen } = await this.useCases.AdminGetSwitch.execute(this.ADMIN_SWITCHES.OPEN)
    if (bOpen) {
      await this.useCases.ShutdownService.execute()
    }
    await this.useCases.RestoreGameState.execute(sFileName)
    if (bOpen) {
      await this.useCases.AdminSetSwitch.execute(this.ADMIN_SWITCHES.OPEN, true)
    }
    logAdmin('game state restored')
    res.send.ok({
      result: RESULT.STATE_RESTORED
    })
  }

  /**
   * Liste des utilisateurs enregistrés
   * Avec filtre
   * @returns {Promise<void>}
   */
  async findUsers (req, res) {
    const sFilter = req.params.filter
    const nCount = (req.params.count | 0) || Infinity
    logAdmin('finding users - filter: %s - count: %d', sFilter, nCount)
    res.send.ok({
      result: await this.useCases.GetUserList.execute(sFilter, nCount)
    })
  }

  async getUser (req, res) {
    const sUser = req.params.user
    logAdmin('getting user by name: %s', sUser)
    try {
      const result = await this.useCases.FindUser.execute(sUser, true)
      res.send.ok({
        result
      })
    } catch (e) {
      switch (e.message) {
        case 'ERR_USER_NOT_FOUND': {
          res.send.notFound()
          break
        }

        default: {
          console.error(e)
          res.send.internalError(e.message)
          break
        }
      }
    }
  }

  async getUserList (req, res) {
    const nCount = (req.params.count | 0) || Infinity
    logAdmin('getting user list - count: %d', nCount)
    res.send.ok({
      result: await this.useCases.GetUserList.execute('', nCount)
    })
  }

  async openService (req, res) {
    logAdmin('opening service')
    res.send.ok({
      result: await this.useCases.AdminSetSwitch.execute(this.ADMIN_SWITCHES.OPEN, true)
    })
  }

  async cancelShutdowService (req, res) {
    logAdmin('canceling service shutdown')
    res.send.ok({
      result: await this.useCases.ShutdownService.execute({ cancel: true })
    })
  }

  async shutdownService (req, res) {
    const { cancel = false, message = '', delay = 0 } = req.body
    if (cancel) {
      return this.cancelShutdowService(req, res)
    } else {
      logAdmin('closing service to incoming client connections.')
      res.send.ok({
        result: await this.useCases.ShutdownService.execute({ cancel, delay, message })
      })
    }
  }

  async banUser (req, res) {
    const user = req.params.user
    try {
      const { duration = '0', reason = '' } = req.body
      const nDuration = parseInt(duration) || 0
      const nDateEnd = nDuration === 0 ? 0 : (Date.now() + nDuration)
      const { user: oBannedUser } = await this.useCases.FindUser.execute(user)
      const { until, forever } = await this.useCases.SetUserBanishment.execute(oBannedUser.id, nDateEnd, reason, '')
      const sUntil = forever ? 'forever' : (new Date(until)).toDateString()
      logAdmin('ban user %s until: %s - reason: "%s"', oBannedUser.name, reason, sUntil)
      res.send.ok({
        user: oBannedUser.name,
        reason,
        until: sUntil
      })
    } catch (e) {
      switch (e.message) {
        case 'ERR_USER_NOT_FOUND': {
          res.send.badRequest('User not found : ' + user)
          break
        }

        default: {
          console.error(e)
          res.send.internalError(e.message)
        }
      }
    }
  }

  async unbanUser (req, res) {
    const user = req.params.user
    logAdmin('unban user %s', user)
    try {
      const { user: oBannedUser } = await this.useCases.FindUser.execute(user)
      const { success } = await this.useCases.RemoveUserBanishment.execute(oBannedUser.id)
      res.send.ok({
        success
      })
    } catch (e) {
      switch (e.message) {
        case 'ERR_USER_NOT_FOUND': {
          res.send.badRequest('User not found : ' + user)
          break
        }

        default: {
          console.error(e)
          res.send.internalError(e.message)
        }
      }
    }
  }

  async addUserRoles (req, res) {
    const { user, role } = req.params
    logAdmin('adding role %s to user %s', role, user)
    try {
      const { user: oUser } = await this.useCases.FindUser.execute(user)
      const { success, roles } = await this.useCases.AddUserRole.execute(oUser.id, role)
      res.send.ok({
        success,
        roles
      })
    } catch (e) {
      switch (e.message) {
        case 'ERR_USER_NOT_FOUND': {
          res.send.badRequest('User not found : ' + user)
          break
        }

        default: {
          console.error(e)
          res.send.internalError(e.message)
        }
      }
    }
  }

  async removeUserRoles (req, res) {
    const { user, role } = req.params
    logAdmin('remove role %s from user %s', role, user)
    try {
      const { user: oUser } = await this.useCases.FindUser.execute(user)
      const { success, roles } = await this.useCases.RemoveUserRole.execute(oUser.id, role)
      res.send.ok({
        success,
        roles
      })
    } catch (e) {
      switch (e.message) {
        case 'ERR_USER_NOT_FOUND': {
          res.send.badRequest('User not found : ' + user)
          break
        }

        case 'ERR_INVALID_ROLE': {
          res.send.badRequest('Invalid role : ' + role)
          break
        }

        default: {
          console.error(e)
          res.send.internalError(e.message)
        }
      }
    }
  }

  async putSwitch (req, res) {
    try {
      const name = req.params.name
      const { value } = req.body
      logAdmin('setting admin switch %s to value %s', name, value.toString())
      switch (name) {
        case 'open': {
          // value:
          // 1 = Les clients peuvent se connecter
          // 0 = Les clients ne peuvent pas se connecter - vire les clients deja connectés.
          const { value } = await this.useCases.AdminGetSwitch.execute(this.ADMIN_SWITCHES.OPEN)
          const bOpen = Boolean(value | 0)
          const sState = (value ? 'o' : 'c') + (bOpen ? 'o' : 'c')
          switch (sState) {
            case 'oo':
            case 'cc': {
              // L'état de OPEN ne change pas
              res.send.ok({ result: { [name]: value } })
              break
            }
            case 'oc': {
              // C'était ouvert on veut fermer
              logAdmin('closing service')
              await this.shutdownService(req, res)
              break
            }
            case 'co': {
              // C'était fermé on veut ouvrir
              logAdmin('opening service')
              await this.openService()
              break
            }
          }
          break
        }

        default: {
          res.send.badRequest('admin switch or command "' + name + '" invalid.')
          return
        }
      }
    } catch (e) {
      console.error(e)
      res.send.internalError(e)
    }
  }

  async broadcastMessage (req, res) {
    const sMessage = req.body.message || ''
    if (sMessage) {
      return this.useCases.AdminBroadcastMessage.execute(sMessage)
    }
  }
}

module.exports = AdminController

const debug = require('debug')
const CommControllerAbstract = require('./abstract/CommControllerAbstract')

const DECORATION = require('../frameworks/common/term-decorators')

const logCtrl = debug('serv:wsctl')

class WebSocketController extends CommControllerAbstract {
  constructor (cradle) {
    super(cradle)
    this.game = cradle.GameInteractor
    this.txat = cradle.ChatInteractor
    this.PROTOCOL = 'ws'
    this.initHandlers()
  }

  log (...args) {
    logCtrl(...args)
  }

  /* ** SPECIFIC TO WEB SOCKETS *** SPECIFIC TO WEB SOCKETS *** SPECIFIC TO WEB SOCKETS *** SPECIFIC TO WEB SOCKETS ** */
  /* ** SPECIFIC TO WEB SOCKETS *** SPECIFIC TO WEB SOCKETS *** SPECIFIC TO WEB SOCKETS *** SPECIFIC TO WEB SOCKETS ** */
  /* ** SPECIFIC TO WEB SOCKETS *** SPECIFIC TO WEB SOCKETS *** SPECIFIC TO WEB SOCKETS *** SPECIFIC TO WEB SOCKETS ** */

  /**
   * Méthode d'envoie de données au client spécifié
   * @param socket {WebSocket} instance du client
   * @param content {string} contenue du message
   * @returns {Promise}
   */
  sendToSocket (socket, content) {
    return new Promise((resolve, reject) => {
      if (socket.readyState === 1) {
        socket.send(content, err => {
          if (err) {
            reject(err)
          } else {
            resolve()
          }
        })
      }
    })
  }

  closeSocket (socket) {
    socket.close()
  }

  /**
   *
   * @param socket {*}
   * @returns {Promise<*>}
   */
  async connectClient (socket) {
    try {
      const { client, context } = await super.connectClient(socket)
      // handler de messages websocket
      socket.on('message', message => {
        this.processContextMessage(context, message.toString())
      })
      socket.on('close', () => {
        this.disconnectClient(client)
      })
      return { client, context }
    } catch (e) {
      console.error(e)
      logCtrl('error while client attempting to connect : %s', e.message)
      throw e
    }
  }

  initHandlers () {
    for (const [sDecorator, pRenderer] of Object.entries(DECORATION)) {
      this.decoration[sDecorator] = s => pRenderer(s)
    }
    this.decoration.default = s => s
    this.clientTransmitter = (client, message) => {
      return this.sendUser(client.uid, this.decorate(message))
    }
    this.game.events.on('output', ({ id, content }) => {
      return this.sendUser(id, this.decorate(content))
    })
    const ts = this.txat
    // prise en charge des évènements du txat
    ts.events.on(ts.TXAT_EVENT_TYPES.OPEN_CHANNEL, ({ client, channel, name }) => {
      return this.sendUser(client.uid, 'CHAT:JOIN ' + channel)
    })
    ts.events.on(ts.TXAT_EVENT_TYPES.CLOSE_CHANNEL, ({ client, channel, name }) => {
      return this.sendUser(client.uid, 'CHAT:LEAVE ' + channel)
    })
    ts.events.on(ts.TXAT_EVENT_TYPES.MESSAGE, ({ client, channel, content }) => {
      return this.sendUser(client.uid, 'CHAT:MSG ' + channel + ' ' + this.decorate(content))
    })
  }
}

module.exports = WebSocketController

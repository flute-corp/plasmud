module.exports = class BatchController {
  constructor ({
    FindUser,
    AdminGetSwitches
  }) {
    this.useCases = {
      FindUser,
      AdminGetSwitches
    }
  }

  async viewAdminSwitches () {
    console.log(await this.useCases.AdminGetSwitches.execute())
  }

  async viewUserInfo (argv) {
    const sUserName = argv[0]
    try {
      const { user } = await this.useCases.FindUser.execute(sUserName, true)
      console.log(user)
    } catch (e) {
      switch (e.message) {
        case 'ERR_USER_NOT_FOUND': {
          console.warn('user not found %s', sUserName)
          break
        }

        default: {
          console.error(e)
          break
        }
      }
    }
  }
}

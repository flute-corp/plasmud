const { getCradleUseCases, getCradleServices } = require('../frameworks/common/get-cradle-components')

class AclController {
  constructor (cradle) {
    /**
     * Les différents uses cases
     * @type {{SetUserBanishment, RegisterChatUser, AuthenticateClient, ChangeUserPassword, JoinChannel, SetClientCurrentCharacter, CreateCharacter, GetUserBanishment, RemoveUserBanishment, FindUser, LeaveChannel, GetUser, GetCharacterList, ParseMessage, DisconnectClient, SendPublicMessage, CreateUser, DeleteCharacter, EnterGame, ConnectClient, AddUserRole, ExecuteCommand, FindUserClients, GetClient, SendPrivateMessage, RemoveUserRole}}
     */
    this.useCases = getCradleUseCases(cradle)
    this.ACL = cradle.ACL
    /**
     * Liste des différents services
     * @type {{CommandRunnerInteractor, ChatInteractor, GameInteractor, DateInteractor}}
     */
    const oServices = getCradleServices(cradle)
    this.commandRunner = oServices.CommandRunnerInteractor
    this.commandRunner.events.on('run', acl => {
      acl.allowed = this.checkCommandACL(acl)
    })
  }

  /**
   * Vérifie que l'utilisateur a le role requis pour executer la commande
   * @param command {string} commande
   * @param parameters {*} paramètre de la commande
   * @param context {*} contient le client (et son uid)
   * @param allowed {boolean} à positionner à false si l'utilisateur n'a pas le droit de lancer la commande
   * @returns {Promise<boolean>}
   */
  async checkCommandACL ({ command, parameters, context, allowed }) {
    const uid = context.client.uid
    if (uid) {
      const { user } = await this.useCases.GetUser.execute(uid)
      const { roles } = user
      // Liste des Roles qui peuvent utiliser la commande
      const aRequiredRoles = []
      for (const [r, aCmd] of Object.entries(this.ACL)) {
        if (aCmd.includes(command)) {
          aRequiredRoles.push(r)
        }
      }
      // Si aRequiredRoles est vide, alors la commande ne réclame aucun role
      // Sinon alors la commande réclame l'un des roles contenu dans aRequiredRoles
      const bAllows = (aRequiredRoles.length === 0) || (aRequiredRoles.some(r => roles.includes(r)))
      allowed = allowed && bAllows
    }
    return allowed
  }
}

module.exports = AclController

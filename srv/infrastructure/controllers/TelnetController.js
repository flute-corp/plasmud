const debug = require('debug')
const CommControllerAbstract = require('./abstract/CommControllerAbstract')
const crypto = require('crypto')

const logCtrl = debug('serv:telnetctl')

const DECORATION = require('../frameworks/common/term-decorators')

const TELNET_PHASE = {
  LOGIN: 0,
  PASSWORD: 1,
  LOGGED_IN: 2
}

class TelnetController extends CommControllerAbstract {
  constructor (cradle) {
    super(cradle)
    this.game = cradle.GameInteractor
    this.txat = cradle.ChatInteractor
    this.PROTOCOL = 'telnet'
    this.initHandlers()
  }

  log (...args) {
    logCtrl(...args)
  }

  /**
   * Méthode d'envoie de données au client spécifié
   * @param socket {Socket} instance du client
   * @param content {string} contenue du message
   * @returns {Promise}
   */
  sendToSocket (socket, content) {
    try {
      socket.write(content + '\n')
      return Promise.resolve()
    } catch (e) {
      console.error('attempt to write to destroyed socket: ' + content)
      return Promise.reject(e)
    }
  }

  closeSocket (socket) {
    socket.emit('end')
    socket.destroy()
  }

  /**
   * Initialisation du dialogue de login
   * @param context {SRVClientContext}
   */
  telnetPhaseInit (context) {
    context.data.telnetSession = {
      loginPhase: TELNET_PHASE.LOGIN,
      login: '',
      password: ''
    }
    context.client.socket.socket.write('Login:')
  }

  /**
   * Considère que le message telnet reçu est un nom de login. Passage à la phase suivante
   * @param context {SRVClientContext}
   * @param sMessage {string}
   */
  telnetPhaseLogin (context, sMessage) {
    // On attend le username
    const oTelnetSession = context.data.telnetSession
    oTelnetSession.login = sMessage.trim()
    oTelnetSession.loginPhase = TELNET_PHASE.PASSWORD
    const socket = context.client.socket.socket
    socket.write('Password:')
    // utilisé si on ne veut pas voir le mot de passe affiché
    // socket._setRawMode(true)
  }

  /**
   * Considère que le message telnet reçu est le mot de passe
   * @param context {SRVClientContext}
   * @param sMessage {string}
   * @returns {Promise<void>}
   */
  async telnetPhasePassword (context, sMessage) {
    // On attend le mot de passe
    const oTelnetSession = context.data.telnetSession
    const socket = context.client.socket.socket
    oTelnetSession.password = sMessage.trim()
    oTelnetSession.password = crypto
      .createHash('sha256')
      .update(oTelnetSession.password)
      .digest('hex')
    await this.processContextMessage(context, 'login ' + oTelnetSession.login + ' ' + oTelnetSession.password)
    oTelnetSession.loginPhase = TELNET_PHASE.LOGGED_IN
  }

  /**
   * Ecoute la connexion telnet afinc de collecter les caractère composant le mot de passe
   * ce handler est utilisé pour proposer une saisie sans echo du mot de passe.
   * @param context {SRVClientContext}
   * @param sMessage {string}
   * @returns {Promise<void>}
   */
  async telnetPhasePasswordRaw (context, sMessage) {
    // On attend le mot de passe
    const oTelnetSession = context.data.telnetSession
    const socket = context.client.socket.socket
    if (sMessage.charCodeAt(0) === 127) {
      // backspace
      oTelnetSession.password = oTelnetSession.password.substring(0, oTelnetSession.password.length - 1)
    } else if (sMessage === '\r') {
      // return
      socket.write('\r\n')
      oTelnetSession.password = crypto
        .createHash('sha256')
        .update(oTelnetSession.password)
        .digest('hex')
      await this.processContextMessage(context, 'login ' + oTelnetSession.login + ' ' + oTelnetSession.password)
      oTelnetSession.loginPhase = TELNET_PHASE.LOGGED_IN
      socket._setRawMode(false)
    } else {
      oTelnetSession.password += sMessage
    }
  }

  /**
   * Handler de connexion client telnet
   * @param socket {*}
   * @returns {Promise<*>}
   */
  async connectClient (socket) {
    const { client, context } = await super.connectClient(socket)
    // handler de messages websocket
    socket.on('window size', function (e) {
      if (e.command === 'sb') {
        // telnet window resized to e.width, e.height
      }
    })
    const pRuntimePhase = async sMessage => {
      await this.processContextMessage(context, sMessage.toString().trim())
    }
    if (!('telnetSession' in context.data)) {
      this.telnetPhaseInit(context)
    }
    const pLoginPhase = async sMessage => {
      sMessage = sMessage.toString()
      switch (context.data.telnetSession.loginPhase) {
        case TELNET_PHASE.LOGIN: {
          this.telnetPhaseLogin(context, sMessage)
          break
        }
        case TELNET_PHASE.PASSWORD: {
          await this.telnetPhasePassword(context, sMessage)
          if (context.data.telnetSession.loginPhase === TELNET_PHASE.LOGGED_IN) {
            socket.off('data', pLoginPhase)
            socket.on('data', pRuntimePhase)
            delete context.data.telnetSession
          }
          break
        }
      }
    }
    socket.on('data', pLoginPhase)
    socket.on('end', () => {
      this.log('user::' + client.uid + ' has ended telnet session')
      this.disconnectClient(client)
    })
    return { client, context }
  }

  initHandlers () {
    for (const [sDecorator, pRenderer] of Object.entries(DECORATION)) {
      this.decoration[sDecorator] = s => pRenderer(s)
    }
    this.decoration.default = s => s
    this.clientTransmitter = (client, message) => {
      return this.sendUser(client.uid, this.decorate(message))
    }
    this.game.events.on('output', ({ id, content }) => {
      return this.sendUser(id, this.decorate(content))
    })
    const ts = this.txat
    // prise en charge des évènements du txat
    ts.events.on(ts.TXAT_EVENT_TYPES.OPEN_CHANNEL, ({ client, channel, name }) => {
      return this.sendUser(client.uid, 'CHAT:JOIN ' + channel)
    })
    ts.events.on(ts.TXAT_EVENT_TYPES.CLOSE_CHANNEL, ({ client, channel, name }) => {
      return this.sendUser(client.uid, 'CHAT:LEAVE ' + channel)
    })
    ts.events.on(ts.TXAT_EVENT_TYPES.MESSAGE, ({ client, channel, content }) => {
      return this.sendUser(client.uid, 'CHAT:MSG ' + channel + ' ' + this.decorate(content))
    })
  }
}

module.exports = TelnetController

const debug = require('debug')
const { getCradleUseCases } = require('../../frameworks/common/get-cradle-components')

const logCtrl = debug('serv:comctl')

class CommControllerAbstract {
  constructor (cradle) {
    this._decoration = {}
    this.PROTOCOL = ''
    /**
     * @type {{SetUserBanishment, RegisterChatUser, AuthenticateClient, ChangeUserPassword, JoinChannel, SetClientCurrentCharacter, CreateCharacter, GetUserBanishment, RemoveUserBanishment, FindUser, LeaveChannel, GetUser, GetCharacterList, ParseMessage, DisconnectClient, SendPublicMessage, CreateUser, DeleteCharacter, EnterGame, ConnectClient, AddUserRole, ExecuteCommand, FindUserClients, GetClient, SendPrivateMessage, RemoveUserRole}}
     */
    this.useCases = getCradleUseCases(cradle)
    /**
     * @type {{CommandRunnerInteractor, ChatInteractor, GameInteractor, DateInteractor}}
     */
    this.CLIENT_STATES = cradle.CLIENT_STATES
    this.ACL = cradle.ACL
    this.contextUseCases = { ...this.useCases }
    this.clientTransmitter = function (client, message) {}
  }

  get decoration () {
    return this._decoration
  }

  set decoration (value) {
    this._decoration = value
  }

  log (...args) {
    logCtrl(...args)
  }

  /**
   * Méthode d'envoie au user spécifié
   * @param uid {string} identifiant utilisateur
   * @param content {string} contenu du message
   * @returns {Promise<void>}
   */
  async sendUser (uid, content) {
    const { clients } = await this.useCases.FindUserClients.execute(uid)
    const client = clients.find(c => c.type === this.PROTOCOL)
    if (client && client.state !== this.CLIENT_STATES.CLIENT_STATE_DISCONNECTED) {
      return client.socket.send(content)
    }
  }

  /**
   * procède à l'analyse et l'exécution de l'ordre issu du client.
   * @param context {*}
   * @param sMessage {string}
   */
  async processContextMessage (context, sMessage) {
    const client = context.client
    try {
      const { message } = await this.useCases.ParseMessage.execute(sMessage)
      if (!message.valid) {
        this.log('client::%s sent an invalid command', client.id)
        return
      }
      this.log('user::%s $ %s [%s]',
        client.uid,
        message.opcode,
        message.arguments.join(' ')
      )
      switch (client.state) {
        case this.CLIENT_STATES.CLIENT_STATE_CONNECTED:
          // le client n'est pas encore associé à un utilisateur authentifié
          // le message sera le username
          // seule commande autorisée : login
          await this.useCases.ExecuteCommand.execute(context, message)
          break

        case this.CLIENT_STATES.CLIENT_STATE_AUTHENTICATED:
        case this.CLIENT_STATES.CLIENT_STATE_IN_GAME:
          // le client est associé à un utilisateur authentifié
          await this.useCases.ExecuteCommand.execute(context, message)
          break

        case this.CLIENT_STATES.CLIENT_STATE_DISCONNECTED:
          // le client est déconnecté : ignorer tous les messages
          break
      }
    } catch (e) {
      try {
        if (e.message.startsWith('ERR_TXAT')) {
          console.error('Chat error', client.id, e.message)
          return
        }
        switch (e.message) {
          case 'ERR_NOT_IN_GAME': {
            this.log('user::%s is using game-only command without entering game.', client.uid)
            context.print.text('generic.error.commandNotInMud')
            break
          }
          case 'ERR_COMMAND_NOT_FOUND': {
            this.log('user::%s is invoking an invalid command.', client.uid)
            context.print.text('generic.error.commandNotFound')
            break
          }
          case 'ERR_ACCESS_DENIED': {
            this.log('user::%s is not allowed to use this command.', client.uid)
            context.print.text('generic.error.commandAccessDenied')
            break
          }
          case 'ERR_AUTH_FAILED': {
            this.log('user::%s authentication failed.', client.uid)
            client.socket.close()
            break
          }
          default: {
            console.error(e)
            break
          }
        }
      } catch (e) {
        console.error(e)
        this.log('client::%s : an error occurred while processing a client message : %s', client.id, e.message)
        client.socket.close()
      }
    }
  }

  async disconnectClient (client) {
    await this.useCases.DisconnectClient.execute(client.id)
    this.log('user::%s disconnected', client.uid)
  }

  /**
   * @typedef VSocket {object}
   * @property send {function(content)} envoie de données à la socket vers le client
   * @property close {function()} fermeture de la socket et déconnection du client.
   */

  /**
   *
   * @param socket {*}
   * @returns {Promise<*>}
   */
  async connectClient (socket) {
    try {
      const vsocket = {
        close: () => this.closeSocket(socket),
        send: content => this.sendToSocket(socket, content),
        socket
      }
      // connexion du client
      const {
        client,
        context
      } = await this.useCases.ConnectClient.execute(
        vsocket,
        this.PROTOCOL,
        (client, message) => this.clientTransmitter(client, message)
      )
      context.uc = this.contextUseCases
      if (!client) {
        vsocket.close()
        return
      }
      this.log('client::%s is now connected', client.id)

      return {
        client,
        context
      }
    } catch (e) {
      await this.clientTransmitter(socket, e.message)
      this.closeSocket(socket)
      throw e
    }
  }

  /**
   * Méthode d'envoie de données au client spécifié
   * @param socket {*} instance du client
   * @param content {string} contenue du message
   * @returns {Promise}
   */
  sendToSocket (socket, content) {
    throw new Error('ERR_NOT_IMPLEMENTED')
  }

  closeSocket (socket) {
    throw new Error('ERR_NOT_IMPLEMENTED')
  }

  /**
   * Décoration d'une chaine de caractère selon le préfixe ([error], [succ], [warn] etc...)
   * @param sString {string} chaine d'entrée
   * @returns {string}
   */
  decorate (sString) {
    sString = sString.trim()
    const REGEX_TYPE = /^\[([-:a-z]+)] /
    const r = sString.match(REGEX_TYPE)
    if (r) {
      const [dummy, sTypes] = r
      const nLength = sTypes.length + 2 + 1
      const aTypes = sTypes.split(':')
      const sType = aTypes.shift()
      return sType in this.decoration
        ? this.decoration[sType](sString.substring(nLength), aTypes)
        : sString
    } else {
      if ('default' in this.decoration) {
        return this.decoration.default(sString)
      } else {
        return sString
      }
    }
  }
}

module.exports = CommControllerAbstract

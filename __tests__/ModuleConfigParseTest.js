const ModuleConfigParser = require('../libs/plasmud/ModuleConfigParser')

describe('get languages', () => {
  it('should return {"fr":"fr"} when languages are set to "fr"', () => {
    const m = new ModuleConfigParser({
      languages: 'fr'
    })
    expect(m.languages).toEqual({ fr: 'fr' })
  })
  it('should return {"fr":"fr","en":"en"} when languages are set to "fr,en"', () => {
    const m = new ModuleConfigParser({
      languages: 'fr,en'
    })
    expect(m.languages).toEqual({ fr: 'fr', en: 'en' })
  })
  it('should return {"fr":"fr","en":"en"} when languages are set to [fr,en]', () => {
    const m = new ModuleConfigParser({
      languages: ['fr', 'en']
    })
    expect(m.languages).toEqual({ fr: 'fr', en: 'en' })
  })
  it('should return {} when languages are set to ""', () => {
    const m = new ModuleConfigParser({
      languages: ''
    })
    expect(m.languages).toEqual({})
  })
  it('should return {"fr":"french","en":"english","jp":"japanese","val":"valyrian"} when languages are set to {"fr":"french","en":"english","jp":"japanese","val":"valyrian"}', () => {
    const m = new ModuleConfigParser({
      languages: { fr: 'french', en: 'english', jp: 'japanese', val: 'valyrian' }
    })
    expect(m.languages).toEqual({ fr: 'french', en: 'english', jp: 'japanese', val: 'valyrian' })
  })
})

describe('get default language', () => {
  it('should return "fr" when languages are set to "fr"', () => {
    const m = new ModuleConfigParser({
      languages: 'fr'
    })
    expect(m.defaultLanguage).toBe('fr')
  })
  it('should return "fr" when languages are set to "fr,en"', () => {
    const m = new ModuleConfigParser({
      languages: 'fr,en'
    })
    expect(m.defaultLanguage).toBe('fr')
  })
  it('should return "fr" when languages are set to [fr,en]', () => {
    const m = new ModuleConfigParser({
      languages: ['fr', 'en']
    })
    expect(m.defaultLanguage).toBe('fr')
  })
  it('should return "fr" when languages are set to {"fr":"fr","en":"en"}', () => {
    const m = new ModuleConfigParser({
      languages: { fr: 'fr', en: 'en' }
    })
    expect(m.defaultLanguage).toBe('fr')
  })
  it('should return "fr" when languages are set to {"fr":"french","en":"english"}', () => {
    const m = new ModuleConfigParser({
      languages: { fr: 'fr', en: 'en' }
    })
    expect(m.defaultLanguage).toBe('fr')
  })
  it('should return "" when languages are set to ""', () => {
    const m = new ModuleConfigParser({
      languages: ''
    })
    expect(m.defaultLanguage).toBe('')
  })
  it('should return defaultLanguage when specified', () => {
    const m = new ModuleConfigParser({
      defaultLanguage: 'en',
      languages: 'fr, en'
    })
    expect(m.defaultLanguage).toBe('en')
  })
  it('should return "" when specifing an invalid language', () => {
    const m = new ModuleConfigParser({
      defaultLanguage: 'jp',
      languages: 'fr, en'
    })
    expect(m.defaultLanguage).toBe('')
  })
})

describe('getLangPath', () => {
  const LANGUAGES = { fr: 'french', en: 'english', jp: 'japanese', val: 'valyrian' }
  describe('with no default language', () => {
    it('should return "french" when specifying "fr"', () => {
      const m = new ModuleConfigParser({
        languages: LANGUAGES
      })
      expect(m.defaultLanguage).toBe('fr')
      expect(m.getLangPath('fr')).toBe('french')
    })
    it('should return "valyrian" when specifying "val"', () => {
      const m = new ModuleConfigParser({
        languages: LANGUAGES
      })
      expect(m.defaultLanguage).toBe('fr')
      expect(m.getLangPath('fr')).toBe('french')
    })
  })
  describe('with default language set to val', () => {
    it('should return "french" when specifying "fr"', () => {
      const m = new ModuleConfigParser({
        languages: LANGUAGES,
        defaultLanguage: 'val'
      })
      expect(m.getLangPath('fr')).toBe('french')
    })
    it('should return "valyrian" when not specifying language', () => {
      const m = new ModuleConfigParser({
        languages: LANGUAGES,
        defaultLanguage: 'val'
      })
      expect(m.getLangPath('')).toBe('valyrian')
    })
  })
})

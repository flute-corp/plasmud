const { replaceIconTokens, isReplacementWorthy } = require('../libs/iconifier')

describe('basic test', function () {
  it('détection correct d\'un token', function () {
    expect(isReplacementWorthy(':warning:')).toBeTruthy()
  })
  it('remplacer une icone', function () {
    const s = replaceIconTokens(':warning:', 'unicode')
    expect(s).toBe('\u26a0')
  })
  it('replacer une icone dans un texte plus grand', function () {
    const s = replaceIconTokens('texte plus grand :warning: portion de texte encode plus grand', 'unicode')
    expect(s).toBe('texte plus grand \u26a0 portion de texte encode plus grand')
  })
  it('icone inexistante', function () {
    const s = replaceIconTokens('texte plus grand :icone_inexistante: portion de texte encode plus grand', 'unicode')
    expect(s).toBe('texte plus grand :icone_inexistante: portion de texte encode plus grand')
  })
  it('fontawesome', function () {
    const s = replaceIconTokens('texte plus grand :icone_inexistante: portion de texte encode plus grand', 'unicode')
    expect(s).toBe('texte plus grand :icone_inexistante: portion de texte encode plus grand')
  })
})

const {
  findDirection,
  findLidOrObject,
  CONTAINER_TYPES
} = require('../libs/plasmud/helpers/object-finder')

function getRoomLocalEntities () {
  return [
    {
      id: 'id1',
      lid: 'i1',
      entity: {
        id: 'id1',
        name: 'Flêche acide de Melf'
      }
    },
    {
      id: 'id2',
      lid: 'i2',
      entity: {
        id: 'id2',
        name: 'Flétrissure horrible d\'Abi-Dalsim'
      }
    },
    {
      id: 'id3',
      lid: 'i3',
      entity: {
        id: 'id3',
        name: 'Sphère résiliente d\'Otiluke'
      }
    },
    {
      id: 'id4',
      lid: 'i4',
      entity: {
        id: 'id4',
        name: 'Main impérieuse de Bigby'
      }
    },
    {
      id: 'id5',
      lid: 'o1',
      entity: {
        id: 'id5',
        name: 'Coffre en bois ouvragé'
      }
    },
    {
      id: 'id16',
      lid: 'o3',
      entity: {
        id: 'id16',
        name: 'Chest you can open with a key'
      }
    },
    {
      id: 'id6',
      lid: 'o2',
      entity: {
        id: 'id6',
        name: 'Coffre en métal rouillé'
      }
    },
    {
      id: 'id7',
      lid: 'i5',
      entity: {
        id: 'id7',
        name: 'Sacoche en cuir véritable'
      }
    },
    {
      id: 'id20',
      lid: 'i6',
      entity: {
        id: 'id20',
        name: 'Potion de rapidité'
      }
    }
  ]
}

function getInvLocalEntities () {
  return [
    {
      id: 'id8',
      lid: 'y0',
      entity: {
        id: 'id8',
        name: 'Clé de coffre'
      }
    },
    {
      id: 'id9',
      lid: 'y1',
      entity: {
        id: 'id9',
        name: 'Clé fantôme'
      }
    },
    {
      id: 'id10',
      lid: 'y2',
      entity: {
        id: 'id10',
        name: 'Pièces d\'or'
      }
    }
  ]
}

const CONTEXT = {
  engine: {
    getLocalEntities (id) {
      if (id === 'r1') {
        return getRoomLocalEntities()
      } else {
        return getInvLocalEntities()
      }
    },
    getEntity () {
      return {
        location: 'r1'
      }
    }
  },
  pid: 'player::p1',
  text: s => {
    switch (s) {
      case 'dir.n':
        return 'north'

      case 'dir.e':
        return 'east'

      case 'dir.w':
        return 'west'

      case 'dir.s':
        return 'south'

      case 'dir.ne':
        return 'north-east'

      case 'dir.nw':
        return 'north-west'

      case 'dir.se':
        return 'south-east'

      case 'dir.sw':
        return 'south-west'

      case 'dir.u':
        return 'up'

      case 'dir.d':
        return 'down'
    }
  }
}

describe('basic', function () {
  it('les directions', function () {
    expect(findDirection(CONTEXT, 'n')).toHaveProperty('direction', 'n')
    expect(findDirection(CONTEXT, 'north')).toHaveProperty('direction', 'n')
    expect(findDirection(CONTEXT, 'east')).toHaveProperty('direction', 'e')
    expect(findDirection(CONTEXT, 'sw')).toHaveProperty('direction', 'sw')
    expect(findDirection(CONTEXT, 'south west')).toHaveProperty('direction', 'sw')
    expect(findDirection(CONTEXT, 'south-west')).toHaveProperty('direction', 'sw')
  })

  it('objets 1', function () {
    const oEnt1 = findLidOrObject(CONTEXT, ['flech', 'acid', 'melf'], ['r1'])
    expect(oEnt1.entity.name).toBe('Flêche acide de Melf')
    const oEnt2 = findLidOrObject(CONTEXT, ['bigby'], ['r1'])
    expect(oEnt2.entity.name).toBe('Main impérieuse de Bigby')
    expect(oEnt2.entity.id).toBe('id4')
    const oEnt3 = findLidOrObject(CONTEXT, ['i2'], ['r1'])
    expect(oEnt3.entity.id).toBe('id2')
    const oEnt4 = findLidOrObject(CONTEXT, ['4', 'i2'], ['r1'])
    expect(oEnt4.entity.id).toBe('id2')
    expect(oEnt4.count).toBe(4)
  })

  it('objets trop éloignés', function () {
    const oEnt1 = findLidOrObject(CONTEXT, ['tarte', 'tatin'], ['r1'])
    expect(oEnt1).toBeNull()
  })

  it('objet avec numérateur', function () {
    const oEnt1 = findLidOrObject(CONTEXT, ['4', 'melfs'], ['r1'])
    expect(oEnt1.entity.name).toBe('Flêche acide de Melf')
    expect(oEnt1.count).toBe(4)
  })
})

const lib2da = require('../libs/lib2da')

describe('basic test', function () {
  it('parse un fichier entier', function () {
    const f2da = `2DA V2.0

          ACBONUS    DEXBONUS   ACCHECK    ARCANEFAILURE%      WEIGHT    COST      DESCRIPTIONS   BASEITEMSTATREF

0          0          100        0          0                   10        1            1727       5411
1          1          8          0          5                   50        5            1728       5432
2          2          6          0          10                  100       10           1729       5435
3          3          4          -1         20                  150       15           1730       5436
4          4          4          -2         20                  300       100          1731       5437
5          5          2          -5         30                  400       150          1732       5438
6          6          1          -7         40                  450       200          1733       5439
7          7          1          -7         40                  500       600          1734       5440
8          8          1          -8         45                  500       1500         1736       5441
`
    const o2DA = lib2da.parse(f2da)
    expect(o2DA[0]).toEqual({
      ACBONUS: 0,
      DEXBONUS: 100,
      ACCHECK: 0,
      'ARCANEFAILURE%': 0,
      WEIGHT: 10,
      COST: 1,
      DESCRIPTIONS: 1727,
      BASEITEMSTATREF: 5411
    })
    expect(o2DA[8]).toEqual({
      ACBONUS: 8,
      DEXBONUS: 1,
      ACCHECK: -8,
      'ARCANEFAILURE%': 45,
      WEIGHT: 500,
      COST: 1500,
      DESCRIPTIONS: 1736,
      BASEITEMSTATREF: 5441
    })
  })
  it('indexation', function () {
    const s = `2DA V2.0

         ACTION                       ACMODIFIER   ATKMODIFIER   DAMAGEMODIFIER

0        ACTION_MODE_EXPERTISE                 3            -3                0
1        ACTION_IMPROVED_EXPERTISE             5            -5                0
2        ACTION_DEFENSIVE_ATTACK               2            -4                0
3        ACTION_POWER_ATTACK                   0            -3                3
4        ACTION_IMPROVED_POWER_ATTACK          0            -5                5
`
    const o2DA = lib2da.parse(s)
    const oActionModes = lib2da.index(o2DA, 'ACTION')
    expect(oActionModes).toHaveProperty('ACTION_MODE_EXPERTISE')
  })
})

const MUDEngine = require('../libs/plasmud/Engine')

function createState1 () {
  return {
    blueprints: {
      gp: {
        type: 'item',
        subtype: 'money',
        name: 'Pièce d\'or',
        desc: [
          'Une pièce d\'or.'
        ],
        weight: 0.0085,
        stackable: true,
        tag: 'gp',
        inventory: false
      },
      bidule1: {
        type: 'item',
        subtype: 'misc',
        name: 'Bidule en bois',
        desc: [
          'Cet objet n\'a rien de particulier'
        ],
        weight: 1,
        stackable: false,
        tag: 'bidule1',
        inventory: false
      },
      bidule2: {
        type: 'item',
        subtype: 'misc',
        name: 'Bidule en verre',
        desc: [
          'Cet objet n\'a rien de particulier'
        ],
        weight: 1,
        stackable: false,
        tag: 'bidule2',
        inventory: false
      },
      bagwonder: {
        type: 'item',
        subtype: 'container',
        name: 'Sac en papier',
        desc: [
          'Cet objet n\'a rien de particulier'
        ],
        weight: 1,
        stackable: false,
        tag: 'sacpap',
        inventory: true,
        lock: null
      }
    },
    sectors: {
      s1: {
        name: 'secteur 1',
        desc: [],
        templates: [
          {
            id: 'genericRoom',
            name: 'pièce générique',
            desc: ['description', 'de pièce', 'générique']
          }
        ]
      }
    },
    rooms: {
      r1: {
        name: 'room 1',
        sector: 's1',
        desc: [
          'Une pièce à une seule issue.'
        ],
        nav: {
          s: {
            to: 'r2',
            name: ''
          }
        },
        content: [
          {
            ref: 'gp',
            stack: 100
          },
          {
            ref: 'bidule1'
          },
          {
            ref: 'bidule2'
          },
          {
            ref: 'bagwonder'
          }
        ]
      },
      r2: {
        name: 'p2',
        desc: ['x'],
        sector: 's1',
        nav: {
          s: {
            to: '22',
            name: '',
            lock: {
              locked: true,
              difficulty: 11,
              key: 'k1'
            }
          },
          d: {
            to: '33',
            name: '',
            secret: {
              difficulty: 20
            }
          },
          n: {
            to: '44',
            name: '',
            lock: {
              locked: true
            }
          }
        },
        content: [
          {
            ref: 'gp',
            stack: 100
          }
        ]
      }
    }
  }
}

function createStateSpeak () {
  return {
    blueprints: {
      speaker: {
        type: 'actor',
        name: 'speaker',
        desc: [
          'Cet objet n\'a rien de particulier'
        ],
        weight: 1,
        tag: 'speaker',
        inventory: true
      },
      listener: {
        type: 'actor',
        name: 'listener',
        desc: [
          'Cet objet n\'a rien de particulier'
        ],
        weight: 1,
        tag: 'listener',
        inventory: true
      }
    },
    sectors: {
      s1: {
        name: 's1',
        desc: ['x']
      }
    },
    rooms: {
      r1: {
        name: 'r1',
        desc: ['x'],
        sector: 's1',
        content: [
          {
            ref: 'speaker',
            tag: 's1'
          },
          {
            ref: 'listener',
            tag: 'l0'
          }
        ],
        nav: {
          n: {
            to: 'r2'
          },
          e: {
            to: 'r10'
          }
        }
      },
      r2: {
        name: 'r2',
        desc: ['x'],
        sector: 's1',
        content: [
          {
            ref: 'listener',
            tag: 'l1'
          }
        ],
        nav: {
          n: {
            to: 'room:r3'
          },
          s: {
            to: 'room:r1'
          }
        }
      },
      r3: {
        name: 'r3',
        desc: ['x'],
        sector: 's1',
        content: [
          {
            ref: 'listener',
            tag: 'l2'
          }
        ],
        nav: {
          s: {
            to: 'r2'
          }
        }
      },
      r10: {
        name: 'r10',
        desc: ['x'],
        sector: 's1',
        content: [
          {
            ref: 'listener',
            tag: 'l3'
          }
        ],
        nav: {
          w: {
            to: 'r1'
          }
        }
      }
    }
  }
}

/*
TO TEST :

getPlayerId
# createEntity
# destroyEntity
# createPlayerEntity
# _cloneEntity
getSectors
getEntities
getRooms
getBlueprint
getSector
getEntity
getRoom
isRoomExist
isEntityExist
# getLocalEntity
# getLocalEntities
# moveEntity
# getPlayerExit
# setExitLocked
# setEntityExitSpotted
# findEntitiesByTag

 */

describe('#createEntity', function () {
  it('création simple', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    const oItem = m.createEntity('bidule2', 'r2')
    expect(oItem.tag).toBe('bidule2')
    expect(oItem.location).toBe('r2')
  })
  it('création blueprint inexistant', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    expect(() => {
      m.createEntity('bidule1000', 'r2')
    }).toThrow('Key "bidule1000" could not be found in collection "blueprints"')
  })
  it('ajout d\'entités dans la liste', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    expect(m.isEntityExist('entity#1')).toBeFalsy()
    const oEntity = m.createEntity('bidule1', 'r2')
    expect(m.isEntityExist('entity#1')).toBeTruthy()
    expect(oEntity.id).toBe('entity#1')
  })
})

describe('#getLocalEntities', function () {
  it('basic', function () {
    const m = new MUDEngine()
    expect(m).toBeDefined()
  })
  it('lister les entités', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    const aEnt = m.getLocalEntities('r1')
    expect(aEnt).toHaveLength(4)
    expect(aEnt[0].id).toBe('entity#1')
    expect(aEnt[1].id).toBe('entity#2')
    expect(aEnt[2].id).toBe('entity#3')
    expect(aEnt[0].lid).toBe('i1')
    expect(aEnt[1].lid).toBe('i2')
    expect(aEnt[2].lid).toBe('i3')
    expect(aEnt[0].entity.blueprint.type).toBe('item')
    expect(aEnt[0].entity.blueprint.subtype).toBe('money')
  })
  it('pièce bidon', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    expect(() => m.getLocalEntities('r1000')).toThrow('Key "r1000" could not be found in collection "entities/rooms"')
  })
})

describe('#getLocalEntity', function () {
  it('obtenir une entité par son lid', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    m.getLocalEntities('r1')
    const oBiduleLid1 = m.getLocalEntity('i2', 'r1')
    const oBidule1 = m.getEntity('entity#2')
    expect(oBidule1.tag).toBe('bidule1')
    expect(m.getLocalEntity('i2', 'r1').tag).toBe('bidule1')
    expect(oBidule1).toBe(oBiduleLid1)
  })
  it('pièce bidon', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    expect(m.getLocalEntity('i15', 'xxx')).toBeNull()
  })
  it('identifiant inexistant', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    expect(m.getLocalEntity('i15', 'r1')).toBeNull()
  })
})

describe('#destroyEntity', function () {
  it('destruction d\'entity : le nombre d\'entités dans le jeu décroit', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    m.getLocalEntities('r1')
    m.getLocalEntities('r2')
    expect(Object.keys(m.getEntities())).toHaveLength(5)
    m.destroyEntity('entity#1')
    expect(Object.keys(m.getEntities())).toHaveLength(4)
    m.destroyEntity('entity#2')
    expect(Object.keys(m.getEntities())).toHaveLength(3)
    m.destroyEntity('entity#3')
    expect(Object.keys(m.getEntities())).toHaveLength(2)
    expect(() => m.destroyEntity('entity#1000')).toThrow('Key "entity#1000" could not be found in collection "entities"')
  })
})

describe('#moveEntity', function () {
  it('positionne une entité', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    expect(m.getLocalEntities('r1')).toHaveLength(4)
    expect(m.getLocalEntities('r2')).toHaveLength(1)
    const oItem3 = m.getEntity('entity#3')
    expect(oItem3.id).toBe('entity#3')
    expect(oItem3.location).toBe('r1')
    m.moveEntity('entity#3', 'r2')
    expect(m.getLocalEntities('r1')).toHaveLength(3)
    expect(m.getLocalEntities('r2')).toHaveLength(2)
    expect(oItem3.location).toBe('r2')
  })
  it('destination inexistante', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    expect(m.getLocalEntities('r1')).toHaveLength(4)
    expect(m.getLocalEntities('r2')).toHaveLength(1)
    const oItem3 = m.getEntity('entity#3')
    expect(oItem3.id).toBe('entity#3')
    expect(oItem3.location).toBe('r1')
    expect(() => {
      m.moveEntity('entity#3', '3333')
    }).toThrow('Key "3333" could not be found in collection "entities/rooms"')
    expect(oItem3.location).toBe('r1')
  })
  it('déplacement et fusion de stack - 1', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    const oGold10 = m.getLocalEntity('i1', 'r1')
    expect(oGold10).not.toBeNull()
    expect(oGold10.blueprint.type).toBe('item')
    expect(oGold10.blueprint.subtype).toBe('money')
    expect(oGold10.location).toBe('r1')
    const oGold20 = m.getLocalEntity('i1', 'r2')
    expect(oGold20).not.toBeNull()
    expect(oGold20.stack).toBe(100)
    m.moveEntity(oGold10.id, 'r2')
    const oGold11 = m.getLocalEntity('i1', 'r1')
    expect(oGold11).toBeNull()
    const oGold21 = m.getLocalEntity('i1', 'r2')
    expect(oGold21).not.toBeNull()
    expect(oGold21.stack).toBe(200)
  })
  it('déplacement d une partie de stack - 2', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    const oGold10 = m.getLocalEntity('i1', 'r1')
    m.moveEntity(oGold10.id, 'r2', 77)
    const oGold21 = m.getLocalEntity('i1', 'r2')
    expect(oGold21).not.toBeNull()
    expect(oGold21.stack).toBe(177)
    expect(oGold10.stack).toBe(23)
  })
  it('déplacement de stack dans les poches d\'un joueur', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    const oPlayer = m.createPlayerEntity('x1', 'g', 'Glouke', 'r1')
    const oGold10 = m.getLocalEntity('i1', 'r1')
    m.moveEntity(oGold10.id, oPlayer.id, 60)
    const oGoldPlayer = m.findEntitiesByTag('gp', oPlayer.id).shift()
    expect(oGoldPlayer.stack).toBe(60)
    expect(oGold10.stack).toBe(40)
    m.moveEntity(oGoldPlayer.id, 'r1', 15)
    expect(oGoldPlayer.stack).toBe(45)
    expect(oGold10.stack).toBe(55)
  })
})

describe('#findEntitiesByTag', function () {
  it('trouve l\'objet correspondant au tag', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    const aItemsBidule1 = m.findEntitiesByTag('bidule1', 'r1')
    expect(aItemsBidule1).toBeDefined()
    expect(aItemsBidule1).toBeInstanceOf(Array)
    expect(aItemsBidule1).toHaveLength(1)
    expect(aItemsBidule1[0].tag).toBe('bidule1')
    expect(aItemsBidule1[0].location).toBe('r1')
  })
  it('objet introuvable', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    const aItemsBidule9 = m.findEntitiesByTag('bidule9', 'r1')
    expect(aItemsBidule9).toBeDefined()
    expect(aItemsBidule9).toBeInstanceOf(Array)
    expect(aItemsBidule9).toHaveLength(0)
  })
  it('piece introuvable', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    expect(() => {
      m.findEntitiesByTag('bidule9', 'r999')
    }).toThrow('Key "r999" could not be found in')
  })
})

describe('#getPlayerExit', function () {
  it('verification du status d\'une porte normale', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    const oPlayer = m.createPlayerEntity('x1', 'g', 'Glouke', 'r2')
    expect(m.getExit(oPlayer.id, 'x').valid).toBe(false)
    expect(m.getExit(oPlayer.id, 'w').valid).toBe(false)
    // s: lock d: secret n: code
    const oStatusS = m.getExit(oPlayer.id, 's')
    const oStatusD = m.getExit(oPlayer.id, 'd')
    const oStatusN = m.getExit(oPlayer.id, 'n')
    expect(oStatusS).toEqual({
      valid: true,
      room: 'r2',
      lockable: true,
      locked: true,
      dcLockpick: 11,
      key: 'k1',
      desc: [],
      secret: false,
      dcSearch: 0,
      visible: true,
      destination: '22',
      discardKey: false,
      name: '',
      direction: 's'
    })
    expect(oStatusN).toEqual({
      valid: true,
      room: 'r2',
      lockable: true,
      locked: true,
      dcLockpick: Infinity,
      key: '',
      desc: [],
      secret: false,
      dcSearch: 0,
      visible: true,
      destination: '44',
      discardKey: false,
      name: '',
      direction: 'n'
    })
    expect(oStatusD).toEqual({
      valid: true,
      room: 'r2',
      lockable: false,
      locked: false,
      dcLockpick: 0,
      key: '',
      desc: [],
      secret: true,
      dcSearch: 20,
      visible: false,
      destination: '33',
      discardKey: false,
      name: '',
      direction: 'd'
    })
  })
})

describe('#setExitLocked', function () {
  it('deverrouiller une porte fermée à clé', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    expect(m.getExit('r2', 's').locked).toBeTruthy()
    m.getExit('r2', 's').locked = false
    expect(m.getExit('r2', 's').locked).toBeFalsy()
    m.getExit('r2', 's').locked = true
    expect(m.getExit('r2', 's').locked).toBeTruthy()
  })
  it('deverrouiller une porte inexistante', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    expect(m.getExit('r2', 'e').valid).toBeFalsy()
    expect(() => {
      m.getExit('r2', 'e').locked = false
    }).not.toThrow()
  })
})

describe('#setExitSpotted', function () {
  it('spotter une porte cachée', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    expect(m.getExit('r2', 'd').visible).toBeFalsy()
    const oPlayer = m.createPlayerEntity('x1', 'g', 'Glouke', 'r2')
    m.setEntityExitSpotted(oPlayer.id, 'd', true)
    expect(m.getExit(oPlayer.id, 'd').visible).toBeTruthy()
    m.setEntityExitSpotted(oPlayer.id, 'd', false)
    expect(m.getExit(oPlayer.id, 'd').visible).toBeFalsy()
  })
})

describe('#getBlueprint', function () {
  it('should not throw when blueprint has null "lock" property', function () {
    expect(() => {
      MUDEngine._checkBlueprintValid({
        type: 'item',
        subtype: 'container',
        name: 'Sac en papier',
        desc: [
          'Cet objet n\'a rien de particulier'
        ],
        weight: 1,
        stackable: false,
        tag: 'sacpap',
        inventory: true,
        lock: null
      }, 'blueprints', 'x')
    }).not.toThrow()
  })
  it('should throw when "lock" property is missing"', function () {
    expect(() => {
      MUDEngine._checkBlueprintValid({
        type: 'item',
        subtype: 'container',
        name: 'Sac en papier',
        desc: [
          'Cet objet n\'a rien de particulier'
        ],
        weight: 1,
        stackable: false,
        tag: 'sacpap',
        inventory: true
      }, 'blueprints')
    }).toThrow()
  })
  it('devrait renvoyer true quand le blueprint ne possède pas le champ requis "lock" mais n est pas un contenant', function () {
    expect(() => {
      MUDEngine._checkBlueprintValid({
        type: 'item',
        subtype: 'container',
        name: 'Sac en papier',
        desc: [
          'Cet objet n\'a rien de particulier'
        ],
        weight: 1,
        stackable: false,
        tag: 'sacpap',
        inventory: false
      }, 'blueprints')
    }).not.toThrow()
  })
  it("verifier qu'on puisse définir une clé", function () {
    expect(() => {
      MUDEngine._checkBlueprintValid({
        type: 'item',
        subtype: 'container',
        name: 'Sac en papier',
        desc: [
          'Cet objet n\'a rien de particulier'
        ],
        weight: 1,
        stackable: false,
        tag: 'sacpap',
        inventory: false,
        lock: {
          locked: true,
          key: 'k'
        }
      }, 'blueprints')
    }).not.toThrow()
  })
})

describe('plasmud export entity', function () {
  it('verifier cohérence des getter', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    const oEntity = m.getLocalEntity('i1', 'r1')
    const oEntity2 = m.getLocalEntity('i2', 'r1')
    const oPlayer = m.createPlayerEntity('x1', 'g', 'Glouke', 'r1')
    expect(oEntity.id).not.toBe('')
    expect(oEntity2.id).not.toBe('')
    m.moveEntity(oEntity.id, oPlayer.id)
    m.moveEntity(oEntity2.id, oPlayer.id)
    expect(oEntity).toBeDefined()
  })
})

describe('save game and load game', function () {
  it('sauvegarde d\'un petit assets', function () {
    const m = new MUDEngine()
    m.setAssets({
      blueprints: {
        chest1: {
          name: 'chest',
          desc: ['x'],
          weight: 1,
          lock: null,
          type: 'placeable',
          tag: '',
          inventory: true
        }
      },
      sectors: {
        s1: {
          name: 's1',
          desc: ['x']
        }
      },
      rooms: {
        r1: {
          name: 'r1',
          desc: ['x'],
          sector: 's1',
          content: [
            {
              ref: 'chest1',
              tag: 'myChest'
            }
          ],
          nav: {
          }
        }
      }
    })
    m.createPlayerEntity('x1', 'g', 'Glouke', 'r1')
    const save1 = m.saveState('ns')
    const m2 = new MUDEngine()
    m2.loadState(save1)
    expect(m._state).toEqual(m2._state)
  })

  it('sauvegarde d\'un assets moyen', function () {
    const m = new MUDEngine()
    m.setAssets(createState1())
    m.createPlayerEntity('x1', 'g', 'Glouke', 'r1')
    const save1 = m.saveState('ns')
    const m2 = new MUDEngine()
    m2.loadState(save1)
    expect(m._state).toEqual(m2._state)
  })
})

describe('MUD By Name', function () {
  it('choisir un nom réellement proche', function () {
    const m = new MUDEngine()
    m.setAssets({
      blueprints: {
        pot1: {
          type: 'item',
          subtype: 'misc',
          name: 'Potion of cure light wound',
          desc: [
            'Cet objet n\'a rien de particulier'
          ],
          weight: 1,
          stackable: false,
          tag: 'bidule1',
          inventory: false
        },
        pot2: {
          type: 'item',
          subtype: 'misc',
          name: 'Potion of cure serious wound',
          desc: [
            'Cet objet n\'a rien de particulier'
          ],
          weight: 1,
          stackable: false,
          tag: 'bidule1',
          inventory: false
        }
      },
      sectors: {
        s1: {
          name: 's1',
          desc: ['x']
        }
      },
      rooms: {
        r1: {
          name: 'r1',
          desc: ['x'],
          sector: 's1',
          content: [
            {
              ref: 'pot1',
              tag: 'pot_clw'
            },
            {
              ref: 'pot2',
              tag: 'pot_csw'
            }
          ],
          nav: {
          }
        }
      }
    })
    m.createPlayerEntity('x1', 'g', 'Glouke', 'r1')
    const a1 = m.findEntitiesByName('light', 'r1')
    expect(a1).toHaveLength(1)
    expect(a1[0].name).toBe('Potion of cure light wound')
    expect(a1[0].tag).toBe('pot_clw')
  })
  it('choisir un nom parmi une liste avec des nom accentués', function () {
    const m = new MUDEngine()
    m.setAssets({
      blueprints: {
        pot1: {
          type: 'item',
          subtype: 'misc',
          name: 'Potion de grâce féline',
          desc: [
            'Cet objet n\'a rien de particulier'
          ],
          weight: 1,
          stackable: false,
          tag: 'bidule1',
          inventory: false
        },
        pot2: {
          type: 'item',
          subtype: 'misc',
          name: 'Potion d\'invisibilité',
          desc: [
            'Cet objet n\'a rien de particulier'
          ],
          weight: 1,
          stackable: false,
          tag: 'bidule1',
          inventory: false
        }
      },
      sectors: {
        s1: {
          name: 's1',
          desc: ['x']
        }
      },
      rooms: {
        r1: {
          name: 'r1',
          desc: ['x'],
          sector: 's1',
          content: [
            {
              ref: 'pot1',
              tag: 'pot_fg'
            },
            {
              ref: 'pot2',
              tag: 'pot_inv'
            }
          ],
          nav: {
          }
        }
      }
    })
    m.createPlayerEntity('x1', 'g', 'Glouke', 'r1')
    const a1 = m.findEntitiesByName('grace', 'r1')
    expect(a1).toHaveLength(1)
    expect(a1[0].name).toBe('Potion de grâce féline')
    expect(a1[0].tag).toBe('pot_fg')
  })
  it('parfois la fonction renvoie deux possibilités', function () {
    const m = new MUDEngine()
    m.setAssets({
      blueprints: {
        pot1: {
          type: 'item',
          subtype: 'misc',
          name: 'a Wutai ferry ticket to Truce',
          desc: [
            'Cet objet n\'a rien de particulier'
          ],
          weight: 1,
          stackable: false,
          tag: 'bidule1',
          inventory: false
        },
        pot2: {
          type: 'item',
          subtype: 'misc',
          name: 'a Wutai ferry ticket to Cinnabar Island',
          desc: [
            'Cet objet n\'a rien de particulier'
          ],
          weight: 1,
          stackable: false,
          tag: 'bidule1',
          inventory: false
        }
      },
      sectors: {
        s1: {
          name: 's1',
          desc: ['x']
        }
      },
      rooms: {
        r1: {
          name: 'r1',
          desc: ['x'],
          sector: 's1',
          content: [
            {
              ref: 'pot1',
              tag: 'pot_fg'
            },
            {
              ref: 'pot2',
              tag: 'pot_inv'
            }
          ],
          nav: {
          }
        }
      }
    })
    m.createPlayerEntity('x1', 'g', 'Glouke', 'r1')
    const a1 = m.findEntitiesByName('ticket', 'r1').sort((a, b) => a.name.length - b.name.length)
    expect(a1).toHaveLength(2)
    expect(a1[0].name).toBe('a Wutai ferry ticket to Truce')
    expect(a1[1].name).toBe('a Wutai ferry ticket to Cinnabar Island')
  })
})

describe('entity remarks', function () {
  it('creation d\'une entité avec remarque', function () {
    const m = new MUDEngine()
    m.setAssets({
      blueprints: {
        vendor: {
          type: 'actor',
          name: 'Vendeur à la sauvette',
          desc: [
            'Cet objet n\'a rien de particulier'
          ],
          weight: 1,
          tag: 'v1',
          inventory: true
        }
      },
      sectors: {
        s1: {
          name: 's1',
          desc: ['x']
        }
      },
      rooms: {
        r1: {
          name: 'r1',
          desc: ['x'],
          sector: 's1',
          content: [
            {
              ref: 'vendor',
              remark: 'Invective les passants afin de leur proposer de voir ses marchandises.'
            }
          ],
          nav: {
          }
        }
      }
    })
    m.createPlayerEntity('x1', 'g', 'Glouke', 'r1')
    const [oVendor] = m.findEntitiesByName('vendor', 'r1')
    expect(oVendor.remark).toBe('Invective les passants afin de leur proposer de voir ses marchandises.')
  })
})

describe('parlotte', function () {
  it('4 pièces avec un parleur et 3 écouteurs', function () {
    const m = new MUDEngine()
    m.setAssets(createStateSpeak())
    m._createAllEntities()
    const [oSpeaker] = m.findEntitiesByTag('s1', 'r1')
    const [oListener0] = m.findEntitiesByTag('l0', 'r1')
    const [oListener1] = m.findEntitiesByTag('l1', 'r2')
    const [oListener2] = m.findEntitiesByTag('l2', 'r3')
    const [oListener3] = m.findEntitiesByTag('l3', 'r10')
    expect(oSpeaker.tag).toBe('s1')
    expect(oListener0.tag).toBe('l0')
    expect(oListener1.tag).toBe('l1')
    expect(oListener2.tag).toBe('l2')
    expect(oListener3.tag).toBe('l3')
  })
  it('le speaker emet un texte entendu par l0', function () {
    const m = new MUDEngine()
    m.setAssets(createStateSpeak())
    m._createAllEntities()
    const [oSpeaker] = m.findEntitiesByTag('s1', 'r1')
    const aLog = []
    m.events.on('entity.speak', ({ entity, interlocutor, speech }) => aLog.push({ entity: entity, interlocutor: interlocutor, speech, volume: 'normal' }))
    m.events.on('entity.speak.shout', ({ entity, interlocutor, speech }) => aLog.push({ entity: entity, interlocutor: interlocutor, speech, volume: 'shout' }))
    m.events.on('entity.speak.echo', ({ entity, interlocutor, speech }) => aLog.push({ entity: entity, interlocutor: interlocutor, speech, volume: 'echo' }))
    m.speakString(oSpeaker.id, 'r1', 'test1')
    // deux entités doivent avoir recu le mesage
    expect(aLog).toHaveLength(2)
    // un des messages doit partir de s1 vers l0
    expect(aLog.find(x => m.getEntity(x.entity).tag === 's1' && m.getEntity(x.interlocutor).tag === 'l0' && x.speech === 'test1')).toBeDefined()
    // un des messages doit partir de s1 vers s1
    expect(aLog.find(x => m.getEntity(x.entity).tag === 's1' && m.getEntity(x.interlocutor).tag === 's1' && x.speech === 'test1')).toBeDefined()
  })
  it('le speaker crie un texte entendu par l0, l1, l2', function () {
    const m = new MUDEngine()
    m.setAssets(createStateSpeak())
    m._createAllEntities()
    const [oSpeaker] = m.findEntitiesByTag('s1', 'r1')
    const aLog = []
    m.events.on('entity.speak', ({ entity, interlocutor, speech, volume }) => aLog.push({ entity: entity, interlocutor: interlocutor, speech, volume }))
    m.speakString(oSpeaker.id, 'r1', 'test1', MUDEngine.CONST.VOLUME_SHOUT)
    // deux entités doivent avoir recu le message
    expect(aLog).toHaveLength(4)
    // un des messages doit partir de s1 vers l0
    expect(aLog.find(
      x => m.getEntity(x.entity).tag === 's1' &&
        m.getEntity(x.interlocutor).tag === 's1' &&
        x.volume === MUDEngine.CONST.VOLUME_SHOUT &&
        x.speech === 'test1')
    ).toBeDefined()
    expect(aLog.find(x => m.getEntity(x.entity).tag === 's1' && m.getEntity(x.interlocutor).tag === 's1' && x.speech === 'test1' && x.volume === MUDEngine.CONST.VOLUME_SHOUT)).toBeDefined()
    expect(aLog.find(x => m.getEntity(x.entity).tag === 's1' && m.getEntity(x.interlocutor).tag === 'l0' && x.speech === 'test1' && x.volume === MUDEngine.CONST.VOLUME_SHOUT)).toBeDefined()
    expect(aLog.find(x => m.getEntity(x.entity).tag === 's1' && m.getEntity(x.interlocutor).tag === 'l1' && x.speech === 'test1' && x.volume === MUDEngine.CONST.VOLUME_ECHO)).toBeDefined()
    expect(aLog.find(x => m.getEntity(x.entity).tag === 's1' && m.getEntity(x.interlocutor).tag === 'l3' && x.speech === 'test1' && x.volume === MUDEngine.CONST.VOLUME_ECHO)).toBeDefined()
  })
})

describe('ajustement de blueprints', function () {
  it('creation d\'une entité un poids personnalisé', function () {
    const m = new MUDEngine()
    m.events.on('asset.blueprint.hook', ({ resref, data }) => {
      if (resref === 'poids') {
        data.weight = 15
      }
    })
    const oAssets = {
      blueprints: {
        poids: {
          type: 'item',
          subtype: 'misc',
          name: 'poids',
          desc: [],
          weight: 0,
          tag: 'v1',
          inventory: false,
          stackable: false
        }
      }
    }
    m.setAssets(oAssets)
    const oPoids = m.createEntity('poids')
    expect(oAssets.blueprints.poids.weight).toBe(15)
    expect(oPoids.weight).toBe(15)
  })
})

describe('container locked at room creation', function () {
  it('creation d\'une entité avec lock structure', function () {
    const m = new MUDEngine()
    m.setAssets({
      blueprints: {
        bag: {
          type: 'item',
          subtype: 'container',
          name: 'sac',
          desc: [
            'Cet objet n\'a rien de particulier'
          ],
          weight: 1,
          tag: 'v1',
          stackable: false,
          inventory: true,
          lock: {
            difficulty: 10,
            locked: false
          }
        }
      },
      sectors: {
        s1: {
          name: 's1',
          desc: ['x']
        }
      },
      rooms: {
        r1: {
          name: 'r1',
          desc: ['x'],
          sector: 's1',
          content: [
            {
              ref: 'bag',
              locked: true
            }
          ],
          nav: {
          }
        }
      }
    })
    m.createPlayerEntity('x1', 'g', 'Glouke', 'r1')
    const [oBag] = m.findEntitiesByName('bag', 'r1')
    expect(oBag.locked).toBeTruthy()
  })
  it('creation d\'une entité avec lock structure par defaut à false', function () {
    const m = new MUDEngine()
    m.setAssets({
      blueprints: {
        bag: {
          type: 'item',
          subtype: 'container',
          name: 'sac',
          desc: [
            'Cet objet n\'a rien de particulier'
          ],
          weight: 1,
          tag: 'v1',
          stackable: false,
          inventory: true,
          lock: {
            difficulty: 10,
            locked: false
          }
        }
      },
      sectors: {
        s1: {
          name: 's1',
          desc: ['x']
        }
      },
      rooms: {
        r1: {
          name: 'r1',
          desc: ['x'],
          sector: 's1',
          content: [
            {
              ref: 'bag',
              locked: false
            }
          ],
          nav: {
          }
        }
      }
    })
    m.createPlayerEntity('x1', 'g', 'Glouke', 'r1')
    const [oBag] = m.findEntitiesByName('bag', 'r1')
    expect(oBag.locked).toBeFalsy()
  })
})

describe('unlocking exits', function () {
  it('should unlock exit with the right key', function () {
    const m = new MUDEngine()
    m.setAssets({
      blueprints: {
        key1: {
          type: 'item',
          subtype: 'key',
          name: 'clé',
          desc: [
            'Cet objet n\'a rien de particulier'
          ],
          weight: 1,
          tag: 'k1',
          stackable: false,
          inventory: false
        }
      },
      sectors: {
        s1: {
          name: 's1',
          desc: ['x']
        }
      },
      rooms: {
        r1: {
          name: 'r1',
          desc: ['x'],
          sector: 's1',
          content: [],
          nav: {
            n: {
              to: 'r2',
              name: 'x',
              lock: {
                locked: true,
                key: 'k1'
              }
            }
          }
        }
      }
    })
    const oPlayer = m.createPlayerEntity('x1', 'g', 'Glouke', 'r1')
    const oKey1 = m.createEntity('key1', oPlayer.id)
    const oExit1 = m.getExit(oPlayer.id, 'n')
    expect(oExit1.locked).toBeTruthy()
    const r1 = m.actionUnlock(oPlayer.id, 'n', oKey1.id)
    expect(r1.success).toBeTruthy()
    expect(oExit1.locked).toBeFalsy()
  })
  it('should not unlock exit with the wrong key', function () {
    const m = new MUDEngine()
    m.setAssets({
      blueprints: {
        key1: {
          type: 'item',
          subtype: 'key',
          name: 'clé',
          desc: [
            'Cet objet n\'a rien de particulier'
          ],
          weight: 1,
          tag: 'k1',
          stackable: false,
          inventory: false
        },
        key2: {
          type: 'item',
          subtype: 'key',
          name: 'clé',
          desc: [
            'Cet objet n\'a rien de particulier'
          ],
          weight: 1,
          tag: 'k2',
          stackable: false,
          inventory: false
        }
      },
      sectors: {
        s1: {
          name: 's1',
          desc: ['x']
        }
      },
      rooms: {
        r1: {
          name: 'r1',
          desc: ['x'],
          sector: 's1',
          content: [],
          nav: {
            n: {
              to: 'r2',
              name: 'x',
              lock: {
                locked: true,
                key: 'k1'
              }
            }
          }
        }
      }
    })
    const oPlayer = m.createPlayerEntity('x1', 'g', 'Glouke', 'r1')
    const oKey1 = m.createEntity('key2', oPlayer.id)
    const oExit1 = m.getExit(oPlayer.id, 'n')
    expect(oExit1.locked).toBeTruthy()
    const r1 = m.actionUnlock(oPlayer.id, 'n', oKey1.id)
    expect(r1.success).toBeFalsy()
    expect(r1.outcome).toBe('WRONG_KEY')
    expect(oExit1.locked).toBeTruthy()
  })
  it('try to unlock door with tools', function () {
    const m = new MUDEngine()
    m.setAssets({
      blueprints: {
        tool1: {
          type: 'item',
          subtype: 'tool',
          name: 'outil 1',
          desc: [
            'Cet objet n\'a rien de particulier'
          ],
          weight: 1,
          tag: 't1',
          stackable: true,
          inventory: false
        },
        tool2: {
          type: 'item',
          subtype: 'tool',
          name: 'outil 2',
          desc: [
            'Cet objet n\'a rien de particulier'
          ],
          weight: 1,
          tag: 't2',
          stackable: true,
          inventory: false
        }
      },
      sectors: {
        s1: {
          name: 's1',
          desc: ['x']
        }
      },
      rooms: {
        r1: {
          name: 'r1',
          desc: ['x'],
          sector: 's1',
          content: [],
          nav: {
            n: {
              to: 'r2',
              name: 'x',
              lock: {
                locked: true,
                key: 'k1'
              }
            }
          }
        }
      }
    })
    m.events.on('entity.task.attempt', oEvent => {
      if (oEvent.skill === 'unlock' && oEvent.tool.tag === 't2') {
        oEvent.success(true)
      }
    })
    const oPlayer = m.createPlayerEntity('x1', 'g', 'Glouke', 'r1')
    const oTool1 = m.createEntity('tool1', oPlayer.id)
    const oTool2 = m.createEntity('tool2', oPlayer.id)
    const oExit1 = m.getExit(oPlayer.id, 'n')
    expect(oExit1.locked).toBeTruthy()
    m.actionUnlock(oPlayer.id, 'n', oTool1.id)
    expect(oExit1.locked).toBeTruthy()
    m.actionUnlock(oPlayer.id, 'n', oTool2.id)
    expect(oExit1.locked).toBeFalsy()
  })
  it('should loose key after unlocking exit', function () {
    const m = new MUDEngine()
    m.setAssets({
      blueprints: {
        key1: {
          type: 'item',
          subtype: 'key',
          name: 'clé',
          desc: [
            'Cet objet n\'a rien de particulier'
          ],
          weight: 1,
          tag: 'k1',
          stackable: false,
          inventory: false
        },
        key2: {
          type: 'item',
          subtype: 'key',
          name: 'clé',
          desc: [
            'Cet objet n\'a rien de particulier'
          ],
          weight: 1,
          tag: 'k2',
          stackable: false,
          inventory: false
        }
      },
      sectors: {
        s1: {
          name: 's1',
          desc: ['x']
        }
      },
      rooms: {
        r1: {
          name: 'r1',
          desc: ['x'],
          sector: 's1',
          content: [],
          nav: {
            n: {
              to: 'r2',
              name: 'x',
              lock: {
                locked: true,
                key: 'k1',
                discardKey: true
              }
            },
            s: {
              to: 'r3',
              name: 'x',
              lock: {
                locked: true,
                key: 'k2',
                discardKey: false
              }
            }
          }
        }
      }
    })
    const oPlayer = m.createPlayerEntity('x1', 'g', 'Glouke', 'r1')
    const oKey1 = m.createEntity('key1', oPlayer.id)
    const oKey2 = m.createEntity('key2', oPlayer.id)
    expect(oKey1.location).toBe(oPlayer.id)
    expect(oKey2.location).toBe(oPlayer.id)

    m.actionUnlock(oPlayer.id, 'n', oKey1.id)
    expect(oKey1.location).not.toBe(oPlayer.id)

    m.actionUnlock(oPlayer.id, 's', oKey2.id)
    expect(oKey2.location).toBe(oPlayer.id)
  })
})

describe('search secret exit', function () {
  it('should reveal secret exit', function () {
    const m = new MUDEngine()
    m.setAssets({
      blueprints: {
      },
      sectors: {
        s1: {
          name: 's1',
          desc: ['x']
        }
      },
      rooms: {
        r1: {
          name: 'r1',
          desc: ['x'],
          sector: 's1',
          content: [],
          nav: {
            n: {
              to: 'r2',
              name: 'x',
              secret: {
                difficulty: 10
              }
            },
            s: {
              to: 'r3',
              name: 'x'
            }
          }
        }
      }
    })
    m.events.on('entity.task.attempt', oEvent => {
      if (oEvent.skill === 'search') {
        oEvent.success(true)
      }
    })
    const oPlayer = m.createPlayerEntity('x1', 'g', 'Glouke', 'r1')
    // déterminer les issues visibles
    const oNorthExit = m.getExit(oPlayer.id, 'n')
    expect(oNorthExit.visible).toBeFalsy()
    const f = m.actionSearchSecret(oPlayer.id, 'n')
    const oNorthExit2 = m.getExit(oPlayer.id, 'n')
    expect(f.outcome).toBe('')
    expect(f.success).toBeTruthy()
    expect(oNorthExit2.visible).toBeTruthy()
  })
})

const KeywordParser = require('../libs/keyword-parser')

describe('parse', function () {
  it('parser des chaines vides sans gueuler', function () {
    const kwp = new KeywordParser()
    const o = kwp.parse('')
    expect(o).toBeNull()
  })
  it('parser des chaines non-vides', function () {
    const kwp = new KeywordParser()
    const o = kwp.parse('x')
    expect(o).toEqual(['x']) // même si ca veut rien dire
  })
  it('parser un simple mot clé', function () {
    const kwp = new KeywordParser()
    const o = kwp.parse('de', 'de')
    expect(o).toEqual([])
  })
  it('parser un mot clé et une valeur', function () {
    const kwp = new KeywordParser()
    const o = kwp.parse('de concert', 'de *')
    expect(o).toEqual(['concert'])
  })
  it('parser une valeur suivie d\'un mot clé', function () {
    const kwp = new KeywordParser()
    const o = kwp.parse('npm install -S', '* -S')
    expect(o).toEqual(['npm install'])
  })
  it('parser une valeur puis un nombre', function () {
    const kwp = new KeywordParser()
    const o = kwp.parse('flèche enflammée 15', '* #')
    expect(o).toEqual(['flèche enflammée', 15])
  })
  it('parser un asterisque', function () {
    const kwp = new KeywordParser()
    const o = kwp.parse('*', '*')
    expect(o).toEqual(['*'])
  })
  it('une chaine spéciale', function () {
    const kwp = new KeywordParser()
    const o = kwp.parse('  une    chaîne       spéciale    ', '*')
    expect(o).toEqual(['une chaîne spéciale'])
  })
  it('une chaine avec un mot clé', function () {
    const kwp = new KeywordParser()
    const o = kwp.parse('une chaîne avec un mot-clé', '* avec *')
    expect(o).toEqual(['une chaîne', 'un mot-clé'])
  })
  it('une chaine avec des mot clé répété', function () {
    const kwp = new KeywordParser()
    const o = kwp.parse('une chaîne avec un mot-clé et avec un autre truc', '* avec *')
    expect(o).toEqual(['une chaîne avec un mot-clé et', 'un autre truc'])
  })
  it('une chaine avec deux mots clés "avec"', function () {
    const kwp = new KeywordParser()
    const o = kwp.parse('une chaîne avec un mot-clé et avec un autre truc', '* avec * avec *')
    expect(o).toEqual(['une chaîne', 'un mot-clé et', 'un autre truc'])
  })
  it('une chaine avec deux mots clés "avec"', function () {
    const kwp = new KeywordParser()
    const o = kwp.parse('une chaîne avec un mot-clé et avec un autre truc', '* avec * avec *')
    expect(o).toEqual(['une chaîne', 'un mot-clé et', 'un autre truc'])
  })
  it('une chaine avec deux mots clés différents', function () {
    const kwp = new KeywordParser()
    const o = kwp.parse('une chaîne avec un mot-clé et un autre truc', '* avec * et *')
    expect(o).toEqual(['une chaîne', 'un mot-clé', 'un autre truc'])
  })
})

describe('dispatcher', function () {
  it ('direct dispatch d\'une chaine complexe', function () {
    const kwp = new KeywordParser()
    let a
    kwp.dispatch(
      'une chaîne avec un mot-clé et un autre truc',
      {
        '* avec * et *': (subject, avec1, et1) => {
          a = `bing ${subject} / ${avec1} / ${et1}`
        }
      }
    )
    expect(a).toBe('bing une chaîne / un mot-clé / un autre truc')
  })
  it ('dispatch multiple', function () {
    const kwp = new KeywordParser()
    let a
    kwp.dispatch(
      'flèche en argent 18 from ratelier',
      {
        '* # from  *': (obj, num, cont) => {
          a = `1. obj ${obj} / num ${num} / cont ${cont}`
        },
        '# * from  *': (obj, num, cont) => {
          a = `2. obj ${obj} / num ${num} / cont ${cont}`
        },
        '* from  *': (obj, cont) => {
          a = `3. obj ${obj} / cont ${cont}`
        },
        '* #': (obj, num) => {
          a = `4. obj ${obj} / num ${num}`
        },
        '# *': (num, obj) => {
          a = `5. num ${num} / obj ${obj}`
        },
        '*': (obj) => {
          a = `6. obj ${obj}`
        }
      }
    )
    expect(a).toBe('1. obj flèche en argent / num 18 / cont ratelier')
  })
})

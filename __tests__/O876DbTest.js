const db = require('../libs/o876-db')
const path = require('path')
const fs = require('fs')
const os = require('os')
const { INDEX_TYPES } = require('../libs/o876-db')

let TMP_DIR

beforeAll(() => {
  TMP_DIR = fs.mkdtempSync(path.join(os.tmpdir(), 'o876dbTest-'))
})

afterAll(() => {
  fs.rmSync(TMP_DIR, { recursive: true, force: true })
})

describe('collection behaviour when folder not exists', function () {
  it('saving on non existant collection folder', async function () {
    const c = new db.Collection('xxxklmsdklm')
    let eError = null
    try {
      await c.save(1, { x: 5 })
    } catch (e) {
      eError = e
    }
    expect(eError).not.toBeNull()
    expect(eError.message).toContain('ENOENT:')
  })

  it('reading non existant document', async function () {
    const c = new db.Collection(path.resolve(__dirname, 'tmp/y'))
    let eError = null
    try {
      await c.get(1)
    } catch (e) {
      eError = e
    }
    expect(eError).not.toBeNull()
    expect(eError.message).toContain('ENOENT:')
  })

  it('removing non existant document', async function () {
    const c = new db.Collection(path.resolve(__dirname, 'tmp/y'))
    let eError = null
    try {
      await c.remove(1111)
    } catch (e) {
      eError = e
    }
    expect(eError).not.toBeNull()
    expect(eError.message).toContain('ENOENT:')
  })

  it('finding in empty collection', async function () {
    const c = new db.Collection(path.resolve(__dirname, 'tmp/y'))
    let eError = null
    let r
    try {
      r = await c.find(d => true)
    } catch (e) {
      eError = e
    }
    expect(eError).toBeNull()
  })

  it('finding in non existant collection', async function () {
    const c = new db.Collection(path.resolve(__dirname, 'tmp/x'))
    let eError = null
    let r
    try {
      r = await c.find(d => true)
    } catch (e) {
      eError = e
    }
    expect(eError).toBeNull()
    expect(r).toEqual([])
  })
})

describe('Indexer', function () {
  it('index simple', function () {
    const oIndexer = new db.Indexer()
    oIndexer.createIndex('name', { unique: true })
    oIndexer.createIndex('element')
    oIndexer.addIndexedValue('name', 'shiva', 100)
    oIndexer.addIndexedValue('name', 'ifrit', 118)
    oIndexer.addIndexedValue('name', 'ramuh', 122)
    oIndexer.addIndexedValue('name', 'ondine', 137)
    oIndexer.addIndexedValue('name', 'bahamut', 150)
    oIndexer.addIndexedValue('name', 'ixion', 153)
    oIndexer.addIndexedValue('name', 'odin', 190)
    oIndexer.addIndexedValue('name', 'titan', 1993)
    oIndexer.addIndexedValue('element', 'ice', 100)
    oIndexer.addIndexedValue('element', 'fire', 118)
    oIndexer.addIndexedValue('element', 'thunder', 122)
    oIndexer.addIndexedValue('element', 'water', 137)
    oIndexer.addIndexedValue('element', 'none', 150)
    oIndexer.addIndexedValue('element', 'thunder', 153)
    oIndexer.addIndexedValue('element', 'divine', 190)
    oIndexer.addIndexedValue('element', 'earth', 1993)
    expect(oIndexer.search('name', 'ramuh')).toEqual(['122'])
    expect(oIndexer.search('element', 'thunder')).toEqual(['122', '153'])
  })
  it('index removal simple', function () {
    const oIndexer = new db.Indexer()
    oIndexer.createIndex('name', { unique: true })
    oIndexer.createIndex('element')
    oIndexer.addIndexedValue('name', 'shiva', 100)
    oIndexer.addIndexedValue('name', 'ifrit', 118)
    oIndexer.addIndexedValue('name', 'ramuh', 122)
    oIndexer.addIndexedValue('name', 'ondine', 137)
    oIndexer.addIndexedValue('name', 'bahamut', 150)
    oIndexer.addIndexedValue('name', 'ixion', 153)
    oIndexer.addIndexedValue('name', 'odin', 190)
    oIndexer.addIndexedValue('name', 'titan', 1993)
    oIndexer.addIndexedValue('element', 'ice', 100)
    oIndexer.addIndexedValue('element', 'fire', 118)
    oIndexer.addIndexedValue('element', 'thunder', 122)
    oIndexer.addIndexedValue('element', 'water', 137)
    oIndexer.addIndexedValue('element', 'none', 150)
    oIndexer.addIndexedValue('element', 'thunder', 153)
    oIndexer.addIndexedValue('element', 'divine', 190)
    oIndexer.addIndexedValue('element', 'earth', 1993)
    oIndexer.removeIndexedValue('element', 'thunder', 122)
    expect(oIndexer.search('element', 'thunder')).toEqual(['153'])
  })
})

async function seedData (oColl) {
  await oColl.save({
    id: 1,
    name: 'ifrit',
    element: 'fire'
  })
  await oColl.save({
    id: 2,
    name: 'shiva',
    element: 'ice'
  })
  await oColl.save({
    id: 3,
    name: 'ramuh',
    element: 'thunder'
  })
  await oColl.save({
    id: 4,
    name: 'bahamut',
    element: ''
  })
  await oColl.save({
    id: 5,
    name: 'ondine',
    element: 'water'
  })
  await oColl.save({
    id: 6,
    name: 'leviathan',
    element: 'water'
  })
  await oColl.save({
    id: 7,
    name: 'ixion',
    element: 'thunder'
  })
  await oColl.save({
    id: 8,
    name: 'titan',
    element: 'earth'
  })
  await oColl.save({
    id: 9,
    name: 'odin',
    element: 'divine'
  })
  await oColl.save({
    id: 10,
    name: 'alexander',
    element: 'light'
  })
}

describe('collection queries', function () {
  it('finds by get', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c1'))
    await c.init()
    await seedData(c)
    const aAllKeys = await c.keys
    const aSortedKeys = aAllKeys
      .map(k => k | 0)
      .sort((a, b) => a - b)
    expect(aSortedKeys).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    const doc = await c.get(1)
    expect(doc).toEqual({ id: 1, name: 'ifrit', element: 'fire' })
    await c.drop()
  })
  it('finds by predicate', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c2'))
    await c.init()
    await seedData(c)
    const doc = await c.find(d => d.name === 'shiva')
    expect(doc).toEqual([{ id: 2, name: 'shiva', element: 'ice' }])
    await c.drop()
  })
  it('finds non existing', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c3'))
    await c.init()
    await seedData(c)
    const doc = await c.find(d => d.name === 'xxx')
    expect(doc).toEqual([])
    await c.drop()
  })
  it('finds by one non-indexed value', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c4'))
    await c.init()
    await seedData(c)
    const doc = await c._findByNonIndexedValues({ name: 'shiva' })
    expect(doc).toEqual([{ id: 2, name: 'shiva', element: 'ice' }])
    await c.drop()
  })
  it('finds by multiple non-indexed value', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c5'))
    await c.init()
    await seedData(c)
    const doc = await c._findByNonIndexedValues({ name: 'ramuh', element: 'thunder' })
    expect(doc).toEqual([{ id: 3, name: 'ramuh', element: 'thunder' }])
    const doc2 = await c._findByNonIndexedValues({ name: 'ixion', element: 'thunder' })
    expect(doc2).toEqual([{ id: 7, name: 'ixion', element: 'thunder' }])
    await c.drop()
  })
})

describe('collection and index', function () {
  it('create an single index BEFORE seed', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c10'))
    await c.init()
    await c.createIndex('element')
    await seedData(c)
    expect(c.index.data).toHaveProperty('element')
    expect(c.index.data.element).toHaveProperty('values')
    expect(c.index.data.element.values).toHaveProperty('fire')
    expect([...c.index.data.element.values.fire]).toEqual(['1'])
    expect(c.index.data.element.values).toHaveProperty('ice')
    expect([...c.index.data.element.values.ice]).toEqual(['2'])
    expect(c.index.data.element.values).toHaveProperty('thunder')
    expect([...c.index.data.element.values.thunder].sort((a, b) => parseInt(a) - parseInt(b))).toEqual(['3', '7'])
    expect(c.index.data.element.values).toHaveProperty('')
    expect([...c.index.data.element.values['']]).toEqual(['4'])
    expect(c.index.data.element.values).toHaveProperty('water')
    expect([...c.index.data.element.values.water].sort((a, b) => parseInt(a) - parseInt(b))).toEqual(['5', '6'])
    expect(c.index.data.element.values).toHaveProperty('earth')
    expect([...c.index.data.element.values.earth]).toEqual(['8'])
    expect(c.index.data.element.values).toHaveProperty('divine')
    expect([...c.index.data.element.values.divine]).toEqual(['9'])
    expect(c.index.data.element.values).toHaveProperty('light')
    expect([...c.index.data.element.values.light]).toEqual(['10'])
  })
  it('create a single index AFTER seed', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c11'))
    await c.init()
    await seedData(c)
    await c.createIndex('element')
    expect(c.index.data).toHaveProperty('element')
    expect(c.index.data.element).toHaveProperty('values')
    expect(c.index.data.element.values).toHaveProperty('fire')
    expect([...c.index.data.element.values.fire]).toEqual(['1'])
    expect(c.index.data.element.values).toHaveProperty('ice')
    expect([...c.index.data.element.values.ice]).toEqual(['2'])
    expect(c.index.data.element.values).toHaveProperty('thunder')
    expect([...c.index.data.element.values.thunder].sort((a, b) => parseInt(a) - parseInt(b))).toEqual(['3', '7'])
    expect(c.index.data.element.values).toHaveProperty('')
    expect([...c.index.data.element.values['']]).toEqual(['4'])
    expect(c.index.data.element.values).toHaveProperty('water')
    expect([...c.index.data.element.values.water].sort((a, b) => parseInt(a) - parseInt(b))).toEqual(['5', '6'])
    expect(c.index.data.element.values).toHaveProperty('earth')
    expect([...c.index.data.element.values.earth]).toEqual(['8'])
    expect(c.index.data.element.values).toHaveProperty('divine')
    expect([...c.index.data.element.values.divine]).toEqual(['9'])
    expect(c.index.data.element.values).toHaveProperty('light')
    expect([...c.index.data.element.values.light]).toEqual(['10'])
  })
  it('find zero document', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c12'))
    await c.init()
    c._explain = true
    await seedData(c)
    await c.createIndex('element')
    const aDocs = await c.find({ x: 15, y: 12 })
    expect(aDocs).toHaveLength(0)
  })
  it('find documents 1', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c13'))
    await c.init()
    c._explain = true
    await seedData(c)
    await c.createIndex('element')
    const aDocs = await c.find({ element: 'water' })
    aDocs.sort((a, b) => a.id - b.id)
    expect(aDocs).toHaveLength(2)
    expect(aDocs[0].id).toBe(5)
    expect(aDocs[1].id).toBe(6)
  })
  it('find documents by mixed clauses', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c13'))
    await c.init()
    await seedData(c)
    await c.createIndex('element')
    const aDocs = await c.find({ element: 'water', name: 'ondine' })
    expect(aDocs).toHaveLength(1)
    expect(aDocs[0].id).toBe(5)
  })
  it('find documents among thousands', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c14-giant'))
    await c.init()
    await c.createIndex('name')
    await c.createIndex('age')
    for (let i = 0; i < 2000; ++i) {
      const doc = {
        id: 'xxx' + i,
        name: Math.floor(Math.random() * 1296).toString(36),
        hex: i.toString(16),
        age: Math.floor(Math.random() * 100)
      }
      await c.save(doc)
    }
    await c.save({
      id: 'xxx2001',
      name: 'zoltec-le-sorcier',
      hex: (2001).toString(16),
      age: 33
    })
    for (let i = 2002; i < 3000; ++i) {
      const doc = {
        id: 'xxx' + i,
        name: Math.floor(Math.random() * 1296).toString(36),
        hex: i.toString(16),
        age: Math.floor(Math.random() * 100)
      }
      await c.save(doc)
    }
    const docZoltec = await c.find({ name: 'zoltec-le-sorcier' })
    expect(docZoltec).toHaveLength(1)
    expect(docZoltec[0].id).toBe('xxx2001')
    expect(docZoltec[0].name).toBe('zoltec-le-sorcier')
    expect(c.index.search('name', 'zoltec-le-sorcier')).toEqual(['xxx2001'])
    await c.remove('xxx2001')
    expect('zoltec-le-sorcier' in c.index.data.name).toBeFalsy()
    expect(c.index.search('name', 'zoltec-le-sorcier')).toBeNull()
    const docZoltec2 = await c.find({ name: 'zoltec-le-sorcier' })
    expect(docZoltec2).toHaveLength(0)
  })
  it('finds a document with lots of properties', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c15'))
    await c.init()
    await c.createIndex('a')
    await c.createIndex('b')
    await c.createIndex('c')
    await c.save({
      id: 1,
      a: 'abc',
      b: 'bcd',
      c: 'cde',
      d: 'def',
      e: 'efg',
      f: 'fgh'
    })
    await c.save({
      id: 2,
      a: 'abc',
      b: 'bcd',
      c: 'xx1',
      d: 'xx2',
      e: 'xx3',
      f: 'xx4'
    })
    await c.save({
      id: 3,
      a: 'abc',
      b: 'xx1',
      c: 'cde',
      d: 'xx2',
      e: 'xx3',
      f: 'xx4'
    })
    const x = c._getIndexedKeys({ a: 'abc', b: 'bcd' })
    expect(x).toEqual(['1', '2'])
    const d = await c.find({ a: 'abc', b: 'bcd', f: 'xx4' })
    expect(d).toHaveLength(1)
    expect(d[0].id).toBe(2)
    const d2 = await c.find({ b: 'xx1', c: 'xx1' })
    expect(d2).toHaveLength(0)
  })
  it('check if null values are indexed', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c16'))
    await c.init()
    await c.createIndex('nullableField')
    await c.save({
      id: 1,
      nullableField: 'aaaaaaaaaa'
    })
    await c.save({
      id: 2,
      nullableField: 'bbbbbbbbbbbbb'
    })
    await c.save({
      id: 3,
      nullableField: 'cccccccccc'
    })
    await c.save({
      id: 4,
      nullableField: null
    })
    await c.save({
      id: 5,
      nullableField: 'ddddddddddddddd'
    })
    await c.save({
      id: 6,
      nullableField: null
    })
    const d1 = await c.find({ nullableField: 'aaaaaaaaaa' })
    expect(d1).toHaveLength(1)
    expect(d1[0].id).toBe(1)
    const d2 = await c.find({ nullableField: null })
    expect(d2).toHaveLength(2)
    expect(d2[0].id).toBe(4)
    expect(d2[1].id).toBe(6)
  })
  it('test indexed value with predicate', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c17'))
    await c.init()
    await c.createIndex('intval1')
    for (let i = 0; i < 20; ++i) {
      const d = {
        id: 10 + i,
        intval1: i
      }
      await c.save(d)
    }
    const dx = c.index._searchValues('intval1', x => parseInt(x) < 10)
    dx.sort((a, b) => parseInt(a) - parseInt(b))
    expect(dx).toEqual(['10', '11', '12', '13', '14', '15', '16', '17', '18', '19'])
  })
  it('test indexed value with operator', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c18'))
    await c.init()
    await seedData(c)
    await c.createIndex('element')
    const d1 = await c.find({ element: { $in: ['fire', 'ice'] } })
    d1.sort((a, b) => a.id - b.id)
    expect(d1).toEqual([
      { id: 1, name: 'ifrit', element: 'fire' },
      { id: 2, name: 'shiva', element: 'ice' }
    ])
    const d2 = await c.find({ element: { $nin: ['fire', 'ice', 'water', '', 'divine', 'light', 'shadow'] } })
    d2.sort((a, b) => a.id - b.id)
    expect(d2).toEqual([
      { id: 3, name: 'ramuh', element: 'thunder' },
      { id: 7, name: 'ixion', element: 'thunder' },
      { id: 8, name: 'titan', element: 'earth' }
    ])
  })
  it('test non-indexed value with operator', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c19'))
    await c.init()
    await seedData(c)
    const d1 = await c.find({ element: { $in: ['fire', 'ice'] } })
    d1.sort((a, b) => a.id - b.id)
    expect(d1).toEqual([
      { id: 1, name: 'ifrit', element: 'fire' },
      { id: 2, name: 'shiva', element: 'ice' }
    ])
    const d2 = await c.find({ element: { $nin: ['fire', 'ice', 'water', '', 'divine', 'light', 'shadow'] } })
    d2.sort((a, b) => a.id - b.id)
    expect(d2).toEqual([
      { id: 3, name: 'ramuh', element: 'thunder' },
      { id: 7, name: 'ixion', element: 'thunder' },
      { id: 8, name: 'titan', element: 'earth' }
    ])
  })
  it('test non-indexed value with mod operator', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c20'))
    await c.init()
    await c.save({ id: 1, value: 1 })
    await c.save({ id: 2, value: 2 })
    await c.save({ id: 3, value: 3 })
    await c.save({ id: 4, value: 4 })
    await c.save({ id: 5, value: 5 })
    await c.save({ id: 6, value: 6 })
    await c.save({ id: 7, value: 7 })
    await c.save({ id: 8, value: 8 })
    await c.save({ id: 9, value: 9 })
    expect(await c.find({ value: { $mod: [2, 0] } })).toEqual([
      { id: 2, value: 2 },
      { id: 4, value: 4 },
      { id: 6, value: 6 },
      { id: 8, value: 8 }
    ])
    expect(await c.find({ value: { $mod: [2, 1] } })).toEqual([
      { id: 1, value: 1 },
      { id: 3, value: 3 },
      { id: 5, value: 5 },
      { id: 7, value: 7 },
      { id: 9, value: 9 }
    ])
  })
  it('test non-indexed value with exists operator', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c21'))
    await c.init()
    await c.save({ id: 1, value: 1 })
    await c.save({ id: 2, value: 2 })
    await c.save({ id: 3, value: 'abc' })
    await c.save({ id: 4, value: 4 })
    await c.save({ id: 5, value: 5 })
    await c.save({ id: 6, value: 6 })
    await c.save({ id: 7, value: 'def' })
    await c.save({ id: 8, value: 8 })
    await c.save({ id: 9, value: 9 })
    expect(await c.find({ value: { $type: 'string' } })).toEqual([
      { id: 3, value: 'abc' },
      { id: 7, value: 'def' }
    ])
  })
  it('test non-indexed value with exists operator 1', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c22'))
    await c.init()
    await c.save({ id: 1, value: 1 })
    await c.save({ id: 2, value: 2 })
    await c.save({ id: 3, value: 'abc', truc: 1 })
    await c.save({ id: 4, value: 4 })
    await c.save({ id: 5, value: 5 })
    await c.save({ id: 6, value: 6 })
    await c.save({ id: 7, value: 'def' })
    await c.save({ id: 8 })
    await c.save({ id: 9, value: 9 })
    expect(await c.find({ truc: { $exists: true } })).toEqual([
      { id: 3, value: 'abc', truc: 1 }
    ])
    expect(await c.find({ value: { $exists: false } })).toEqual([
      { id: 8 }
    ])
  })
  it('test non-indexed value with regexp operator', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c23'))
    await c.init()
    await seedData(c)

    const d1 = await c.find({ name: { $match: /amu/ } })
    const names1 = d1.map(({ name }) => name)
    expect(names1.includes('ramuh')).toBeTruthy()
    expect(names1.includes('bahamut')).toBeTruthy()
    expect(names1).toHaveLength(2)
    await c.save({
      id: 50,
      name: 'musashi',
      element: ''
    })
    const d2 = await c.find({ name: /SHI/i })
    const names2 = d2.map(({ name }) => name)
    expect(names2.includes('shiva')).toBeTruthy()
    expect(names2.includes('musashi')).toBeTruthy()
    expect(names2).toHaveLength(2)
  })
  it('test indexed value with regexp operator', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c23'))
    await c.init()
    await seedData(c)
    await c.createIndex('name')

    const d1 = await c.find({ name: { $match: /amu/ } })
    const names1 = d1.map(({ name }) => name)
    expect(names1.includes('ramuh')).toBeTruthy()
    expect(names1.includes('bahamut')).toBeTruthy()
    expect(names1).toHaveLength(2)
    await c.save({
      id: 50,
      name: 'musashi',
      element: ''
    })
    const d2 = await c.find({ name: /SHI/i })
    const names2 = d2.map(({ name }) => name)
    expect(names2.includes('shiva')).toBeTruthy()
    expect(names2.includes('musashi')).toBeTruthy()
    expect(names2).toHaveLength(2)
  })
  it('test non-indexed value with arrayed regexp operator', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c23'))
    await c.init()
    await seedData(c)

    const d1 = await c.find({ name: { $in: [/amu/, 'shiva'] } })
    const names1 = d1.map(({ name }) => name)
    expect(names1.includes('shiva')).toBeTruthy()
    expect(names1.includes('ramuh')).toBeTruthy()
    expect(names1.includes('bahamut')).toBeTruthy()
    expect(names1).toHaveLength(3)
  })
  it('test indexed value with arrayed regexp operator', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c23'))
    await c.init()
    await seedData(c)
    await c.createIndex('name')

    const d1 = await c.find({ name: { $in: [/amu/, 'shiva'] } })
    const names1 = d1.map(({ name }) => name)
    expect(names1.includes('shiva')).toBeTruthy()
    expect(names1.includes('ramuh')).toBeTruthy()
    expect(names1.includes('bahamut')).toBeTruthy()
    expect(names1).toHaveLength(3)
  })
})

describe('custom operator', function () {
  it('a simple get length string function', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c19'))
    await c.init()
    await seedData(c)
    db.operators.$maxlen = operand => value => value.length <= parseInt(operand)
    const d = await c.find({ name: { $maxlen: 4 } })
    expect(d[0].name).toBe('odin')
    expect(d).toHaveLength(1)
  })
})

describe('case insensitive index', function () {
  it('index must return id', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c20'))
    await c.init()
    await seedData(c)
    const d1 = await c.find({ name: 'SHIVA' })
    expect(d1).toHaveLength(0)
    const d2 = await c.find({ name: { $eq: 'SHIVA' } })
    expect(d2).toHaveLength(1)
    await c.createIndex('name', {
      caseInsensitive: true,
      unique: true
    })
    const s1 = c.index.search('name', 'shiva')
    expect(s1).toEqual(['2'])
    const s2 = c.index.search('name', 'SHIVA')
    expect(s2).toEqual(['2'])
  })
})

describe('hashed index', function () {
  it('search for indexed hashed value crc32', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c21'))
    await c.init()
    await seedData(c)
    c.index._DEBUG = true
    await c.createIndex('name', {
      unique: false,
      caseInsensitive: true,
      type: db.INDEX_TYPES
    })
    const s1 = c.index.search('name', 'shiva')
    expect(s1).toEqual(['2'])
    const s2 = c.index.search('name', 'SHIVA')
    expect(s2).toEqual(['2'])
  })
  it('search for indexed hashed value crc16', async function () {
    const c = new db.Collection(path.join(TMP_DIR, 'c22'))
    await c.init()
    await seedData(c)
    c.index._DEBUG = true
    await c.createIndex('name', {
      unique: false,
      caseInsensitive: true,
      type: INDEX_TYPES.CRC16
    })
    const s1 = c.index.search('name', 'shiva')
    expect(s1).toEqual(['2'])
    const s2 = c.index.search('name', 'SHIVA')
    expect(s2).toEqual(['2'])
  })
})

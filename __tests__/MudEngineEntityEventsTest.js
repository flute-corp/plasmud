const MUDEngine = require('../libs/plasmud/Engine')

function getEntitiesByTagInRoom (engine, aTags, sLocation) {
  const oEntitites = {}
  aTags.forEach(t => {
    const aFound = engine.findEntitiesByTag(t, sLocation, true)
    if (aFound) {
      oEntitites[t] = aFound.shift().id
    } else {
      throw new Error('Not found : ' + t)
    }
  })
  return oEntitites
}

describe('blocked event', () => {
  it('should fire blocked event when actor tries to move to another room through a locked door', () => {
    const mud = new MUDEngine()
    const assets = {
      blueprints: {
        acteur: {
          type: MUDEngine.CONST.ENTITY_TYPES.ACTOR,
          name: 'acteur',
          desc: [
            'Une créature.'
          ],
          weight: 1,
          inventory: true,
          tag: 'acteur',
          events: {
            blocked: 'acteur_is_blocked'
          }
        }
      },
      sectors: {
        s00: {
          name: 'x',
          desc: ['x']
        }
      },
      rooms: {
        r00: {
          name: 'r00',
          desc: ['r00'],
          sector: 's00',
          nav: {
            n: {
              to: 'r01',
              lock: {
                locked: true,
                difficulty: 1
              }
            }
          },
          content: [
            {
              ref: 'acteur'
            }
          ]
        },
        r01: {
          name: 'r01',
          desc: ['r01'],
          sector: 's00',
          nav: {
            s: {
              to: 'r00'
            }
          }
        }
      }
    }
    const aEventLog = []
    mud.setAssets(assets)
    mud.events.on('entity.event.script', ({
      script,
      payload
    }) => {
      aEventLog.push({ script, actor: payload.self })
    })
    const { acteur } = getEntitiesByTagInRoom(mud, ['acteur'], 'r00')
    // Tester si on deverrouille la porte (l'evènement ne dois pas etre généré)
    expect(aEventLog).toHaveLength(0)
    mud.getExit('r00', 'n').locked = false
    mud.actionPassThruExit(acteur, 'n')
    expect(aEventLog).toHaveLength(0)
    expect(mud.getEntity(acteur).location).toBe('r01')
    mud.actionPassThruExit(acteur, 's')
    expect(mud.getEntity(acteur).location).toBe('r00')

    // Verrouiller la porte
    mud.getExit('r00', 'n').locked = true
    expect(aEventLog).toHaveLength(0)
    // Tenter de passer la porte verrouillée
    mud.actionPassThruExit(acteur, 'n')
    expect(aEventLog).toHaveLength(1)
    expect(aEventLog.pop()).toEqual({ script: 'acteur_is_blocked', actor: acteur })
  })
})

describe('spawn event', () => {
  it('should fire spawn event when actor is created during "setAsset"', () => {
    const mud = new MUDEngine()
    const assets = {
      blueprints: {
        acteur: {
          type: MUDEngine.CONST.ENTITY_TYPES.ACTOR,
          name: 'acteur',
          desc: [
            'Une créature.'
          ],
          weight: 1,
          inventory: true,
          tag: 'acteur',
          events: {
            spawn: 'acteur_spawns'
          }
        }
      },
      sectors: {
        s00: {
          name: 'x',
          desc: ['x']
        }
      },
      rooms: {
        r00: {
          name: 'r00',
          desc: ['r00'],
          sector: 's00',
          nav: {
            n: {
              to: 'r01',
              lock: {
                locked: true,
                difficulty: 1
              }
            }
          },
          content: [
            {
              ref: 'acteur'
            }
          ]
        },
        r01: {
          name: 'r01',
          desc: ['r01'],
          sector: 's00',
          nav: {
            s: {
              to: 'r00'
            }
          }
        }
      }
    }
    const aEventLog = []
    mud.events.on('entity.event.script', ({
      script,
      payload
    }) => {
      aEventLog.push({ script, actor: payload.self })
    })
    expect(aEventLog).toHaveLength(0)
    mud.setAssets(assets)
    const { acteur } = getEntitiesByTagInRoom(mud, ['acteur'], 'r00')
    expect(aEventLog).toHaveLength(1)
    expect(aEventLog.pop()).toEqual({ script: 'acteur_spawns', actor: acteur })
  })
  it('should fire spawn event when item is created during "setAsset"', () => {
    const mud = new MUDEngine()
    const assets = {
      blueprints: {
        pomme: {
          type: MUDEngine.CONST.ENTITY_TYPES.ITEM,
          subtype: 'plot',
          name: 'pomme',
          desc: [
            'Il existe de nombreux objets inhabituel dans le jeu. En voici un.'
          ],
          weight: 1,
          stackable: false,
          inventory: false,
          tag: 'pomme',
          events: {
            spawn: 'pomme_spawns'
          }
        }
      },
      sectors: {
        s00: {
          name: 'x',
          desc: ['x']
        }
      },
      rooms: {
        r00: {
          name: 'r00',
          desc: ['r00'],
          sector: 's00',
          nav: {
            n: {
              to: 'r01',
              lock: {
                locked: true,
                difficulty: 1
              }
            }
          },
          content: [
            {
              ref: 'pomme'
            }
          ]
        },
        r01: {
          name: 'r01',
          desc: ['r01'],
          sector: 's00',
          nav: {
            s: {
              to: 'r00'
            }
          }
        }
      }
    }
    const aEventLog = []
    mud.events.on('entity.event.script', ({
      script,
      payload
    }) => {
      aEventLog.push({ script, entity: payload.self })
    })
    expect(aEventLog).toHaveLength(0)
    mud.setAssets(assets)
    const { pomme } = getEntitiesByTagInRoom(mud, ['pomme'], 'r00')
    expect(aEventLog).toHaveLength(1)
    expect(aEventLog.pop()).toEqual({ script: 'pomme_spawns', entity: pomme })
  })
})

describe('despawn event', () => {
  it('should fire despawn event when actor is destroyed', () => {
    const mud = new MUDEngine()
    const assets = {
      blueprints: {
        acteur: {
          type: MUDEngine.CONST.ENTITY_TYPES.ACTOR,
          name: 'acteur',
          desc: [
            'Une créature.'
          ],
          weight: 1,
          inventory: true,
          tag: 'acteur',
          events: {
            despawn: 'acteur_despawns'
          }
        },
        pomme: {
          type: MUDEngine.CONST.ENTITY_TYPES.ITEM,
          subtype: 'plot',
          name: 'pomme',
          desc: [
            'Il existe de nombreux objets inhabituel dans le jeu. En voici un.'
          ],
          weight: 1,
          stackable: false,
          inventory: false,
          tag: 'pomme',
          events: {
            despawn: 'pomme_despawns'
          }
        }
      },
      sectors: {
        s00: {
          name: 'x',
          desc: ['x']
        }
      },
      rooms: {
        r00: {
          name: 'r00',
          desc: ['r00'],
          sector: 's00',
          nav: {
            n: {
              to: 'r01',
              lock: {
                locked: true,
                difficulty: 1
              }
            }
          },
          content: [
            {
              ref: 'acteur'
            },
            {
              ref: 'pomme'
            }
          ]
        },
        r01: {
          name: 'r01',
          desc: ['r01'],
          sector: 's00',
          nav: {
            s: {
              to: 'r00'
            }
          }
        }
      }
    }
    const aEventLog = []
    mud.events.on('entity.event.script', ({
      script,
      payload
    }) => {
      aEventLog.push({ script, entity: payload.self })
    })
    expect(aEventLog).toHaveLength(0)
    mud.setAssets(assets)
    const { acteur, pomme } = getEntitiesByTagInRoom(mud, ['acteur', 'pomme'], 'r00')
    mud.destroyEntity(pomme)
    expect(aEventLog).toHaveLength(1)
    expect(aEventLog[0]).toEqual({ script: 'pomme_despawns', entity: pomme })
    mud.destroyEntity(acteur)
    expect(aEventLog).toHaveLength(2)
    expect(aEventLog[1]).toEqual({ script: 'acteur_despawns', entity: acteur })
  })
})

describe('disturbed event', () => {
  it('should fire disturbed on chest event when actor takes apple from it and put the apple back in the container', () => {
    const aEventLog = []
    const mud = new MUDEngine()
    const assets = {
      blueprints: {
        acteur: {
          type: MUDEngine.CONST.ENTITY_TYPES.ACTOR,
          name: 'acteur',
          desc: [
            'Une créature.'
          ],
          weight: 1,
          inventory: true,
          tag: 'acteur'
        },
        pomme: {
          type: MUDEngine.CONST.ENTITY_TYPES.ITEM,
          subtype: 'plot',
          name: 'pomme',
          desc: [
            'Il existe de nombreux objets inhabituel dans le jeu. En voici un.'
          ],
          weight: 1,
          stackable: false,
          inventory: false,
          tag: 'pomme'
        },
        coffre: {
          type: MUDEngine.CONST.ENTITY_TYPES.PLACEABLE,
          name: 'coffre',
          desc: [
            'Le coffre de test.'
          ],
          weight: 1,
          stackable: false,
          inventory: true,
          tag: 'coffre',
          events: {
            disturbed: 'coffre_disturbed'
          }
        }
      },
      sectors: {
        s00: {
          name: 'x',
          desc: ['x']
        }
      },
      rooms: {
        r00: {
          name: 'r00',
          desc: ['r00'],
          sector: 's00',
          nav: {
            n: {
              to: 'r01',
              lock: {
                locked: true,
                difficulty: 1
              }
            }
          },
          content: [
            {
              ref: 'acteur'
            },
            {
              ref: 'coffre',
              content: [
                {
                  ref: 'pomme'
                }
              ]
            }
          ]
        },
        r01: {
          name: 'r01',
          desc: ['r01'],
          sector: 's00',
          nav: {
            s: {
              to: 'r00'
            }
          }
        }
      }
    }
    mud.setAssets(assets)
    mud.events.on('entity.event.script', ({
      script,
      payload
    }) => {
      aEventLog.push({ script, entity: payload.self, type: payload.type, item: payload.item, disturber: payload.interactor })
    })
    const { acteur, pomme, coffre } = getEntitiesByTagInRoom(mud, ['acteur', 'coffre', 'pomme'], 'r00')
    mud.moveEntity(pomme, acteur)
    expect(aEventLog).toHaveLength(1)
    expect(aEventLog[0]).toEqual({
      script: 'coffre_disturbed',
      entity: coffre,
      item: pomme,
      disturber: acteur,
      type: 'DISTURBANCE_ITEM_REMOVED'
    })
    mud.moveEntity(pomme, coffre)
    expect(aEventLog).toHaveLength(2)
    expect(aEventLog[1]).toEqual({
      script: 'coffre_disturbed',
      entity: coffre,
      item: pomme,
      disturber: acteur,
      type: 'DISTURBANCE_ITEM_ADDED'
    })
  })
})

describe('openFailure events', () => {
  let mud
  const aEventLog = []
  afterEach(() => {
    mud = null
    aEventLog.splice(0, aEventLog.length)
  })
  beforeEach(() => {
    mud = new MUDEngine()
    const assets = {
      blueprints: {
        acteur: {
          type: MUDEngine.CONST.ENTITY_TYPES.ACTOR,
          name: 'acteur',
          desc: [
            'Une créature.'
          ],
          weight: 1,
          inventory: true,
          tag: 'acteur'
        },
        sacferme: {
          type: MUDEngine.CONST.ENTITY_TYPES_ITEM,
          subtype: MUDEngine.CONST.ITEM_TYPES.CONTAINER,
          stackable: false,
          name: 'sac fermé',
          desc: ['Un sac fermé'],
          weight: 1,
          inventory: true,
          tag: 'sacferme',
          lock: {
            locked: true
          },
          events: {
            openFailure: 'container_unlock_failed'
          }
        },
        coffre: {
          type: MUDEngine.CONST.ENTITY_TYPES.PLACEABLE,
          name: 'coffre',
          desc: [
            'Le coffre de test.'
          ],
          weight: 1,
          stackable: false,
          inventory: true,
          lock: {
            locked: true
          },
          tag: 'coffre',
          events: {
            openFailure: 'container_unlock_failed'
          }
        }
      },
      sectors: {
        s00: {
          name: 'x',
          desc: ['x']
        }
      },
      rooms: {
        r00: {
          name: 'r00',
          desc: ['r00'],
          sector: 's00',
          nav: {
            n: {
              to: 'r01',
              lock: {
                locked: true,
                difficulty: 1
              }
            }
          },
          events: {
            openFailure: 'door_unlock_failed'
          },
          content: [
            {
              ref: 'acteur',
              content: [
                {
                  ref: 'sacferme'
                }
              ]
            },
            {
              ref: 'coffre'
            }
          ]
        },
        r01: {
          name: 'r01',
          desc: ['r01'],
          sector: 's00',
          nav: {
            s: {
              to: 'r00'
            }
          }
        }
      }
    }
    mud.setAssets(assets)
    mud.events.on('entity.event.script', ({
      script,
      payload
    }) => {
      aEventLog.push({ script, entity: payload.self, interactor: payload.interactor })
    })
  })
  it('should trigger openFailure event when trying to unlock door', () => {
    const { acteur } = getEntitiesByTagInRoom(mud, ['acteur'], 'r00')
    mud.events.on('entity.task.attempt', ({ success }) => {
      success(false)
    })
    mud.actionUnlock(acteur, 'n')
    expect(aEventLog).toHaveLength(1)
    expect(aEventLog[0]).toEqual({
      script: 'door_unlock_failed',
      entity: 'r00#n',
      interactor: acteur
    })
  })
  it('should trigger openFailure event when trying to unlock a locked bag', () => {
    const { acteur, sacferme } = getEntitiesByTagInRoom(mud, ['acteur', 'sacferme'], 'r00')
    mud.events.on('entity.task.attempt', ({ success }) => {
      success(false)
    })
    expect(mud.getEntity(sacferme).locked).toBeTruthy()
    const { success, outcome } = mud.actionUnlock(acteur, sacferme)
    expect(success).toBeFalsy()
    expect(outcome).toBe(MUDEngine.CONST.UNLOCK_OUTCOMES.KEY_INVALID)
    expect(mud.getEntity(sacferme).locked).toBeTruthy()
    expect(aEventLog).toHaveLength(1)
    expect(aEventLog[0]).toEqual({
      script: 'container_unlock_failed',
      entity: sacferme,
      interactor: acteur
    })
  })
  it('should trigger openFailure event when trying to unlock a locked chest', () => {
    const { acteur, coffre } = getEntitiesByTagInRoom(mud, ['acteur', 'coffre'], 'r00')
    mud.events.on('entity.task.attempt', ({ success }) => {
      success(false)
    })
    expect(mud.getEntity(coffre).locked).toBeTruthy()
    const { success, outcome } = mud.actionUnlock(acteur, coffre)
    expect(success).toBeFalsy()
    expect(outcome).toBe(MUDEngine.CONST.UNLOCK_OUTCOMES.KEY_INVALID)
    expect(mud.getEntity(coffre).locked).toBeTruthy()
    expect(aEventLog).toHaveLength(1)
    expect(aEventLog[0]).toEqual({
      script: 'container_unlock_failed',
      entity: coffre,
      interactor: acteur
    })
  })
})

describe('unlocked events', () => {
  let mud
  const aEventLog = []
  afterEach(() => {
    mud = null
    aEventLog.splice(0, aEventLog.length)
  })
  beforeEach(() => {
    mud = new MUDEngine()
    const assets = {
      blueprints: {
        acteur: {
          type: MUDEngine.CONST.ENTITY_TYPES.ACTOR,
          name: 'acteur',
          desc: [
            'Une créature.'
          ],
          weight: 1,
          inventory: true,
          tag: 'acteur'
        },
        sacferme: {
          type: MUDEngine.CONST.ENTITY_TYPES_ITEM,
          subtype: MUDEngine.CONST.ITEM_TYPES.CONTAINER,
          stackable: false,
          name: 'sac fermé',
          desc: ['Un sac fermé'],
          weight: 1,
          inventory: true,
          tag: 'sacferme',
          lock: {
            locked: true
          },
          events: {
            unlocked: 'container_unlocked'
          }
        },
        coffre: {
          type: MUDEngine.CONST.ENTITY_TYPES.PLACEABLE,
          name: 'coffre',
          desc: [
            'Le coffre de test.'
          ],
          weight: 1,
          stackable: false,
          inventory: true,
          lock: {
            locked: true
          },
          tag: 'coffre',
          events: {
            unlocked: 'container_unlocked'
          }
        }
      },
      sectors: {
        s00: {
          name: 'x',
          desc: ['x']
        }
      },
      rooms: {
        r00: {
          name: 'r00',
          desc: ['r00'],
          sector: 's00',
          nav: {
            n: {
              to: 'r01',
              lock: {
                locked: true,
                difficulty: 1
              }
            }
          },
          events: {
            unlocked: 'door_unlocked'
          },
          content: [
            {
              ref: 'acteur',
              content: [
                {
                  ref: 'sacferme'
                }
              ]
            },
            {
              ref: 'coffre'
            }
          ]
        },
        r01: {
          name: 'r01',
          desc: ['r01'],
          sector: 's00',
          nav: {
            s: {
              to: 'r00'
            }
          }
        }
      }
    }
    mud.setAssets(assets)
    mud.events.on('entity.event.script', ({
      script,
      payload
    }) => {
      aEventLog.push({ script, entity: payload.self, interactor: payload.interactor })
    })
    mud.events.on('entity.task.attempt', ({ success }) => {
      success(true)
    })
  })
  it('should trigger unlocked event when trying to unlock door', () => {
    const { acteur } = getEntitiesByTagInRoom(mud, ['acteur'], 'r00')
    mud.actionUnlock(acteur, 'n')
    expect(aEventLog).toHaveLength(1)
    expect(aEventLog[0]).toEqual({
      script: 'door_unlocked',
      entity: 'r00#n',
      interactor: acteur
    })
  })
  it('should trigger unlocked event when trying to unlock a locked bag', () => {
    const { acteur, sacferme } = getEntitiesByTagInRoom(mud, ['acteur', 'sacferme'], 'r00')
    expect(mud.getEntity(sacferme).locked).toBeTruthy()
    const { success, outcome } = mud.actionUnlock(acteur, sacferme)
    expect(success).toBeTruthy()
    expect(outcome).toBe(MUDEngine.CONST.UNLOCK_OUTCOMES.SUCCESS)
    expect(mud.getEntity(sacferme).locked).toBeFalsy()
    expect(aEventLog).toHaveLength(1)
    expect(aEventLog[0]).toEqual({
      script: 'container_unlocked',
      entity: sacferme,
      interactor: acteur
    })
  })
  it('should trigger unlocked event when trying to unlock a locked chest', () => {
    const { acteur, coffre } = getEntitiesByTagInRoom(mud, ['acteur', 'coffre'], 'r00')
    expect(mud.getEntity(coffre).locked).toBeTruthy()
    const { success, outcome } = mud.actionUnlock(acteur, coffre)
    expect(success).toBeTruthy()
    expect(outcome).toBe(MUDEngine.CONST.UNLOCK_OUTCOMES.SUCCESS)
    expect(mud.getEntity(coffre).locked).toBeFalsy()
    expect(aEventLog).toHaveLength(1)
    expect(aEventLog[0]).toEqual({
      script: 'container_unlocked',
      entity: coffre,
      interactor: acteur
    })
  })
})

describe('enter/exit events', () => {
  let mud
  afterEach(() => {
    mud = null
  })
  beforeEach(() => {
    mud = new MUDEngine()
    const assets = {
      blueprints: {
        acteur: {
          type: MUDEngine.CONST.ENTITY_TYPES.ACTOR,
          name: 'acteur',
          desc: [
            'Une créature.'
          ],
          weight: 1,
          inventory: true,
          tag: 'acteur'
        }
      },
      sectors: {
        s00: {
          name: 'x',
          desc: ['x']
        }
      },
      rooms: {
        r00: {
          name: 'r00',
          desc: ['r00'],
          sector: 's00',
          nav: {
            n: {
              to: 'r01'
            }
          },
          content: [
            {
              ref: 'acteur'
            }
          ],
          events: {
            enter: 'someone_enters_r00',
            exit: 'someone_exits_r00'
          }
        },
        r01: {
          name: 'r01',
          desc: ['r01'],
          sector: 's00',
          nav: {
            s: {
              to: 'r00'
            }
          },
          events: {
            enter: 'someone_enters_r01',
            exit: 'someone_exits_r01'
          }
        }
      }
    }
    mud.setAssets(assets)
  })
  it('should trigger exit event when actor is leaving room', () => {
    const oEventLog = {}
    let nTIME = 0
    mud.events.on('entity.event.script', ({
      script,
      payload
    }) => {
      oEventLog[script] = { time: ++nTIME, actor: payload.interactor, room: payload.self, from: payload.from, to: payload.to }
    })
    const { acteur } = getEntitiesByTagInRoom(mud, ['acteur'], 'r00')
    mud.actionPassThruExit(acteur, 'n')

    // Quand une entité spawn, elle déclenche 'enter' dans la pièce où elle apparait
    expect(oEventLog.someone_enters_r00).toEqual({
      time: 1,
      actor: acteur,
      room: 'r00'
    })

    // Bougeage = sortie -> entrée
    // D'abord la sortie
    expect(oEventLog.someone_exits_r00).toEqual({
      time: 2,
      actor: acteur,
      room: 'r00',
      to: 'r01'
    })

    // Puis l'entrée
    expect(oEventLog.someone_enters_r01).toEqual({
      time: 3,
      actor: acteur,
      room: 'r01',
      from: 'r00',
      to: undefined
    })
  })
})

describe('acquired/dropped events', () => {
  it('should fire acquired on apple event when actor takes it from chest', () => {
    const aEventLog = []
    const mud = new MUDEngine()
    const assets = {
      blueprints: {
        acteur: {
          type: MUDEngine.CONST.ENTITY_TYPES.ACTOR,
          name: 'acteur',
          desc: [
            'Une créature.'
          ],
          weight: 1,
          inventory: true,
          tag: 'acteur'
        },
        pomme: {
          type: MUDEngine.CONST.ENTITY_TYPES.ITEM,
          subtype: 'plot',
          name: 'pomme',
          desc: [
            'Il existe de nombreux objets inhabituel dans le jeu. En voici un.'
          ],
          weight: 1,
          stackable: false,
          inventory: false,
          tag: 'pomme',
          events: {
            acquired: 'apple_acquired'
          }
        },
        coffre: {
          type: MUDEngine.CONST.ENTITY_TYPES.PLACEABLE,
          name: 'coffre',
          desc: [
            'Le coffre de test.'
          ],
          weight: 1,
          stackable: false,
          inventory: true,
          tag: 'coffre'
        }
      },
      sectors: {
        s00: {
          name: 'x',
          desc: ['x']
        }
      },
      rooms: {
        r00: {
          name: 'r00',
          desc: ['r00'],
          sector: 's00',
          nav: {
            n: {
              to: 'r01'
            }
          },
          content: [
            {
              ref: 'acteur'
            },
            {
              ref: 'coffre',
              content: [
                {
                  ref: 'pomme'
                }
              ]
            }
          ]
        },
        r01: {
          name: 'r01',
          desc: ['r01'],
          sector: 's00',
          nav: {
            s: {
              to: 'r00'
            }
          }
        }
      }
    }
    mud.setAssets(assets)
    mud.events.on('entity.event.script', ({
      script,
      payload
    }) => {
      aEventLog.push({ script, entity: payload.self, interactor: payload.interactor })
    })
    const { acteur, pomme, coffre } = getEntitiesByTagInRoom(mud, ['acteur', 'coffre', 'pomme'], 'r00')
    mud.moveEntity(pomme, acteur)
    expect(aEventLog).toHaveLength(1)
    expect(aEventLog[0]).toEqual({
      script: 'apple_acquired',
      entity: pomme,
      interactor: acteur
    })
  })

  it('should fire dropped on apple event when actor put apple in chest', () => {
    const aEventLog = []
    const mud = new MUDEngine()
    const assets = {
      blueprints: {
        acteur: {
          type: MUDEngine.CONST.ENTITY_TYPES.ACTOR,
          name: 'acteur',
          desc: [
            'Une créature.'
          ],
          weight: 1,
          inventory: true,
          tag: 'acteur'
        },
        pomme: {
          type: MUDEngine.CONST.ENTITY_TYPES.ITEM,
          subtype: 'plot',
          name: 'pomme',
          desc: [
            'Il existe de nombreux objets inhabituel dans le jeu. En voici un.'
          ],
          weight: 1,
          stackable: false,
          inventory: false,
          tag: 'pomme',
          events: {
            dropped: 'apple_dropped'
          }
        },
        coffre: {
          type: MUDEngine.CONST.ENTITY_TYPES.PLACEABLE,
          name: 'coffre',
          desc: [
            'Le coffre de test.'
          ],
          weight: 1,
          stackable: false,
          inventory: true,
          tag: 'coffre'
        }
      },
      sectors: {
        s00: {
          name: 'x',
          desc: ['x']
        }
      },
      rooms: {
        r00: {
          name: 'r00',
          desc: ['r00'],
          sector: 's00',
          nav: {
            n: {
              to: 'r01'
            }
          },
          content: [
            {
              ref: 'acteur',
              content: [
                {
                  ref: 'pomme'
                }
              ]
            },
            {
              ref: 'coffre'
            }
          ]
        },
        r01: {
          name: 'r01',
          desc: ['r01'],
          sector: 's00',
          nav: {
            s: {
              to: 'r00'
            }
          }
        }
      }
    }
    mud.setAssets(assets)
    mud.events.on('entity.event.script', ({
      script,
      payload
    }) => {
      aEventLog.push({ script, entity: payload.self, interactor: payload.interactor })
    })
    const { acteur, pomme, coffre } = getEntitiesByTagInRoom(mud, ['acteur', 'coffre', 'pomme'], 'r00')
    mud.moveEntity(pomme, coffre)
    expect(aEventLog).toHaveLength(1)
    expect(aEventLog[0]).toEqual({
      script: 'apple_dropped',
      entity: pomme,
      interactor: acteur
    })
  })
})

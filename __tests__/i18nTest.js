const I18n = require('../libs/i18n-manager')

describe('etude de contexte', function () {
  it('test basic', async function () {
    const i18n = new I18n()
    await i18n.init({
      fr: {
        test: 'la chaîne complète'
      }
    })
    expect(i18n.render('test')).toBe('la chaîne complète')
  })
  it('test contexte', async function () {
    const i18n = new I18n()
    await i18n.init({
      fr: {
        test: 'la chaîne complète',
        test_truc: 'la chaîne complète du truc'
      }
    })
    expect(i18n.render('test', { context: 'truc' })).toBe('la chaîne complète du truc')
  })
  it('mega nesting', async function () {
    const i18n = new I18n()
    await i18n.init({
      fr: {
        test: 'la chaîne complète $t(owner)',
        owner: 'avec un titre : {{ titre }}'
      }
    })
    expect(i18n.render('test', { titre: 'méga' })).toBe('la chaîne complète avec un titre : méga')
  })
})

const SimpleCache = require('../libs/simple-cache')

describe('cache system', function () {
  it('cache vide au début', function () {
    const r = new SimpleCache()
    expect(r._CACHE).toEqual({})
  })
  it('cache storage', function () {
    const r = new SimpleCache()
    r.storeValue('test', { x: 1 })
    expect(r._CACHE).toEqual({ test: { x: 1 }})
    expect(r.getValue('test')).toEqual({ x: 1 })
  })
})

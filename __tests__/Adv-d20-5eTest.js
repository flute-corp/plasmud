const Rules = require('../libs/adv-d20-5e/Rules')
const CONST = require('../libs/adv-d20-5e/consts')

describe('Adv-d20-5e', function () {
  it('instanciation sans erreur', function () {
    const r = new Rules()
    expect(r).toBeDefined()
  })

  it('creation goblin', function () {
    const r = new Rules()
    const oGob1 = r.createEntity('c-goblin')
    expect(oGob1).toBeDefined()
    expect(oGob1.entityType).toBe(CONST.ENTITY_TYPE_ACTOR)
    const oWeapon = oGob1.equip[CONST.EQUIPMENT_SLOT_WEAPON_MELEE]
    expect(oWeapon).toBeDefined()
    expect(oWeapon.entityType).toBe(CONST.ENTITY_TYPE_ITEM)
    expect(oWeapon.itemBaseType).toBe(CONST.ITEM_BASE_TYPE_WEAPON)
    expect(r.isProficient(oGob1, oGob1.equip[CONST.EQUIPMENT_SLOT_WEAPON_MELEE])).toBeTruthy()
    expect(r.isProficient(oGob1, oGob1.equip[CONST.EQUIPMENT_SLOT_CHEST])).toBeTruthy()
    const oMorningStar = r.createEntity('wpn-morningstar')
    const oDagger = r.createEntity('wpn-dagger')
    expect(r.isProficient(oGob1, oMorningStar)).toBeFalsy()
    expect(r.isProficient(oGob1, oDagger)).toBeTruthy()
  })

  it('equipper une autre arme', function () {
    const r = new Rules()
    const oGob1 = r.createEntity('c-goblin')
    const oDagger = r.createEntity('wpn-dagger')
    const oOtherWeapon = r.equipItem(oGob1, oDagger, CONST.EQUIPMENT_SLOT_WEAPON_MELEE)
    const oCheckWeapon = r.getEquippedItem(oGob1, CONST.EQUIPMENT_SLOT_WEAPON_MELEE)
    expect(oCheckWeapon.itemSubType).toBe('WEAPON_TYPE_DAGGER')
    expect(oOtherWeapon.itemSubType).toBe('WEAPON_TYPE_CLUB')
  })

  it('tester get ability', function () {
    const r = new Rules()
    const oGob = r.createEntity('c-goblin')
    // 15 14 14 12 10 9
    expect(r.getAbility(oGob, CONST.ABILITY_STRENGTH)).toBe(15)
    expect(r.getAbility(oGob, CONST.ABILITY_DEXTERITY)).toBe(14)
    expect(r.getAbility(oGob, CONST.ABILITY_CONSTITUTION)).toBe(14)
    expect(r.getAbility(oGob, CONST.ABILITY_INTELLIGENCE)).toBe(12)
    expect(r.getAbility(oGob, CONST.ABILITY_WISDOM)).toBe(10)
    expect(r.getAbility(oGob, CONST.ABILITY_CHARISMA)).toBe(9)
  })

  it('tester bonus ability', function () {
    const r = new Rules()
    const oGob = r.createEntity('c-goblin')
    // associer un effet bonus
    const eBonusStr = r.createEffect(CONST.EFFECT_ABILITY_MODIFIER, { ability: CONST.ABILITY_STRENGTH, value: 4 })
    // 15 14 14 12 10 9
    expect(r.getAbility(oGob, CONST.ABILITY_STRENGTH)).toBe(15)
    r.applyEffect(eBonusStr, oGob, oGob, 10)
    expect(r.getAbility(oGob, CONST.ABILITY_STRENGTH)).toBe(19)
  })

  it('tester bonus ability - equippement et effets', function () {
    const r = new Rules()
    const oGob = r.createEntity('c-goblin')
    // creer des gant de force
    const oGloves = r.createEntity('eqp-gloves')
    const ipStr = r.createItemProperty(CONST.ITEM_PROPERTY_ABILITY_MODIFIER, { ability: CONST.ABILITY_STRENGTH, value: 2 })
    r.addItemProperty(oGloves, ipStr)
    r.equipItem(oGob, oGloves)
    expect(r.getEquippedItem(oGob, CONST.EQUIPMENT_SLOT_ARMS)).toEqual(oGloves)
    // appliquer un effet bonus au gobelin
    const eBonusStr = r.createEffect(CONST.EFFECT_ABILITY_MODIFIER, { ability: CONST.ABILITY_STRENGTH, value: 4 })
    r.applyEffect(eBonusStr, oGob, oGob, 10)
    // base stats : 15 14 14 12 10 9
    expect(r.getAbility(oGob, CONST.ABILITY_STRENGTH)).toBe(21)
  })

  it('getAC', function () {
    const r = new Rules()
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    expect(r.getAC(oGob1)).toBe(15)
    expect(r.getAC(oGob1, oGob2)).toBe(15)
  })

  it('getAC against alignment', function () {
    const r = new Rules()
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const oGob3 = r.createEntity('c-goblin')

    const eACAgainstEvil = r.createEffect(CONST.EFFECT_AC_MODIFIER, {
      value: 3,
      against: {
        morality: CONST.ALIGNMENT_MORALITY_EVIL
      }
    })
    expect(oGob2.morality).toBe(-1)
    expect(oGob2.entropy).toBe(0)
    expect(r.getAlignment(oGob2)).toBe(CONST.ALIGNMENT_NEUTRAL_EVIL)
    r.initAlignment(oGob3, CONST.ALIGNMENT_LAWFUL_GOOD)
    r.applyEffect(eACAgainstEvil, oGob1, oGob1, 10)
    expect(r.getAC(oGob1)).toBe(15)
    expect(r.getAC(oGob1, oGob2)).toBe(18)
    expect(r.getAC(oGob1, oGob3)).toBe(15)
  })

  it('getAC with heavy armor', function () {
    const r = new Rules()
    const oGob1 = r.createEntity('c-goblin')
    const oArmRing = r.createEntity('arm-ring-mail')
    r.equipItem(oGob1, oArmRing)
    expect(r.getAC(oGob1)).toBe(14 + 2)
  })

  it('compute attack roll I', function () {
    const r = new Rules()
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    r._dice.debug(true, 0.5)
    const ar = r.computeAttackRoll(oGob1, oGob2, { weapon: r.getEquippedItem(oGob1, CONST.EQUIPMENT_SLOT_WEAPON_MELEE) })
    expect(ar).toBeDefined()
    expect(ar.bonus).toBe(4)
    expect(ar.ability).toBe(CONST.ABILITY_STRENGTH)
    expect(ar.advantage).toBeFalsy()
    expect(ar.disadvantage).toBeFalsy()
  })

  it('compute attack roll II unarmed', function () {
    const r = new Rules()
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    r._dice.debug(true, 0.5)
    r.unequipItem(oGob1, CONST.EQUIPMENT_SLOT_WEAPON_MELEE)
    const ar = r.computeAttackRoll(oGob1, oGob2)
    expect(ar).toBeDefined()
    expect(ar.bonus).toBe(4)
    expect(ar.ability).toBe(CONST.ABILITY_DEXTERITY)
    expect(ar.advantage).toBeFalsy()
    expect(ar.disadvantage).toBeFalsy()
  })

  it('compute attack roll III magic club (item prop)', function () {
    const r = new Rules()
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const oClub = r.getEquippedItem(oGob1, CONST.EQUIPMENT_SLOT_WEAPON_MELEE)
    const ipEnh = r.createItemProperty(CONST.ITEM_PROPERTY_ENHANCEMENT, { value: 1 })
    r.addItemProperty(oClub, ipEnh)
    r._dice.debug(true, 0.5)
    const ar = r.computeAttackRoll(oGob1, oGob2, { weapon: r.getEquippedItem(oGob1, CONST.EQUIPMENT_SLOT_WEAPON_MELEE) })
    expect(r.getItemProperties(oClub)).toHaveLength(1)
    expect(r.getItemProperties(oClub)[0].property).toBe(CONST.ITEM_PROPERTY_ENHANCEMENT)
    expect(r.getItemProperties(oClub)[0].value).toBe(1)
    expect(r.aggregateModifiers(oClub, CONST.ITEM_PROPERTY_ENHANCEMENT).sum).toBe(1)
    expect(ar.bonus).toBe(5)
    expect(ar.advantage).toBeFalsy()
    expect(ar.disadvantage).toBeFalsy()
  })

  it('compute attack roll III armor no proficiency', function () {
    const r = new Rules()
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const oHeavyArmor = r.createEntity('arm-ring-mail')
    r.equipItem(oGob1, oHeavyArmor)
    r._dice.debug(true, 0.5)
    const ar = r.computeAttackRoll(oGob1, oGob2, { weapon: r.getEquippedItem(oGob1, CONST.EQUIPMENT_SLOT_WEAPON_MELEE) })
    const ad = r.getRollCircumstances(oGob1, oGob2, r.CONST.ROLL_TYPE_ATTACK, ar.ability)
    expect(ad).toEqual({ advantages: [], disadvantages: ['DIS_RULE_UNPROF_ARMOR'] })
    expect(r.isProficient(oGob1, oHeavyArmor)).toBeFalsy()
    expect(ar.bonus).toBe(4)
    expect(ar.advantage).toBeFalsy()
    expect(ar.disadvantage).toBeTruthy() // not proficient with heavy armor -> disadvantage on attack rolls using strength or dex
  })

  it('compute attack roll III weapon no proficiency', function () {
    const r = new Rules()
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const oMorningStar = r.createEntity('wpn-morningstar')
    r.equipItem(oGob1, oMorningStar)
    r._dice.debug(true, 0.5)
    const ar = r.computeAttackRoll(oGob1, oGob2, { weapon: r.getEquippedItem(oGob1, CONST.EQUIPMENT_SLOT_WEAPON_MELEE) })
    expect(r.isProficient(oGob1, oMorningStar)).toBeFalsy()
    expect(ar.roll).toBe(11)
    expect(ar.bonus).toBe(2)
    expect(ar.ac).toBe(15)
    expect(ar.hit).toBeFalsy()
    expect(ar.advantage).toBeFalsy()
    expect(ar.disadvantage).toBeFalsy()
  })

  it('compute damages I', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const oWeapon = r.getEquippedItem(oGob1, CONST.EQUIPMENT_SLOT_WEAPON_MELEE)
    const ar = {
      roll: 5,
      advantage: false,
      disadvantage: false,
      attack: 4,
      ability: CONST.ABILITY_STRENGTH,
      critical: false,
      miss: false,
      weapon: oWeapon,
      ammo: null
    }
    const dr = r.computeAttackDamage(oGob1, oGob2, ar)
    expect(dr.damages).toEqual([{ amount: 5, type: 'DAMAGE_TYPE_CRUSHING' }])
  })
})

describe('adv-d20-5e mephit-magma', function () {
  it('instanciation du mephit', function () {
    const r = new Rules()
    const oMephit = r.createEntity('c-magma-mephit')
    const oClaw = r.getEquippedItem(oMephit, CONST.EQUIPMENT_SLOT_CWEAPON_R)
    expect(oClaw.itemBaseType).toBe(CONST.ITEM_BASE_TYPE_CREATUREWEAPON)
  })
  it('le mephit de magma fait des dégats de feu', function () {
    const r = new Rules()
    const oMephit = r.createEntity('c-magma-mephit')
    const oGob = r.createEntity('c-goblin')
    const oClaw = r.getEquippedItem(oMephit, CONST.EQUIPMENT_SLOT_CWEAPON_R)
    const ar = r.computeAttackRoll(oMephit, oGob, { weapon: oClaw })
    const dr = r.computeAttackDamage(oMephit, oGob, ar)
    expect(dr.damages.findIndex(d => d.type === r.CONST.DAMAGE_TYPE_FIRE)).toBeGreaterThanOrEqual(0)
  })
  it('le sort du mephite de magma', function () {
    const r = new Rules()
    r.dice.debug(true, 0.5)
    const oMephit = r.createEntity('c-magma-mephit')
    const oGob = r.createEntity('c-goblin')
    r.events.on('request.distance', event => {
      event.distance = 10
    })
    r.events.on('request.nearest-entities', event => {
      event.entities = [oGob]
    })
    let bGobBurned = false
    r.events.on('effect.applied', ({ effect }) => {
      if (
        effect.source === oMephit &&
        effect.target === oGob &&
        effect.tag === r.CONST.EFFECT_DAMAGE &&
        effect.type === r.CONST.DAMAGE_TYPE_FIRE
      ) {
        bGobBurned = true
      }
    })
    expect(oGob.hp).toBe(12)
    r.castSpell(oMephit, 'SPELL_FIRE_BREATH', 0, r.CONST.ABILITY_DEXTERITY, oGob)
    expect(oGob.hp).not.toBe(12)
    expect(bGobBurned).toBeTruthy()
  })
})

describe('adv-d20-5e advantage system', function () {
  it('no advantages', function () {
    const r = new Rules()
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    r.computeAttackRoll(oGob1, oGob2, { weapon: r.getEquippedItem(oGob1, r.CONST.EQUIPMENT_SLOT_WEAPON_MELEE) })
    const aAdv = r.getRollCircumstances(oGob1, oGob2, r.CONST.ROLL_TYPE_ATTACK, r.CONST.ABILITY_STRENGTH).advantages
    expect(aAdv).toHaveLength(0)
  })
  it('invisibility grants advantage', function () {
    const r = new Rules()
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const eInvis = r.createEffect(r.CONST.EFFECT_INVISIBILITY)
    r.applyEffect(eInvis, oGob1, oGob1, 10)
    r.computeAttackRoll(oGob1, oGob2, { weapon: r.getEquippedItem(oGob1, r.CONST.EQUIPMENT_SLOT_WEAPON_MELEE) })
    const aAdv = r.getRollCircumstances(oGob1, oGob2, r.CONST.ROLL_TYPE_ATTACK, r.CONST.ABILITY_STRENGTH).advantages
    expect(aAdv).toHaveLength(1)
    expect(aAdv).toEqual(['ADV_RULE_SURPRISE_ATTACK'])
    const aAdv2 = r.getRollCircumstances(oGob1, oGob2, r.CONST.ROLL_TYPE_SAVE, r.CONST.ABILITY_STRENGTH).advantages
    expect(aAdv2).toHaveLength(0)
  })
  it('invisibility grants advantage canceled by target see invis', function () {
    const r = new Rules()
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const eInvis = r.createEffect(r.CONST.EFFECT_INVISIBILITY)
    const eSeeInvis = r.createEffect(r.CONST.EFFECT_SEE_INVISIBILITY)
    r.applyEffect(eInvis, oGob1, oGob1, 10)
    r.applyEffect(eSeeInvis, oGob2, oGob2, 10)
    r.computeAttackRoll(oGob1, oGob2, { weapon: r.getEquippedItem(oGob1, r.CONST.EQUIPMENT_SLOT_WEAPON_MELEE) })
    const aAdv = r.getRollCircumstances(oGob1, oGob2, r.CONST.ROLL_TYPE_ATTACK, r.CONST.ABILITY_STRENGTH).advantages
    expect(aAdv).toHaveLength(0)
  })
})

describe('attack and damages', function () {
  it('apply damage correctly', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const oWeapon = r.getEquippedItem(oGob1, r.CONST.EQUIPMENT_SLOT_WEAPON_MELEE)
    const ar = {
      roll: 5,
      advantage: false,
      disadvantage: false,
      attack: 4,
      ability: CONST.ABILITY_STRENGTH,
      critical: false,
      miss: false,
      weapon: oWeapon,
      ammo: null
    }
    expect(r.computeAttackDamage(oGob1, oGob2, ar).damages[0].amount).toBe(5)
  })

  it('damage effect control', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const eDam = r.createEffect(r.CONST.EFFECT_DAMAGE, {
      value: 5,
      type: r.CONST.DAMAGE_TYPE_CRUSHING,
      weapon: r.getEquippedItem(oGob1, r.CONST.EQUIPMENT_SLOT_WEAPON_MELEE)
    })
    expect(oGob2.hp).toBe(12)
    r.applyEffect(eDam, oGob2, oGob1)
    expect(oGob2.hp).toBe(7)
  })

  it('damage effect resistance 1', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const eDam = r.createEffect(r.CONST.EFFECT_DAMAGE, {
      value: 5,
      type: r.CONST.DAMAGE_TYPE_CRUSHING,
      weapon: r.getEquippedItem(oGob1, r.CONST.EQUIPMENT_SLOT_WEAPON_MELEE)
    })
    const eRes = r.createEffect(
      r.CONST.EFFECT_DAMAGE_RESISTANCE,
      {
        value: 2,
        types: [r.CONST.DAMAGE_TYPE_CRUSHING, r.CONST.DAMAGE_TYPE_SLASHING, r.CONST.DAMAGE_TYPE_PIERCING]
      }
    )
    expect(oGob2.hp).toBe(12)
    r.applyEffect(eRes, oGob2, oGob2, 10)
    r.applyEffect(eDam, oGob2, oGob1)
    expect(oGob2.hp).toBe(9)
  })

  it('damage effect resistance 2 - buffer', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const eDam = r.createEffect(r.CONST.EFFECT_DAMAGE, {
      value: 5,
      type: r.CONST.DAMAGE_TYPE_CRUSHING,
      weapon: r.getEquippedItem(oGob1, r.CONST.EQUIPMENT_SLOT_WEAPON_MELEE)
    })
    const eRes = r.createEffect(
      r.CONST.EFFECT_DAMAGE_RESISTANCE,
      {
        value: 2,
        types: [r.CONST.DAMAGE_TYPE_CRUSHING, r.CONST.DAMAGE_TYPE_SLASHING, r.CONST.DAMAGE_TYPE_PIERCING],
        buffer: 10
      }
    )
    expect(eRes.buffer).toBe(10)
    expect(oGob2.hp).toBe(12)
    r.applyEffect(eRes, oGob2, oGob2, 10)
    r.applyEffect(eDam, oGob2, oGob1)
    expect(oGob2.hp).toBe(9)
    expect(eRes.buffer).toBe(8)
  })

  it('damage effect resistance 2 - insufficient buffer', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const eDam = r.createEffect(r.CONST.EFFECT_DAMAGE, {
      value: 5,
      type: r.CONST.DAMAGE_TYPE_CRUSHING,
      weapon: r.getEquippedItem(oGob1, r.CONST.EQUIPMENT_SLOT_WEAPON_MELEE)
    })
    const eRes = r.createEffect(
      r.CONST.EFFECT_DAMAGE_RESISTANCE,
      {
        value: 3,
        types: [r.CONST.DAMAGE_TYPE_CRUSHING, r.CONST.DAMAGE_TYPE_SLASHING, r.CONST.DAMAGE_TYPE_PIERCING],
        buffer: 2
      }
    )
    expect(eRes.buffer).toBe(2)
    expect(oGob2.hp).toBe(12)
    r.applyEffect(eRes, oGob2, oGob2, 10)
    r.applyEffect(eDam, oGob2, oGob1)
    expect(oGob2.hp).toBe(10) // 5 - (3)
    expect(eRes.buffer).toBe(0)
  })

  it('damage effect immunity 50%', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const eDam = r.createEffect(r.CONST.EFFECT_DAMAGE, {
      value: 6,
      type: r.CONST.DAMAGE_TYPE_CRUSHING,
      weapon: r.getEquippedItem(oGob1, r.CONST.EQUIPMENT_SLOT_WEAPON_MELEE)
    })
    const eImm = r.createEffect(
      r.CONST.EFFECT_DAMAGE_IMMUNITY,
      {
        value: 0.5,
        types: [r.CONST.DAMAGE_TYPE_CRUSHING, r.CONST.DAMAGE_TYPE_SLASHING, r.CONST.DAMAGE_TYPE_PIERCING]
      }
    )
    r.applyEffect(eImm, oGob2, oGob2, 10)
    r.applyEffect(eDam, oGob2, oGob1)
    expect(oGob2.hp).toBe(9) // damage = 6 * (1 - 0.5) = 3
  })

  it('damage effect immunity 25%', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const eDam = r.createEffect(r.CONST.EFFECT_DAMAGE, {
      value: 8,
      type: r.CONST.DAMAGE_TYPE_CRUSHING,
      weapon: r.getEquippedItem(oGob1, r.CONST.EQUIPMENT_SLOT_WEAPON_MELEE)
    })
    const eImm = r.createEffect(
      r.CONST.EFFECT_DAMAGE_IMMUNITY,
      {
        value: 0.25,
        types: [r.CONST.DAMAGE_TYPE_CRUSHING, r.CONST.DAMAGE_TYPE_SLASHING, r.CONST.DAMAGE_TYPE_PIERCING]
      }
    )
    r.applyEffect(eImm, oGob2, oGob2, 10)
    r.applyEffect(eDam, oGob2, oGob1)
    expect(oGob2.hp).toBe(6) // damage = 8 - (8 * 0.25) -> 8 - 2 -> 6 // 12 - 6 > 6
  })

  it('damage effect immunity 75%', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const eDam = r.createEffect(r.CONST.EFFECT_DAMAGE, {
      value: 8,
      type: r.CONST.DAMAGE_TYPE_CRUSHING,
      weapon: r.getEquippedItem(oGob1, r.CONST.EQUIPMENT_SLOT_WEAPON_MELEE)
    })
    const eImm = r.createEffect(
      r.CONST.EFFECT_DAMAGE_IMMUNITY,
      {
        value: 0.75,
        types: [r.CONST.DAMAGE_TYPE_CRUSHING, r.CONST.DAMAGE_TYPE_SLASHING, r.CONST.DAMAGE_TYPE_PIERCING]
      }
    )
    r.applyEffect(eImm, oGob2, oGob2, 10)
    r.applyEffect(eDam, oGob2, oGob1)
    expect(oGob2.hp).toBe(10) // damage = 8 - (8 * 0.75) -> 8 - 6 -> 2 // 12 - 2 > 10
  })

  it('damage effect immunity 50% + resistance', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const eDam = r.createEffect(r.CONST.EFFECT_DAMAGE, {
      value: 8,
      type: r.CONST.DAMAGE_TYPE_CRUSHING,
      weapon: r.getEquippedItem(oGob1, r.CONST.EQUIPMENT_SLOT_WEAPON_MELEE)
    })
    const eImm = r.createEffect(
      r.CONST.EFFECT_DAMAGE_IMMUNITY,
      {
        value: 0.5,
        types: [r.CONST.DAMAGE_TYPE_CRUSHING, r.CONST.DAMAGE_TYPE_SLASHING, r.CONST.DAMAGE_TYPE_PIERCING]
      }
    )
    const eRes = r.createEffect(
      r.CONST.EFFECT_DAMAGE_RESISTANCE,
      {
        value: 2,
        types: [r.CONST.DAMAGE_TYPE_CRUSHING, r.CONST.DAMAGE_TYPE_SLASHING, r.CONST.DAMAGE_TYPE_PIERCING]
      }
    ) // 8 - 2 -> 6 / 6 * 0.25 -> 3 : les dégat ne sont plus que de 3
    expect(oGob2.hp).toBe(12)
    r.applyEffect(eRes, oGob2, oGob2, 10)
    r.applyEffect(eImm, oGob2, oGob2, 10)
    r.applyEffect(eDam, oGob2, oGob1)
    expect(oGob2.effects[0]).toBe(eRes)
    expect(eRes.duration).toBe(9)
    expect(eRes.expired).toBeFalsy()
    expect(r.runScript('d20-lib-get-damage-resistance', { subject: oGob2, rules: r, type: r.CONST.DAMAGE_TYPE_CRUSHING }))
      .toBe(2)
    // damage ini : 8
    // immunity : 0.5
    // resist : 2
    // remainAfterResist = 8 - 2 = 6
    // remainAfterImmunity = 6 * 0.5 = 3
    // damage final = 3
    // hp left = 12 - 3 = 9
    expect(oGob2.hp).toBe(9)
  })

  it('damage multiple resistance non stackable', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const eImm1 = r.createEffect(
      r.CONST.EFFECT_DAMAGE_IMMUNITY,
      {
        value: 0.1,
        types: [r.CONST.DAMAGE_TYPE_CRUSHING, r.CONST.DAMAGE_TYPE_SLASHING, r.CONST.DAMAGE_TYPE_PIERCING]
      }
    )
    const eImm2 = r.createEffect(
      r.CONST.EFFECT_DAMAGE_IMMUNITY,
      {
        value: 0.25,
        types: [r.CONST.DAMAGE_TYPE_CRUSHING, r.CONST.DAMAGE_TYPE_SLASHING, r.CONST.DAMAGE_TYPE_PIERCING]
      }
    )
    r.applyEffect(eImm1, oGob2, oGob2, 10)
    expect(r.runScript('d20-lib-get-damage-immunity', { subject: oGob2, type: r.CONST.DAMAGE_TYPE_CRUSHING }))
      .toBe(0.1)
    r.applyEffect(eImm2, oGob2, oGob2, 10)
    expect(r.runScript('d20-lib-get-damage-immunity', { subject: oGob2, type: r.CONST.DAMAGE_TYPE_CRUSHING }))
      .toBe(0.25)
    expect(r.runScript('d20-lib-get-damage-immunity', { subject: oGob2, type: r.CONST.DAMAGE_TYPE_FIRE }))
      .toBe(0)
  })

  it('damage vulnerability test', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const eVuln = r.createEffect(
      r.CONST.EFFECT_DAMAGE_IMMUNITY,
      {
        value: -1, // +100% damage
        types: [r.CONST.DAMAGE_TYPE_CRUSHING, r.CONST.DAMAGE_TYPE_SLASHING, r.CONST.DAMAGE_TYPE_PIERCING]
      }
    )
    const eDam = r.createEffect(r.CONST.EFFECT_DAMAGE, {
      value: 5,
      type: r.CONST.DAMAGE_TYPE_CRUSHING,
      weapon: r.getEquippedItem(oGob1, r.CONST.EQUIPMENT_SLOT_WEAPON_MELEE)
    })
    r.applyEffect(eVuln, oGob2, oGob2, 10)
    r.applyEffect(eDam, oGob2, oGob1)
    const getUnimmunizedDamageAmount = (amount, immunity) => {
      return Math.floor(amount * Math.max(0, 1 - immunity))
    }
    const getUnresistedDamageAmount = (amount, resistance) => {
      return resistance < amount
        ? amount - resistance
        : 0
    }
    const comp = (d, dr, di, dv) => {
      return getUnimmunizedDamageAmount(
        getUnresistedDamageAmount(d, dr),
        di - dv
      )
    }

    expect(getUnimmunizedDamageAmount(100, 0.25)).toBe(75)
    expect(getUnimmunizedDamageAmount(100, 0)).toBe(100)
    expect(getUnimmunizedDamageAmount(100, -0.1)).toBe(110)
    expect(getUnimmunizedDamageAmount(100, -0.5)).toBe(150)
    expect(getUnimmunizedDamageAmount(100, -1)).toBe(200)
    expect(comp(100, 0, 0, 0)).toBe(100)
    expect(comp(100, 10, 0, 0)).toBe(90)
    expect(comp(100, 100, 0, 0)).toBe(0)
    expect(comp(100, 0, 0.5, 0)).toBe(50)
    expect(comp(100, 0, 0.75, 0)).toBe(25)
    expect(comp(100, 0, 1, 0)).toBe(0)
    expect(comp(100, 0, -0.1, 0)).toBe(110)
    expect(comp(100, 40, -0.2, 0)).toBe(60 * 1.2)
    expect(r.aggregateModifiers(oGob2, [r.CONST.EFFECT_DAMAGE_IMMUNITY]).min).toBe(-1)
    expect(eDam.info.vulnerability).toBe(1)
    expect(eDam.info.damage.initial).toBe(5)
    expect(eDam.info.damage.amplified).toBe(10)
    expect(eDam.info.damage.final).toBe(10)
    expect(oGob2.hp).toBe(2)
  })

  // TODO tester multiple resistance / immunité
  // TODO tester vulnerabilité
  // TODO tester vulnerabilité + résistance
  // TODO tester mix item property / effects
})

describe('distances', function () {
  it('commands de calcul de distance détecte à portée de mélée', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    let nDistanceGobs = 0
    r.events.on('request.distance', event => {
      event.distance = nDistanceGobs
    })
    const oWeapon = r.getEquippedItem(oGob1, CONST.EQUIPMENT_SLOT_WEAPON_MELEE)
    nDistanceGobs = 30
    expect(r.runScript('d20-ad-target-at-melee-range', { subject: oGob1, target: oGob2, weapon: oWeapon })).toBeFalsy()
    nDistanceGobs = 5
    expect(r.runScript('d20-ad-target-at-melee-range', { subject: oGob1, target: oGob2, weapon: oWeapon })).toBeTruthy()
    nDistanceGobs = 2
    expect(r.runScript('d20-ad-target-at-melee-range', { subject: oGob1, target: oGob2, weapon: oWeapon })).toBeTruthy()
    nDistanceGobs = 6
    expect(r.runScript('d20-ad-target-at-melee-range', { subject: oGob1, target: oGob2, weapon: oWeapon })).toBeFalsy()
    nDistanceGobs = 9
    expect(r.runScript('d20-ad-target-at-melee-range', { subject: oGob1, target: oGob2, weapon: oWeapon })).toBeFalsy()
    nDistanceGobs = 10
    expect(r.runScript('d20-ad-target-at-melee-range', { subject: oGob1, target: oGob2, weapon: oWeapon })).toBeFalsy()
    nDistanceGobs = 11
    expect(r.runScript('d20-ad-target-at-melee-range', { subject: oGob1, target: oGob2, weapon: oWeapon })).toBeFalsy()
  })
  it('commands de calcul de distance détecte à portée de mélée - arme reach', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const oHalberd = r.createEntity('wpn-halberd')
    r.equipItem(oGob1, oHalberd)
    let nDistanceGobs = 0
    r.events.on('request.distance', event => {
      event.distance = nDistanceGobs
    })
    nDistanceGobs = 30
    expect(r.getDistance(oGob1, oGob2)).toBe(30)
    expect(r.runScript('d20-ad-target-at-melee-range', { subject: oGob1, target: oGob2, weapon: oHalberd })).toBeFalsy()
    nDistanceGobs = 5
    expect(r.runScript('d20-ad-target-at-melee-range', { subject: oGob1, target: oGob2, weapon: oHalberd })).toBeTruthy()
    nDistanceGobs = 2
    expect(r.runScript('d20-ad-target-at-melee-range', { subject: oGob1, target: oGob2, weapon: oHalberd })).toBeTruthy()
    nDistanceGobs = 6
    expect(r.runScript('d20-ad-target-at-melee-range', { subject: oGob1, target: oGob2, weapon: oHalberd })).toBeTruthy()
    nDistanceGobs = 9
    expect(r.runScript('d20-ad-target-at-melee-range', { subject: oGob1, target: oGob2, weapon: oHalberd })).toBeTruthy()
    nDistanceGobs = 10
    expect(r.runScript('d20-ad-target-at-melee-range', { subject: oGob1, target: oGob2, weapon: oHalberd })).toBeTruthy()
    nDistanceGobs = 11
    expect(r.runScript('d20-ad-target-at-melee-range', { subject: oGob1, target: oGob2, weapon: oHalberd })).toBeFalsy()
  })
})

describe('jet de sauvegarde', function () {
  it('jet normal pour voir si ça marche (toujour echec)', function () {
    const r = new Rules()
    const g = r.createEntity('c-goblin')
    r._dice.debug(true, 0.5)
    expect(r.rollSavingThrow(g, r.CONST.ABILITY_DEXTERITY, 50)).toBeFalsy()
  })
  it('jet normal pour voir si ça marche (toujour success)', function () {
    const r = new Rules()
    const g = r.createEntity('c-goblin')
    r._dice.debug(true, 0.5)
    expect(r.rollSavingThrow(g, r.CONST.ABILITY_DEXTERITY, 0)).toBeTruthy()
  })
  it('prise en compte des bonus', function () {
    const r = new Rules()
    const g = r.createEntity('c-goblin')
    r._dice.debug(true, 0.5)
    expect(r.rollSavingThrow(g, r.CONST.ABILITY_DEXTERITY, 50)).toBeFalsy()
    const eBonus = r.createEffect(r.CONST.EFFECT_SAVING_THROW_MODIFIER, { value: 100, ability: CONST.ABILITY_DEXTERITY })
    r.applyEffect(eBonus, g, g, 100)

    const am1 = r.aggregateModifiers(g, [
      CONST.EFFECT_SAVING_THROW_MODIFIER,
      CONST.ITEM_PROPERTY_SAVING_THROW_MODIFIER
    ], {})
    expect(am1.sum).toBe(100)

    const am2 = r.aggregateModifiers(g, [
      CONST.EFFECT_SAVING_THROW_MODIFIER,
      CONST.ITEM_PROPERTY_SAVING_THROW_MODIFIER
    ], {
      effectFilter: eff => eff.ability === CONST.ABILITY_DEXTERITY,
      propFilter: prop => prop.ability === CONST.ABILITY_DEXTERITY
    })
    expect(am2.sum).toBe(100)

    const sSaveType = r.CONST.SAVE_TYPE_ALL
    const am3 = r.aggregateModifiers(g, [
      CONST.EFFECT_SAVING_THROW_MODIFIER,
      CONST.ITEM_PROPERTY_SAVING_THROW_MODIFIER
    ], {
      effectFilter: eff => eff.ability === CONST.ABILITY_DEXTERITY && (eff.type === r.CONST.SAVE_TYPE_ALL ? true : eff.type === sSaveType),
      propFilter: prop => prop.ability === CONST.ABILITY_DEXTERITY && (prop.type === r.CONST.SAVE_TYPE_ALL ? true : prop.type === sSaveType)
    })
    expect(am3.sum).toBe(100)

    expect(r.rollSavingThrow(g, r.CONST.ABILITY_DEXTERITY, 50)).toBeTruthy()
  })
  it('prise en compte des bonus élémentaux', function () {
    const r = new Rules()
    const g = r.createEntity('c-goblin')
    r._dice.debug(true, 0.5)
    expect(r.rollSavingThrow(g, r.CONST.ABILITY_DEXTERITY, 50)).toBeFalsy()
    const eBonusSaveFire = r.createEffect(r.CONST.EFFECT_SAVING_THROW_MODIFIER, {
      value: 100,
      ability: CONST.ABILITY_DEXTERITY,
      type: r.CONST.SAVE_TYPE_FIRE
    })
    r.applyEffect(eBonusSaveFire, g, g, 100)
    expect(r.rollSavingThrow(g, r.CONST.ABILITY_DEXTERITY, 50, r.CONST.SAVE_TYPE_FIRE)).toBeTruthy()
    expect(r.rollSavingThrow(g, r.CONST.ABILITY_DEXTERITY, 50, r.CONST.SAVE_TYPE_COLD)).toBeFalsy()
  })
})

describe('imp-spitter creation process', function () {
  it('on peut creer le lutin cracheur avec de l\'équipement standard', function () {
    const r = new Rules()
    r.blueprints['c-imp-spitter'] = {
      entityType: 'ENTITY_TYPE_ACTOR',
      specie: 'SPECIE_HUMANOID',
      gender: 'GENDER_MALE',
      size: 'CREATURE_SIZE_SMALL',
      speed: 30,
      ac: 10,
      feats: [],
      skills: [
        {
          skill: 'SKILL_STEALTH',
          value: 6
        }
      ],
      classes: [
        {
          class: 'CLASS_MONSTER',
          level: 2
        }
      ],
      abilities: [
        {
          ability: 'ABILITY_STRENGTH',
          value: 10
        },
        {
          ability: 'ABILITY_DEXTERITY',
          value: 16
        },
        {
          ability: 'ABILITY_CONSTITUTION',
          value: 10
        },
        {
          ability: 'ABILITY_INTELLIGENCE',
          value: 8
        },
        {
          ability: 'ABILITY_WISDOM',
          value: 10
        },
        {
          ability: 'ABILITY_CHARISMA',
          value: 9
        }
      ],
      proficiencies: [
        'PROFICIENCY_ARMOR_LIGHT',
        'PROFICIENCY_SHIELD',
        'PROFICIENCY_WEAPON_SIMPLE'
      ],
      alignment: 'ALIGNMENT_NEUTRAL_EVIL',
      equipment: [
        {
          slot: 'EQUIPMENT_SLOT_CHEST',
          item: 'arm-leather'
        },
        {
          slot: 'EQUIPMENT_SLOT_WEAPON_MELEE',
          item: 'wpn-dagger'
        }
      ]
    }
    const oImp = r.createEntity('c-imp-spitter')
    expect(oImp).toBeDefined()
    const oWpn = r.getEquippedItem(oImp, r.CONST.EQUIPMENT_SLOT_WEAPON_MELEE)
    expect(oWpn.itemSubType).toBe(r.CONST.WEAPON_TYPE_DAGGER)
    const oArm = r.getEquippedItem(oImp, r.CONST.EQUIPMENT_SLOT_CHEST)
    expect(oArm.itemBaseType).toBe(r.CONST.ITEM_BASE_TYPE_ARMOR)
  })
  it('création du lutin cracheur avec de l\'équipement personalisé', function () {
    const r = new Rules()
    r.blueprints['wpn-dagger-imp-1'] = {
      entityType: r.CONST.ENTITY_TYPE_ITEM,
      itemBaseType: r.CONST.ITEM_BASE_TYPE_WEAPON,
      itemSubType: r.CONST.WEAPON_TYPE_DAGGER,
      magical: false,
      properties: [
        {
          property: r.CONST.ITEM_PROPERTY_ENHANCEMENT,
          value: 1
        }
      ]
    }

    r.blueprints['arm-leather-imp-1'] = {
      entityType: r.CONST.ENTITY_TYPE_ITEM,
      itemBaseType: r.CONST.ITEM_BASE_TYPE_ARMOR,
      itemSubType: 'ARMOR_TYPE_LEATHER',
      properties: [
        {
          property: r.CONST.ITEM_PROPERTY_AC_MODIFIER,
          value: 1
        }
      ]
    }

    r.blueprints['c-imp-spitter'] = {
      entityType: 'ENTITY_TYPE_ACTOR',
      specie: 'SPECIE_HUMANOID',
      gender: 'GENDER_MALE',
      size: 'CREATURE_SIZE_SMALL',
      speed: 30,
      ac: 10,
      feats: [],
      skills: [
        {
          skill: 'SKILL_STEALTH',
          value: 6
        }
      ],
      classes: [
        {
          class: 'CLASS_MONSTER',
          level: 2
        }
      ],
      abilities: [
        {
          ability: 'ABILITY_STRENGTH',
          value: 10
        },
        {
          ability: 'ABILITY_DEXTERITY',
          value: 16
        },
        {
          ability: 'ABILITY_CONSTITUTION',
          value: 10
        },
        {
          ability: 'ABILITY_INTELLIGENCE',
          value: 8
        },
        {
          ability: 'ABILITY_WISDOM',
          value: 10
        },
        {
          ability: 'ABILITY_CHARISMA',
          value: 9
        }
      ],
      proficiencies: [
        'PROFICIENCY_ARMOR_LIGHT',
        'PROFICIENCY_SHIELD',
        'PROFICIENCY_WEAPON_SIMPLE'
      ],
      alignment: 'ALIGNMENT_NEUTRAL_EVIL',
      equipment: [
        {
          slot: 'EQUIPMENT_SLOT_CHEST',
          item: 'arm-leather-imp-1'
        },
        {
          slot: 'EQUIPMENT_SLOT_WEAPON_MELEE',
          item: 'wpn-dagger-imp-1'
        }
      ]
    }
    const oImp = r.createEntity('c-imp-spitter')
    const oGob = r.createEntity('c-goblin')
    expect(oImp).toBeDefined()
    const ar = r.computeAttackRoll(oImp, oGob, { weapon: r.getEquippedItem(oImp, r.CONST.EQUIPMENT_SLOT_WEAPON_MELEE) })
    expect(ar.bonusDetail.ability).toEqual(3) // 16 en dex
    expect(ar.bonusDetail.weapon).toEqual(1) // Dague +1
    expect(ar.bonusDetail.proficiency).toEqual(2) // Level 2 = +2
    const ar2 = r.computeAttackRoll(oGob, oImp, { weapon: r.getEquippedItem(oGob, r.CONST.EQUIPMENT_SLOT_WEAPON_MELEE) })
    expect(ar2.bonusDetail.ability).toEqual(2) // 14 en dex
    expect(ar2.bonusDetail.weapon).toEqual(0) // Dague +1
    expect(ar2.bonusDetail.proficiency).toEqual(2) // Level 2 = +2
    const ac = r.getAC(oImp, oGob, r.getEquippedItem(oGob, r.CONST.EQUIPMENT_SLOT_WEAPON_MELEE))
    expect(ac).toBe(11 + // ac leather base
      1 + // armor + 1
      3 // 16 dex
    )
  })
  it('creation du lutin cracheur avec le sort "crache à la gueule"', function () {
    const r = new Rules()
    r.scripts['s1-crache-a-la-gueule'] = function ({ rules, spell }) {
      const { subject, target, dc, attack } = spell
      // lance un projectile d'acide 1d6 sur la cible, pas de jet de sauvegarde
      // en général un sort dirigé par une attaque.
      if (attack.hit) {
        let nDam = rules.dice.roll(6)
        if (attack.critical) {
          nDam *= rules.DATA.VARIABLES.CRITICAL_FACTOR
        }
        const eDam = rules.createEffect(rules.CONST.EFFECT_DAMAGE, { value: nDam, type: r.CONST.DAMAGE_TYPE_ACID })
        rules.applyEffect(eDam, target, subject)
      }
    }
    expect(r.CONST).toBeDefined()
    r.blueprints['wpn-dagger-imp-1'] = {
      entityType: r.CONST.ENTITY_TYPE_ITEM,
      itemBaseType: r.CONST.ITEM_BASE_TYPE_WEAPON,
      itemSubType: r.CONST.WEAPON_TYPE_DAGGER,
      magical: false,
      properties: [
        {
          property: r.CONST.ITEM_PROPERTY_ENHANCEMENT,
          value: 1
        }
      ]
    }

    r.blueprints['arm-leather-imp-1'] = {
      entityType: r.CONST.ENTITY_TYPE_ITEM,
      itemBaseType: r.CONST.ITEM_BASE_TYPE_ARMOR,
      itemSubType: 'ARMOR_TYPE_LEATHER',
      properties: [
        {
          property: r.CONST.ITEM_PROPERTY_AC_MODIFIER,
          value: 1
        }
      ]
    }

    r.DATA.SPELLS.SPELL_ACID_SPIT = {
      school: 'SCHOOL_EVOCATION',
      range: 15,
      targetType: ['TARGET_TYPE_CREATURE'],
      script: 's1-crache-a-la-gueule',
      level: 1,
      nature: ['SPELL_NATURE_INNATE'],
      castTime: 500,
      immunityType: r.CONST.DAMAGE_TYPE_ACID,
      subSpells: [],
      master: null,
      description: 'Sort de test de crachat d\'acide',
      harmfull: true,
      aoe: false,
      discriminant: false,
      concentration: false,
      spontaneouslyCast: false,
      hostile: true,
      projectile: true
    }

    r.blueprints['c-imp-spitter'] = {
      entityType: 'ENTITY_TYPE_ACTOR',
      specie: 'SPECIE_HUMANOID',
      gender: 'GENDER_MALE',
      size: 'CREATURE_SIZE_SMALL',
      speed: 30,
      ac: 10,
      feats: [],
      skills: [
        {
          skill: 'SKILL_STEALTH',
          value: 6
        }
      ],
      classes: [
        {
          class: 'CLASS_MONSTER',
          level: 2
        }
      ],
      abilities: [
        {
          ability: 'ABILITY_STRENGTH',
          value: 10
        },
        {
          ability: 'ABILITY_DEXTERITY',
          value: 16
        },
        {
          ability: 'ABILITY_CONSTITUTION',
          value: 10
        },
        {
          ability: 'ABILITY_INTELLIGENCE',
          value: 8
        },
        {
          ability: 'ABILITY_WISDOM',
          value: 10
        },
        {
          ability: 'ABILITY_CHARISMA',
          value: 9
        }
      ],
      proficiencies: [
        'PROFICIENCY_ARMOR_LIGHT',
        'PROFICIENCY_SHIELD',
        'PROFICIENCY_WEAPON_SIMPLE'
      ],
      alignment: 'ALIGNMENT_NEUTRAL_EVIL',
      equipment: [
        {
          slot: 'EQUIPMENT_SLOT_CHEST',
          item: 'arm-leather-imp-1'
        },
        {
          slot: 'EQUIPMENT_SLOT_WEAPON_MELEE',
          item: 'wpn-dagger-imp-1'
        }
      ],
      actions: [
        {
          id: 'SPELL_ACID_SPIT'
        }
      ]
    }
    const oImp = r.createEntity('c-imp-spitter')
    const oGob = r.createEntity('c-goblin')
    r.dice.debug(true, 0.5)
    const aAppliedEffects = []
    r.events.on('effect.applied', ({ effect }) => {
      if (effect.source === oImp) {
        aAppliedEffects.push(effect)
      }
    })
    const sc = r.castSpell(oImp, 'SPELL_ACID_SPIT', 0, r.CONST.ABILITY_DEXTERITY, oGob)
    expect(aAppliedEffects.length).toBeGreaterThan(0)
    expect(aAppliedEffects[0].tag).toBe(r.CONST.EFFECT_DAMAGE)
    expect(aAppliedEffects[0].type).toBe(r.CONST.DAMAGE_TYPE_ACID)
    expect(sc.attack.distance).toBe(0)
    expect(sc.attack.hit).toBeTruthy()
    expect(sc.attack.ac).toBe(15)
    expect(sc.attack.disadvantage).toBeTruthy()
  })
})

describe('group effect and spell', function () {
  it('grouper plusieurs effets dans un même spell', function () {
    const r = new Rules()
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const eE1 = r.createEffect(r.CONST.EFFECT_CONCEALMENT, { value: 0.5 })
    const eE2 = r.createEffect(r.CONST.EFFECT_DEAFNESS, {})
    const eE3 = r.createEffect(r.CONST.EFFECT_BLINDNESS, {})
    const eGroup = r.createEffect(r.CONST.EFFECT_GROUP, {
      group: [eE1, eE2, eE3],
      tag: 'Spell-1'
    })
    r.applyEffect(eE1, oGob1, oGob2, 10)
    r.applyEffect(eE2, oGob1, oGob2, 17)
    r.applyEffect(eE3, oGob1, oGob2, 12)
    r.applyEffect(eGroup, oGob1, oGob2, 10)
    expect(r.getActiveEffects(oGob1).length).toBe(4)
    expect(eGroup.duration).toBe(16)
    expect(eE1.duration).toBe(9)
    expect(eE2.duration).toBe(16)
    expect(eE3.duration).toBe(11)
    r.nextRound()
    expect(r.getActiveEffects(oGob1).length).toBe(4)
    expect(eGroup.duration).toBe(15)
    expect(eE1.duration).toBe(8)
    expect(eE2.duration).toBe(15)
    expect(eE3.duration).toBe(10)
    // expect(r.effectProcessor.isEffectExpired(eGroup)).toBeFalsy()
    expect(eGroup.expired).toBeFalsy()
    r.nextRound()
    r.nextRound()
    r.nextRound()
    r.nextRound()
    r.nextRound()
    r.nextRound()
    r.nextRound()
    expect(r.getActiveEffects(oGob1).length).toBe(4)
    expect(eGroup.duration).toBe(8)
    expect(eE1.duration).toBe(1)
    expect(eE2.duration).toBe(8)
    expect(eE3.duration).toBe(3)
    r.nextRound()
    expect(r.getActiveEffects(oGob1).length).toBe(3)
    expect(eGroup.duration).toBe(7)
    expect(eE1.duration).toBe(0)
    expect(eE2.duration).toBe(7)
    expect(eE3.duration).toBe(2)
    r.nextRound()
    r.nextRound()
    expect(r.getActiveEffects(oGob1).length).toBe(2)
    expect(eGroup.duration).toBe(5)
    expect(eE1.duration).toBe(0)
    expect(eE2.duration).toBe(5)
    expect(eE3.duration).toBe(0)
    r.nextRound()
    r.nextRound()
    r.nextRound()
    r.nextRound()
    r.nextRound()
    expect(r.getActiveEffects(oGob1).length).toBe(0)
    expect(eGroup.duration).toBe(0)
    expect(eE1.duration).toBe(0)
    expect(eE2.duration).toBe(0)
    expect(eE3.duration).toBe(0)
  })
  it('grouper plusieurs effets dans un même spell puis désactiver le groupe', function () {
    const r = new Rules()
    const oGob1 = r.createEntity('c-goblin')
    const oGob2 = r.createEntity('c-goblin')
    const eE1 = r.createEffect(r.CONST.EFFECT_CONCEALMENT, { value: 0.5 })
    const eE2 = r.createEffect(r.CONST.EFFECT_DEAFNESS, {})
    const eE3 = r.createEffect(r.CONST.EFFECT_BLINDNESS, {})
    const eGroup = r.createEffect(r.CONST.EFFECT_GROUP, {
      group: [eE1, eE2, eE3],
      tag: 'Spell-1'
    })
    const aLogEffectRemoved = []
    r.events.on('effect.expired', ({
      effect
    }) => {
      aLogEffectRemoved.push(effect.tag)
    })
    r.applyEffect(eE1, oGob1, oGob2, 10)
    r.applyEffect(eE2, oGob1, oGob2, 17)
    r.applyEffect(eE3, oGob1, oGob2, 12)
    r.applyEffect(eGroup, oGob1, oGob2, 10)
    r.nextRound()
    r.nextRound()
    r.nextRound()
    aLogEffectRemoved.sort()
    expect(aLogEffectRemoved).toEqual([])
    expect(r.getActiveEffects(oGob1).length).toBe(4)
    eGroup.terminate()
    r.nextRound()
    expect(r.getActiveEffects(oGob1).length).toBe(0)
    aLogEffectRemoved.sort()
    expect(aLogEffectRemoved).toEqual([
      r.CONST.EFFECT_BLINDNESS,
      r.CONST.EFFECT_CONCEALMENT,
      r.CONST.EFFECT_DEAFNESS,
      r.CONST.EFFECT_GROUP
    ])
  })
})

describe('tester la resistance aux arme', function () {
  it('est ce que cet effet marche ?', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)

    // 1: créer une arme normal
    const oDagger = r.createEntity('wpn-dagger')

    // 2: créer une arme bypass
    const oDagMag = r.createEntity('wpn-dagger')
    oDagMag.magical = true

    // 3: créer une armure wepaon resist
    const oArmor = r.createEntity('arm-leather')
    const ipWR = r.createItemProperty(r.CONST.ITEM_PROPERTY_WEAPON_RESISTANCE, { value: 20 })
    r.addItemProperty(oArmor, ipWR)

    // 4: créer 2 gobs
    const g1 = r.createEntity('c-goblin')
    const g2 = r.createEntity('c-goblin')

    // 5: equi un gob avec l'armure
    r.equipItem(g2, oArmor)

    // 6: equi l'autre gob avec l'arme normal
    r.equipItem(g1, oDagger)

    // 7: attaquer
    const ar1 = r.computeAttackRoll(g1, g2, { weapon: oDagger })
    const ac1 = r.getAC(g2, g1, oDagger)
    const d1 = r.computeAttackDamage(g1, g2, ar1)
    const aDamages = r.applyAttackDamage(g1, g2, ar1)
    expect(aDamages).toHaveLength(1)
    const dam1 = aDamages[0].info

    // 8: constater le manque d'efficacité
    expect(dam1.type).toBe('DAMAGE_TYPE_PIERCING')
    expect(dam1.resistance).toBe(20)
    expect(dam1.damage.initial).toBe(5)
    expect(dam1.damage.resisted).toBe(5)
    expect(dam1.damage.immunized).toBe(0)
    expect(dam1.damage.amplified).toBe(0)
    expect(dam1.damage.final).toBe(0)

    // 9: equiper le gob avec l'arme benié
    r.equipItem(g1, oDagMag)

    // 10: re attaquer
    const ar2 = r.computeAttackRoll(g1, g2, { weapon: oDagMag })
    const ac2 = r.getAC(g2, g1, oDagMag)
    const d2 = r.computeAttackDamage(g1, g2, ar2)
    const aDamages2 = r.applyAttackDamage(g1, g2, ar2)
    expect(aDamages2).toHaveLength(1)
    const dam2 = aDamages2[0].info

    // 11: constater que ca fait des dégats
    expect(dam2.type).toBe('DAMAGE_TYPE_PIERCING')
    expect(dam2.resistance).toBe(0)
    expect(dam2.damage.initial).toBe(5)
    expect(dam2.damage.resisted).toBe(0)
    expect(dam2.damage.immunized).toBe(0)
    expect(dam2.damage.amplified).toBe(0)
    expect(dam2.damage.final).toBe(5)
  })

  it('qu en est il des munitions', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)

    // 1: créer une mun normal
    const oShortBow = r.createEntity('wpn-shortbow')
    const oArrowNorm = r.createEntity('ammo-arrow')

    // 2: créer une mun bypass
    const oArrowMag = r.createEntity('ammo-arrow')
    oArrowMag.magical = true

    // 3: créer une armure wepaon resist
    const oArmor = r.createEntity('arm-leather')
    const ipWR = r.createItemProperty(r.CONST.ITEM_PROPERTY_WEAPON_RESISTANCE, { value: 20 })
    r.addItemProperty(oArmor, ipWR)

    // 4: créer 2 gobs
    const g1 = r.createEntity('c-goblin')
    const g2 = r.createEntity('c-goblin')

    // 5: equi un gob avec l'armure
    r.equipItem(g2, oArmor)

    // 6: equi l'autre gob avec l'arme normal
    r.equipItem(g1, oShortBow)
    r.equipItem(g1, oArrowNorm)

    // 7: attaquer
    const ar1 = r.computeAttackRoll(g1, g2, { weapon: oShortBow, ammo: oArrowNorm })
    r.computeAttackDamage(g1, g2, ar1)
    const aDamages = r.applyAttackDamage(g1, g2, ar1)
    expect(aDamages).toHaveLength(1)
    const dam1 = aDamages[0].info

    // 8: constater le manque d'efficacité
    expect(dam1.type).toBe('DAMAGE_TYPE_PIERCING')
    expect(dam1.resistance).toBe(20)
    expect(dam1.damage.initial).toBe(6)
    expect(dam1.damage.resisted).toBe(6)
    expect(dam1.damage.immunized).toBe(0)
    expect(dam1.damage.amplified).toBe(0)
    expect(dam1.damage.final).toBe(0)

    // 9: equiper le gob avec l'arme benié
    r.equipItem(g1, oArrowMag)

    // 10: re attaquer
    const ar2 = r.computeAttackRoll(g1, g2, { weapon: oShortBow, ammo: oArrowMag })
    r.computeAttackDamage(g1, g2, ar2)
    const aDamages2 = r.applyAttackDamage(g1, g2, ar2)
    expect(aDamages2).toHaveLength(1)
    const dam2 = aDamages2[0].info

    // 11: constater que ca fait des dégats
    expect(dam2.type).toBe('DAMAGE_TYPE_PIERCING')
    expect(dam2.resistance).toBe(0)
    expect(dam2.damage.initial).toBe(6)
    expect(dam2.damage.resisted).toBe(0)
    expect(dam2.damage.immunized).toBe(0)
    expect(dam2.damage.amplified).toBe(0)
    expect(dam2.damage.final).toBe(6)
  })
})

describe('les armes thrown', function () {
  it('avec un marteau léger', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)

    const oLightHammer = r.createEntity('wpn-light-hammer')
    const g1 = r.createEntity('c-goblin')
    const g2 = r.createEntity('c-goblin')
    r.equipItem(g1, oLightHammer)

    const ar1 = r.computeAttackRoll(g1, g2, { weapon: oLightHammer })
    expect(ar1.ability).toBe('ABILITY_STRENGTH')

    const ar2 = r.computeAttackRoll(g1, g2, { weapon: oLightHammer, thrown: true })
    expect(ar2.ability).toBe('ABILITY_DEXTERITY')
  })
  it('avec une épée longue', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)

    const oLongSword = r.createEntity('wpn-longsword')
    const g1 = r.createEntity('c-goblin')
    const g2 = r.createEntity('c-goblin')
    r.equipItem(g1, oLongSword)

    const ar1 = r.computeAttackRoll(g1, g2, { weapon: oLongSword })
    expect(ar1.ability).toBe('ABILITY_STRENGTH')

    const ar2 = r.computeAttackRoll(g1, g2, { weapon: oLongSword, thrown: true })
    expect(ar2.ability).toBe('ABILITY_STRENGTH')
  })
  it('tester si on peut frapper loin avec une arme a lancer', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)
    r.events.on('request.distance', event => {
      event.distance = 30
    })

    const oLightHammer = r.createEntity('wpn-light-hammer')
    const g1 = r.createEntity('c-goblin')
    const g2 = r.createEntity('c-goblin')
    r.equipItem(g1, oLightHammer)

    const ar1 = r.computeAttackRoll(g1, g2, { weapon: oLightHammer })
    expect(ar1.hit).toBeFalsy()
    expect(ar1.miss).toBe(r.CONST.ATTACK_MISS_OUT_OF_MELEE_RANGE)

    const ar2 = r.computeAttackRoll(g1, g2, { weapon: oLightHammer, thrown: true })
    expect(ar2.miss).toBe('')
    expect(ar2.hit).toBeTruthy()
  })

  it('tester si on peut frapper sans arme', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)

    const g1 = r.createEntity('c-goblin')
    const g2 = r.createEntity('c-goblin')

    const ar1 = r.computeAttackRoll(g1, g2, { weapon: null })
    expect(ar1.ability).toBe('ABILITY_DEXTERITY')
    expect(ar1.weapon).toBeNull()
    const d1 = r.computeAttackDamage(g1, g2, ar1)
    expect(d1.damages[0].type).toBe(CONST.DAMAGE_TYPE_CRUSHING)
  })

  it('il se passe quoi quand on frappe avec une baguette', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)

    const g1 = r.createEntity('c-goblin')
    const g2 = r.createEntity('c-goblin')

    const oWand = r.createEntity('itm-magicwand')
    r.equipItem(g1, oWand)

    const ar1 = r.computeAttackRoll(g1, g2, { weapon: oWand })
    expect(ar1.ability).toBe('ABILITY_STRENGTH')
    const d1 = r.computeAttackDamage(g1, g2, ar1)
    expect(d1.damages[0].type).toBe(CONST.DAMAGE_TYPE_CRUSHING)
  })
})

describe('destroying equipped entities', function () {
  it('destroy entity', function () {
    const r = new Rules()
    r._dice.debug(true, 0.5)

    let oLastUneqItem = null
    r.events.on('entity.unequip', ({ actor, item, slot }) => {
      oLastUneqItem = item
    })
    const g1 = r.createEntity('c-goblin')
    const oWand = r.createEntity('itm-magicwand')
    r.equipItem(g1, oWand)
    expect(r.getEquippedItem(g1, CONST.EQUIPMENT_SLOT_WEAPON_MELEE)).toBe(oWand)
    r.destroyEntity(oWand)
    expect(r.getEquippedItem(g1, CONST.EQUIPMENT_SLOT_WEAPON_MELEE)).toBeNull()
    expect(oLastUneqItem).toBe(oWand)
  })
})

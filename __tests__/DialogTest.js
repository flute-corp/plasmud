const {
  Node,
  Context
} = require('../libs/dialogs')

describe('basic', function () {
  it('should not throw constructor error', function () {
    expect(() => {
      return new Node()
    }).not.toThrow()
  })
  it('should have events', function () {
    const d = new Node()
    expect(d.events).toBeDefined()
  })
})

describe('#Node.isDisplayed', function () {
  it('should be displayed', function () {
    const d = new Node()
    let bResult = true
    d.events.on('dialog.resolve.condition', oEvent => {
      oEvent.result(bResult)
    })
    d.scripts.condition = 'test1'
    expect(d.isDisplayed()).toBeTruthy()
    bResult = false
    expect(d.isDisplayed()).toBeFalsy()
    bResult = true
    expect(d.isDisplayed()).toBeTruthy()
  })
})

describe('#Node.getDisplayedNodes', function () {
  it('should display odd Nodes', function () {
    const s = new Node()
    const oTests = {
      ttrue: () => true,
      tfalse: () => false
    }
    const scriptManager = oEvent => {
      const sCond = oEvent.condition
      const f = oTests[sCond]
      oEvent.result(f())
    }
    const buildNode = (sText, sCondition) => {
      const o = new Node()
      o.text = sText
      o.scripts.condition = sCondition
      o.events.on('dialog.resolve.condition', scriptManager)
      return o
    }
    s.children.push(buildNode('t1', 'ttrue'))
    s.children.push(buildNode('t2', 'ttrue'))
    s.children.push(buildNode('t3', 'tfalse'))
    s.children.push(buildNode('t4', 'tfalse'))
    s.children.push(buildNode('t5', 'ttrue'))
    s.children.push(buildNode('t6', 'tfalse'))
    s.children.push(buildNode('t7', 'ttrue'))
    const aDO = s.getDisplayedChildren()
    expect(aDO).toHaveLength(4)
    expect(aDO.map(x => x.text)).toEqual(['t1', 't2', 't5', 't7'])
  })
})

describe('#Node.getFirstValidChild', function () {
  it('should display first valid children (4th)', function () {
    const s = new Node()
    const oTests = {
      ttrue: () => true,
      tfalse: () => false
    }
    const scriptManager = oEvent => {
      const sCond = oEvent.condition
      const f = oTests[sCond]
      oEvent.result(f())
    }
    const buildNode = (sText, sCondition) => {
      const o = new Node()
      o.text = sText
      o.scripts.condition = sCondition
      o.events.on('dialog.resolve.condition', scriptManager)
      return o
    }
    s.children.push(buildNode('t1', 'tfalse')) // 1st
    s.children.push(buildNode('t2', 'tfalse')) // 2nd
    s.children.push(buildNode('t3', 'tfalse')) // 3rd
    s.children.push(buildNode('t4', 'ttrue')) // 4th
    s.children.push(buildNode('t5', 'ttrue'))
    s.children.push(buildNode('t6', 'tfalse'))
    s.children.push(buildNode('t7', 'ttrue'))
    const oChild = s.getFirstValidChild()
    expect(oChild.text).toBe('t4')
  })
})

describe('#Node.import', function () {
  it('should build simplest dialog', function () {
    const oDialogs = Node.createFromArrayStructure([{
      id: 's1',
      text: 'bonjour',
      action: '',
      condition: '',
      options: []
    }])
    expect(oDialogs.s1.id).toBe('s1')
    expect(oDialogs.s1.text).toBe('bonjour')
    expect(oDialogs.s1.children).toHaveLength(0)
  })
  it('should build two simplest dialogs', function () {
    const oDialogs = Node.createFromArrayStructure([{
      id: 's1',
      text: 'bonjour',
      action: '',
      condition: '',
      options: []
    }, {
      id: 's2',
      text: 'bonjour 222',
      action: '',
      condition: '',
      options: []
    }])
    expect(oDialogs.s1.id).toBe('s1')
    expect(oDialogs.s1.text).toBe('bonjour')
    expect(oDialogs.s1.children).toHaveLength(0)
    expect(oDialogs.s2.id).toBe('s2')
    expect(oDialogs.s2.text).toBe('bonjour 222')
    expect(oDialogs.s2.children).toHaveLength(0)
  })
  it('should build two nested dialogs', function () {
    const oDialogs = Node.createFromArrayStructure([{
      id: 's1',
      text: 'bonjour',
      action: '',
      condition: '',
      options: ['s2']
    }, {
      id: 's2',
      text: 'bonjour 222',
      action: '',
      condition: '',
      options: []
    }])
    expect(oDialogs.s1.id).toBe('s1')
    expect(oDialogs.s1.text).toBe('bonjour')
    expect(oDialogs.s1.children).toHaveLength(1)
    expect(oDialogs.s1.children[0].id).toBe('s2')
  })
  it('recursive dialog should not be a problem', function () {
    const oDialogs = Node.createFromArrayStructure([{
      id: 's1',
      text: 'bonjour',
      action: '',
      condition: '',
      options: ['s2']
    }, {
      id: 's2',
      text: 'oui bonjour',
      action: '',
      condition: '',
      options: ['s1']
    }])
    expect(oDialogs.s1.id).toBe('s1')
    expect(oDialogs.s1.children[0].id).toBe('s2')
    expect(oDialogs.s1.children[0].children[0].id).toBe('s1')
    expect(oDialogs.s1.children[0].children[0].children[0].id).toBe('s2')
    expect(oDialogs.s1.children[0].children[0].children[0].children[0].id).toBe('s1')
  })
  it('three level recursivity dialog', function () {
    const oDialogs = Node.createFromArrayStructure([{
      id: 's1',
      text: 'bonjour', // Gobelin
      action: '',
      condition: '',
      options: ['s2'] // joueur répond s2 oui bonjour
    }, {
      id: 's2',
      text: 'oui bonjour', // joueur
      action: '',
      condition: '',
      options: ['s3']
    }, {
      id: 's3',
      text: 'hello',
      action: '',
      condition: '',
      options: ['s1', 's2']
    }])
    expect(oDialogs.s1.id).toBe('s1')
    expect(oDialogs.s1.children[0].id).toBe('s2')
    expect(oDialogs.s1.children[0].children[0].id).toBe('s3')
    expect(oDialogs.s1.children[0].children[0].children[0].id).toBe('s1')
    expect(oDialogs.s1.children[0].children[0].children[1].id).toBe('s2')
  })
})

describe('Context events', function () {
  const aDefs = [
    {
      id: 's_bonjour',
      text: 'Bonjour que voulez-vous savoir ?',
      options: ['s_heure', 's_rien']
    },
    {
      id: 's_heure',
      text: 'Quelle heure est il ?',
      options: ['s_donner_heure', 's_pas_de_montre']
    },
    {
      id: 's_rien',
      text: 'Rien pour le moment merci'
    },
    {
      id: 's_donner_heure',
      condition: 't_jai_une_montre',
      text: 'Il est midi.',
      options: ['s_merci_aurevoir']
    },
    {
      id: 's_merci_aurevoir',
      text: 'Merci au revoir.'
    },
    {
      id: 's_pas_de_montre',
      text: 'Désolé je n\'ai pas de montre.',
      options: ['s_merci_aurevoir']
    }
  ]
  it('should splash first screen', function () {
    const oContext = new Context()
    const oDialog = Node.createFromArrayStructure(aDefs)
    let bJaiUneMontre = true
    for (const [sDialog, d] of Object.entries(oDialog)) {
      d.events.on('dialog.resolve.condition', oEvent => {
        switch (oEvent.condition) {
          case 't_jai_une_montre': {
            oEvent.result(bJaiUneMontre)
            break
          }
        }
      })
    }
    oContext.screen = oDialog.s_bonjour
    expect(oDialog.s_bonjour.text).toBe('Bonjour que voulez-vous savoir ?')
    expect(oDialog.s_bonjour.children).toHaveLength(2)
    expect(oDialog.s_bonjour.getDisplayedChildren()).toHaveLength(2)
    let aLastOutput = []
    oContext.events.on('dialog.screen', oEvent => {
      const a = []
      a.push(oEvent.text)
      oEvent.options.forEach(o => {
        a.push(o.text)
      })
      aLastOutput = a
    })
    oContext.displayCurrentScreen()
    expect(aLastOutput).toEqual(['Bonjour que voulez-vous savoir ?', 'Quelle heure est il ?', 'Rien pour le moment merci'])
    oContext.selectOption('s_heure')
    expect(aLastOutput).toEqual(['Il est midi.', 'Merci au revoir.'])
    oContext.selectOption('s_merci_aurevoir')

    bJaiUneMontre = false
    oContext.screen = oDialog.s_bonjour
    oContext.displayCurrentScreen()
    expect(aLastOutput).toEqual(['Bonjour que voulez-vous savoir ?', 'Quelle heure est il ?', 'Rien pour le moment merci'])
    oContext.selectOption('s_heure')
    expect(aLastOutput).toEqual(['Désolé je n\'ai pas de montre.', 'Merci au revoir.'])
  })
  it('should splash first screen : options are numeric', function () {
    const oContext = new Context()
    const oDialog = Node.createFromArrayStructure(aDefs)
    const bJaiUneMontre = true
    for (const [sDialog, d] of Object.entries(oDialog)) {
      d.events.on('dialog.resolve.condition', oEvent => {
        switch (oEvent.condition) {
          case 't_jai_une_montre': {
            oEvent.result(bJaiUneMontre)
            break
          }
        }
      })
    }
    oContext.screen = oDialog.s_bonjour
    let aLastOutput = []
    oContext.events.on('dialog.screen', oEvent => {
      const a = []
      a.push(oEvent.text)
      oEvent.options.forEach(o => {
        a.push(o.text)
      })
      aLastOutput = a
    })
    // reset
    oContext.screen = oDialog.s_bonjour
    oContext.displayCurrentScreen()
    expect(aLastOutput).toEqual(['Bonjour que voulez-vous savoir ?', 'Quelle heure est il ?', 'Rien pour le moment merci'])
    oContext.selectOption(0)
    expect(aLastOutput).toEqual(['Il est midi.', 'Merci au revoir.'])
  })
  it('should splash first screen with another numeric option', function () {
    const oContext = new Context()
    const oDialog = Node.createFromArrayStructure(aDefs)
    const bJaiUneMontre = true
    for (const [sDialog, d] of Object.entries(oDialog)) {
      d.events.on('dialog.resolve.condition', oEvent => {
        switch (oEvent.condition) {
          case 't_jai_une_montre': {
            oEvent.result(bJaiUneMontre)
            break
          }
        }
      })
    }
    let aLastOutput = []
    oContext.events.on('dialog.screen', oEvent => {
      const a = []
      a.push(oEvent.text)
      oEvent.options.forEach(o => {
        a.push(o.text)
      })
      aLastOutput = a
    })
    // reset
    oContext.screen = oDialog.s_bonjour
    expect(aLastOutput).toEqual(['Bonjour que voulez-vous savoir ?', 'Quelle heure est il ?', 'Rien pour le moment merci'])
    oContext.selectOption(1)
    expect(aLastOutput).not.toEqual(['Il est midi.', 'Merci au revoir.'])
  })
  it('devrait afficher un second dialogue la seconde fois', function () {
    const oContext = new Context()
    oContext.load({
      variables: {},
      chapters: [
        {
          id: 's_first',
          condition: 't_is_first_met',
          action: 'at_not_first_anymore',
          text: 'Je n\'ai rien à vous dire.'
        },
        {
          id: 's_second',
          text: 'J\'ai dis que je n\'avais rien à vous dire.'
        }
      ]
    })
    let bFirstMet = true
    oContext.events.on('dialog.condition', oEvent => {
      if (oEvent.condition === 't_is_first_met') {
        oEvent.result(bFirstMet)
      }
    })
    oContext.events.on('dialog.action', oEvent => {
      if (oEvent.action === 'at_not_first_anymore') {
        bFirstMet = false
      }
    })
    let aLastOutput = []
    oContext.events.on('dialog.screen', oEvent => {
      const a = []
      a.push(oEvent.text)
      oEvent.options.forEach(o => {
        a.push(o.text)
      })
      aLastOutput = a
    })
    oContext.reset()
    oContext.displayCurrentScreen()
    expect(aLastOutput).toEqual(['Je n\'ai rien à vous dire.'])
    oContext.reset()
    oContext.displayCurrentScreen()
    expect(aLastOutput).toEqual(['J\'ai dis que je n\'avais rien à vous dire.'])
  })
})

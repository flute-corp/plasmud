const ArgumentParser = require('../libs/plasmud/helpers/argument-parser')

describe('ArgumentParser', function () {
  it('keywords / isKeyword', function () {
    const ap = new ArgumentParser()
    ap.keywords = ['using', 'for', 'to']
    expect(ap.isKeyword('USING')).toBeTruthy()
    expect(ap.isKeyword('using')).toBeTruthy()
    expect(ap.isKeyword('Using')).toBeTruthy()
    expect(ap.isKeyword('for')).toBeTruthy()
    expect(ap.isKeyword('to')).toBeTruthy()
    expect(ap.isKeyword('To')).toBeTruthy()
    expect(ap.isKeyword(' USING')).toBeFalsy()
    expect(ap.isKeyword('us1ng')).toBeFalsy()
    expect(ap.isKeyword('Using ')).toBeFalsy()
    expect(ap.isKeyword('')).toBeFalsy()
    expect(ap.isKeyword(null)).toBeFalsy()
    expect(ap.isKeyword(undefined)).toBeFalsy()
    expect(ap.isKeyword(0)).toBeFalsy()
  })

  it('sliceBetweenKeywords', function () {
    const ap = new ArgumentParser()
    ap.keywords = ['using', 'for', 'to']
    const aSlices = ap.sliceBetweenKeywords(['coffre', 'en', 'bois', 'using', 'clé', 'en', 'bronze'])
    expect(aSlices).toHaveLength(2)
    expect(aSlices[0].arguments).toEqual(['coffre', 'en', 'bois'])
    expect(aSlices[0].keyword).toBe('')
    expect(aSlices[1].arguments).toEqual(['clé', 'en', 'bronze'])
    expect(aSlices[1].keyword).toBe('using')
  })

  it('defineType/isType', function () {
    const ap = new ArgumentParser()
    ap.defineType('NUMBER', aArgs => aArgs.length === 1 && !isNaN(aArgs[0]) ? parseFloat(aArgs[0]) : null)
    expect(ap.isType('number')).toBeTruthy()
    expect(ap.isType('NUMBER')).toBeTruthy()
  })

  it('callTypeValidator', function () {
    const ap = new ArgumentParser()
    ap.defineType('NUMBER', aArgs => aArgs.length === 1 && !isNaN(aArgs[0]) ? parseFloat(aArgs[0]) : null)
    expect(ap.callTypeValidator('NUMBER', ['999'])).toBe(999)
    expect(ap.callTypeValidator('NUMBER', ['1e2'])).toBe(100)
    expect(ap.callTypeValidator('NUMBER', ['1', '2'])).toBeNull()
    expect(ap.callTypeValidator('NUMBER', ['abc'])).toBeNull()
  })

  it('checkSlice', function () {
    const ap = new ArgumentParser()
    ap.defineType('NUMBER', aArgs => aArgs.length === 1 && !isNaN(aArgs[0]) ? parseFloat(aArgs[0]) : null)
    ap.defineType('ALPHA', aArgs => aArgs.length === 1 && aArgs[0].match(/^[a-z]+$/i) ? aArgs[0] : null)
    ap.keywords = ['and']
    const x = ap.checkSlices([
      { keyword: '', arguments: ['abcdef'] },
      { keyword: 'and', arguments: ['555'] }
    ], ['ALPHA', 'AND', 'NUMBER'])
    expect(x).toEqual(['abcdef', 555])
  })

  it('checkSlice with empty argument list', function () {
    const ap = new ArgumentParser()
    ap.defineType('NUMBER', aArgs => aArgs.length === 1 && !isNaN(aArgs[0]) ? parseFloat(aArgs[0]) : null)
    ap.defineType('ALPHA', aArgs => aArgs.length === 1 && aArgs[0].match(/^[a-z]+$/i) ? aArgs[0] : null)
    ap.keywords = ['and']
    const x = ap.checkSlices([
      { keyword: '', arguments: [] },
      { keyword: 'and', arguments: ['555'] }
    ], ['ALPHA', 'AND', 'NUMBER'])
    expect(x).toBeNull()
  })

  it('checkSlice with empty argument list and empty patterns', function () {
    const ap = new ArgumentParser()
    ap.defineType('NUMBER', aArgs => aArgs.length === 1 && !isNaN(aArgs[0]) ? parseFloat(aArgs[0]) : null)
    ap.defineType('ALPHA', aArgs => aArgs.length === 1 && aArgs[0].match(/^[a-z]+$/i) ? aArgs[0] : null)
    ap.keywords = ['and']
    const x = ap.checkSlices([
      { keyword: '', arguments: [] }
    ], [])
    expect(x).toEqual([])
  })

  it('check when arguments are too small', function () {
    const ap = new ArgumentParser()
    ap.defineType('NUMBER', aArgs => aArgs.length === 1 && !isNaN(aArgs[0]) ? parseFloat(aArgs[0]) : null)
    ap.defineType('ALPHA', aArgs => aArgs.length === 1 && aArgs[0].match(/^[a-z]+$/i) ? aArgs[0] : null)
    ap.keywords = ['and']
    const x = ap.checkSlices([
      { keyword: '', arguments: ['gob'] }
    ], ['ALPHA'])
    expect(x).toEqual(['gob'])
  })
})

const r = require('@laboralphy/did-you-mean')

describe('basic recognition', function () {
  it('scenario du magasin de ticket', function () {
    expect(r.suggest(
      'ticket truce',
      [
        'another thing',
        'a Wutai ferry ticket to Truce',
        'a Wutai ferry ticket to Cinnabar Island'
      ], { relevance: 234 }
    )).toEqual(['a Wutai ferry ticket to Truce'])
  })
  it('requete peu précise', function () {
    expect(r.suggest(
      'ferry ticket',
      [
        'another thing',
        'a Wutai ferry ticket to Truce',
        'a Wutai ferry ticket to Cinnabar Island'
      ], { relevance: 234 }
    )).toEqual([
      'a Wutai ferry ticket to Truce',
      'a Wutai ferry ticket to Cinnabar Island'
    ])
  })
  it('requete mal écrite', function () {
    expect(r.suggest(
      'ferry tocket truc',
      [
        'another thing',
        'a Wutai ferry ticket to Truce',
        'a Wutai ferry ticket to Cinnabar Island'
      ], { relevance: 234 }
    )).toEqual([
      'a Wutai ferry ticket to Truce'
    ])
    expect(r.suggest(
      'fery tocket truc',
      [
        'another thing',
        'a Wutai ferry ticket to Truce',
        'a Wutai ferry ticket to Cinnabar Island'
      ], { relevance: 234 }
    )).toEqual([
      'a Wutai ferry ticket to Truce'
    ])
    expect(r.suggest(
      'fery tocket cinabar',
      [
        'another thing',
        'a Wutai ferry ticket to Truce',
        'a Wutai ferry ticket to Cinnabar Island'
      ], { relevance: 234 }
    )).toEqual([
      'a Wutai ferry ticket to Cinnabar Island'
    ])
    expect(r.suggest(
      'fery tocket cinbra',
      [
        'another thing',
        'a Wutai ferry ticket to Truce',
        'a Wutai ferry ticket to Cinnabar Island'
      ], { relevance: 234 }
    )).toEqual([
      'a Wutai ferry ticket to Cinnabar Island'
    ])
    expect(r.suggest(
      'trève',
      [
        'un autre truc à la con',
        'un ticket pour l\'île de la trève',
        'un ticket pour l\'arène des champions'
      ], { relevance: 234 }
    )).toEqual([
      'un ticket pour l\'île de la trève'
    ])
    expect(r.suggest(
      'treve',
      [
        'un autre truc à la con',
        'un ticket pour l\'île de la trève',
        'un ticket pour l\'arène des champions'
      ], { relevance: 234 }
    )).toEqual([
      'un ticket pour l\'île de la trève'
    ])
  })
})

const debug = require('debug')
const Rules = require('../adv-d20-5e/Rules')

const ADV_BLUEPRINT_BASE_PLAYER = 'c-base-player-with-dp1'
const ADV_DATA_NAMESPACE = 'ADVD205E'
const log = debug('mod:adv')

const ADVITEMS = new Set(['weapon', 'armor', 'shield', 'equipment'])

class AdvMudInterconnector {
  constructor () {
    this._rules = null
    this._engine = null
    this._system = null
  }

  /**
   * Lorsqu'une adv-entité de type actor est crée, on override la création de son équipement
   * On créé les objet avec MUD.createEntity (on ne laisse pas Rules s'en charger)
   * L'évènement entity.create déclenché par MUD.createEntity permettra de crééer l'adv-entité correspondante
   * Cet adv-entité existe sitôt MUD.createEntity appelée
   * Placer l'adv-entité en équipement.
   * @param entity
   * @param equipment
   */
  advEntityEquipment ({ entity, equipment }) {
    const oMudEntity = this._engine.getEntity(entity.id)
    if (oMudEntity.EQUIPPED) throw new Error('WTF')
    while (equipment.length > 0) {
      const oItemDef = equipment.shift()
      const oMudItem = this._engine.createEntity(oItemDef.item, oMudEntity.id)
      // cela a dû provoqué un évènement MUD "entity.create"
      // donc on a dû créé l'entité ADV équivalente
      const oAdvItem = this._rules.entities[oMudItem.id]
      if (!oAdvItem) {
        throw new Error('the item could not be created ADV-side : maybe the item subtype is not declared in ADVITEMS')
      }
      this._rules.equipItem(entity, oAdvItem, oItemDef.slot)
    }
    oMudEntity.EQUIPPED = true
  }

  advEntityCreated (oAdvEntity) {
    const idEntity = oAdvEntity.id
    if (!this._engine.isEntityExist(idEntity)) {
      this._engine.createEntity(oAdvEntity.ref)
    }
  }

  mudEntityCreated (entity, location) {
    const oEntity = this._engine.getEntity(entity)
    log('%s created: %s "%s" location: "%s"', oEntity.blueprint.type, oEntity.id, oEntity.name, location || 'LIMBO')

    const id = oEntity.id
    const sType = oEntity.blueprint.type
    // y a t-il un equivalent ADV ?
    let oAdvEntity = this._rules.entities[id]
    if (!oAdvEntity) {
      switch (sType) {
        case 'player': {
          if (!this._rules.isBlueprintExists(ADV_BLUEPRINT_BASE_PLAYER)) {
            this.addBlueprintToRules(ADV_BLUEPRINT_BASE_PLAYER)
          }
          oAdvEntity = this._rules.createEntity(ADV_BLUEPRINT_BASE_PLAYER, id)
          break
        }

        case 'actor': {
          if (!this._rules.isBlueprintExists(oEntity.ref)) {
            this.addBlueprintToRules(oEntity.ref)
          }
          oAdvEntity = this._rules.createEntity(oEntity.ref, id)
          break
        }

        case 'item': {
          const sSubType = oEntity.blueprint.subtype
          if (ADVITEMS.has(sSubType)) {
            // seuls certains sous type d'item son éligible à un alter ego ADV
            // les clés, les gemmes et les items de quêtes ne sont pas éligibles
            if (!this._rules.isBlueprintExists(oEntity.ref)) {
              this.addBlueprintToRules(oEntity.ref)
            }
            oAdvEntity = this._rules.createEntity(oEntity.ref, id)
            const oItemData = this._rules.getItemData(oEntity.ref)
            if (oItemData) {
              oEntity._baseWeight = oItemData.weight
            } else {
              oEntity._baseWeight = oAdvEntity.weight
            }
          }
          break
        }
      }
    }
  }

  mudEntityDestroyed (engine, entity) {
    log('%s destroyed: %s "%s" location: "%s"', entity.blueprint.type, entity.id, entity.name, entity.location || 'LIMBO')
    // détruire l'équivalent ADV
    if (entity in this._rules.entities) {
      const oEntities = this._rules.entities[entity]
      this._rules.destroyEntity(oEntities)
    }
  }

  addBlueprintToRules (resref) {
    const adv = ADV_DATA_NAMESPACE
    const oBlueprint = this._engine.getBlueprint(resref)
    if (oBlueprint.data && oBlueprint.data[adv] && oBlueprint.data[adv].blueprint) {
      const d = oBlueprint.data[adv].blueprint
      this._rules.blueprintFactory.addBlueprint(resref, d)
    }
  }

  createRulesBlueprints () {
    for (const resref of this._engine.blueprintRefs) {
      this.addBlueprintToRules(resref)
    }
  }

  run ({ system }) {
    const rules = new Rules()
    const engine = system.engine
    const ee = engine.events
    this._rules = rules
    this._system = system
    this._engine = engine
    this.createRulesBlueprints()

    rules.events.on('entity.created', ({ entity }) => {
      this.advEntityCreated(entity)
    })

    rules.events.on('entity.equipment', oEvent => {
      this.advEntityEquipment(oEvent)
    })

    ee.on('entity.create', ({ entity, location }) => {
      this.mudEntityCreated(entity, location)
    })

    ee.on('entity.destroy', ({ entity }) => {
      this.mudEntityDestroyed(engine, entity)
    })
  }
}

function main (evt) {
  const amc = new AdvMudInterconnector()
  amc.run(evt)
  log('Advanced Dungeons & Dragons 5e rules (SRD)')
  log('Open Game License v1.0a © 2000, Wizards of the Coast, Inc.')
}

main($event)

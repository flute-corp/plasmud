const debug = require('debug')

const log = debug('mod:world')

function main (evt) {
  log('Development world ! v0.0.1')
}

main($event)

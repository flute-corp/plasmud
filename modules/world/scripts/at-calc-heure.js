function main (evt) {
  const d = new Date()
  const h = d.getHours()
  const m = d.getMinutes()
  const s = h.toString() + ' heure' + (h > 1 ? 's' : '') + ' ' + m.toString()
  evt.variables.serverTime = s
}

main($event)

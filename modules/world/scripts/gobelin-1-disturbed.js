function main ({ engine, self, interactor, item, type }) {
  const oPlayer = engine.getEntity(interactor)
  const oSelf = engine.getEntity(self)
  const oItem = engine.getEntity(item)

  if (type === engine.Const.DISTURBANCE_ITEM_ADDED) {
    if (oItem.tag === 'wpn-dagger') {
      engine.speakString(self, interactor, engine.text('gobelinQuest.accepteCadeau', { giver: oPlayer.name, item: oItem.name, count: oItem.stack }))
      oSelf.remark = engine.text('gobelinQuest.happy', { giver: oPlayer.name })
    } else {
      engine.speakString(self, interactor, engine.text('gobelinQuest.noThx'))
      engine.moveEntity(item, oSelf.location)
    }
  }
}

main($event)

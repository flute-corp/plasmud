/**
 *
 * @param engine {Engine}
 * @param interactor
 */
function main ({ engine, interactor }) {
  const oEnterer = engine.getEntity(interactor)
  if (oEnterer.blueprint.type === engine.Const.ENTITY_TYPES_PLAYER) {
    console.log(oEnterer.name, 'vient d\'entrer dans R00')
  }
}

main($event)

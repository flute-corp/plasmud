function main ({ engine, self, entity, types }) {
  const { heard, seen, vanished, inaubible } = types
  const oGob = engine.getEntity(self)
  const oEntity = engine.getEntity(entity)
  const sRoom = oGob.location
  console.log('event perception fired')
  if (seen) {
    engine.speakString(self, sRoom, 'Moi, ' + oGob.name + ' voit apparaitre : ' + oEntity.name)
  }
}

main($event)

import { createNamespacedHelpers } from 'vuex'

const {
  mapActions: terminalMapActions,
  mapMutations: terminalMapMutations,
  mapGetters: terminalMapGetters
} = createNamespacedHelpers('terminal')

export default {
  computed: {
    ...terminalMapGetters({
      isFormLoginVisible: 'isFormLoginVisible',
      isFormPasswdVisible: 'isFormPasswdVisible',
      isConnected: 'isConnected',
      isAuthenticated: 'isAuthenticated'
    }),
    formLoginVisible: {
      get () {
        return this.isFormLoginVisible && !this.isAuthenticated
      },
      set (value, oldValue) {
        if (value !== oldValue) {
          if (value) {
            this.showFormLogin()
          } else {
            this.hideFormLogin()
          }
        }
      }
    },
    formPasswdVisible: {
      get () {
        return this.isFormPasswdVisible && this.isAuthenticated
      },
      set (value, oldValue) {
        if (value !== oldValue) {
          if (value) {
            this.showFormPasswd()
          } else {
            this.hideFormPasswd()
          }
        }
      }
    },
    connected: {
      get () {
        return this.isConnected
      },
      set (value) {
        this.setConnected({ value })
      }
    },
    authenticated: {
      get () {
        return this.isAuthenticated
      },
      set (value) {
        this.setAuthenticated({ value })
      }
    }
  },
  methods: {
    ...terminalMapActions({
      termSubmitCommand: 'submitCommand',
      termSubmitChatMessage: 'submitChatMessage',
      termConnect: 'connect'
    }),
    ...terminalMapMutations({
      showFormLogin: 'showFormLogin',
      hideFormLogin: 'hideFormLogin',
      showFormPasswd: 'showFormPasswd',
      hideFormPasswd: 'hideFormPasswd',
      setConnected: 'setConnected',
      setAuthenticated: 'setAuthenticated'
    })
  }
}

import { createNamespacedHelpers } from 'vuex'

const {
  mapMutations: mapMapMutations,
  mapActions: mapMapActions,
  mapGetters: mapMapGetters
} = createNamespacedHelpers('map')

export default {
  methods: {
    ...mapMapActions({
      upsertBlock: 'upsertBlock',
      generateDefaultDoors: 'generateDefaultDoors'
    }),
    ...mapMapMutations({
      // MAP
      setSize: 'setSize',
      setCell: 'setCell',
      setCoords: 'setCoords',
      // SELECTION
      setSelectCell: 'setSelectCell',
      // BLOC
      removeBlock: 'removeBlock',
      setDoorBlock: 'setDoorBlock'
    })
  },
  computed: {
    ...mapMapGetters({
      size: 'getSize',
      nCell: 'getNbCell',
      getCoords: 'getCoords',
      getSelectCell: 'getSelectCell',
      blocks: 'getBlocks',
      typeDoors: 'getTypeDoors',
      getDefaultDirections: 'getDefaultDirections'
    }),
    coords: {
      get: function () {
        return this.getCoords
      },
      set: function (xyz) {
        this.setCoords(xyz)
      }
    },
    selectCell: {
      get: function () {
        return this.getSelectCell
      },
      set: function (block) {
        this.setSelectCell(block)
      }
    }
  }
}

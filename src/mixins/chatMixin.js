import { createNamespacedHelpers } from 'vuex'

const {
  mapMutations: chatMapMutations,
  mapActions: chatMapActions,
  mapGetters: chatMapGetters
} = createNamespacedHelpers('chat')

export default {
  methods: {
    ...chatMapActions({
      chatWriteLine: 'writeLine'
    }),
    ...chatMapMutations({
      chatSetActiveScreen: 'setActiveScreen',
      chatStoreText: 'storeMessage',
      chatReplaceLastText: 'replaceLastText'
    })
  },
  computed: {
    ...chatMapGetters({
      chatGetActiveScreen: 'getActiveScreen',
      chatGetTabs: 'getTabs'
    })
  }
}

import A256 from '../../../libs/ansi-256-colors'

const a256 = new A256()

export default function () {
  return {
    screens: [
      {
        id: '#main',
        lines: [
        ],
        status: '',
        maxLineCount: 300,
        password: false
      }
    ],
    activeScreen: '#main'
  }
}

import Vue from 'vue'
import Vuex from 'vuex'
import connectorPlugin from './plugin/connector'
import screens from './screens'
import terminal from './terminal'
import map from './map'
import chat from './chat'

// import example from './assets-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      terminal,
      screens,
      map,
      chat
    },
    plugins: [connectorPlugin()],
    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEBUGGING
  })
  window.$STORE = Store
  return Store
}

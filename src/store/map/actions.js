/**
 * Ajoute ou modifie un bloc
 * @param commit {function}
 * @param getters {{}}
 * @param x {number}
 * @param y {number}
 * @param z {number}
 * @param types [string]
 */
export function upsertBlock ({
  commit,
  getters
}, {
  x,
  y,
  z,
  types
}) {
  const id = `block::x${x}y${y}z${z}`
  const oBlock = getters.getBlocks.find(b => b.id === id)
  if (oBlock) {
    commit('setBlock', {
      id,
      types
    })
  } else {
    commit('addBlock', {
      id,
      x,
      y,
      z,
      types
    })
  }
}

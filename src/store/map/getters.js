export const getSize = state => state.size
export const getNbCell = state => state.nCell
export const getCoords = state => {
  return {
    x: state.x,
    y: state.y,
    z: state.z
  }
}
export const getBlocks = state => state.blocks
export const getSelectCell = state => state.selectCell
export const getTypeDoors = state => state.typeDoors

export const getDefaultDirections = state => state.directions

export default function () {
  return {
    size: 64,
    nCell: 11,
    x: 0,
    y: 0,
    z: 0,
    typeDoors: [
      '',
      'unlock',
      'lock',
      'secret'
    ],
    directions: ['n', 's', 'e', 'w', 'ne', 'nw', 'se', 'sw', 'up', 'down'],
    selectCell: {},
    blocks: []
  }
}

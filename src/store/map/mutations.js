import ArrayHelper from 'app/libs/array-helper'

/**
 * Change la taille de la cellule
 * @param state {object}
 * @param size {number}
 */
export function setSize (state, size) {
  state.size = size
}

/**
 * Change la taille de la cellule
 * @param state {object}
 * @param nCell {number}
 */
export function setCell (state, nCell) {
  state.nCell = nCell | 1
}

/**
 * Ajoute un bloc dans la collection
 * @param state {object}
 * @param block {{id: string, x: number, y: number, z: number, type: [string]}}
 */
export function addBlock (state, block) {
  block.doors = {}
  state.blocks.push(block)
}

/**
 * Modifie un bloc (seulement les types et les doors) de la collection
 * @param state {object}
 * @param id {string}
 * @param types {[string]}
 * @param doors {{}}
 */
export function setBlock (state, {
  id,
  types,
  doors
}) {
  const block = state.blocks.find(b => b.id === id)
  block.types = types
  block.doors = doors || {}
  ArrayHelper.updateElement(state.blocks, block)
}

/**
 * Supprime un bloc de la collection de blocs
 * @param state {object}
 * @param id {string}
 */
export function removeBlock (state, { id }) {
  const block = state.blocks.find(b => b.id === id)
  ArrayHelper.remove(state.blocks, block)
}

/**
 * Ajout ou modifie une porte
 * @param state
 * @param id {string}
 * @param dir {string}
 * @param type {string}
 * @param level {number}
 */
export function setDoorBlock (state, {
  id,
  dir,
  type,
  level
}) {
  const block = state.blocks.find(b => b.id === id)
  if (block.types.includes('room')) {
    if (!block.doors) {
      block.doors = {}
    }
    block.doors[dir] = {
      type,
      level
    }
    ArrayHelper.updateElement(state.blocks, block)
  }
}

/**
 * Change les coordonnées courantes
 * @param state {object}
 * @param x {number}
 * @param y {number}
 * @param z {number}
 */
export function setCoords (state, {
  x,
  y,
  z
}) {
  state.x = x || state.x
  state.y = y || state.y
  state.z = z || state.z
}

/**
 * Modifie le bloc selectionné
 * @param state {object}
 * @param cell {number}
 */
export function setSelectCell (state, cell) {
  state.selectCell = cell
}

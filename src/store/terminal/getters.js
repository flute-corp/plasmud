export function isFormLoginVisible (state) {
  return state.visibility.formLogin
}

export function isFormPasswdVisible (state) {
  return state.visibility.formPasswd
}

export function isConnected (state) {
  return state.connected
}

export function isAuthenticated (state) {
  return state.authenticated
}

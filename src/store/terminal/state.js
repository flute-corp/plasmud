export default function () {
  return {
    visibility: {
      formLogin: false,
      formPasswd: false
    },
    connected: false,
    authenticated: false
  }
}

/**
 * A command is submitted, this action is intercepted by terminal plugin
 */
export function submitCommand ({ dispatch }, { command, echo }) {
}

export function submitPassword ({ dispatch }, { password }) {
}

export function submitChatMessage ({ dispatch }, { channel, message }) {
}

export function connect ({ dispatch }) {
}

export function showFormLogin (state) {
  state.visibility.formLogin = true
}

export function hideFormLogin (state) {
  state.visibility.formLogin = false
}

export function showFormPasswd (state) {
  state.visibility.formPasswd = true
}

export function hideFormPasswd (state) {
  state.visibility.formPasswd = false
}

export function setConnected (state, { value }) {
  state.connected = !!value
  if (!value) {
    state.authenticated = false
  }
}

export function setAuthenticated (state, { value }) {
  state.authenticated = !!value
}

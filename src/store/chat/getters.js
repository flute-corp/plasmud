export const getActiveScreen = state => state.screens.find(s => s.id === state.activeScreen) || {}
export const getScreens = state => state.screens
export const getTabs = state => state.screens.map(({ id, caption }) => ({ id, caption }))

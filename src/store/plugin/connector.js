import WSConnector from 'app/libs/ws-connector'
import quoteSplit from 'app/libs/quote-split'
import { SOCKET_PATH } from 'app/libs/socket-path'
import TERM_CONSTS from 'app/libs/term-consts'
import Ansi256 from 'app/libs/ansi-256-colors'

const TERM_MAIN_ID = '#main'

class StoreIO {
  constructor (store) {
    this._store = store
  }

  /**
   * Commit a mutation
   * @param namespace {string} namespace to commit to
   * @param mutation {string} mutation type
   * @param payload {object} payload object
   */
  commit (namespace, mutation, payload) {
    this._store.commit(namespace + '/' + mutation, payload)
  }

  /**
   * dispatch an action to store
   * @param namespace {string} namespace to dispatch to
   * @param action {string} action type
   * @param payload {object} payload object
   * @return {Promise}
   */
  dispatch (namespace, action, payload) {
    return this._store.dispatch(namespace + '/' + action, payload)
  }

  /**
   * return a getter's value
   * @param namespace {string} namespace to query to
   * @param name {string} getter name
   * @return {*}
   */
  getter (namespace, name) {
    return this._store.getters[namespace + '/' + name]
  }

  /**
   * print a line of text on terminal
   * @param screen {string} what screen ?
   * @param text {string} text to print
   * @returns {Promise}
   */
  termPrint (screen, text) {
    return this.dispatch('screens', 'writeLine', {
      screen,
      text
    })
  }

  async termError (text) {
    const sCurrScreen = this.getter('getActiveScreen').id
    if (sCurrScreen !== TERM_MAIN_ID) {
      await this.termPrint(sCurrScreen, text)
    }
    return this.termPrint(TERM_MAIN_ID, text)
  }

  getTermActiveScreen () {
    const oScreen = this.getter('screens', 'getActiveScreen')
    if (oScreen) {
      return oScreen.id
    } else {
      throw new Error('there is no active screen')
    }
  }
}

class CommandRepository {
  constructor (storeIO) {
    this._connector = new WSConnector()
    this._store = storeIO
    this._username = ''
    this._ansi256 = new Ansi256()

    const sSocketPath = this._connector.path = SOCKET_PATH || null
    if (sSocketPath) {
      console.info('socket path is', sSocketPath)
      this._connector.path = sSocketPath
    }

    this._connector.events.on('disconnect', () => {
      console.warn('Connection with server has been closed.')
      this._store.termPrint('*', 'Connection with server has been closed.')
      this._store.commit('terminal', 'setConnected', { value: false })
    })

    this._connector.events.on('message', ({ content }) => {
      // déterminer pour qui ce message est destiné
      const aWords = content.split(' ')
      if (aWords.length > 0) {
        switch (aWords[0].toUpperCase()) {
          case 'CHAT:JOIN': {
            const channel = aWords[1]
            return this._store.dispatch('chat', 'createScreen', { id: channel, go: true })
          }
          case 'CHAT:LEAVE': {
            const channel = aWords[1]
            return this._store.dispatch('chat', 'destroyScreen', { screen: channel })
          }
          case 'CHAT:MSG': {
            const channel = aWords[1]
            const content = aWords.slice(2).join(' ')
            return this._store.dispatch('chat', 'writeLine', { screen: channel, text: content })
          }
          default: {
            return this._store.termPrint(TERM_MAIN_ID, content)
          }
        }
      }
    })

    /**
     * Lorsque le client reçoi cet ordre il affiche un message dans le screen spécifié
     * @param screen {string} identifiant du screen dans lequel afficher la chaine
     * @param content {string} message à afficher.
     */
    this._connector.events.on(TERM_CONSTS.TERM_PRINT, ({
      screen = null,
      content
    }) => {
      this._store.termPrint(screen, content)
    })
  }

  async _send (sCommand, ...aArgs) {
    if (this._connector.connected) {
      return this._connector.send(sCommand + ' ' + aArgs.join(' '))
    }
  }

  async _connect () {
    try {
      if (this._connector.connected) {
        return
      }
      if (!this._connector.connecting) {
        await this._store.termPrint(TERM_MAIN_ID, 'Connecting...')
        await this._connector.connect()
        this._store.commit('terminal', 'setConnected', { value: true })
        await this._store.termPrint(TERM_MAIN_ID, this._ansi256.render('[#2F2]Connected to ' + this._connector.remoteAddress))
      } else {
        this._store.termPrint(TERM_MAIN_ID, 'Still attempting to connect...')
      }
    } catch (e) {
      await this._store.termPrint(TERM_MAIN_ID, this._ansi256.render('[#D00]' + e.message))
    }
  }

  /**
   * Indentification de l'utilisateur.
   * @param sIdentifier {string} nom de compte de l'utilisateur
   * @param sPassword {string} Mot de passe
   */
  async cmdLogin (sIdentifier = '', sPassword = '') {
    if (sIdentifier === '') {
      this._store.commit('terminal', 'showFormLogin')
      return
    }
    this._username = sIdentifier
    await this._connect()
    return this._send('login', this._username, sPassword)
  }

  async cmdPasswd (sPassword) {
    try {
      if (this._connector.connected) {
        if (sPassword) {
          return this._send('passwd', sPassword)
        } else {
          this._store.commit('terminal', 'showFormPasswd')
        }
      }
    } catch (e) {
      await this._store.termPrint(TERM_MAIN_ID, this._ansi256.render('[#D00]' + e.message))
    }
  }

  parseCommandLine (sInput, ns) {
    const sLine = sInput.trim()
    const aParams = quoteSplit(sLine) // sLine.split(' ');
    // extract command
    const sCommand = aParams.shift().toLowerCase()
    const sMeth = ns + sCommand.charAt(0).toUpperCase() + sCommand.substring(1).toLowerCase()
    return {
      line: sLine,
      method: sMeth,
      command: sCommand,
      params: aParams
    }
  }

  async command (sInput, bEcho) {
    try {
      if (bEcho) {
        await this._store.termPrint(TERM_MAIN_ID, this._ansi256.render('[#FF4] > ' + sInput))
      }
      const { params, command, method } = this.parseCommandLine(sInput, 'cmd')
      // send command to server
      if (method in this) {
        await this[method](...params)
      } else {
        await this._send(command, ...params)
      }
    } catch (e) {
      await this._store.termPrint(TERM_MAIN_ID, this._ansi256.render('[#D00]' + e.message))
      console.error(sInput, 'has been rejected :', e.message)
      console.error(e)
    }
  }

  cmdChatJoin (sCurrentChannel, sChannel) {
    return this._send('chat-join', sChannel)
  }

  cmdChatLeave (sCurrentChannel) {
    if (sCurrentChannel === 'lobby') {
      this._store.dispatch('chat', 'writeLine', {
        screen: sCurrentChannel,
        text: this._ansi256.render('[#FF4] lobby is the main channel and cannot be quit.')
      })
    } else {
      return this._send('chat-leave', sCurrentChannel)
    }
  }

  cmdChatMsg (sCurrentChannel, sDestUserId, ...message) {
    return this._send('chat-msg', sDestUserId, ...message)
  }

  async chatCommand (sInput, sChannel) {
    try {
      sInput = sInput.trim()
      const bInitialSlash = sInput.startsWith('/')
      if (bInitialSlash) {
        // chat command like /join /msg ...
        sInput = sInput.substring(1) // remove /
      } else {
        // normal txat message : must prepend "say" !
        sInput = 'say ' + sInput
      }
      const { params, command, method } = this.parseCommandLine(sInput, 'cmdChat')
      // send command to server
      params.unshift(sChannel)
      if (method in this) {
        await this[method](...params)
      } else {
        await this._send('chat/' + command, ...params)
      }
    } catch (e) {
      console.error(sInput, 'has been rejected :', e.message)
      console.error(e)
    }
  }

  async autoexec (aCommands) {
    const pause = t => new Promise(resolve => {
      setTimeout(resolve, t)
    })
    await pause(300)
    for (const c of aCommands) {
      await this.command(c)
      await pause(200)
    }
  }
}

export default function main () {
  return store => {
    const storeIO = new StoreIO(store)
    const cmd = new CommandRepository(storeIO)

    store.subscribeAction(async (action) => {
      switch (action.type) {
        case 'terminal/submitCommand':
          await cmd.command(action.payload.command, action.payload.echo)
          if (action.payload.command.split(' ').shift() === 'login') {
            await cmd.autoexec([
              'enter',
              'look'
            ])
          }
          break

        case 'terminal/submitChatMessage':
          await cmd.chatCommand(action.payload.message, action.payload.channel)
          break
      }
    })
  }
}

# Erreurs fréquentes

## Ordre des plugin MUD
L'ordre de chargement des plugin est important. Il faut charger en premier
les assets. Certains plugins nécessite d'avoir accès aux blueprints pour pouvoir
travailler dessus...
En tout cas ça été le cas pour ADVD205E.

## Problème de blueprints lors d'une interaction ADVD205E et Mud
N'oubliez pas que des blueprint créé du coté ADVD205E doivent etre créé du coté MUD.
Sinon lors des phase d'équipement, cela manque.

## Attention type d'item
Seuls certains type d'item sont créable par ADV.
Mettre a jour la liste au besoin.
Par exemple j'ai eu des soucis avec les bouclier.
j'avais oublié de mettre le type 'shield'

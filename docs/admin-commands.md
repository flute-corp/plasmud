# Commandes d'administration

## Commandes

### close
Lorsque le service est démarré, les clients peuvent s'y connecter. Mais il est possible
de fermer l'accès aux clients et d'éjecter les clients actuellement connectés.
Cela permet de procéder à des commandes d'administration ou de maintenance nécessite
l'absence de clients connectés.
La commande close permet de fermer le service aux connexions clients et éjecte les clients
présentement connectés.
```
npm run admin -- close
```

### open
Permet d'ouvrir ou fermer l'accès aux connexions clients si les services a été précédemment
fermé par la commande close.
```
npm run admin -- open
```

### status
Affiche l'état du service
```
npm run admin -- status
```


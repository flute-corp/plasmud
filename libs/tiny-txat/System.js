const Events = require('events')
const util = require('util')
const User = require('./User')
const Channel = require('./Channel')

class System {
  constructor () {
    this._users = {}
    this._channels = {}
    this._events = new Events()
    this._channelNames = {}
  }

  on (sEvent, pHandler) {
    this._events.on(sEvent, pHandler)
    return this
  }

  get channels () {
    return this._channels
  }

  channelPresent (c) {
    return c.id in this._channels
  }

  userPresent (u) {
    return u.id in this._users
  }

  _eventUserJoins (event) {
    // transmettre l'évènement à tous les utilisateurs du canal
    this._events.emit('log', { message: util.format('%s joins channel %s', event.user.display, event.channel.display) })
    event.channel.users.forEach(u => {
      this._events.emit('user-joined', {
        to: u.id,
        uid: event.user.id,
        cid: event.channel.id
      })
    })
  }

  _eventUserLeaves (event) {
    this._events.emit('log', { message: util.format('%s leaves channel %s', event.user.display, event.channel.display) })
    event.channel.users.forEach(u =>
      this._events.emit('user-left', {
        to: u.id,
        uid: event.user.id, // le user est sur le point de disparaitre
        uname: event.user.name,
        cid: event.channel.id
      })
    )
  }

  _eventUserGotMessage (event) {
    this._events.emit('user-message', {
      to: event.to.id,
      uid: event.from.id,
      cid: event.channel ? event.channel.id : null,
      message: event.message
    })
  }

  createUser ({ id = '', name = '' }) {
    return new User({ id, name })
  }

  createChannel ({ id = '', name = '' }) {
    return new Channel({ id, name })
  }

  addUser (u) {
    this._events.emit('log', { message: util.format('%s is registered', u.display) })
    if (!this.userPresent(u)) {
      this._users[u.id] = u
      u.on('message-received', event => this._eventUserGotMessage(event))
    } else {
      throw new Error('ERR_TXAT_USER_ALREADY_REGISTERED: ' + u.display + ' is already registered on the system')
    }
  }

  dropUser (u) {
    this._events.emit('log', { message: util.format('%s is dropped', u.display) })
    if (this.userPresent(u)) {
      // remove from all channels
      this.getUserChannels(u).forEach(c => {
        c.dropUser(u)
        if (c.users.length === 0) {
          this.dropChannel(c)
        }
      })
      delete this._users[u.id]
    }
  }

  /**
   * Renvoie la liste des canaux auxquels le user est connecté
   * @param u
   */
  getUserChannels (u) {
    return Object.values(this._channels).filter(function (c) {
      return c.userPresent(u)
    })
  }

  getUser (id) {
    if (id in this._users) {
      return this._users[id]
    } else {
      throw new Error('ERR_TXAT_USER_NOT_FOUND: user not found : "' + id + '"')
    }
  }

  addChannel (c) {
    if (!c.id) {
      throw new Error('ERR_TXAT_CHANNEL_INVALID_ID: cannot register channel : it has no valid identifier')
    }
    if (!c.name) {
      throw new Error('ERR_TXAT_CHANNEL_INVALID_NAME: cannot register channel : it has no valid name')
    }
    if (this.hasChannel(c.id)) {
      throw new Error('ERR_TXAT_CHANNEL_ID_ALREADY_USED: cannot register ' + c.display + ' : channel-id is already in use')
    }
    if (this.searchChannel(c.name)) {
      throw new Error('ERR_TXAT_CHANNEL_NAME_ALREADY_USED: cannot register ' + c.display + ' : channel name is already in use')
    }
    if (this.channelPresent(c)) {
      throw new Error('ERR_TXAT_CHANNEL_INSTANCE_ALREADY_REGISTERED: cannot register ' + c.display + ' : already registered')
    }
    c.on('namechanged', ({ name, oldname }) => {
      delete this._channelNames[oldname]
      this._channelNames[name] = c
    })
    this._events.emit('log', { message: 'adding new ' + c.display })
    this._channels[c.id] = c
    this._channelNames[c.name] = c
    c.on('user-added', event => this._eventUserJoins(event))
    c.on('user-dropped', event => this._eventUserLeaves(event))
  }

  getChannel (id) {
    if (this.hasChannel(id)) {
      return this._channels[id]
    } else {
      throw new Error('ERR_TXAT_CHANNEL_NOT_FOUND: channel not found : "' + id + '"')
    }
  }

  hasChannel (id) {
    return id in this._channels
  }

  searchChannel (sName) {
    return this._channelNames[sName]
  }

  dropChannel (c) {
    this._events.emit('log', { message: 'dropping ' + c.display })
    if (c.types.has('persistent')) {
      this._events.emit('log', { message: c.display + ' has persistent type and will not be dropped' })
      return
    }
    if (this.channelPresent(c)) {
      c.purge()
      delete this._channels[c.id]
      delete this._channelNames[c.name]
      this._events.emit('channel-dropped', { channel: c })
    } else {
      throw new Error('ERR_TXAT_CHANNEL_NOT_REGISTERED: cannot drop ' + c.display + ' : not registered')
    }
  }
}

module.exports = System

const Events = require('events')

class User {
  /**
     *
     * @param id {string|null}
     * @param name {string|null}
     */
  constructor ({ id = '', name = '' }) {
    this._id = id
    this._sName = name
    this._events = new Events()
    this._data = {}
  }

  get data () {
    return this._data
  }

  on (sEvent, pHandler) {
    this._events.on(sEvent, pHandler)
    return this
  }

  /**
     * renvoie identifiant du user
     * @returns {string}
     */
  get id () {
    return this._id
  }

  /**
     * défini identifiant du user
     * @param value {string}
     */
  set id (value) {
    this._id = value
  }

  /**
     * renvoie nom du user
     * @returns {string}
     */
  get name () {
    return this._sName
  }

  /**
     * défini nom du user
     * @param value {string}
     */
  set name (value) {
    this._sName = value
  }

  idName () {
    return { id: this.id, name: this.name }
  }

  /**
     * Affiche une ligne de description pour les logs
     * @return {string}
     */
  get display () {
    return 'user::' + this.id + ' (' + this.name + ')'
  }

  /**
   * Envoie un message à un utilisateur ou un canal
   * @param oDestination
   * @param sMessage
   */
  sendMessage (oDestination, sMessage) {
    oDestination.transmitMessage(this, sMessage)
  }

  transmitMessage (uFrom, sMessage, cFrom = null) {
    this._events.emit('message-received', { from: uFrom, to: this, channel: cFrom, message: sMessage })
  }
}

module.exports = User

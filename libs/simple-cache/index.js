class SimpleCache {
  constructor () {
    this._CACHE = {}
  }

  storeValue (sId, value) {
    this._CACHE[sId] = value
  }

  getValue (sId) {
    if (sId in this._CACHE) {
      return this._CACHE[sId]
    } else {
      return undefined
    }
  }

  deleteValue (sId) {
    delete this._CACHE[sId]
  }

  clear () {
    const c = this._CACHE
    const aKeys = Object.keys(c)
    aKeys.forEach(s => {
      delete c[s]
    })
  }
}

module.exports = SimpleCache

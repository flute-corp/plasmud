const EventManager = require('events')
/**
 * Gestion d'un repertoire de commands
 */
class Scriptorium {
  constructor () {
    this._routes = {}
    this._defaultContext = {}
    this._events = new EventManager()
  }

  get events () {
    return this._events
  }

  /**
   * Renvoie le nombre de routes de commands défini (par defineRoutes ou addRoute)
   * @returns {number} nombre de route de commands
   */
  get count () {
    return Object.keys(this._routes).length
  }

  /**
   * Définit en un seul appel plusieur routes
   * l'objet spécifié associe une route (identifiant) de script, au script lui même (type function)
   * @param oRoutes {object}
   * @return {object} ensemble des commands défini jusqu'a présents
   */
  defineRoutes (oRoutes) {
    return Object.assign(this._routes, oRoutes)
  }

  /**
   * Renvoie le script correspondant au nom de route spécifié
   * @param sRoute {string}
   * @returns {function}
   */
  getRoute (sRoute) {
    return this._routes[sRoute]
  }

  /**
   * Ajoute une commande et défini la route permettant de la lancer
   * @param sRoute {string}
   * @param pCommand {function}
   */
  addRoute (sRoute, pCommand) {
    this._routes[sRoute] = pCommand
  }

  /**
     * Fabrique une contexte contenant les trucs suivants :
     * un contexte par défaut
     * un contexte additionel
     * les fonctions get, put, post, delete
     * @param context
     * @returns {*}
     */
  composeContext (context = {}) {
    const command = (sCommand, ...args) => this.runScript(sCommand, context, ...args)
    command.exists = sCommand => this.scriptExists(sCommand)
    return {
      ...this._defaultContext,
      ...context,
      command
    }
  }

  /**
   * indique si la route existe (aurai dû s'appeler routeExists)
   * @param sId {string} nom de la route
   * @returns {boolean} true = le script existe
   */
  scriptExists (sId) {
    return sId in this._routes
  }

  /**
     * exécute un script
     * @param sId {string} identifiant du script
     * @param context {object}
     * @param params []
     * @returns {*}
     */
  async runScript (sId, context, ...params) {
    if (this.scriptExists(sId)) {
      const script = this._routes[sId]
      const oACL = {
        command: sId,
        params,
        context,
        allowed: true
      }
      this._events.emit('run', oACL)
      const bAllowed = oACL.allowed instanceof Promise
        ? await oACL.allowed
        : oACL.allowed
      if (bAllowed) {
        const oReturn = script(this.composeContext(context), ...params)
        return oReturn instanceof Promise
          ? await oReturn
          : oReturn
      } else {
        throw new Error('ERR_ACCESS_DENIED')
      }
    } else {
      throw new Error('ERR_INVALID_SCRIPT_ROUTE')
    }
  }

  /**
   * exécute un script
   * @param sId {string} identifiant du script
   * @param context {object}
   * @param params []
   * @returns {*}
   */
  runScriptSync (sId, context, ...params) {
    if (this.scriptExists(sId)) {
      const script = this._routes[sId]
      const oACL = {
        command: sId,
        params,
        context,
        allowed: true
      }
      this._events.emit('run', oACL)
      const bAllowed = oACL.allowed
      if (bAllowed) {
        return script(this.composeContext(context), ...params)
      } else {
        throw new Error('ERR_ACCESS_DENIED')
      }
    } else {
      throw new Error('ERR_INVALID_SCRIPT_ROUTE')
    }
  }

  /**
   * Renvoie toutes les route et les commands associés
   * @returns {*|{}}
   */
  get routes () {
    return this._routes
  }

  /**
   * définit les routes d'un coup, détruit les anciennes routes précédemment défini
   * @param value
   */
  set routes (value) {
    this._routes = {}
    this.defineRoutes(value)
  }

  /**
   * Renvoie le contexte par défaut
   * @returns {*|{}}
   */
  get defaultContext () {
    return this._defaultContext
  }

  /**
   * défini le contexte par défaut
   * Ce contexte est envoie comme premier paramètre aux commands appelés
   * @param value {object}
   */
  set defaultContext (value) {
    this._defaultContext = value
  }
}

module.exports = Scriptorium

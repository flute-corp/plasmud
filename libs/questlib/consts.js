const QUEST_STATUS = {
  EVOKED: 'QUEST_STATUS_EVOKED', // Quête evoquée par un pnj, mais non acceptée (les quêtes importantes peuvent avoir un chapitre dédié)
  ACCEPTED: 'QUEST_STATUS_ACCEPTED', // Quête acceptée par le joueur
  REJECTED: 'QUEST_STATUS_REJECTED', // Quête rejetée par le joueur. Celui-ci ne veux pas la faire
  COMPLETED: 'QUEST_STATUS_COMPLETED', // Quête terminée avec succès.
  ABORTED: 'QUEST_STATUS_ABORTED', // Quête abandonnée par action du joueur
  FAILED: 'QUEST_STATUS_FAILED' // Quête échouée. Le joueur n'a pas réussi à la terminée (personnage-clé éliminé, temps écoulé, etc...)
}

module.exports = {
  QUEST_STATUS
}

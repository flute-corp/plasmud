const validate = require('../schema-validation')
const QUEST_SCHEMA = require('./schemas/quest.json')

function validateFunction (oDef) {
  return validate(oDef, QUEST_SCHEMA)
}

module.exports = {
  validate: validateFunction
}

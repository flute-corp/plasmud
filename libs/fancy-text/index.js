const ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWYYZ'
const FANCY_ORDER = ALPHABET + ALPHABET.toLowerCase()

function fancyLetter (s, nStyle = 0) {
  const i = FANCY_ORDER.indexOf(s)
  return i >= 0
    ? '\uD835' + String.fromCharCode(0xDC00 + i + 52 * nStyle)
    : s
}

function fancyString (s, nStyle = 0) {
  const sNormalized = s.normalize('NFD')
  let sOutput = ''
  for (let i = 0, l = sNormalized.length; i < l; ++i) {
    const sLetter = sNormalized.substring(i, i + 1)
    const sFancyLetter = fancyLetter(sLetter, nStyle)
    sOutput += sFancyLetter
  }
  return sOutput
}

module.exports = {
  fancy: fancyString,
  FANCY_STYLE_NORMAL: 0,
  FANCY_STYLE_ITALIC: 1,
  FANCY_STYLE_CURSIVE: 2,
  FANCY_STYLE_MANUSCRIPT: 4,
  FANCY_STYLE_GOTHIC: 5,
  FANCY_STYLE_DECO: 6,
  FANCY_STYLE_GOTHIC_BOLD: 7,
  FANCY_STYLE_SANS: 8,
  FANCY_STYLE_SANS_BOLD: 9,
  FANCY_STYLE_SANS_ITALIC: 10,
  FANCY_STYLE_SANS_ITALIC_BOLD: 11,
  FANCY_STYLE_FLAT: 11
}

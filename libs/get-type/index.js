function getType (x) {
  const tox = typeof x
  switch (tox) {
    case 'undefined': {
      return 'undefined'
    }
    case 'object': {
      if (x === null) {
        return 'null'
      }
      if (Array.isArray(x)) {
        return 'array'
      }
      if (x instanceof RegExp) {
        return 'regexp'
      }
      if (x instanceof Set) {
        return 'set'
      }
      if (x instanceof Date) {
        return 'date'
      }
      return 'object'
    }
    default: {
      return tox
    }
  }
}

const TYPE_TO_CHAR = {
  array: 'A',
  date: 'D',
  null: 'u',
  set: 'S',
  regexp: 'R'
}

function _convertTypeToSingleChar (sType) {
  if (sType in TYPE_TO_CHAR) {
    return TYPE_TO_CHAR[sType]
  }
  return sType.charAt(0)
}

function getTypes (...args) {
  /**
   * Types d'origine typeof
   *
   * b = boolean
   * f = function
   * n = number
   * o = object
   * u = null, undefined
   *
   * Instance de classes (Généré par new)
   *
   * A = array
   * D = date
   * R = regexp
   * S = Set
   */
  return args.map(x => _convertTypeToSingleChar(getType(x))).join('')
}

module.exports = {
  getType,
  getTypes
}

/**
 * Itération simple sur les clé de la collection spécifée
 * @param oObject {object}
 * @param fCallback {function}
 */
module.exports = function (oObject, fCallback) {
  const oObj = {}
  for (const sKey in oObject) {
    if (Object.prototype.hasOwnProperty.call(oObject, sKey)) {
      oObj[sKey] = fCallback(oObject[sKey], sKey, oObject)
    }
  }
  return oObj
}

const ansiParser = require('ansi-parser')
const EC = require('./escape-codes')
const Ansi256 = require('./index')
const a = new Ansi256()

const a1 = a.render('[#ff2]yellow string [#2ff]cyan line [#f2f]magenta line')
const a2 = a.parse(a1)

console.log(a1)
console.log(a2)
console.log(EC.fg256(EC.getAnsi256Code('#2ff')) + 'xxxxxxxx')

console.log(a.parse('\u001b[31;1;4mXYZ\u001b[0mxxx'))

console.log(a.render('abc[:u:b:i#f00]def[#]xyz'))
console.log(a.render('ab[#0f0]c[:d:u#f00]def[#]xy[#]z'))

const ESC = '\u001B'
const CSI = ESC + '['
const OUT = ESC + ']'
const FG8BIT = '38;5;'
const BG8BIT = '48;5;'
const ST_RESET = 0
const ST_BOLD = 1
const ST_DIM = 2
const ST_ITALIC = 3
const ST_UNDERLINE = 4
const ST_BLINK = 5
const ST_INVERSE = 7
const ST_HIDDEN = 8
const ST_STRIKE = 9
const RESET = CSI + ST_RESET + 'm'
const RESET_BG = CSI + '49m'
const RESET_FG = CSI + '39m'

function isEmpty (color) {
  return color === '' || color === null
}

function isRESET (color) {
  return color === RESET || color === RESET_FG || color === RESET_BG
}

function fg256 (color) {
  return isEmpty(color)
    ? ''
    : isRESET(color)
      ? color
      : CSI + FG8BIT + color.toString() + 'm'
}

function bg256 (color) {
  return isEmpty(color)
    ? ''
    : isRESET(color)
      ? color
      : CSI + BG8BIT + color.toString() + 'm'
}

function getColorCode (r, g, b) {
  if (r === g && r === b) {
    // grayscale
    const gs = Math.round(25 * (r / 255))
    if (gs === 0) {
      return '0'
    }
    if (gs === 25) {
      return '15'
    }
    return (gs - 1 + 232).toString()
  }
  const r6 = Math.round(5 * r / 255)
  const g6 = Math.round(5 * g / 255)
  const b6 = Math.round(5 * b / 255)
  return (r6 * 36 + g6 * 6 + b6 + 16).toString().padStart(3, '0')
}

function parseRGB (sColor) {
  if (sColor.startsWith('#')) {
    sColor = sColor.substring(1)
  }
  if (sColor.length === 3) {
    sColor = sColor.charAt(0) + sColor.charAt(0) + sColor.charAt(1) + sColor.charAt(1) + sColor.charAt(2) + sColor.charAt(2)
  }
  const nColor = parseInt('0x' + sColor)
  const r = (nColor >> 16) & 0xFF
  const g = (nColor >> 8) & 0xFF
  const b = nColor & 0xFF
  return { r, g, b }
}

function getAnsi256Code (sColor) {
  const bStartsZero = sColor.startsWith('0')
  const ac = parseInt(sColor)
  if (bStartsZero || isNaN(ac)) {
    const { r, g, b } = parseRGB(sColor)
    return getColorCode(r, g, b)
  } else {
    return ac.toString()
  }
}

function color (sFg, sBg) {
  if (sBg === undefined) {
    return fg256(getAnsi256Code(sFg))
  }
  if (sFg === null || sFg === '') {
    return bg256(getAnsi256Code(sBg))
  }
  return fg256(getAnsi256Code(sFg)) + bg256(getAnsi256Code(sBg))
}

module.exports = {
  color,
  getColorCode,
  parseRGB,
  getAnsi256Code,
  fg256,
  bg256,
  RESET,
  RESET_FG,
  RESET_BG,
  ST_RESET,
  ST_BOLD,
  ST_DIM,
  ST_ITALIC,
  ST_UNDERLINE,
  ST_BLINK,
  ST_INVERSE,
  ST_HIDDEN,
  ST_STRIKE,
  CSI
}

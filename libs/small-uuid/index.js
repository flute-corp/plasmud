const { createGenerator } = require('../rnd-seq-36')

let nSeqNumber = null

const genSeq1296 = createGenerator(2, 733)

function getNextSeqNumber () {
  if (nSeqNumber === null) {
    nSeqNumber = Math.floor(Math.random() * 1296)
  }
  return ++nSeqNumber
}

function generate () {
  const nSeconds = Math.floor(Date.now() / 1000)
  const nCounter = getNextSeqNumber()
  return nSeconds.toString(36) + genSeq1296(nCounter)
}

module.exports = generate

const i18next = require('i18next')

class I18n {
  constructor () {
    this._t = null
  }

  /**
   * Défini le langage parmis ceux définis. Cette fonction renvoie une Promesse pour permettre à i18n de
   * charger de nouvelle chaïne de caractères
   * @param value
   * @returns {Promise<function>}
   */
  setLang (value) {
    return i18next.changeLanguage(value)
  }

  /**
   * Renvoie le langage sélectionné précédemment avec setLang
   * @returns {string}
   */
  getLang () {
    return i18next.language
  }

  /**
   * Initialise le système. Défini les chaînes
   * @param oStrings {object} dictionnaire contenant toutes les chaînes dans toutes les langues
   * @returns {Promise<void>}
   */
  async init (oStrings) {
    this._t = await this._initializeI18n(oStrings)
  }

  exists (sKey) {
    return i18next.exists(sKey)
  }

  /**
   * Rendu d'une chaîne avec des paramètre de substitution qui peuvent être sous forme de tableau
   * (paramètre séquentiels) ou sous forme d'objet
   * Les paramètres séquentiellement ordonnés convienent à des chaînes au format sprintf
   * les paramètres sous forme d'objets conviennet aux chaîne formaté comme dans les moteur de template
   * @param sKey {string} clé de la chîne qu'on veut rendre
   * @param oParams {object} diste des paramètres
   * @returns {string}
   */
  render (sKey, oParams) {
    if (sKey.indexOf('\u00a0') >= 0) {
      return sKey
    }
    return this._t(sKey, {
      ...oParams,
      interpolation: { escapeValue: false }
    })
  }

  /**
   * Initialisation de i18n
   * @param oStrings {object}
   * @returns {Promise<function>}
   * @private
   */
  _initializeI18n (oStrings) {
    const resources = {}
    for (const lang in oStrings) {
      if (Object.prototype.hasOwnProperty.call(oStrings, lang)) {
        resources[lang] = {
          translation: oStrings[lang]
        }
      }
    }
    const oI18n = i18next.createInstance()
    return oI18n
      .init({
        lng: Object.keys(oStrings).shift(),
        debug: false,
        resources
      })
  }
}

module.exports = I18n

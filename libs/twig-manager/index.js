const Twig = require('twig')
const PromFS = require('../prom-fs')
const path = require('path')
const A256 = require('../ansi-256-colors')

class TwigManager {
  constructor () {
    this._count = 0
    this._ids = new Set()
    this._a256 = new A256()
  }

  get count () {
    return this._count
  }

  hasTemplate (sTemplate) {
    return this._ids.has(sTemplate)
  }

  async loadTemplate (sBasePath, filename) {
    const sTemplate = await PromFS.read(path.resolve(sBasePath, filename))
    return this.defineTemplate(filename, sTemplate)
  }

  defineTemplate (filename, sTemplate) {
    const dir = path.dirname(filename)
    const name = path.basename(filename, '.twig')
    const id = path.posix.join(dir, name).replace(/\\/g, '/')
    const sTplTrimmed = sTemplate
      .split('\n')
      .map(s => s.trim())
      .join('\n\n')
    const template = Twig.twig({ id, data: sTplTrimmed })
    if (!template.tokens) {
      throw new Error('twig template ' + template.id + ' could not be parsed')
    }
    this._ids.add(id)
    ++this._count
    return {
      id,
      template
    }
  }

  static getFilters () {
    return {
      /**
       * Padding à droite : la chaine de caractère est complétée par le caractère à droite
       * @param value {string} chaine du pipe
       * @param length {number} longueur finale voulue de la chaine
       * @param char {string} caractètre servant de remplissage
       * @returns {string} chaine à afficher
       */
      rpad: function (value, [length, char = ' '] = []) {
        return value.toString().replace(/ /g, ' ').padEnd(length, char)
      },

      /**
       * Padding à gauche : la chaine de caractère est complétée par le caractère à gauche
       * @param value {string} chaine du pipe
       * @param length {number} longueur finale voulue de la chaine
       * @param char {string} caractètre servant de remplissage
       * @returns {string} chaine à afficher
       */
      lpad: function (value, [length, char = ' '] = []) {
        return value.toString().replace(/ /g, ' ').padStart(length, char)
      },

      /**
       * Padding des deux cotés : la chaine de caractère est complétée par le caractère à droite et à gauche
       * @param value {string} chaine du pipe
       * @param length {number} longueur finale voulue de la chaine
       * @param char {string} caractètre servant de remplissage
       * @returns {string} chaine à afficher
       */
      spad: function (value, [length, char = ' '] = []) {
        const sInput = value.toString().replace(/ /g, ' ')
        const nLenInput = sInput.length
        const nLenLeft = length - nLenInput
        const nr = nLenLeft >> 1
        const nl = nLenLeft - nr
        const sr = char.toString().repeat(nr)
        const sl = char.toString().repeat(nl)
        return sl + sInput + sr
      },

      /**
       * Remplace tous les espace par des espaces insécables
       * @param value {string} chaine d'entrée
       * @returns {string} chaine de sortie
       */
      pre: function (value) {
        return value.toString().replace(/ /g, ' ')
      },

      /**
       * Remplace tous les sauts de ligne par des espaces, trim toutes les lignes avant concaténation
       * @param value
       */
      nl2space: function (value) {
        return value.toString().split('\n').map(s => s.trimStart()).join(' ')
      }
    }
  }

  static extendTwig () {
    const oRoundDirectionMapper = {
      common: 'round',
      floor: 'floor',
      ceil: 'ceil'
    }

    const oFilters = TwigManager.getFilters()

    for (const [sFilter, fFilter] of Object.entries(oFilters)) {
      Twig.extendFilter(sFilter, fFilter)
    }
  }

  /**
   * Indexe un ensemble de commands dans un emplacement donné.
   * @param sBasePath {string} emplacement des commands
   * @returns {Promise<object>}
   */
  async index (sBasePath) {
    const t1 = await PromFS.tree(sBasePath)
    const t2 = t1
      .filter(x => x.endsWith('.twig'))
      .map(x => this.loadTemplate(sBasePath, x))
    return Promise.all(t2)
  }

  render (id, locals) {
    const oTmpl = Twig.twig({ ref: id })
    if (oTmpl) {
      return oTmpl.render(locals)
        .trim()
        .split('\n')
        .filter(s => s !== '')
        .map(s => this._a256.render(s))
        .join('\n')
    } else {
      throw new Error('twig template not found : "' + id + '"')
    }
  }
}

TwigManager.extendTwig()

module.exports = TwigManager

function isDefined (s) {
  return typeof s !== 'undefined'
}

function isString (s) {
  return typeof s === 'string'
}

function isNumeric (s) {
  return isDefined(s) && !isNaN(s)
}

function checkString (s) {
  if (!isDefined(s)) {
    throw new Error('ERR_VALUE_UNDEFINED')
  }
  if (!isString(s)) {
    throw new TypeError('ERR_TYPE_MISMATCH')
  }
}

function checkNumeric (s) {
  if (!isNumeric(s)) {
    throw new TypeError('ERR_TYPE_MISMATCH')
  }
}

module.exports = {
  isDefined,
  isString,
  isNumeric,
  checkString,
  checkNumeric
}

const {
  VMScript,
  VM
} = require('vm2')
const path = require('path')
const EventEmitter = require('events')

/**
 * @classdesc This class allows to compile scripts from source strings during run time
 * It can be used to create generated script, or zip-extracted script.
 * The class is based en VM2 and the compiled scripts can use "console" and "require".
 * @class
 */
class DynamicScriptManager {
  constructor () {
    this._scripts = {
      scripts: {}, // regular scripts
      modules: {} // scripts that are "required" by other scripts
    }
    this._events = new EventEmitter()
    this._aliases = {}
  }

  get aliases () {
    return this._aliases
  }

  get events () {
    return this._events
  }

  /**
   * Compiles a scripts and returns a VMScript instance
   * @param id {string} script identifier
   * @param script {string} script source code
   * @returns {VMScript}
   */
  compileScript (id, script) {
    return new VMScript(script, id)
  }

  /**
   * Compile and register script for future use
   * acces by id
   * @param id {string} script identifier
   * @param script {string} script source code
   * @return {VMScript}
   */
  registerScript (id, script) {
    const s = this.compileScript(id, script)
    this._scripts.scripts[id] = s
    return s
  }

  resolveAlias (id) {
    const r = id.match(/^(@[^/\\]*)/)
    if (r) {
      const sAlias = r[1]
      if (sAlias in this._aliases) {
        const sAliasValue = this._aliases[sAlias]
        return sAliasValue + id.substring(sAlias.length)
      }
    }
    return id
  }

  /**
   * Called when a script "requires" another scripts
   * @param id {string} required script identifier
   * @param parent {string} parent script identifier (for path scanning purpose)
   * @returns {{}|*} whatever the script returns
   */
  requireScript (id, parent) {
    const sParentDir = path.dirname(parent)
    let sModule = ''
    if (id.startsWith('.')) {
      sModule = path.join(sParentDir, id)
      if (!sModule.endsWith('.js')) {
        sModule += '.js'
      }
    } else {
      sModule = id
    }
    const nReqType = sModule in this._scripts.modules
      ? 0
      : sModule in this._scripts.scripts
        ? 1
        : 2
    switch (nReqType) {
      case 0: {
        const payload = {
          id: sModule,
          type: 'internal',
          compiled: true
        }
        Object.freeze(payload)
        this._events.emit('require', payload)
        return this._scripts.modules[sModule]
      }

      case 1: {
        const payload = {
          id: sModule,
          type: 'internal',
          compiled: false
        }
        Object.freeze(payload)
        this._events.emit('require', payload)
        const r = this.runScript(this._scripts.scripts[sModule], {})
        this._scripts.modules[sModule] = r.module.exports
        return r.module.exports
      }

      case 2: {
        const payload = {
          id,
          type: 'external',
          compiled: true
        }
        this._events.emit('required', payload)
        try {
          payload.id = this.resolveAlias(payload.id)
          return require(payload.id)
        } catch (e) {
          console.error(e)
          throw e
        }
      }
    }
  }

  isScriptExists (script) {
    return script in this._scripts.scripts
  }

  /**
   * runs a script
   * @param script {string|VMScript} script identifier
   * @param oGlobals {object} an object containing all needed globals
   * @returns {*&{console: *, module: {exports: {}}, require: (function(*): {}|*)}}
   */
  runScript (script, oGlobals) {
    const bScriptParamStr = typeof script === 'string'
    const oScript = bScriptParamStr
      ? this._scripts.scripts[script]
      : script
    if (!(oScript instanceof VMScript)) {
      console.error(oScript)
      throw new Error('ERR_INVALID_SCRIPT: ' + script)
    }
    const vm = new VM({
      console: 'inherit',
      timeout: 1000,
      allowAsync: false
    })
    const module = {
      exports: {}
    }
    const g = {
      ...oGlobals,
      require: id => {
        return this.requireScript(id, script.filename)
      },
      console,
      module
    }
    vm.setGlobals(g)
    try {
      vm.run(oScript)
    } catch (e) {
      console.error('Error while running script:', script.filename)
      throw e
    }
    return g
  }
}

module.exports = DynamicScriptManager

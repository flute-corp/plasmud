const quoteSplit = require('../quote-split')

function parseValue (x) {
  switch (x) {
    case '****':
      return null

    case 'Infinity':
    case '\u221e':
      return Infinity

    case '-Infinity':
    case '-\u221e':
      return -Infinity

    default:
      return isNaN(x)
        ? x === '****'
          ? null
          : x
        : parseInt(x)
  }
}

function parse (sString) {
  const aLines = sString
    .replace(/\r/g, '')
    .replace(/\t/g, ' ')
    .split('\n')
    .map(s => s.trim())
    .filter(s => s !== '' && !s.startsWith('DEFAULT:')) // default are ignored
  const sSign = aLines.shift()
  const rSign = sSign.match(/^2DA V([0-9]+)\.([0-9]+)/i)
  if (!rSign) {
    throw new Error('bad 2DA format')
  }
  const sVerMax = parseInt(rSign[1])
  const sVerMin = parseInt(rSign[2])
  const aFields = quoteSplit(aLines.shift())
  const aOutput = []
  aLines
    .map(line => {
      const aValues = quoteSplit(line)
        .slice(1) // id is ignored
        .filter(sItem => sItem !== '')
        .map(parseValue)
      const oStruct = {}
      aValues.forEach((value, iValue) => {
        oStruct[aFields[iValue]] = value
      })
      aOutput.push(oStruct)
    })
  return aOutput
}

function index (a2DA, sColumn) {
  const oOutput = {}
  for (let iRow = 0, l = a2DA.length; iRow < l; ++iRow) {
    const oLine = {
      ...a2DA[iRow]
    }
    const id = oLine[sColumn]
    delete oLine[sColumn]
    oOutput[id] = oLine
  }
  return oOutput
}

module.exports = {
  parse,
  index
}

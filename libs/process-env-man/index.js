const dotenv = require('dotenv')

dotenv.config()

const _privateEnv = {}

function checkOne (sVariable, oDefinition) {
  const sValue = process.env[sVariable]
  const bRequired = Boolean(oDefinition.required)
  if (sValue === undefined) {
    if (bRequired) {
      throw new Error('ERR_REQUIRED_ENV_VARIABLE_MISSING: ' + sVariable)
    } else {
      return undefined
    }
  }
  if (oDefinition.pattern) {
    const rPattern = new RegExp(oDefinition.pattern)
    if (!sValue.match(rPattern)) {
      throw new Error('ERR_ENV_VARIABLE_INVALID_FORMAT: ' + sVariable)
    }
  }
  switch (oDefinition.type) {
    case 'boolean':
    case 'bool': {
      if (sValue.toLowerCase() === 'true') {
        return true
      }
      if (sValue.toLowerCase() === 'false') {
        return false
      }
      return Boolean(sValue)
    }

    case 'number': {
      const n = parseFloat(sValue)
      if (isNaN(n)) {
        throw new Error('ERR_ENV_VARIABLE_TYPE_MISMATCH: ' + sVariable)
      }
      return n
    }

    case 'int':
    case 'integer': {
      const n = Math.floor(parseFloat(sValue))
      if (isNaN(n)) {
        throw new Error('ERR_ENV_VARIABLE_TYPE_MISMATCH: ' + sVariable)
      }
      return n
    }

    case undefined: {
      return oDefinition.default
    }

    case 'string': {
      return sValue
    }

    default: {
      throw new Error('ERR_TYPE_INVALID: ' + oDefinition.type)
    }
  }
}

function checkAll (oSchema) {
  for (const [sVariable, oDefinition] of Object.entries(oSchema)) {
    _privateEnv[sVariable] = checkOne(sVariable, oDefinition)
  }
}

function getEnv (sVariable) {
  return _privateEnv[sVariable]
}

module.exports = {
  checkAll,
  getEnv
}

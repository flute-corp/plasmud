class TextRenderer {
  constructor ({ intl = null, tpl = null } = {}) {
    this._intl = intl
    this._tpl = tpl
  }

  get intl () {
    return this._intl
  }

  set intl (value) {
    this._intl = value
  }

  get tpl () {
    return this._tpl
  }

  set tpl (value) {
    this._tpl = value
  }

  render (sResRef, oVariables) {
    let sOutput = ''
    if (!this._tpl) {
      throw new Error('TextRenderer : template engine is not defined')
    }
    if (this._tpl.hasTemplate(sResRef)) {
      sOutput = this._tpl.render(sResRef, oVariables)
    } else {
      sOutput = this._intl.render(sResRef, oVariables) || sResRef
    }

    if (sResRef.includes('.error')) {
      sOutput = '[error] ' + sOutput
    }
    if (sResRef.includes('.info')) {
      sOutput = '[info] ' + sOutput
    }
    if (sResRef.includes('.warning')) {
      sOutput = '[warn] ' + sOutput
    }
    if (sResRef.includes('.failure')) {
      sOutput = '[fail] ' + sOutput
    }
    if (sResRef.includes('.hint')) {
      sOutput = '[hint] ' + sOutput
    }
    if (sResRef.includes('.action')) {
      sOutput = '[succ] ' + sOutput
    }
    if (sResRef.includes('.room')) {
      sOutput = '[room] ' + sOutput
    }
    return sOutput
  }
}

module.exports = TextRenderer

const Events = require('events')
const validateSchema = require('../schema-validation')
const DisposableIdRegistry = require('../disposable-id-registry')
const deepClone = require('../deep-clone')
const DidYouMean = require('@laboralphy/did-you-mean')
const SCHEMAS = {
  blueprints: require('./schemas/blueprints.json'),
  rooms: require('./schemas/rooms.json'),
  sectors: require('./schemas/sectors.json')
}
const {
  KeyNotFoundError,
  InvalidSchemasError
} = require('./errors')

const DIRECTION_CODES = ['n', 'e', 'w', 's', 'ne', 'nw', 'se', 'sw', 'u', 'd']
const CONSTS = require('./consts')
const ID_SEPARATOR = '#'
const SAVE_STRUCT_TYPE = 'save'
const FIND_BY_NAME_RELEVANCE = 234
const ENTITY_SPOTTED_REGISTRY = 'ENTITY_SPOTTED_REGISTRY'

/**
 * @typedef MUDLockDef {object} objet de définition des serrures pour les blueprints
 * @property locked {boolean} état initial du verrou
 * @property difficulty {number} difficulté à crocheter la serrure
 * @property discard {boolean} supprimer la clé après usage
 * @property key {string} tag de la clé qui déverrouille la serrure
 *
 * @typedef MUDItemAcquiredEvent {object} Un item a été acquis
 * @property engine {Engine}
 * @property entity {string} Identifiant de l'item ayant été acquis
 * @property acquiredBy {string} Identifiant de l'entité qui a acquis l'item
 * @property acquiredFrom {string} Identifiant de l'entité qui possédait l'item avant. Peut-être vide si l'item est
 * directement créé dans l'inventaire de l'entité qui a acquis l'item.
 *
 * @typedef MUDItemLoseEvent {objet} Une entité a perdu un item
 * @property engine {Engine}
 * @property entity {string} Identifiant de l'item ayant été perdu
 * @property lostBy {string} Identifiant de l'entité qui a perdu l'item
 * @property acquiredBy {string} Identifiant de l'entité qui a acquis l'item perdu
 *
 * @typedef MUDItemDropEvent {objet} Une entité lache un item sur le sol de la pièce
 * @property engine {Engine}
 * @property entity {string} Identifiant de l'item ayant été abandonné
 * @property droppedBy {string} Identifiant de l'entité qui a laché l'item
 * @property location {string} Identifiant de la pièce dans laquelle a été posé l'item
 *
 * @typedef MUDEntityEvents {object}
 * @property blocked {function({ engine: Engine, self: string, room: string, direction: string })}
 * @property conversation {function({ engine: Engine, self: string, pattern: string, speaker: string, volume: string })}
 * @property disturbed {function({ engine: Engine, self: string, disturber: string, item: string, type: string })}
 * @property heartbeat {function({ engine: Engine, self: string })}
 * @property perception {function({ engine: Engine, self: string, entity: string, type: string })}
 * @property spawn {function({ engine: Engine, self: string })}
 * @property lock {function({ engine: Engine, self: string, lockedBy: string })}
 * @property open {function({ engine: Engine, self: string, openedBy: string })}
 * @property openFailure {function({ engine: Engine, self: string, inspectedBy: string })}
 * @property unlock {function({ engine: Engine, self: string, actor: string })}
 * @property enter {function({ engine: Engine, self: string, enterer: string })}
 * @property exit {function({ engine: Engine, self: string, exiter: string })}
 *
 * @typedef MUDBlueprint {object}
 * @property tag {string} tag permettant d'identifier les entités construite avec ce blueprint comme ayant certaine props
 * @property name {string} nom affichable de l'entité, lorsque identifiée (pour des items)
 * @property desc {string[]} description affichable de l'entité, lorsque identifié (pour des items)
 * @property type {string} type d'entité (actor, player, item, placeable)
 * @property weight {number} poids de l'entité
 * @property subtype {string} ITEM : sous type d'objet
 * @property weight {number} ITEM : poids de l'item
 * @property stackable {boolean} ITEM : l'item peut-il être empilable
 * @property inventory {boolean} ITEM : l'item a-t-il un inventaire
 * @property lock {MUDLockDef} état du vérrou ---
 * @property events {MUDEntityEvents}
 * @property data {object} objet libre
 *
 * @typedef MUDEntity {object}
 * @property id {string} identifiant entité
 * @property uid {string} identifiant de l'application cliente
 * @property ref {string} référence du blueprint ayant servit à créer l'entité
 * @property name {string} nom affichable de l'entité
 * @property desc {string} description affichable de l'entité
 * @property blueprint {MUDBlueprint} blueprint complet
 * @property tag {string} tag de reconnaissance du blueprint (peut servir pour les quête)
 * @property remark {string} chaine descriptive courte supplémentaire pour cette entité
 * @property location {string} localisation de l'entité (id de la pièce dans laquelle elle se trouve)
 * @property weight {number} poids de l'entité
 * @property inventory {object} contenu de l'inventaire
 * @property data {object} objet libre de stockage d'info supplémentaire (non géré par MUD)
 * @property stack {number} ITEM: nombre d'élément dans la pile
 * @property locked {boolean} ITEM / PLACEABLE: entité vérrouillé. N'a de sens que si l'entité a un inventaire ---
 * @property dead {boolean} ACTOR: la creature est décédée (dans le sens : 0 HP)
 */

/**
 * @class Engine
 * @desc Moteur de gestion du MUD
 */
class Engine {
  constructor () {
    this._events = new Events()
    this._lastId = 0
    this._state = null
    this._validObjects = new Set() // les objets qui ont passé le json-validator sont stocké ici pour ne pas avoir à les revalider
    /**
     * Correspondance entre identifiant entité et identifiant joueur
     * @type {Object.<string, string>}
     * @private
     */
    this._players = {}
    this._initCustomEventEmitter()
  }

  /**
   * Correspondance entre identifiant entité et identifiant joueur
   * @returns {Object<string, string>}
   */
  get players () {
    return this._players
  }

  //               _     _ _           _      ____  ___
  //   _ __  _   _| |__ | (_) ___     / \    |  _ \|_ _|
  //  | '_ \| | | | '_ \| | |/ __|   / _ \   | |_) || |
  //  | |_) | |_| | |_) | | | (__   / ___ \ _|  __/ | | _
  //  | .__/ \__,_|_.__/|_|_|\___| /_/   \_(_)_| (_)___(_)
  //  |_|

  /**
   * Constantes utilisées par les différentes partie de l'application
   * @return {object}
   */
  static get CONST () {
    return CONSTS
  }

  get Const () {
    return CONSTS
  }

  /**
   * Liste des références de ressources définies
   * @returns {string[]}
   */
  get blueprintRefs () {
    if (this._state && !!this._state.blueprints) {
      return Object.keys(this._state.blueprints)
    } else {
      return []
    }
  }

  _pickEntityAutoIncId () {
    let sEntityId = ''
    do {
      sEntityId = Engine.composeId('entity', ++this._lastId)
    } while (this.isEntityExist(sEntityId))
    return sEntityId
  }

  /**
   * Création d'une entité.
   * @param sBlueprint {string} référence du blueprint (modèle) d'après lequel on construit l'entité
   * @param sLocation {string} localisation initiale, identifiant de la pièce ou du contenant où est crée l'entité
   * si la location est vide, l'entité sera créée mais ne sera pas intégrée au jeu, il faut utiliser la fonction
   * registerEntity()
   * @param count {number} nombre d'exemplaires dans la pile, pour le cas des objets empilables (flèche, or, potions...}
   * @param id {string} identifiant forcé
   * @returns {MUDEntity} instance de l'entité créée
   */
  createEntity (sBlueprint, sLocation, { id = '', count = 1 } = {}) {
    const bRegister = !!sLocation
    const idEntity = id === ''
      ? this._pickEntityAutoIncId()
      : id
    const oBlueprint = this.getBlueprint(sBlueprint)
    const engine = this
    const oEntity = {
      id: idEntity,
      ref: sBlueprint,
      blueprint: oBlueprint,
      tag: oBlueprint.tag,
      name: oBlueprint.name,
      desc: [...oBlueprint.desc],
      remark: '',
      location: '',
      _baseWeight: oBlueprint.weight || 0,
      inventory: oBlueprint.inventory ? {} : null,
      // ACTORS
      /**
       * @type {object}
       */
      data: 'data' in oBlueprint
        ? deepClone(oBlueprint.data)
        : {}
    }
    // ITEM
    if (oBlueprint.type === Engine.CONST.ENTITY_TYPES.ITEM) {
      Object.assign(oEntity, {
        stack: Math.max(0, count),
        locked: oBlueprint.lock ? oBlueprint.lock.locked : false
      })
    }
    if (oBlueprint.type === Engine.CONST.ENTITY_TYPES.ACTOR) {
      Object.assign(oEntity, {
        dead: false
      })
    }
    // PLACEABLE
    if (oBlueprint.type === Engine.CONST.ENTITY_TYPES.PLACEABLE) {
      Object.assign(oEntity, {
        locked: oBlueprint.lock ? oBlueprint.lock.locked : false
      })
    }
    Object.defineProperty(oEntity, 'weight', {
      enumerable: false,
      configurable: false,
      get: function () {
        const nInvWeight = this.inventory
          ? engine
            .getLocalEntities(this.id)
            .reduce((prev, curr) => curr.entity.weight + prev, 0)
          : 0
        const nMyWeight = this._baseWeight
        const nStack = this.blueprint.stackable ? this.stack : 1
        return nMyWeight * nStack + nInvWeight
      }
    })
    if (oBlueprint.inventory) {
      this.localIdRegistry[idEntity] = {
        [this._getMiniType(Engine.CONST.ENTITY_TYPES.ITEM)]: new DisposableIdRegistry('y')
      }
    }
    if (bRegister) {
      this._registerEntity(oEntity, sLocation)
    }
    return oEntity
  }

  /**
   * Destruction d'une entité et de tous les objets qu'elle possède.
   * @param idEntity {string} identifiant de l'entité à détruire
   */
  destroyEntity (idEntity) {
    const oEntities = this.getEntities()
    if (idEntity in oEntities) {
      const oEntity = oEntities[idEntity]
      this._events.emit('entity.destroy', {
        engine: this,
        entity: idEntity
      })
      // détruire les objets qu'il transportait
      if (oEntity.inventory) {
        Object.keys(oEntity.inventory).forEach(id => {
          this.destroyEntity(id)
        })
      }
      const idLocation = oEntity.location
      if (this.isRoomExist(idLocation)) {
        this._removeRoomEntity(idLocation, idEntity)
      } else if (this.isEntityExist(idLocation)) {
        this._removeInventoryEntity(idLocation, idEntity)
      }
      if (idEntity in this.localIdRegistry) {
        delete this.localIdRegistry[idEntity]
      }
      delete oEntities[idEntity]
    } else {
      throw new KeyNotFoundError(idEntity, 'entities')
    }
  }

  /**
   * Création d'un nouveau personnage joueur.
   * @param uid {string} identifiant externe de l'utilisateur (désigné par l'appli cliente) qui sera transformé en identifiant interne
   * @param cid {string} identifiant externe du personnage (désigné par l'appli cliente) qui sera transformé en identifiant interne
   * @param sName {string} nom du joueur
   * @param sLocation {string} identifiant de la pièce ou sera localisé l'entité
   * @returns {MUDEntity|null} instance de l'entité générée
   */
  createPlayerEntity (uid, cid, sName, sLocation = undefined) {
    // Vérifier si l'entité existe déja
    const idPlayer = cid // Engine.composeId('player', cid)
    if (this.isEntityExist(idPlayer)) {
      const oPrevEntity = this.getEntity(idPlayer)
      this._players[uid] = idPlayer
      this._events.emit('player.character.create', {
        id: idPlayer,
        uid
      })
      return oPrevEntity
    }
    // verifier si le nom est valide
    if (!sName.match(/^\S{2,20}$/)) {
      return null
    }
    if (sLocation === undefined) {
      if ('startLocation' in this._state.data) {
        sLocation = this._state.data.startLocation
      }
    }
    // checking location
    if (!sLocation) {
      throw new Error('Entity "' + sName + '" (' + uid + ') starting location is invalid')
    }
    this._players[uid] = idPlayer
    const oPlayer = this.getEntities()[idPlayer] = {
      id: idPlayer,
      uid,
      blueprint: {
        type: Engine.CONST.ENTITY_TYPES.PLAYER
      },
      desc: [
        'Description par défaut'
      ],
      name: sName,
      dead: false,
      location: '', // localisation (pièce) du joueur
      sector: '', // indique le secteur dans lequel le joueur est.
      inventory: {},
      skills: {},
      data: {},
      creation: Date.now()
    }
    this._events.emit('player.character.create', {
      id: idPlayer,
      uid
    })
    this.localIdRegistry[idPlayer] = {
      [this._getMiniType(Engine.CONST.ENTITY_TYPES.ITEM)]: new DisposableIdRegistry('u')
    }
    this._setEntityLocation(idPlayer, sLocation)
    return oPlayer
  }

  /**
   * Renvoie une collection objet de description de secteurs
   * @returns {object} totalité des secteurs existant dans le plasmud
   */
  getSectors () {
    return this.state.sectors
  }

  /**
   * Dictionnaire des entités
   * Dans le MUD les entités sont les objets mobiles qui ont des caractéristiques différentes selon leur type
   * Il y a 4 types d'entités : objet d'inventaire, créature, joueur, objet fixe.
   * @returns {object} totalité des entités présentes dans le plasmud
   */
  getEntities () {
    return this.state.entities
  }

  /**
   * structure de stockage des données des pièces
   * @returns {object} totalité des pièces créées dans le plasmud
   */
  getRooms () {
    return this.state.rooms
  }

  /**
   * objet blueprint correspondant à l'identifiant demandé
   * @param idBlueprint {string} identifiant du blueprint
   * @returns {object} instance du blueprint recherché
   */
  getBlueprint (idBlueprint) {
    return this._getValidObject(idBlueprint, 'blueprints', true)
  }

  /**
   * objet sector coorespondant à l'identifiant demandé
   * @param idSector {string} identifiant du secteur recherché
   * @returns {object} instance du secteur recherché
   */
  getSector (idSector) {
    return this._getValidObject(idSector, 'sectors', true)
  }

  /**
   * entité dont l'identifiant est spécifié en paramètre
   * @param idEntity {string} identifiant de l'entité recherchée
   * @returns {MUDEntity} instance de l'entité recherchée
   */
  getEntity (idEntity) {
    const oEntities = this.getEntities()
    if (idEntity in oEntities) {
      return oEntities[idEntity]
    } else {
      throw new KeyNotFoundError(idEntity, 'entities')
    }
  }

  /**
   * pièce dont l'identifiant est spécifié en paramètre
   * @param idRoom {string} identifiant de la pièce recherchée
   * @returns {object} instance de la pièce recherchée
   */
  getRoom (idRoom) {
    return this._getValidObject(idRoom, 'rooms', false)
  }

  /**
   * vrai si l'identifiant spécifié en paramètre correspond à une pièce définie
   * @param idRoom {string} identifiant pièce présumée
   * @returns {boolean} vrai si la pièce existe
   */
  isRoomExist (idRoom) {
    return idRoom in this.getRooms()
  }

  /**
   * vrai si l'identifiant spécifié correspond à une entité instanciée dans le jeu
   * @param idEntity {string} identifiant entité presumée
   * @returns {boolean} vrai si l'entité existe
   */
  isEntityExist (idEntity) {
    return idEntity in this.getEntities()
  }

  /**
   * vrai si l'identifiant spécifié identifie une entité de type Placeable qui possède un inventaire
   * @param idEntity {string} identifiant
   * @return {boolean}
   */
  isContainer (idEntity) {
    if (this.isEntityExist(idEntity)) {
      const oEntity = this.getEntity(idEntity)
      return oEntity.blueprint.inventory && (
        oEntity.blueprint.type === CONSTS.ENTITY_TYPES_PLACEABLE ||
        oEntity.blueprint.type === CONSTS.ENTITY_TYPES_ITEM
      )
    }
    return false
  }

  /**
   * Vrai si l'entité spécifiée est un joueur
   * @param idEntity {string} identifiant entité
   * @returns {boolean}
   */
  isPlayer (idEntity) {
    return this.isEntityExist(idEntity) && this.getEntity(idEntity).blueprint.type === CONSTS.ENTITY_TYPES_PLAYER
  }

  /**
   * Vrai si l'entité spécifiée est un actor
   * @param idEntity {string} identifiant entité
   * @returns {boolean}
   */
  isActor (idEntity) {
    if (this.isEntityExist(idEntity)) {
      const sType = this.getEntity(idEntity).blueprint.type
      return sType === CONSTS.ENTITY_TYPES_ACTOR || sType === CONSTS.ENTITY_TYPES_PLAYER
    } else {
      return false
    }
  }

  /**
   * Renvoie vrai si le paramètre spécifié est un code valide pour une direction
   * @param sDirection {string} identifiant à vérifier
   * @return {boolean} vrai si l'identifiant spécifié est une direction valide
   */
  isDirectionValid (sDirection) {
    return DIRECTION_CODES.includes(sDirection)
  }

  /**
   * Renvoie l'entité désignée par l'identifiant local au conteneur spécifié en paramètre
   * Si le joueur regarde dans un contenant, le local id sera l'un des objets contenus
   * si le lid spécifié ne correspond pas à une entité dans le contenant, la fonction renvoie null
   * @param idContainer {string} pièce ou entité possédant un inventaire qui contient l'entité désigné par le lid
   * @param lid {string} identifiant local de l'entité recherchée
   * @returns {object|null} entité trouvée, ou null si non trouvée
   */
  getLocalEntity (lid, idContainer) {
    if (this.isRoomExist(idContainer)) {
      return this._getRoomLocalEntity(idContainer, lid)
    } else if (this.isEntityExist(idContainer)) {
      return this._getInventoryLocalEntity(idContainer, lid)
    } else {
      return null
    }
  }

  /**
   * Renvoie la liste des entités présentes dans un conteneur ou une pièce.
   * @typedef InventoryEntry {object} structure décrivant une entrée d'un inventaire particulier.
   * Cette structure permet d'associer une entité à un local-id (lid) : un identifiant raccourci,
   * valide dans le contexte du conteneur. Le conteneur peut être une pièce entière, ou l'inventaire d'une entité
   * @property id {string} identifiant de l'entité contenue
   * @property lid {string} identifiant local de l'entité contenue
   * @property entity {MUDEntity} instance d el'entité contenue
   *
   * @param idContainer {string} identifiant du conteneur ou de la pièce
   * @returns {InventoryEntry[]} liste des entités présentes dans le contenant spécifié
   */
  getLocalEntities (idContainer) {
    let oInventory
    if (this.isRoomExist(idContainer)) {
      oInventory = this._getRoomEntityStorage(idContainer)
    } else if (this.isEntityExist(idContainer)) {
      const oContOwner = this.getEntity(idContainer)
      oInventory = oContOwner.inventory
    } else {
      throw new KeyNotFoundError(idContainer, 'entities/rooms')
    }
    if (!oInventory) {
      return []
    }
    const aEntities = []
    for (const idEntity in oInventory) {
      if (Object.prototype.hasOwnProperty.call(oInventory, idEntity)) {
        const lid = oInventory[idEntity]
        const i = parseInt(lid.match(/[0-9]+$/)[0])
        aEntities.push({
          id: idEntity,
          lid,
          i
        })
      }
    }
    return aEntities
      .sort((a, b) => a.i - b.i)
      .map(({
        id,
        lid
      }) => ({
        lid,
        id,
        entity: this.getEntity(id)
      }))
  }

  /**
   * Déplacement d'une entité vers une nouvelle destination, cela peut être un inventaire (pour les items uniquement)
   * ou une nouvelle pièce (pour les personnages).
   * @param idEntity {string} identifiant de l'objet à ramasser
   * @param idReceiver {string} identifiant de l'entité qui effectue l'action
   * @param [count=Infinity] {number} nombre d'exemplaire à prendre
   * @return {object|null} instance de l'objet transféré à titre purement informatif (parfois c'est un clone)
   */
  moveEntity (idEntity, idReceiver, count = Infinity) {
    // vérifier la nature de l'entité
    if (count < 1) {
      return null
    }
    const oEntity = this.getEntity(idEntity)
    if (oEntity.location === idReceiver) {
      return null
    }
    const bToRoom = this.isRoomExist(idReceiver)
    if (oEntity.blueprint.type !== Engine.CONST.ENTITY_TYPES.ITEM) {
      // l'entité n'est pas un objet stockable dans un inventaire
      // donc l'idReeiver doit etre une pièce : un simple setEntityLocation suffit
      if (bToRoom) {
        this._setEntityLocation(idEntity, idReceiver)
        return oEntity
      }
      return null
    }
    const oItem = oEntity
    let oRecvInventory
    if (bToRoom) {
      oRecvInventory = this._getRoomEntityStorage(idReceiver)
    } else if (this.isEntityExist(idReceiver)) {
      const oReceiver = this.getEntity(idReceiver)
      if (oReceiver.inventory) {
        oRecvInventory = oReceiver.inventory
      } else {
        throw new Error('This entity has no inventory, and cannot receive any item : "' + oReceiver.id + '" blueprint ref : "' + oReceiver.ref + '"')
      }
    } else {
      throw new KeyNotFoundError(idReceiver, 'entities/rooms')
    }
    // l'entité bénéficiaire a bien un inventaire
    // on ne dépasse pasles borne, on corrige le parametre
    count = Math.max(1, Math.min(oItem.stack, count))
    if (oItem.blueprint.stackable) {
      // cas où l'élément convoité contient davantage d'exemplaires que ce qu'on veut prendre
      // dans ce cas on va laisser l'élément dans son ancien inventaire, on réduit sa pile
      // on le clone dans le nouvel inventaire
      const oReturn = this._cloneEntity(oItem, false)
      oReturn.stack = count
      // l'item qu'on veut déposer est stackable
      // recherche une éventuelle pile d'objet du meme blueprint
      const oInvItem = this._findEntityStack(oItem, oRecvInventory)
      oItem.stack -= count
      if (oItem.stack <= 0) {
        this.destroyEntity(oItem.id)
      }
      if (oInvItem) {
        // une pile du même type d'objet existe déja dans l'inventaire
        // additionner les piles
        // on ne veut qu'une partie de la pile
        oInvItem.stack += count
      } else {
        // aucune pile du même type déja dans l'inv.
        // il faut créer notre propre pile
        // créer un clone de la pile d'origin
        const oClone = this._cloneEntity(oItem, false)
        oClone.stack = count
        this._registerEntity(oClone, idReceiver)
        if (!this._setEntityLocation(oClone.id, idReceiver)) {
          return null
        }
      }
      return oReturn
    } else {
      if (this._setEntityLocation(idEntity, idReceiver)) {
        return oItem
      }
    }
    return null
  }

  /**
   * Renvoie les renseignements concernant une porte.
   * @typedef ExitStatus {object} Etat d'une porte
   * @property name {string} Nom affichable de l'issue
   * @property desc {string[]} Description de la pièce
   * @property valid {boolean} Validité de la porte. Faux = la porte n'existe pas et l'état n'est pas valide.
   * @property dcLockpick {number} Difficulté de l'action de crochetage.
   * @property dcSearch {number} Difficulté de l'action de recherche si la porte est camouflée.
   * @property visible {boolean} La porte est visible par le joueur qui a intérrogé l'état
   * @property secret {boolean} La porte est camouflée d'origine (les joueurs qui ne l'ont pas repérée ne la verront pas)
   * @property lockable {boolean} La porte est verrouillable
   * @property locked {boolean} La porte est actuellement verrouillée
   * @property key {string} Tag de la clé permettant de verrouiller/déverrouiler la porte
   * @property destination {string} Identifiant de la pièce où mène cette porte
   * @property direction {string} direction vers laquelle donne la porte
   * @property room {string} identifiant de la pièce dans laquelle se trouve la porte
   * @property discardKey {string} si true alors la clé disparait si on l'utilise pour déverouiller la porte
   *
   * @param idRoomOrEntity {string} identifiant pièce ou d'une entité dans la pièce
   * @param sDirection {string} direction de la porte
   * @returns {ExitStatus} état de la porte
   */
  getExit (idRoomOrEntity, sDirection = undefined) {
    if (!sDirection) {
      [idRoomOrEntity, sDirection] = Engine.decomposeId(idRoomOrEntity)
    }
    // Si l'id en param est une entité certaines propriétés auront du sens (visible notamment...)
    /**
     * @type {MUDEntity|null}
     */
    const bIsEntity = this.isEntityExist(idRoomOrEntity)
    const bIsRoom = this.isRoomExist(idRoomOrEntity)
    if (!bIsEntity && !bIsRoom) {
      throw new KeyNotFoundError(idRoomOrEntity, 'rooms/entities')
    }
    const oSubjectEntity = bIsEntity
      ? this.getEntity(idRoomOrEntity)
      : null
    const idRoom = bIsRoom
      ? idRoomOrEntity
      : oSubjectEntity.location
    if (!this.isRoomExist(idRoom)) {
      return {
        id: Engine.composeId(idRoom, sDirection),
        name: '',
        desc: '',
        valid: false,
        dcLockpick: 0,
        dcSearch: 0,
        visible: false,
        secret: false,
        lockable: false,
        locked: false,
        key: '',
        destination: '',
        direction: '',
        room: '',
        discardKey: false
      }
    }
    const oRoom = this.getRoom(idRoom)
    const oExit = oRoom.nav[sDirection]
    const valid = oExit !== undefined

    /**
     * Etat de la sortie
     * @type {ExitStatus}
     */
    const oStatus = {
      direction: sDirection,
      room: idRoom,
      valid,
      lockable: false,
      /**
       * Permet de connaitre l'état du verrou
       * @returns {boolean}
       */
      get locked () {
        return this.lockable && this._exit.lock.locked
      },
      /**
       * Permet de verrouiller/déverrouiller la porte
       * @param value {boolean}
       */
      set locked (value) {
        if (this.lockable) {
          this._exit.lock.locked = value
        }
      },
      name: '',
      dcLockpick: 0,
      key: '',
      secret: false,
      dcSearch: 0,
      visible: false,
      destination: '',
      discardKey: false
    }
    Object.defineProperty(oStatus, '_exit', {
      enumerable: false,
      configurable: false,
      value: oExit
    })
    if (valid) {
      oStatus.name = oExit.name || oStatus.name
      oStatus.desc = oExit.desc || oStatus.desc || []
      oStatus.destination = oExit.to
      oStatus.lockable = !!oExit.lock
      if (oStatus.lockable) {
        const oExitLock = oExit.lock
        oStatus.locked = oStatus.lockable && oExitLock.locked
        oStatus.dcLockpick = 'difficulty' in oExitLock ? oExitLock.difficulty : Infinity
        oStatus.key = oExitLock.key || ''
        oStatus.discardKey = !!oExitLock.discardKey
      }
      oStatus.secret = !!oExit.secret
      if (oStatus.secret) {
        oStatus.dcSearch = 'difficulty' in oExit.secret ? oExit.secret.difficulty : Infinity
      }
      oStatus.visible = !oStatus.secret
    }
    const bSecretSpotted = !!oSubjectEntity && oStatus.secret && this.hasEntitySpottedExit(oSubjectEntity.id, oStatus.direction)
    oStatus.visible = oStatus.visible || bSecretSpotted
    return oStatus
  }

  /**
   * Renvoie la liste des portes valides pour une pièce donnée
   * @param idRoom {string} identifiant pièce
   * @return {string[]} liste des directions valides
   */
  getRoomValidExits (idRoom) {
    return Object.keys(this.getRoom(idRoom).nav)
  }

  /**
   * Renvoie le registre de repérage d'une entity.
   * C'est une registre qui sert à stocker tous les identifiant que l'entité à visité/repéré/mémorisé
   * Ainsi il est possible de stocker la liste des piège repéré par une entité, ou la liste des pièces visitées.
   * @param entity {MUDEntity}
   * @returns {object}
   */
  getEntitySpottedRegistry (entity) {
    if (!(ENTITY_SPOTTED_REGISTRY in entity.data)) {
      entity.data[ENTITY_SPOTTED_REGISTRY] = {}
    }
    return entity.data[ENTITY_SPOTTED_REGISTRY]
  }

  /**
   * Ajoute l'id d'une porte cachée à la liste des portes cachées repérées par le joueur.
   * Cette opération rend cette porte effectivement visible par le joueur.
   * @param idEntity {string} joueur
   * @param sDirection {string} une direction valide
   * @param value {boolean} true = repéré ; false = non-repéré
   */
  setEntityExitSpotted (idEntity, sDirection, value) {
    if (!this.isDirectionValid(sDirection)) {
      throw new KeyNotFoundError(sDirection, 'directions')
    }
    const oEntity = this.getEntity(idEntity)
    const sId = this._getSecretId(oEntity.location, sDirection)
    if (value) {
      // vérifier existence de spotted registry
      this.getEntitySpottedRegistry(oEntity)[sId] = true
    } else if (this.hasEntitySpottedExit(idEntity, sDirection)) {
      delete this.getEntitySpottedRegistry(oEntity)[sId]
    }
  }

  /**
   * cherche et renvoie un tableau contenant tous les objets possédant un tag particulier et se trouvant
   * dans l'inventaire de l'entité spécifiée.
   * Cet fonction est récursive, elle va cherche dans la location spécifiée, ainsi que dans les contenant de cette
   * location.
   * @param sTag {string}
   * @param idLocation {string}
   * @param [bRecursive] {boolean} si vrai alors la fonction fouille également dans les contenants recursivement
   * defaut : faux
   * @returns {object[]} liste des entités correspondant au tag, dans la pièce spécifiée
   */
  findEntitiesByTag (sTag, idLocation, bRecursive = false) {
    const aContainer = this.getLocalEntities(idLocation)
    const aItems = []
    const aInventories = []
    aContainer.forEach(({ entity }) => {
      if (entity.tag === sTag) {
        aItems.push(entity)
      }
      if (bRecursive && entity.inventory) {
        aInventories.push(entity)
      }
    })
    while (aInventories.length > 0) {
      const idNext = aInventories.shift().id
      aItems.push(...this.findEntitiesByTag(sTag, idNext))
    }
    return aItems
  }

  /**
   * Renvoie la liste des entité correspondant au pattern de texte spécifié. Permet de rechercher une entité par son nom
   * @param sText {string} portion de texte qu'il faut rechercher dans les noms des entité de la location spécifiée
   * @param idLocation {string} location (id de pièce ; id d'entité ayant un inventaire)
   * @param [bRecursive] {boolean} si vrai alors la fonction fouille également dans les contenants recursivement
   * defaut : faux
   * @returns {object[]} liste des entité correspondant au nom recherché
   */
  findEntitiesByName (sText, idLocation, bRecursive = false) {
    // trouver la liste des entité susceptible d'être suggérées
    const aInventories = []
    const aEntities = this
      .getLocalEntities(idLocation)
      .map(({
        id,
        entity
      }) => ({
        id,
        entity,
        name: entity.name
      }))
    if (bRecursive) {
      aEntities.forEach(({ entity }) => {
        if (entity.inventory) {
          aInventories.push(entity)
        }
      })
    }
    const aNames = aEntities.map(({ name }) => name)
    const r = DidYouMean.suggest(sText, aNames, {
      count: 0,
      relevance: FIND_BY_NAME_RELEVANCE
    })
    const aItems = aEntities
      .filter(({ name }) => r.includes(name))
      .map(({ entity }) => entity)
    while (aInventories.length > 0) {
      const idNext = aInventories.shift().id
      aItems.push(...this.findEntitiesByName(sText, idNext))
    }
    return aItems
  }

  /**
   * Renvoie la liste des parents de l'entité, cela peut être utile pour obtenir le parent direct (shift)
   * ou bien connaitre la pièce dans laquelle se trouve l'objet, même si cet objet est dans un conteneur
   * @param idEntity {string}
   * @param [aChain] {array} utilisée par la récursivité
   * @return {array} liste des identifiant des entités parentes
   * @throws Error renvoie une erreur avec le message recursive inventory si un objet A est contenu dans une entité contenu dans A
   */
  getParentEntities (idEntity, aChain = []) {
    const oEntity = this.getEntity(idEntity)
    const sLocation = oEntity.location
    if (aChain.includes(sLocation)) {
      throw new Error('Recursive inventory : ' + sLocation)
    }
    aChain.push(sLocation)
    if (this.isRoomExist(sLocation)) {
      return aChain
    } else if (this.isEntityExist(sLocation)) {
      return this.getParentEntities(sLocation, aChain)
    } else {
      throw new KeyNotFoundError(sLocation, 'rooms/entities')
    }
  }

  /**
   * Indique si le joueur a été détecté la direction donnée une porte secrète, dans la pièce ou il se trouve.
   * @param idEntity {string} identifiant joueur
   * @param sDirection {string} direction
   * @returns {boolean} true = la porte dans la direction donnée est bien secrète mais le joueur l'a déja repéré
   */
  hasEntitySpottedExit (idEntity, sDirection) {
    const oEntity = this.getEntity(idEntity)
    return this._getSecretId(oEntity.location, sDirection) in this.getEntitySpottedRegistry(oEntity)
  }

  /**
   * Détermine si une tache est réussie
   * @param oEntity {MUDEntity} identifiant du joueur effectuant la tâche
   * @param sSkill {string} nom du skill testé
   * @param nDifficulty {number} difficulté à battre
   * @param oTool {MUDEntity|null} parfois un outil est utilisé dans la rzsolution de la tâche
   * @returns {boolean} vrai : la tâche est surmontée
   */
  resolveTaskOutcome (oEntity, sSkill, nDifficulty, oTool = null) {
    let bSuccess = false
    const oEvt = {
      engine: this,
      entity: oEntity,
      skill: sSkill,
      difficulty: nDifficulty,
      success: (b = true) => {
        bSuccess = b
      },
      value: 0,
      tool: oTool
    }
    this._events.emit('entity.task.attempt', oEvt)
    return bSuccess
  }

  /**
   * Fait parler l'entité spécifiée. Le message est envoyer à l'interlocuteur désigné ce qui peut être
   * Une pièce, une autre entité. L'entité parleuse, recoit sont propre message également
   * @param idEntity {string} entité qui parle
   * @param idInterlocutor {string} entité ou pièce qui recoit le message
   * @param speech {string} contenu du message
   * @param nVolume {number} 0 = silent, 1 = normal, 2 = shout : intervient lorsque l'on parle à la pièce entière
   */
  speakString (idEntity, idInterlocutor, speech, nVolume = CONSTS.VOLUME_NORMAL) {
    if (this.isRoomExist(idInterlocutor)) {
      // le message est envoyé dans la pièce
      // isoler la liste des entités qui recevront le message
      this
        .getLocalEntities(idInterlocutor)
        .filter(({ entity }) =>
          entity.blueprint.type === Engine.CONST.ENTITY_TYPES.ACTOR ||
          entity.blueprint.type === Engine.CONST.ENTITY_TYPES.PLACEABLE ||
          entity.blueprint.type === Engine.CONST.ENTITY_TYPES.PLAYER
        )
        .forEach(({ entity }) => {
          this.speakString(idEntity, entity.id, speech, nVolume)
        })
      if (nVolume === Engine.CONST.VOLUME_SHOUT) {
        this.getRoomValidExits(idInterlocutor).forEach(dir => {
          const oExit = this.getExit(idInterlocutor, dir)
          this.speakString(idEntity, oExit.destination, speech, Engine.CONST.VOLUME_ECHO)
        })
      }
    } else if (this.isEntityExist(idInterlocutor)) {
      this._events.emit('entity.speak', {
        engine: this,
        entity: idEntity,
        interlocutor: idInterlocutor,
        speech: speech,
        volume: nVolume
      })
    } else {
      throw new KeyNotFoundError(idInterlocutor, 'rooms/entities')
    }
  }

  text (textref, context) {
    const oEvent = { textref, context, text: textref }
    this._events.emit('resolve.textref', oEvent)
    return oEvent.text
  }

  //         _   _ _ _ _   _
  //   _   _| |_(_) (_) |_(_) ___  ___
  //  | | | | __| | | | __| |/ _ \/ __|
  //  | |_| | |_| | | | |_| |  __/\__ \
  //   \__,_|\__|_|_|_|\__|_|\___||___/

  /**
   * Exportation des données du jeu, en vue d'une sauvegarde
   * La sauvegarde se compose d'un ensemble de document JSON.
   * L'un de ces documents possède la liste des identifiant des autres documents.
   * @returns {object[]} liste des documents constituant la sauvegarde
   */
  saveState (sNameSpace = '') {
    this._createAllEntities()
    const exportEntity = oEntity => {
      const oOutput = Object.assign({}, oEntity)
      delete oOutput.blueprint
      return oOutput
    }

    const exportOneLocalIdRegistry = oLIR => {
      const oLIROutput = {}
      for (const sType in oLIR) {
        if (Object.prototype.hasOwnProperty.call(oLIR, sType)) {
          oLIROutput[sType] = oLIR[sType].state
        }
      }
      return oLIROutput
    }

    const exportLocalIdRegistries = () => {
      return Object
        .entries(this._state.localIdRegistry)
        .map(([id, value]) => ({
          id: Engine.composeId(SAVE_STRUCT_TYPE, sNameSpace, id),
          type: 'lir',
          did: id,
          data: exportOneLocalIdRegistry(value)
        }))
    }

    /**
     * Il faudrait que les entités contenues dans d'autre soit exportée après
     * @returns {{data: *, id: string, type: string, did: *}[]}
     */
    const exportEntities = () => {
      return Object
        .entries(this._state.entities)
        .map(([id, value]) => ({
          id: Engine.composeId(SAVE_STRUCT_TYPE, sNameSpace, id),
          type: 'entity',
          did: id,
          data: exportEntity(value)
        }))
    }

    const exportRooms = () => {
      return Object
        .entries(this._state.rooms)
        .map(([id, value]) => ({
          id: Engine.composeId(SAVE_STRUCT_TYPE, sNameSpace, id),
          type: 'room',
          did: id,
          data: deepClone(value)
        }))
    }

    const exportSectors = () => {
      return Object
        .entries(this._state.sectors)
        .map(([id, value]) => ({
          id: Engine.composeId(SAVE_STRUCT_TYPE, sNameSpace, id),
          type: 'sector',
          did: id,
          data: deepClone(value)
        }))
    }

    const exportBlueprints = () => {
      return Object
        .entries(this._state.blueprints)
        .map(([id, value]) => ({
          id: Engine.composeId(SAVE_STRUCT_TYPE, sNameSpace, id),
          type: 'blueprint',
          did: id,
          data: deepClone(value)
        }))
    }

    const aDocuments = [
      ...exportBlueprints(),
      ...exportSectors(),
      ...exportLocalIdRegistries(),
      ...exportEntities(),
      ...exportRooms()
    ]

    const oIndex = {
      id: Engine.composeId(SAVE_STRUCT_TYPE, sNameSpace),
      name: sNameSpace,
      date: Date.now(),
      lastId: this._lastId,
      documents: aDocuments.map(d => d.id),
      type: SAVE_STRUCT_TYPE
    }

    aDocuments.unshift(oIndex)
    return aDocuments
  }

  /**
   * Importation à partir de donnée sauvegardée, remplacement de l'actuel state
   * @param aData
   */
  loadState (aData) {
    // création des rooms
    const oState = this._state = {
      blueprints: {},
      sectors: {},
      localIdRegistry: {},
      roomEntityStorage: {},
      entities: {},
      rooms: {}
    }

    const importEntity = entity => {
      if (!this.isRoomExist(entity.location) && !this.isEntityExist(entity.location)) {
        throw new Error('Cannot import this entity ' + entity.id + ' : its location is unknown ' + entity.location)
      }
      const oEntity = this.createEntity(
        entity.ref,
        entity.location,
        {
          id: entity.id,
          count: 'stack' in entity ? entity.stack : 1
        }
      )
      return Object.assign(oEntity, entity)
    }

    const importPlayer = entity => {
      const oEntity = this.createPlayerEntity(
        entity.uid,
        entity.id,
        entity.name,
        entity.location
      )
      return Object.assign(oEntity, entity)
    }

    const importLocalIdRegistry = lir => {
      const oLIR = {}
      Object.entries(lir).map(([sType, value]) => {
        const d = new DisposableIdRegistry()
        d.state = value
        oLIR[sType] = d
      })
      return oLIR
    }
    aData
      .filter(d => d.type === 'blueprint')
      .forEach(({
        did,
        data
      }) => {
        oState.blueprints[did] = deepClone(data)
      })
    aData
      .filter(d => d.type === 'sector')
      .forEach(({
        did,
        data
      }) => {
        oState.sectors[did] = deepClone(data)
      })
    aData
      .filter(d => d.type === 'room')
      .forEach(({
        did,
        data
      }) => {
        oState.rooms[did] = deepClone(data)
      })
    const aEntitiesToImport = aData
      .filter(d => d.type === 'entity')
      .map(ent => ({
        ...ent,
        rebuked: false
      }))
    let iEntityIndex = 0
    while (iEntityIndex < aEntitiesToImport.length) {
      const oCurrentEntity = aEntitiesToImport[iEntityIndex]
      const { did, data, rebuked } = oCurrentEntity
      let bCanBeImported = true
      if (data.location) {
        if (!this.isRoomExist(data.location) && !this.isEntityExist(data.location)) {
          bCanBeImported = false
        }
      }
      if (bCanBeImported) {
        oState.entities[did] = 'ref' in data
          ? importEntity(data)
          : importPlayer(data)
        ++iEntityIndex
      } else if (rebuked) {
        // ici c'est bizarre, une entité a été réordonnée plus d'une fois : son contenant n'existe absolument pas.
        throw new Error('Error while restoring state. Entity ' + did + ' inside container ' + data.location + ' is rebuked twice.')
      } else {
        oCurrentEntity.rebuked = true
        aEntitiesToImport.splice(iEntityIndex, 1)
        aEntitiesToImport.push(oCurrentEntity)
      }
    }
    aData
      .filter(d => d.type === 'lir')
      .forEach(({
        did,
        data
      }) => {
        oState.localIdRegistry[did] = importLocalIdRegistry(data)
      })
    const oSaveStruct = aData.find(d => d.type === SAVE_STRUCT_TYPE)
    this._lastId = oSaveStruct.lastId
  }

  /**
   * utilitaire de freezage d'objet. Permet de rendre un objet totalement immutable
   * @param o {object} objet à freezer
   * @returns {object} objet freezé
   * @private
   */
  static _deepFreeze (o) {
    if (Object.isFrozen(o)) {
      return o
    }
    Object.freeze(o)
    if (o === undefined) {
      return o
    }

    Object.getOwnPropertyNames(o).forEach(function (prop) {
      if (o[prop] !== null &&
        (typeof o[prop] === 'object' || typeof o[prop] === 'function')
      ) {
        Engine._deepFreeze(o[prop])
      }
    })

    return o
  }

  /**
   * Par défaut les entités qui sont définies dans les pièces ne sont pas créées
   * tant qu'un joueur n'y mets pas les pieds.
   * Cette fonction permet des les créer toutes
   * @private
   */
  _createAllEntities () {
    Object.keys(this.getRooms()).forEach(idRoom => {
      this.getLocalEntities(idRoom)
    })
  }

  /**
   * fabrique un identifiant par concaténation de composant
   * @param args {string} liste des composant à concaténé
   * @return {string} valeur de l'identifiant
   */
  static composeId (...args) {
    if (args[0] === 'room') {
      throw new Error('ERR_FORBIDDEN')
    }
    return args.join(ID_SEPARATOR)
  }

  static decomposeId (sId) {
    return sId.split(ID_SEPARATOR)
  }

  //  _                      _                                                                                 _
  // (_)_ ____   _____ _ __ | |_ ___  _ __ _   _   _ __ ___   __ _ _ __   __ _  __ _  ___ _ __ ___   ___ _ __ | |_
  // | | '_ \ \ / / _ \ '_ \| __/ _ \| '__| | | | | '_ ` _ \ / _` | '_ \ / _` |/ _` |/ _ \ '_ ` _ \ / _ \ '_ \| __|
  // | | | | \ V /  __/ | | | || (_) | |  | |_| | | | | | | | (_| | | | | (_| | (_| |  __/ | | | | |  __/ | | | |_
  // |_|_| |_|\_/ \___|_| |_|\__\___/|_|   \__, | |_| |_| |_|\__,_|_| |_|\__,_|\__, |\___|_| |_| |_|\___|_| |_|\__|
  //                                       |___/                               |___/

  /**
   * Clonage d'une entité
   * @param oEntity {MUDEntity} instance de l'entité source
   * @param [bRegister] {boolean} si true alors enregsttre l'objet et lui attribue un id
   * @returns {MUDEntity} objet cloné
   * @private
   */
  _cloneEntity (oEntity, bRegister = true) {
    const oClone = this.createEntity(
      oEntity.ref,
      bRegister ? oEntity.location : '',
      { count: oEntity.blueprint.stackable ? oEntity.stack : 1 }
    )
    oClone.tag = oEntity.tag
    oClone.inventory = oEntity.blueprint.inventory ? {} : null
    if ('locked' in oEntity) {
      oClone.locked = oEntity.locked
    }
    if (oEntity.blueprint.inventory) {
      const de = this.localIdRegistry[oEntity.id][this._getMiniType(Engine.CONST.ENTITY_TYPES.ITEM)]
      const dc = new DisposableIdRegistry()
      dc.prefix = de.prefix
      dc.bufferSize = de.bufferSize
      this.localIdRegistry[oClone.id] = {
        [this._getMiniType(Engine.CONST.ENTITY_TYPES.ITEM)]: dc
      }
    }
    oClone.data = deepClone(oEntity.data)
    return oClone
  }

  /**
   * Enregistre une entité dans le MUD. Cela convien particulièrement aux entité clonées qui n'ont pas encore
   * été intégrée au MUD parce qu'il fallait appliquer des modification au clone auparavent.
   * @param oEntity {*}
   * @param sLocation {string}
   * @private
   */
  _registerEntity (oEntity, sLocation) {
    const idEntity = oEntity.id
    this.getEntities()[idEntity] = oEntity
    this._events.emit('entity.register', {
      engine: this,
      entity: oEntity
    })
    this._setEntityLocation(idEntity, sLocation)
  }

  /**
   * A partir du local_id Recherche une entity dans la pièce spécifiée.
   * Le local_id est un identifiant simplifié propre à chaque pièce.
   * @param idRoom {string} identifiant pièce
   * @param lid {string} identifiant local de l'objet recherché
   * @private
   * @returns {null|*}
   */
  _getRoomLocalEntity (idRoom, lid) {
    const oFoundEntity = this
      .getLocalEntities(idRoom)
      .find(e => e.lid === lid)
    if (oFoundEntity) {
      return oFoundEntity.entity
    } else {
      return null
    }
  }

  /**
   * Renvoie l'item correspondant à l'identifiant local valide dans l'inventaire de l'entité spécifiée
   * @param idContainer {string} identifiant de l'entité contenante
   * @param lid {string} identifiant local de l'item recherché
   * @returns {null|*} item trouvé
   * @private
   */
  _getInventoryLocalEntity (idContainer, lid) {
    const oContainer = this.getEntity(idContainer)
    if (!oContainer.inventory) {
      return null
    }
    const oFoundItem = this
      .getLocalEntities(idContainer)
      .find(e => e.lid === lid)
    if (oFoundItem) {
      return oFoundItem.entity
    } else {
      return null
    }
  }

  /**
   * Recherche et trouve, dans l'inventaire donné, une pile du même accabi que l'objet transmis en paramètre
   * @param oItem {MUDEntity}
   * @param oInventory {object|null}
   * @private
   */
  _findEntityStack (oItem, oInventory) {
    for (const id in oInventory) {
      if (Object.prototype.hasOwnProperty.call(oInventory, id)) {
        const oOtherItem = this.getEntity(id)
        if (oOtherItem.ref === oItem.ref &&
          oOtherItem.tag === oItem.tag
        ) {
          return oOtherItem
        }
      }
    }
    return null
  }

  //             _   _
  //   __ _  ___| |_| |_ ___ _ __ ___
  //  / _` |/ _ \ __| __/ _ \ '__/ __|
  // | (_| |  __/ |_| ||  __/ |  \__ \
  //  \__, |\___|\__|\__\___|_|  |___/
  //  |___/

  /**
   * état du monde
   * @returns {object} état du monde
   */
  get state () {
    return this._state
  }

  /**
   * renvoie le gestionnaire d'évènements
   * @returns {module:events.EventEmitter} instance du gestionnaire d'évènement
   */
  get events () {
    return this._events
  }

  /**
   * renvoie le caractère initial d'un type. permet de construire de identifiant locaux
   * @param sType {string}
   * @returns {string}
   * @private
   */
  _getMiniType (sType) {
    switch (sType) {
      case Engine.CONST.ENTITY_TYPES.PLAYER:
        return 'p'

      case Engine.CONST.ENTITY_TYPES.ITEM:
        return 'i'

      case Engine.CONST.ENTITY_TYPES.PLACEABLE:
        return 'o'

      case Engine.CONST.ENTITY_TYPES.ACTOR:
        return 'c'

      default:
        throw new KeyNotFoundError(sType, 'types')
    }
  }

  /**
   * vrai si l'objet est valide selon les schemas de validation json
   * @param oObject
   * @param sType {string}
   * @param id {string}
   * @private
   */
  static _checkBlueprintValid (oObject, sType, id) {
    if (!(sType in SCHEMAS)) {
      throw new KeyNotFoundError(sType, 'schemas')
    }
    const aMessages = []
    const options = {
      throwError: false,
      output: s => {
        aMessages.push(s)
      }
    }
    if (!validateSchema(oObject, SCHEMAS[sType], options)) {
      throw new InvalidSchemasError(id, sType, aMessages)
    }
  }

  /**
   * Renvoie l'objet (room, blueprints, sector...) spécifié après vérification
   * il ne s'agit pas d'objets manipulables.
   * pour ces "objets" il s'agit principalement d'un objet abstrait comme une pièce ou un blueprint
   * @param id {string} identifiant
   * @param sTypes {string} fammile de types auquel appartient l'objet
   * @param freeze {boolean|function} après vérif, l'objet est gelé
   * @returns {object}
   * @private
   */
  _getValidObject (id, sTypes, freeze = false) {
    const oState = this.state
    if (!(sTypes in oState)) {
      throw new KeyNotFoundError(sTypes, 'state')
    }
    const oTypes = oState[sTypes]
    if (!(id in oTypes)) {
      throw new KeyNotFoundError(id, sTypes)
    }
    const oObject = oTypes[id]
    if (!this._validObjects.has(id)) {
      if (!(sTypes in SCHEMAS)) {
        throw new KeyNotFoundError(sTypes, 'schemas')
      }
      Engine._checkBlueprintValid(oObject, sTypes, id)
      // blueprint valide
      this._validObjects.add(id)
      let bFreeze
      if (typeof freeze === 'function') {
        bFreeze = freeze(id, oObject)
      } else {
        bFreeze = !!freeze
      }
      if (bFreeze) {
        // Lorsqu'une asset est utilisée pour la première fois, il est transformé en objet immutable
        // Avant cela un évent est déclenché pour permettre à l'application d'apporter des corrections.
        // à l'asset.
        const sType = sTypes.substring(0, sTypes.length - 1)
        // Cet event à toute lattitude pour modifier l'objet avant sa mise en immutabilité.
        this._events.emit('asset.' + sType + '.hook', {
          engine: this,
          resref: id,
          data: oObject
        })
        Engine._deepFreeze(oObject)
      }
    }
    return oObject
  }

  /**
   * retourne la zone de stockage des entité appartenant à une pièce
   * @param idRoom {string} identifiant de la pièce
   * @returns {*}
   * @private
   */
  _getRoomEntityStorage (idRoom) {
    const oRoom = this.getRoom(idRoom)
    if (!(idRoom in this.localIdRegistry)) {
      this.localIdRegistry[idRoom] = {
        [this._getMiniType(Engine.CONST.ENTITY_TYPES.ITEM)]: new DisposableIdRegistry(this._getMiniType(Engine.CONST.ENTITY_TYPES.ITEM)), // les objets d'inventaire
        [this._getMiniType(Engine.CONST.ENTITY_TYPES.PLAYER)]: new DisposableIdRegistry(this._getMiniType(Engine.CONST.ENTITY_TYPES.PLAYER)), // les joueurs
        [this._getMiniType(Engine.CONST.ENTITY_TYPES.PLACEABLE)]: new DisposableIdRegistry(this._getMiniType(Engine.CONST.ENTITY_TYPES.PLACEABLE)), // les objets placables
        [this._getMiniType(Engine.CONST.ENTITY_TYPES.ACTOR)]: new DisposableIdRegistry(this._getMiniType(Engine.CONST.ENTITY_TYPES.ACTOR)) // les créatures et les pnj
      }
    }
    const oStorage = this.state.roomEntityStorage
    if (!(oStorage[idRoom])) {
      // construction du registre d'identifiant locaux
      // construction du stockage des entités
      oStorage[idRoom] = {}
      // on a profite pour coller tous les objet par default
      const aContent = oRoom.content
      if (aContent) {
        aContent.forEach(oContentDef => {
          const {
            ref,
            tag = null,
            remark = null,
            locked = null,
            stack = 1,
            content = []
          } = oContentDef
          const oEntity = this.createEntity(ref, idRoom, { count: stack })
          if (tag) {
            oEntity.tag = tag
          }
          if (remark) {
            oEntity.remark = remark
          }
          if (locked !== null) {
            oEntity.locked = locked
          }
          if (oEntity.inventory) {
            this._setEntityContent(oEntity, content)
          }
        })
        delete oRoom.content
      }
    }
    return oStorage[idRoom]
  }

  /**
   * Renvoie le code technique d'une issue secrete
   * permet principalement d'attribuer un identifiant à un passage secret précis pour déterminer
   * quels sont les joueurs qui l'ont découvert.
   * @param idRoom {string} identifiant dans la pièce ou se trouve l'issue secrete
   * @param sDirection {string} direction
   * @returns {string}
   * @private
   */
  _getSecretId (idRoom, sDirection) {
    return Engine.composeId(idRoom, sDirection, 'secret')
  }

  get localIdRegistry () {
    return this.state.localIdRegistry
  }

  //                  _        _   _
  //  _ __ ___  _   _| |_ __ _| |_(_) ___  _ __  ___
  // | '_ ` _ \| | | | __/ _` | __| |/ _ \| '_ \/ __|
  // | | | | | | |_| | || (_| | |_| | (_) | | | \__ \
  // |_| |_| |_|\__,_|\__\__,_|\__|_|\___/|_| |_|___/
  //

  /**
   * Chargement des données initiales d'une aventure
   * @param value
   */
  setAssets (value) {
    this._state = {
      ...value,
      entities: {},
      localIdRegistry: {},
      roomEntityStorage: {}
    }
    // vérification des pièces et des blueprints
    if ('rooms' in value) {
      Object.entries(value.rooms).forEach(([idRoom, room]) => {
        Engine._checkBlueprintValid(room, 'rooms', idRoom)
      })
    }
    if ('blueprints' in value) {
      Object.entries(value.blueprints).forEach(([idBlueprint, blueprint]) => {
        Engine._checkBlueprintValid(blueprint, 'blueprints', idBlueprint)
      })
    }
  }

  /**
   * Supprime une entité d'un container
   * @param idContainer
   * @param idItem
   * @private
   */
  _removeInventoryEntity (idContainer, idItem) {
    const oContainerOwner = this.getEntity(idContainer)
    const oContainer = oContainerOwner.inventory
    if (oContainer) {
      const lid = oContainer[idItem]
      this.localIdRegistry[idContainer][this._getMiniType(Engine.CONST.ENTITY_TYPES.ITEM)].disposeId(lid)
      delete oContainer[idItem]
      const oItem = this.getEntity(idItem)
      oItem.location = ''
    }
  }

  /**
   * Ajoute une entité à l'inventaire, et lui associe un identifiant local
   * @param idContainer {string}
   * @param idItem {string}
   * @private
   */
  _addInventoryEntity (idContainer, idItem) {
    const oContainerOwner = this.getEntity(idContainer)
    const oContainer = oContainerOwner.inventory
    if (oContainer) {
      const oItem = this.getEntity(idItem)
      const sMiniType = this._getMiniType(Engine.CONST.ENTITY_TYPES.ITEM)
      oContainer[idItem] = this.localIdRegistry[idContainer][sMiniType].getId()
      oItem.location = idContainer
    }
  }

  /**
   * Suprime une entity du registre des entités de la pièce
   * @param idRoom {string} identifiant pièce
   * @param idEntity {string} identifiant entité
   * @private
   */
  _removeRoomEntity (idRoom, idEntity) {
    const lid = this._getRoomEntityStorage(idRoom)[idEntity]
    const sMiniType = lid.charAt(0).toLowerCase()
    this.localIdRegistry[idRoom][sMiniType].disposeId(lid)
    delete this._getRoomEntityStorage(idRoom)[idEntity]
    const oEntity = this.getEntity(idEntity)
    oEntity.location = ''
  }

  /**
   * Ajoute une entity au registre des entités de la pièce.
   * attribue un numéro d'identification local.
   * Selon le type d'objet, la lettre associé au lid sera différente
   * @param idRoom {string} identifiant pièce
   * @param idEntity {string} identifiant entité
   * @private
   */
  _addRoomEntity (idRoom, idEntity) {
    // déterminer le type d'une entité
    const oEntity = this.getEntity(idEntity)
    const sType = oEntity.blueprint.type
    const sMiniType = this._getMiniType(sType)
    const oRES = this._getRoomEntityStorage(idRoom)
    oRES[idEntity] = this.localIdRegistry[idRoom][sMiniType].getId()
    oEntity.location = idRoom
  }

  /**
   * Change la localisation d'une entité
   * @param idTo {string} identifiant pièce
   * @param idEntity {string} identifiant entité
   * @private
   */
  _setEntityLocation (idEntity, idTo) {
    // vérifier les problèmme de récursivité
    // un objet A ne doit pas entrée dans un inventaire déja appartenant à A
    if (idEntity === idTo) {
      // grossière tentative de mettre un objet dans lui-même
      return false
    }
    if (this.isEntityExist(idTo)) {
      const aChainInv = this.getParentEntities(idTo)
      if (aChainInv.includes(idEntity)) {
        // opération interdite : Si un coffre A contient un sac B alors on ne peut pas placer A dans B
        return false
      }
    }
    const oEntity = this.getEntity(idEntity)
    const idPrevLocation = oEntity.location

    const codeLoc = idx => this.isRoomExist(idx)
      ? 'r'
      : this.isEntityExist(idx)
        ? 'e'
        : '*'

    const loc = {
      prev: codeLoc(idPrevLocation),
      to: codeLoc(idTo)
    }
    if (loc.prev === 'r') {
      // l'entité était dans une pièce au sol
      this._removeRoomEntity(idPrevLocation, idEntity)
    } else if (loc.prev === 'e') {
      // l'enttité était dans l'inventaire d'une autre entité
      this._removeInventoryEntity(idPrevLocation, idEntity)
    } // else osef
    if (loc.to === 'r') {
      this._addRoomEntity(idTo, idEntity)
    } else if (loc.to === 'e') {
      // on va stocker l'entité dans un inventaire
      this._addInventoryEntity(idTo, idEntity)
    } else {
      throw new KeyNotFoundError(idTo, 'rooms/entites')
    }
    // déplacement d'entité d'une pièce à une autre
    switch (loc.prev + loc.to) {
      case '*r':
        // une entité apparait dans une pièce
        this._events.emit('entity.create', {
          engine: this,
          entity: idEntity,
          location: idTo
        })
        break

      case 'er':
        // objet posé au sol d'une pièce
        this._events.emit('item.drop', {
          engine: this,
          entity: idEntity,
          droppedBy: idPrevLocation,
          location: idTo
        })
        break

      case 'rr':
        // entité se déplaçant d'une pièce vers une autre
        this._events.emit('entity.move', {
          engine: this,
          entity: idEntity,
          from: idPrevLocation,
          to: idTo
        })
        break

      case 'ee':
        // objet passant d'une entité à une autre
        this._events.emit('item.lose', {
          engine: this,
          entity: idEntity,
          lostBy: idPrevLocation,
          acquiredBy: idTo
        })
        this._events.emit('item.acquire', {
          engine: this,
          entity: idEntity,
          acquiredBy: idTo,
          acquiredFrom: idPrevLocation
        })
        break

      case 'e*':
        // une entité perd l'objet qui est détruit
        this._events.emit('item.lose', {
          engine: this,
          entity: idEntity,
          lostBy: idPrevLocation
        })
        break

      case 're':
        // une entité rammasse l'objet qui était sur le sol de la pièce
        this._events.emit('item.acquire', {
          engine: this,
          entity: idEntity,
          acquiredBy: idTo,
          acquiredFrom: idPrevLocation
        })
        break

      case '*e':
        // objet créé directement dans l'inventaire
        this._events.emit('entity.create', {
          engine: this,
          entity: idEntity,
          location: idTo
        })
        this._events.emit('item.acquire', {
          engine: this,
          entity: idEntity,
          acquiredBy: idTo,
          acquiredFrom: idPrevLocation
        })
        break
    }
    return true
  }

  /**
   * @typedef MUDContentDefinitionItem {object}
   * @property ref {string} référence de l'item
   * @property tag {string} tag à associer à l'entité-item
   * @property stack {number} nombre d'exemplaires de l'objet (si il est stackable)
   * @property content {MUDContentDefinitionItem[]} contenu de l'obket si c'est un contenant
   *
   * Définition du contenu d'un objet pourvu d'un inventaire
   * @param oEntity {MUDEntity}
   * @param aContent {MUDContentDefinitionItem[]}
   */
  _setEntityContent (oEntity, aContent) {
    aContent.forEach(({
      ref,
      tag = null,
      stack = 1,
      content = []
    }) => {
      const oSubEntity = this.createEntity(ref, oEntity.id, { count: stack })
      if (tag !== null) {
        oSubEntity.tag = tag
      }
      if (Array.isArray(content) && oSubEntity.inventory) {
        this._setEntityContent(oSubEntity, content)
      }
    })
  }

  /**
   * Ouverture d'un contenant à l'aide d'une clé (ou d'un outil)
   * @param idActor {string}
   * @param idContainer {string}
   * @param idKey {string}
   * @returns {{success: boolean, outcome: string}}
   */
  _unlockContainer (idActor, idContainer, idKey = undefined) {
    if (!this.isContainer(idContainer)) {
      return {
        success: false,
        outcome: CONSTS.UNLOCK_OUTCOMES.NOT_A_CONTAINER
      }
    }
    const oContainer = this.getEntity(idContainer)
    const oActor = this.getEntity(idActor)
    const oKey = this.isEntityExist(idKey) ? this.getEntity(idKey) : null
    if (!oContainer.locked) {
      return {
        success: false,
        outcome: CONSTS.UNLOCK_OUTCOMES.NOT_LOCKED
      }
    }
    if (
      oKey &&
      oKey.blueprint.type === CONSTS.ENTITY_TYPES.ITEM &&
      oKey.blueprint.subtype === CONSTS.ITEM_TYPES.KEY
    ) {
      const oLock = oContainer.blueprint.lock
      if (oKey.tag === oLock.key) {
        oContainer.locked = false // Ouvrir la porte
        if (oLock.discardKey) { // tester discardKey
          // virer la clé de l'inventaire
          this.destroyEntity(oKey.id)
        }
        this._events.emit('container.unlock', {
          container: idContainer,
          actor: idActor
        })
        return {
          success: true,
          outcome: ''
        }
      } else {
        this._events.emit('container.unlock.failure', {
          container: idContainer,
          actor: idActor
        })
        return {
          success: false,
          outcome: CONSTS.UNLOCK_OUTCOMES.WRONG_KEY
        }
      }
    } else {
      // Ce n'est pas une vrai clé, voir même pas un item valide
      // Vérifier par évènement si un sous-système prévoi de considéré l'item comme un outil pouvant aider
      // à déverrouiller la porte
      const bSuccess = this.resolveTaskOutcome(oActor, 'unlock', oContainer.blueprint.lock.difficulty, oKey)
      if (bSuccess) {
        oContainer.locked = false
        this._events.emit('container.unlock', {
          container: idContainer,
          actor: idActor
        })
        return {
          success: true,
          outcome: CONSTS.UNLOCK_OUTCOMES.SUCCESS
        }
      } else {
        this._events.emit('container.unlock.failure', {
          container: idContainer,
          actor: idActor
        })
        // La tentative a échoué
        return {
          success: false,
          outcome: CONSTS.UNLOCK_OUTCOMES.KEY_INVALID
        }
      }
    }
  }

  /**
   * Verrouillage d'une issue à l'aide d'une clé.
   * L'issue à verrouillée doit avoir été déclarée avec un verrou et une clé
   * @param idActor {string} identifiant de l'acteur qui verrouille
   * @param sDirection {string} direction de la porte
   * @param idKey {string}
   * @return {{success: boolean, outcome: string}}
   *
   *     SUCCESS: 'SUCCESS',
   *     WRONG_KEY: 'WRONG_KEY',
   *     ALREADY_LOCKED: 'ALREADY_LOCKED',
   *     INVALID_TARGET: 'INVALID_TARGET',
   *     INVALID_KEY: 'KEY_INVALID'
   * @private
   */
  _lockExit (idActor, sDirection, idKey) {
    if (!this.isActor(idActor)) {
      throw new Error('Only actors may lock doors or containers')
    }
    const oActor = this.getEntity(idActor)
    const oKey = this.getEntity(idKey)
    const idRoom = oActor.location
    const oExit = this.getExit(idRoom, sDirection)
    if (!oExit.lockable || !oExit.valid || !oExit.visible) {
      return {
        success: false,
        outcome: CONSTS.LOCK_OUTCOMES.INVALID_TARGET
      }
    }
    if (oExit.locked) {
      return {
        success: false,
        outcome: CONSTS.LOCK_OUTCOMES.ALREADY_LOCKED
      }
    }
    if (oExit.key !== oKey.tag) {
      return {
        success: false,
        outcome: CONSTS.LOCK_OUTCOMES.WRONG_KEY
      }
    }
    oExit.locked = true
    this._events.emit('exit.lock', { room: idRoom, direction: sDirection, actor: idActor })
    return {
      success: true,
      outcome: CONSTS.LOCK_OUTCOMES.SUCCESS
    }
  }

  _lockContainer (idActor, idContainer, idKey) {
    if (!this.isActor(idActor)) {
      throw new Error('Only actors may lock doors or containers')
    }
    const oContainer = this.getEntity(idContainer)
    if (!oContainer.blueprint.lock) {
      return {
        success: false,
        outcome: CONSTS.LOCK_OUTCOMES.INVALID_TARGET
      }
    }
    if (oContainer.locked) {
      return {
        success: false,
        outcome: CONSTS.LOCK_OUTCOMES.ALREADY_LOCKED
      }
    }
    const oKey = this.getEntity(idKey)
    if (oKey.tag !== oContainer.blueprint.lock?.key) {
      return {
        success: false,
        outcome: CONSTS.LOCK_OUTCOMES.WRONG_KEY
      }
    }
    oContainer.locked = true
    this._events.emit('container.lock', { container: idContainer, actor: idActor })
    return {
      success: true,
      outcome: CONSTS.LOCK_OUTCOMES.SUCCESS
    }
  }

  /**
   * Ouverture d'une issue par un actor à l'aide d'une clé
   * @param idActor {string}
   * @param [idKey] {string}
   * @param sDirection {string}
   * @returns {{success: boolean, outcome: string}}
   */
  _unlockExit (idActor, sDirection, idKey = undefined) {
    const oExit = this.getExit(idActor, sDirection)
    const oActor = this.getEntity(idActor)
    const oKey = this.isEntityExist(idKey) ? this.getEntity(idKey) : null
    if (!oExit.valid || !oExit.visible) {
      return {
        success: false,
        outcome: CONSTS.UNLOCK_OUTCOMES.EXIT_INVALID
      }
    }
    if (!oExit.locked) {
      return {
        success: false,
        outcome: CONSTS.UNLOCK_OUTCOMES.NOT_LOCKED
      }
    }
    if (
      oKey &&
      oKey.blueprint.type === CONSTS.ENTITY_TYPES.ITEM &&
      oKey.blueprint.subtype === CONSTS.ITEM_TYPES.KEY
    ) {
      // c'est bon : c'est une clé. Vérifier son tag
      if (oKey.tag === oExit.key) {
        oExit.locked = false // Ouvrir la porte
        if (oExit.discardKey) { // tester discardKey
          // virer la clé de l'inventaire
          this.destroyEntity(oKey.id)
        }
        this._events.emit('exit.unlock', {
          room: oActor.location,
          direction: sDirection,
          actor: idActor
        })
        return {
          success: true,
          outcome: ''
        }
      } else {
        this._events.emit('exit.unlock.failure', {
          room: oActor.location,
          direction: sDirection,
          actor: idActor
        })
        // Pas la bonne clé
        return {
          success: false,
          outcome: CONSTS.UNLOCK_OUTCOMES.WRONG_KEY
        }
      }
    } else {
      // Ce n'est pas une vrai clé, voir même pas un item valide
      // Vérifier par évènement si un sous-système prévoi de considéré l'item comme un outil pouvant aider
      // à déverrouiller la porte
      const bSuccess = this.resolveTaskOutcome(oActor, 'unlock', oExit.dcLockpick, oKey)
      if (bSuccess) {
        oExit.locked = false
        this._events.emit('exit.unlock', {
          room: oActor.location,
          direction: sDirection,
          actor: idActor
        })
        return {
          success: true,
          outcome: ''
        }
      } else {
        // La tentative à finalement réussi
        this._events.emit('exit.unlock.failure', {
          room: oActor.location,
          direction: sDirection,
          actor: idActor
        })
        return {
          success: false,
          outcome: CONSTS.UNLOCK_OUTCOMES.KEY_INVALID
        }
      }
    }
  }

  //        _                                    _   _
  //  _ __ | | __ _ _   _  ___ _ __    __ _  ___| |_(_) ___  _ __  ___
  // | '_ \| |/ _` | | | |/ _ \ '__|  / _` |/ __| __| |/ _ \| '_ \/ __|
  // | |_) | | (_| | |_| |  __/ |    | (_| | (__| |_| | (_) | | | \__ \
  // | .__/|_|\__,_|\__, |\___|_|     \__,_|\___|\__|_|\___/|_| |_|___/
  // |_|            |___/

  /**
   * @typedef ActionOpenDoorStruct {object}
   * @property valid {boolean} existance de l'issue
   * @property visible {boolean} visibilité de l'issue (a été détectée par l'acteur)
   * @property locked {boolean} issue fermée et verrouillée
   * @property destination {string} identifiant de la pièce de destination
   * @property success {boolean} l'acteur a réussi à passer la porte
   *
   * Lance une action d'ouverture de porte. L'action échoue si la porte fermant l'issue est verrouilée
   * @param idActor {string} identigiant de l'acteur qui se déplace
   * @param sDirection {string} direction de l'issue que veut emprunter l'acteur
   * @return {ActionOpenDoorStruct}
   */
  actionPassThruExit (idActor, sDirection) {
    const {
      valid,
      visible,
      locked,
      destination
    } = this.getExit(idActor, sDirection)
    if (valid && visible && !locked) {
      this.moveEntity(idActor, destination)
      return {
        success: true,
        valid,
        visible,
        locked,
        destination
      }
    } else {
      this.triggerEntityEvent(idActor, 'blocked', {
        room: this.getEntity(idActor).location,
        direction: sDirection
      })
      return {
        success: false,
        valid,
        visible,
        locked,
        destination
      }
    }
  }

  /**
   * Lance une action de déverrouillage du container ou de l'issue spécifiée
   * L'action est effectuée par un acteur de sorte que la résolution dépendant de la qualité des compétence de l'acteur.
   * @param idActor {string} identifiant de l'actor qui effectue l'action
   * @param idContainerOrDirection {string} identifiant du container ou de la direction
   * @param idKey {string} identifiant de la clé ou de l'outil
   * @returns {{success: boolean, outcome: string}}
   */
  actionUnlock (idActor, idContainerOrDirection, idKey = undefined) {
    if (this.isDirectionValid(idContainerOrDirection)) {
      return this._unlockExit(idActor, idContainerOrDirection, idKey)
    }
    return this._unlockContainer(idActor, idContainerOrDirection, idKey)
  }

  /**
   * L'action de verrouillage n'est possible qu'à l'aide de la clé correspondant à la serrure.
   * @param idActor
   * @param idContainerOrDirection
   * @param idKey
   */
  actionLock (idActor, idContainerOrDirection, idKey) {
    if (this.isDirectionValid(idContainerOrDirection)) {
      return this._lockExit(idActor, idContainerOrDirection, idKey)
    }
    return this._lockContainer(idActor, idContainerOrDirection, idKey)
  }

  /**
   * Lance une action de recherche d'issues cachées
   * L'action est effectuée par un acteur de sorte que la résolution dépendant de la qualité des compétence de l'acteur.
   * @param idActor {string} identifiant de l'acteur qui tente de rechercher l'issue cachée
   * @param sDirection {string} direction dans laquelle la recherche est effectuée
   * @param idTool {string} identifiant optionnel de l'outil qui aide à l'action
   * @returns {{success: boolean, outcome: string}}
   */
  actionSearchSecret (idActor, sDirection, idTool = undefined) {
    if (this.isDirectionValid(sDirection)) {
      const {
        valid,
        secret,
        visible,
        dcSearch
      } = this.getExit(idActor, sDirection)
      const bNeedDetectExit = valid && secret && !visible
      if (!bNeedDetectExit) {
        return {
          success: false,
          outcome: 'NO_NEED_DETECTION'
        }
      }
      const oPlayer = this.getEntity(idActor)
      const oTool = idTool ? this.getEntity(idTool) : null
      const bFound = this.resolveTaskOutcome(oPlayer, 'search', dcSearch, oTool)
      if (bFound) {
        this.setEntityExitSpotted(idActor, sDirection, true)
      }
      return {
        success: bFound,
        outcome: bFound ? '' : 'NOT_FOUND'
      }
    } else {
      throw new KeyNotFoundError(sDirection, 'direction')
    }
  }

  //                 _                                         _                        _ _   _
  //   ___ _   _ ___| |_ ___  _ __ ___     _____   _____ _ __ | |_ ___    ___ _ __ ___ (_) |_| |_ ___ _ __
  //  / __| | | / __| __/ _ \| '_ ` _ \   / _ \ \ / / _ \ '_ \| __/ __|  / _ \ '_ ` _ \| | __| __/ _ \ '__|
  // | (__| |_| \__ \ || (_) | | | | | | |  __/\ V /  __/ | | | |_\__ \ |  __/ | | | | | | |_| ||  __/ |
  //  \___|\__,_|___/\__\___/|_| |_| |_|  \___| \_/ \___|_| |_|\__|___/  \___|_| |_| |_|_|\__|\__\___|_|

  triggerEntityEvent (idEntity, event, payload) {
    let events
    if (this.isEntityExist(idEntity)) {
      events = this.getEntity(idEntity).blueprint.events
    } else if (this.isRoomExist(idEntity)) {
      events = this.getRoom(idEntity).events
    } else {
      const oExit = this.getExit(idEntity)
      if (oExit.valid) {
        const oRoom = this.getRoom(oExit.room)
        events = oRoom.events
      }
    }
    if (events && (typeof events[event] === 'string')) {
      this._events.emit('entity.event.script', {
        script: events[event],
        payload: {
          engine: this,
          self: idEntity,
          ...payload
        }
      })
    }
  }

  _initCustomEventEmitter () {
    // blocked            ? (unable to open door)
    // conversation       ? say
    // disturbed          ? inventory changed
    this._events.on('item.acquire', ({
      entity,
      acquiredFrom,
      acquiredBy
    }) => {
      // il faut que entity soit item
      const oEntity = this.getEntity(entity)
      if (oEntity.blueprint.type !== CONSTS.ENTITY_TYPES_ITEM) {
        return
      }
      let sTransfer = ''
      if (this.isActor(acquiredFrom) || this.isPlayer(acquiredFrom)) {
        sTransfer += 'A'
      } else if (this.isContainer(acquiredFrom)) {
        sTransfer += 'C'
      } else if (this.isRoomExist(acquiredFrom)) {
        sTransfer += 'R'
      } else {
        sTransfer += 'X'
      }
      if (this.isActor(acquiredBy) || this.isPlayer(acquiredBy)) {
        sTransfer += 'A'
      } else if (this.isContainer(acquiredBy)) {
        sTransfer += 'C'
      } else if (this.isRoomExist(acquiredBy)) {
        sTransfer += 'R'
      } else {
        sTransfer += 'X'
      }
      switch (sTransfer) {
        case 'AR':
        case 'AC': {
          // l'objet est transféré depuis un actor vers un contenant
          // l'actor dépose donc l'objet dans un contenant ou la pièce
          this.triggerEntityEvent(acquiredBy, 'disturbed', {
            interactor: acquiredFrom,
            type: CONSTS.DISTURBANCE_ITEM_ADDED,
            item: entity
          })
          this.triggerEntityEvent(entity, 'dropped', { interactor: acquiredFrom })
          break
        }

        case 'RA':
        case 'CA': {
          // l'objet est transféré depuis un contenant vers un actor
          // l'actor a donc pris l'objet
          this.triggerEntityEvent(acquiredFrom, 'disturbed', {
            interactor: acquiredBy,
            type: CONSTS.DISTURBANCE_ITEM_REMOVED,
            item: entity
          })
          this.triggerEntityEvent(entity, 'acquired', { interactor: acquiredBy })
          break
        }

        case 'AA': {
          // Un actor donne un objet à un autre actor
          this.triggerEntityEvent(acquiredFrom, 'disturbed', {
            interactor: acquiredBy,
            type: CONSTS.DISTURBANCE_ITEM_REMOVED,
            item: entity
          })
          this.triggerEntityEvent(acquiredBy, 'disturbed', {
            interactor: acquiredFrom,
            type: CONSTS.DISTURBANCE_ITEM_ADDED,
            item: entity
          })
          this.triggerEntityEvent(entity, 'dropped', { interactor: acquiredFrom })
          this.triggerEntityEvent(entity, 'acquired', { interactor: acquiredBy })
          break
        }

        default: {
          // Les autres cas sont ignorés
          break
        }
      }
    })

    // heartbeat ? indexer les entités qui ont un blueprint contenant cet event
    // perception
    // spawn & enter
    this._events.on('entity.create', ({ entity, location }) => {
      this.triggerEntityEvent(entity, 'spawn', {})
      if (this.isRoomExist(location)) {
        this.triggerEntityEvent(location, 'enter', { interactor: entity, from: undefined })
      }
    })
    // lock
    // open
    // openFailure
    this._events.on('container.unlock.failure', ({ container, actor }) => {
      this.triggerEntityEvent(container, 'openFailure', { interactor: actor })
    })
    this._events.on('exit.unlock.failure', ({ room, direction, actor }) => {
      this.triggerEntityEvent(Engine.composeId(room, direction), 'openFailure', { interactor: actor })
    })
    // unlock
    this._events.on('container.unlock', ({ container, actor }) => {
      this.triggerEntityEvent(container, 'unlocked', { interactor: actor })
    })
    this._events.on('exit.unlock', ({ room, direction, actor }) => {
      this.triggerEntityEvent(Engine.composeId(room, direction), 'unlocked', { interactor: actor })
    })
    // lock
    this._events.on('container.lock', ({ container, actor }) => {
      this.triggerEntityEvent(container, 'locked', { interactor: actor })
    })
    this._events.on('exit.lock', ({ room, direction, actor }) => {
      this.triggerEntityEvent(Engine.composeId(room, direction), 'locked', { interactor: actor })
    })

    // enter
    // exit
    // acquired
    // dropped
    this._events.on('entity.move', ({ entity, from, to }) => {
      // il faut que entity soit actor ou player
      if (this.isActor(entity)) {
        if (this.isRoomExist(from)) {
          this.triggerEntityEvent(from, 'exit', { interactor: entity, to })
        }
        if (this.isRoomExist(to)) {
          this.triggerEntityEvent(to, 'enter', {
            interactor: entity,
            from
          })
        }
      }
    })
    // exit
    this._events.on('entity.destroy', ({ entity }) => {
      const oMovingEntity = this.getEntity(entity)
      const sLocation = oMovingEntity.location
      if (this.isRoomExist(sLocation)) {
        this.triggerEntityEvent(sLocation, 'exit', { interactor: entity })
      }
      this.triggerEntityEvent(entity, 'despawn', { location: entity })
    })
  }
}

module.exports = Engine

const { getType } = require('../get-type')

class ModuleConfigParser {
  constructor (oConfig) {
    this._config = oConfig
  }

  get languages () {
    return this.parseConfigLanguages(this._config.languages)
  }

  hasLanguage (s) {
    return this.getLangSet(this._config.languages).has(s)
  }

  get defaultLanguage () {
    if ('defaultLanguage' in this._config) {
      return this.hasLanguage(this._config.defaultLanguage)
        ? this._config.defaultLanguage
        : ''
    } else {
      return this.parseFirstLanguage(this._config.languages)
    }
  }

  parseFirstLanguage (lang) {
    switch (getType(lang)) {
      case 'string': {
        return this.parseFirstLanguage(lang.split(',').map(s => s.trim()))
      }

      case 'array': {
        return lang.length > 0 ? lang[0] : ''
      }

      case 'object': {
        return this.parseFirstLanguage(Object.keys(lang))
      }

      default: {
        return ''
      }
    }
  }

  getLangSet (lang) {
    switch (getType(lang)) {
      case 'string': {
        return this.getLangSet(lang.split(',').map(s => s.trim()))
      }

      case 'array': {
        return new Set(lang)
      }

      case 'object': {
        return this.getLangSet(Object.keys(lang))
      }

      default: {
        return new Set()
      }
    }
  }

  parseConfigLanguages (lang) {
    switch (getType(lang)) {
      case 'string': {
        return this.parseConfigLanguages(lang.split(',').map(s => s.trim()))
      }

      case 'array': {
        const o = {}
        lang.forEach(s => {
          if (s !== '') {
            o[s] = s
          }
        })
        return o
      }

      case 'object': {
        return lang
      }

      default: {
        return {}
      }
    }
  }

  getLangPath (sLang = '') {
    return this.languages[sLang || this.defaultLanguage] || ''
  }
}

module.exports = ModuleConfigParser

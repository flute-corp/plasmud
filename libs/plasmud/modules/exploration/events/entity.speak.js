function main ({ system, engine, interlocutor, speech }) {
  if (engine.isPlayer(interlocutor)) {
    const { print } = system.getPlayerContext(interlocutor)
    print(speech)
  }
}

main($event)

const debug = require('debug')

const log = debug('mod:exploration')

function main (evt) {
  log('Exploration module')
  log('A basic set of commands to navigate through rooms, interact with items, placeables, and exits.')
}

main($event)

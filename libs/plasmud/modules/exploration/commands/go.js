function goDirection (context, sDirection) {
  const { print, engine, pid } = context
  const { locked, destination, success } = engine.actionPassThruExit(pid, sDirection)
  if (success) {
    const oPlayer = engine.getEntity(pid)
    print.room('go.room.playerExitRoom', { dir: sDirection, name: oPlayer.name })
    const oNewRoom = engine.getRoom(destination)
    print('go.action.youMoveRoom', { dir: sDirection, room: oNewRoom.name })
    print.room('go.room.playerEnterRoom', { dir: sDirection, name: oPlayer.name })
  } else {
    if (locked) {
      print('go.failure.exitIsLocked', { dir: sDirection })
    } else {
      print('go.failure.noExitHere', { dir: sDirection })
    }
  }
}

function main (context, aArguments) {
  context.parse(aArguments, {
    [context.PARSER_PATTERN.DIRECTION]: p => goDirection(context, p.direction),
    [context.PARSER_PATTERN.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
  })
}

main($context, $parameters)

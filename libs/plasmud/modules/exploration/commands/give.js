/**
 *
 * @param context {MUDContext}
 * @param item
 * @param count
 * @param other
 */
function giveItemToOther (context, item, count, other) {
  const { engine, print, pid } = context
  if (other.id !== pid && (engine.isActor(other) || engine.isPlayer)) {
    const oGiver = engine.getEntity(pid)
    engine.moveEntity(item.id, other.id, count)
    const giver = oGiver.name
    const receiver = other.name
    print('give.action.itemGiven', { item: item.name, count, receiver })
    if (engine.isPlayer(other)) {
      print.to(other.id, 'give.action.itemReceived', { item: item.name, count, giver })
    }
    print.room.except([pid, other.id], 'give.room.itemGiven', { giver, item: item.name, count, receiver })
  } else {
    print('give.error.badPlayer')
  }
}

function main (context, aArguments) {
  context.parse(aArguments, {
    [context.PARSER_PATTERN.ITEM_TO_ENTITY]: p => giveItemToOther(context, p.item, p.count, p.target),
    [context.PARSER_PATTERN.NONE]: () => context.print('generic.error.needArgument'),
    [context.PARSER_PATTERN.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
  })
}

main($context, $parameters)

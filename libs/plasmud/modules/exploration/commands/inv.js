const { getInventoryItems } = require('helpers/containers')

/**
 * GET
 * @author ralphy
 * @date 2022-03-03
 * Cette commande visualise le contenu de l'inventaire du PJ
 * Syntaxe : INV
 */

function displayInventory (context) {
  const { print, pid } = context
  const oInvEntities = getInventoryItems(context, pid)
  print('inventory', { entities: oInvEntities })
}

function main (context, aArguments) {
  context.parse(aArguments, {
    [context.PARSER_PATTERN.NONE]: () => displayInventory(context),
    [context.PARSER_PATTERN.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
  })
}

main($context, $parameters)

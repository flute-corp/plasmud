/**
 * GET
 * @author ralphy
 * @date 2022-03-03
 * Cette commande sert à récupérer un objet au sol (ITEM uniquement)
 * Ou un item contenu dans un contenant type coffre ou sac...
 * Syntaxe : GET <item>
 * Syntaxe : GET <item> from <container>
 */

/**
 * Rammasser l'item spécifié depuis le sol de la pièce
 * @param context
 * @param oItem {MUDEntity}
 * @param nCount
 */
function takeItemFromAnywhere (context, oItem, nCount) {
  const { print, pid, engine } = context
  const oMe = engine.getEntity(pid)
  print('get.action.itemTaken', { item: oItem.name })
  print.room('get.room.itemTaken', { name: oMe.name, item: oItem.name })
  engine.moveEntity(oItem.id, pid, nCount)
}

function main (context, aArguments) {
  context.parse(aArguments, {
    [context.PARSER_PATTERN.ITEM_FROM_CONTAINER]: p => takeItemFromAnywhere(context, p.item, p.count),
    [context.PARSER_PATTERN.ENTITY]: p => takeItemFromAnywhere(context, p.entity, p.count),
    [context.PARSER_PATTERN.NONE]: () => context.print('generic.error.argumentNeeded'),
    [context.PARSER_PATTERN.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
  })
}

main($context, $parameters)

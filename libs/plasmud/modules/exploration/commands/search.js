/**
 * SEARCH
 * @author ralphy
 * @date 25-12-2021
 * Le joueur utilise cette commande pour tenter de détecter les issues cachées dans une pièce.
 * Les passage secrets etc...
 * Il faut pouvoir limiter cette compétence. Un PJ ne peut pas passer son temps à chercher des passages secrets sans
 * qu'il y ait une répercussion sur le temps et l'énergie que ça lui coute.
 * 1) soit la commande "prend du temps"...
 * 2) soit la commande prend de l'énergie (celle utilisée pour effectuer les autres actions). L'energie se recharge au cour du temps
 * 3) soit la commande nécessite un outil consommable (une "sonde"...)
 * 4) soit un item du type carte au trésor ou indice
 */

/**
 * Chercher dans une direction s'il existe une issue secrete, ou si l'issue visible est piégée
 * @param context {MUDContext}
 * @param sDirection {string}
 * @param oTool {MUDEntity|null}
 */
function searchDirection2 (context, sDirection, oTool = null) {
  const { print, engine, pid } = context
  const { valid, secret, visible, dcSearch } = engine.getExit(pid, sDirection)
  // obtenir valeur skill
  // déterminer succès
  // a-t-on besoin de détecter la porte ?
  const bNeedDetectExit = valid && secret && !visible
  if (!bNeedDetectExit) {
    print('search.failure.exitNotFound', { dir: sDirection })
    return
  }
  const oPlayer = engine.getEntity(pid)
  const idRoom = oPlayer.location
  // La découverte d'un secret sera partagée avec les autres joueurs de la pièce
  const aOtherPlayers = engine.getLocalEntities(idRoom)
    .filter(({ id, entity }) => entity.blueprint.type === engine.Const.ENTITY_TYPES.PLAYER)
    .map(({ entity }) => entity)
  if (engine.resolveTaskOutcome(oPlayer, 'search', dcSearch, oTool)) {
    // partager la découverte avec les autres joueurs de la pièce
    aOtherPlayers
      .forEach(({ id }) => {
        engine.setEntityExitSpotted(id, sDirection, true)
      })
    // on a découvert une nouvelle porte cachée : le dire à tous
    print('search.action.exitFound', { dir: sDirection })
    print.room('search.room.exitFound', { name: oPlayer.name, dir: sDirection })
  } else if (!visible) {
    // on n'arrive pas à déceler la porte
    print('search.failure.exitNotFound', { dir: sDirection })
  }
}

/**
 * Chercher dans une direction s'il existe une issue secrete, ou si l'issue visible est piégée
 * @param context {MUDContext}
 * @param sDirection {string}
 * @param oTool {MUDEntity|null}
 */
function searchDirection (context, sDirection, oTool = null) {
  const { print, engine, pid } = context
  const { success, outcome } = engine.actionSearchSecret(pid, sDirection, oTool ? oTool.id : null)
  const { valid, secret, visible, dcSearch } = engine.getExit(pid, sDirection)
  // obtenir valeur skill
  // déterminer succès
  // a-t-on besoin de détecter la porte ?
  if (outcome === 'NO_NEED_DETECTION') {
    print('search.failure.exitNotFound', { dir: sDirection })
    return
  }
  const oPlayer = engine.getEntity(pid)
  const idRoom = oPlayer.location
  // La découverte d'un secret sera partagée avec les autres joueurs de la pièce
  const aOtherPlayers = engine.getLocalEntities(idRoom)
    .filter(({ id, entity }) => entity.blueprint.type === engine.Const.ENTITY_TYPES.PLAYER)
    .map(({ entity }) => entity)
  if (success) {
    // partager la découverte avec les autres joueurs de la pièce
    aOtherPlayers
      .forEach(({ id }) => {
        engine.setEntityExitSpotted(id, sDirection, true)
      })
    // on a découvert une nouvelle porte cachée : le dire à tous
    print('search.action.exitFound', { dir: sDirection })
    print.room('search.room.exitFound', { name: oPlayer.name, dir: sDirection })
  } else {
    // on n'arrive pas à déceler la porte
    print('search.failure.exitNotFound', { dir: sDirection })
  }
}

function main (context, params) {
  context.parse(params, {
    [context.PARSER_PATTERN.DIRECTION_WITH_ITEM]: p => searchDirection(context, p.direction, p.item),
    [context.PARSER_PATTERN.DIRECTION]: p => searchDirection(context, p.direction),
    [context.PARSER_PATTERN.NONE]: () => context.print('generic.error.needArgument'),
    [context.PARSER_PATTERN.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
  })
}

main($context, $parameters)

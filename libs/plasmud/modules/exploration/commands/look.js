const {
  getContainedEntities
} = require('helpers/containers') // this script is in this module in the "helpers" subdir...

/**
 * LOOK
 * @author ralphy
 * @date 25-12-2021
 * Le joueur utilise cette commande pour regarder la description de :
 * 1) la pièce dans laquelle il se trouve.
 * 2) n'importe quelle entité du jeu (créature, meuble, objet, issue).
 * 3) un objet de l'inventaire du dernier contenant ouvert
 */

function getRoomDescriptor (oContext) {
  const { pid, engine, text } = oContext
  const oPlayer = engine.getEntity(pid)
  const idRoom = oPlayer.location
  const oRoom = engine.getRoom(idRoom)
  const name = oRoom.name
  const desc = oRoom.desc
  const aExitStatus = Object.keys(oRoom.nav).map(dir => engine.getExit(pid, dir))
  const exits = aExitStatus.filter(e => e.visible).map(e => ({
    dir: e.direction,
    label: text('dir.' + e.direction),
    locked: e.locked,
    lockable: e.lockable
  }))
  return {
    name,
    desc,
    exits,
    entities: getContainedEntities(oContext, idRoom)
  }
}

function getItemDescriptor (oContext, oEntity) {
  const { pid, engine, text } = oContext
  // si c'est un contenant, récupérer les liste des objets contenu
  const oInventory = {}
  const bLocked = oEntity.locked
  const bContainer = oEntity.blueprint.inventory
  if (bContainer && !bLocked) {
    Object.assign(oInventory, getContainedEntities(oContext, oEntity.id))
  }
  return {
    name: oEntity.name,
    desc: oEntity.desc,
    type: oEntity.blueprint.subtype,
    weight: oEntity.weight,
    locked: bLocked,
    lockable: oEntity.lockable,
    container: bContainer,
    inventory: oInventory
  }
}

function getPlaceableDescriptor (oContext, oEntity) {
  const { pid, engine, text } = oContext
  // si c'est un contenant, récupérer les liste des objets contenu
  const oInventory = {}
  const bLocked = oEntity.locked
  const bContainer = oEntity.blueprint.inventory
  if (bContainer && !bLocked) {
    Object.assign(oInventory, getContainedEntities(oContext, oEntity.id))
  }
  return {
    name: oEntity.name,
    desc: oEntity.desc,
    type: oEntity.blueprint.subtype,
    locked: bLocked,
    lockable: oEntity.lockable,
    container: bContainer,
    inventory: oInventory
  }
}

function getActorDescriptor ({ pid, engine, text }, entity) {
  return {
    name: entity.name,
    desc: entity.desc,
    dead: entity.dead
  }
}

function getPlayerDescriptor ({ pid, engine, text }, entity) {
  return {
    name: entity.name,
    desc: entity.desc,
    dead: entity.dead
  }
}

function getDirectionDescriptor ({ pid, engine, text }, direction) {
  const oExit = engine.getExit(pid, direction)
  if (oExit.valid && oExit.visible) {
    return {
      direction: text('toDirection', { dir: direction }),
      name: oExit.name,
      desc: oExit.desc.length > 0 ? oExit.desc : null,
      locked: oExit.locked,
      lockable: oExit.locked,
      discovered: true,
      roomdest: engine.getRoom(oExit.destination).name
    }
  } else {
    return null
  }
}

/**
 * Affiche la description d'un objet
 * @param context
 * @param entity {MUDEntity}
 */
function lookEntity (context, entity) {
  const { print } = context
  const CONST = context.engine.Const
  switch (entity.blueprint.type) {
    case CONST.ENTITY_TYPES.ITEM: {
      const oDescriptor = getItemDescriptor(context, entity)
      print('look/item', oDescriptor)
      break
    }
    case CONST.ENTITY_TYPES.PLACEABLE: {
      const oDescriptor = getItemDescriptor(context, entity)
      print('look/placeable', oDescriptor)
      break
    }
    case CONST.ENTITY_TYPES.ACTOR: {
      const oDescriptor = getActorDescriptor(context, entity)
      print('look/actor', oDescriptor)
      break
    }
    case CONST.ENTITY_TYPES.PLAYER: {
      const oDescriptor = getPlayerDescriptor(context, entity)
      print('look/player', oDescriptor)
      break
    }
  }
}

/**
 * affiche la description d'une direction
 * si la direction est invalide : affiche un message d'erreur
 * @param context
 * @param direction {string}
 */
function lookDirection (context, direction) {
  const { print } = context
  const oDescriptor = getDirectionDescriptor(context, direction)
  if (oDescriptor) {
    print('look/direction', oDescriptor)
  } else {
    print('generic.error.directionInvalid')
  }
}

/**
 * Affichage d'un descriptif de la pièce dans laquel se trouve le joueur
 * @param context
 */
function lookRoom (context) {
  const { print } = context
  const oDescriptor = getRoomDescriptor(context)
  print('look/room', oDescriptor)
}

function main (context, aArguments) {
  context.parse(aArguments, {
    [context.PARSER_PATTERN.ITEM_FROM_CONTAINER]: p => lookEntity(context, p.item),
    [context.PARSER_PATTERN.DIRECTION]: p => lookDirection(context, p.direction),
    [context.PARSER_PATTERN.ENTITY]: p => lookEntity(context, p.entity),
    [context.PARSER_PATTERN.ITEM]: p => lookEntity(context, p.item),
    [context.PARSER_PATTERN.NONE]: () => lookRoom(context),
    [context.PARSER_PATTERN.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
  })
}

main($context, $parameters)

function main (context, text) {
  const { print, engine, pid } = context
  const sText = text.join(' ')
  const oPlayer = engine.getEntity(pid)
  print('say.action.speak', { text: sText })
  print.room('say.room.speak', { text: sText, name: oPlayer.name })
  engine.speakString(pid, oPlayer.location, sText)
}

main($context, $parameters)

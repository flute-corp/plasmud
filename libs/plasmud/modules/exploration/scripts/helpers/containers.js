function getRegisterCategory (aRegister, type) {
  const c = aRegister.find(e => e.type === type)
  if (c) {
    return c
  } else {
    const oCat = {
      type,
      label: '',
      entities: []
    }
    aRegister.push(oCat)
    return oCat
  }
}

/**
 * Renvoie la liste des item contenu dans un contenant,
 * ce contenant peut etre :
 * - une entité ou créature ayant un inventaire
 * - une pièce
 * @param pid
 * @param engine
 * @param text
 * @param idContainer
 * @returns {{}}
 */
function getContainedEntities ({ pid, engine, text }, idContainer) {
  const entities = []
  const TYPES = [
    engine.Const.ENTITY_TYPES_PLACEABLE,
    engine.Const.ENTITY_TYPES_ITEM,
    engine.Const.ENTITY_TYPES_ACTOR,
    engine.Const.ENTITY_TYPES_PLAYER
  ]
  const oPlayer = engine.getEntity(pid)
  engine
    .getLocalEntities(idContainer)
    .forEach(({ lid, entity }, i, aEntities) => {
      if (entity === oPlayer) {
        return
      }
      getRegisterCategory(entities, entity.blueprint.type).entities.push({
        lid,
        name: entity.name,
        dead: entity.dead,
        remark: entity.remark
      })
    })
  // Enrichir le label
  entities.forEach(e => {
    e.order = TYPES.indexOf(e.type)
    e.label = text('look.section', { count: e.entities.length, type: e.type })
  })
  // Trier par catégories
  return entities.sort((a, b) => a.order - b.order)
}

function getInventoryItems ({ pid, engine, text }, idContainer) {
  const entities = []
  const oPlayer = engine.getEntity(pid)
  engine
    .getLocalEntities(idContainer)
    .forEach(({ lid, entity }, i, aEntities) => {
      if (entity === oPlayer) {
        return
      }
      const sType = entity.blueprint.type
      if (sType !== engine.Const.ENTITY_TYPES_ITEM) {
        throw new Error('ERR_NON_ITEM_ENTITY_IN_INVENTORY')
      }
      const sSubType = entity.blueprint.subtype
      const oCat = getRegisterCategory(entities, sSubType)
      /*
      if (!(sSubType in entities)) {
        entities[sSubType] = {
          label: text('inventory.section', { count: aEntities.length, type: sSubType }),
          entities: []
        }
      }
       */
      oCat.entities.push({
        lid,
        name: entity.name
      })
    })
  // Enrichir le label
  entities.forEach(e => {
    e.label = text('inventory.section', { count: e.entities.length, type: e.type })
  })
  return entities
}

module.exports = {
  getContainedEntities,
  getInventoryItems
}

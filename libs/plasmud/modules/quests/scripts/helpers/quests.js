const Quest = require('@libs/questlib')
const debug = require('debug')
const log = debug('mod:quests')

/**
 * accepted, limit,
 * @typedef QuestData {object} metadata de la quete
 * @property area {string} référence à une zone géographique
 */

class QuestHelper {
  constructor () {
    log('starting quest helper')
    this.DURATION_DECORATIONS = {}
    this._questManager = new Quest.Manager()
    this._questManager.events.on('quest.advance', ({ player, quest, chapter, status }) => {
      const sTitle = this._questManager.quests[quest].title
      log('%s quest #%s : %s ; progress to chapter %d with status %s.', player.id, quest, sTitle, chapter, status)
    })
    this._questManager.events.on('quest.abort', ({ player, quest }) => {
      const sTitle = this._questManager.quests[quest].title
      log('%s aborts quest #%s : %s', player.id, quest, sTitle)
    })
    this._questManager.events.on('quest.accept', ({ player, quest }) => {
      const sTitle = this._questManager.quests[quest].title
      log('%s accepts quest #%s : %s', player.id, quest, sTitle)
    })
  }

  initDecorations (t) {
    const DD = this.DURATION_DECORATIONS
    const t2 = (k, v) => t('quests.durationDecorations.' + k, v)
    if (!DD.init) {
      const aKeys = ['year', 'month', 'day', 'hour', 'minute', 'second']
      DD.init = true
      aKeys.forEach(k => {
        DD[k] = t2('proxy', { dur: k })
        DD[k + 's'] = t2('proxy', { dur: k + 's' })
      })
      DD.now = t2('now', {})
    }
  }

  get questManager () {
    return this._questManager
  }

  /**
   * Chargement des assets de quête.
   * @param oAssets {object}
   */
  loadAssets (oAssets) {
    if (!oAssets) {
      throw new Error('WTF no asset')
    }
    let nQuests = 0
    for (const oAsset of Object.values(oAssets)) {
      this._questManager.defineQuest(oAsset)
      ++nQuests
    }
    log('%d quest(s) loaded', nQuests)
  }

  /**
   * Liste des quêtes de l'entité
   * La liste doit pouvoir s'afficher sous forme de tableau à colonnes.
   * colonne 1 : identifiant de la quête, utile pour rapidement référencer la quête
   * colonne 2 : Nom de la quête
   * colonne 3 : Nom de la zone
   *
   * MAX_LINES:
   * SORT_BY: nearest_region, date, difficulty
   * PAGE:
   *
   * @param oEntity
   * @param oFilters {object} filtre de selection : généralement par zone
   */
  getQuestList (oEntity, oFilters = {}) {
    const r = this._questManager.getEntityQuestReportList(oEntity)
    return r.map(q => ({
      id: q.qid,
      title: q.title,
      chapter: q.chapter.title,
      status: q.status,
      date: q.date,
      duration: q.duration
    }))
  }

  getQuestDetail (oEntity, idQuest) {
    return this._questManager.getEntityQuestReport(oEntity, idQuest)
  }

  advanceQuest (oEntity, idQuest, ref) {
    this._questManager.advanceEntityQuest(oEntity, idQuest, ref)
    const oQuest = this._questManager.getEntityQuestProgress(oEntity, idQuest)
    oQuest.date = Date.now()
    log('%s is advancing quest %s', oEntity.id, idQuest)
  }

  /**
   * L'entité accepte une quête.
   * @param oEntity {MUDEntity}
   * @param idQuest {string}
   */
  acceptQuest (oEntity, idQuest) {
    this._questManager.addQuest(oEntity, idQuest)
    const oQuest = this._questManager.getEntityQuestProgress(oEntity, idQuest)
    oQuest.date = Date.now()
    log('%s has accepted quest %s', oEntity.id, idQuest)
    return oQuest
  }

  getQuestState (oEntity, idQuest) {
    const oQuest = this._questManager.getEntityQuestProgress(oEntity, idQuest)
    return oQuest.state
  }
}

module.exports = new QuestHelper()

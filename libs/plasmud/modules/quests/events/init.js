const questHelper = require('helpers/quests')

const debug = require('debug')
const log = debug('mod:quests')

function main (evt) {
  log('Quest module')
  log('An extension to the base system, providing quest and mission management.')
  questHelper.loadAssets(evt.system.getAsset('quests'))
  questHelper.initDecorations((k, v) => evt.system.renderText(k, v))
}

main($event)

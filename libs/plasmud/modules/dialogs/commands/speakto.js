const entityDialogManager = require('helpers/dialogs')

function initDialog (context, oEntity) {
  const oPlayer = context.engine.getEntity(context.pid)
  entityDialogManager.createDialogContext(context, oEntity.id, {
    playerName: oPlayer.name
  })
}

function main (context, aArguments) {
  context.parse(aArguments, {
    [context.PARSER_PATTERN.ENTITY]: p => initDialog(context, p.entity),
    [context.PARSER_PATTERN.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
  })
}

main($context, $parameters)

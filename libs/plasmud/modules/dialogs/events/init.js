const entityDialogManager = require('helpers/dialogs')
const debug = require('debug')

function doDialogAction (oSystem, oEvent) {
  const { action, player, actor, variables } = oEvent
  oSystem.dsm.runScript(action, {
    $event: {
      engine: oSystem.engine,
      player,
      actor,
      variables
    }
  })
}

function doDialogCondition (oSystem, oEvent) {
  const { condition, result, player, actor, variables } = oEvent
  oSystem.dsm.runScript(condition, {
    $event: {
      engine: oSystem.engine,
      player,
      actor,
      variables,
      result
    }
  })
}

function main (evt) {
  entityDialogManager.events.on('dialog.action', oEvent => doDialogAction(evt.system, oEvent))
  entityDialogManager.events.on('dialog.condition', oEvent => doDialogCondition(evt.system, oEvent))
  const log = debug('mod:dialogs')
  log('Dialog module')
  log('An extension to the base system, providing dialog support.')
  entityDialogManager.dialogs = evt.system.getAsset('dialogs')
}

main($event)

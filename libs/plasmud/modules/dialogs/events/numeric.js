const entityDialogManager = require('helpers/dialogs')
/**
 *
 * @param engine {Engine}
 * @param command {number}
 * @param $context {MUDContext}
 */
function main ({ engine, command, $context }) {
  /**
   * @type {}
   */
  const dlgctx = entityDialogManager.getDialogContext($context.pid)
  if (dlgctx) {
    try {
      dlgctx.selectOption(command - 1)
    } catch (e) {
      console.error(e)
      // L'option n'existe pas
    }
  } else {
    $context.print('dialogModule.failure.noActiveDialog')
  }
}

main($event)

const path = require('path')
const util = require('util')
const Events = require('events')
const I18n = require('../i18n-manager')
const TwigManager = require('../twig-manager')
const Engine = require('./Engine')
const mc = require('../multiple-chrono')
const deepMerge = require('../deep-merge')
const { AbortError } = require('./errors')
const debug = require('debug')
const ResourceLoader = require('../resource-loader')
const DynamicScriptManager = require('../dynamic-script-manager')
const TextRenderer = require('./TextRenderer')
const { getType } = require('../get-type')
const Parser = require('./helpers/parser')
const ModuleConfigParser = require('./ModuleConfigParser')

const logMud = debug('serv:plasmud')

/**
 * @typedef MUDContext
 * @property system {System}
 * @property engine {Engine}
 * @property parser {Parser}
 * @property pid {string}
 * @property uid {string}
 * @property print {object}
 * @property text {function}
 * @property abort {function}
 * @property PARSER_PATTERN {Object.<string, string>}
 */

/**
 * Gestion du MUD au niveau système.
 * @desc Cette classe permet de construire tout l'environnement technique nécessaire au bon fonctionnement de l'Engine.
 * Propose la fonctionnalité de module, propose la possibilité de charger les commands, les templates, les chaînes
 * d'internationalisation.
 * @class System
 */
class System {
  constructor () {
    this._acceptNumericCommand = true
    this._language = 'common'
    this._strings = {}
    this._i18n = new I18n()
    this._templateManager = new TwigManager()
    this._engine = new Engine()
    this._textRenderer = new TextRenderer({ intl: this._i18n, tpl: this._templateManager })
    this._players = {}
    this._events = new Events()
    this._contextAdditionalProperties = {}
    this._module = {
      blueprints: {},
      sectors: {},
      rooms: {},
      data: {}
    }
    this._dupChecker = new Set()
    this._scripts = {
      commands: {},
      scripts: {},
      events: {},
      modules: {}
    }
    this._dsm = new DynamicScriptManager()
    const sBasePath = path.resolve(path.dirname(require.main.filename), '..')
    this._dsm.aliases['@'] = sBasePath
    this._dsm.aliases['@libs'] = path.join(sBasePath, 'libs')
    for (const sAlias in this._dsm.aliases) {
      logMud('dsm alias %s : %s', sAlias, this._dsm.aliases[sAlias])
    }
    this.declareEventHandlers()
  }

  get textRenderer () {
    return this._textRenderer
  }

  get dsm () {
    return this._dsm
  }

  /**
   * Liste des players enregistrés
   * @returns {*|{}}
   */
  get players () {
    return this._players
  }

  /**
   * Instance de l'Engine
   * @returns {Engine}
   */
  get engine () {
    return this._engine
  }

  /**
   * Instance du gestionnaire d'évènements
   * @returns {module:events.EventEmitter}
   */
  get events () {
    return this._events
  }

  /**
   * Objet de configuration
   * @returns {*|{modules, assets}}
   */
  get config () {
    return this._config
  }

  /**
   * Instance du moteur d'internationalisation des chaînes
   * @returns {I18n}
   */
  get i18n () {
    return this._i18n
  }

  /**
   * Définition de la configuration
   * Permet de définir les modules et les assets.
   * @param modules {object}
   * @param language {string}
   */
  set config ({ modules, language = '' }) {
    this._config = { modules, language }
  }

  getPlayerContext (sId) {
    const oPlayer = Object.values(this._players).find(p => p.context.pid === sId)
    if (oPlayer) {
      return oPlayer.context
    } else {
      return null
    }
  }

  /**
   * Initialisation du système. La configuration doit avoir été initialisée avant d'appeler cette méthode, avec la
   * propriété config.
   * @returns {Promise<System>}
   */
  async init () {
    try {
      logMud('initialization')
      const {
        modules,
        language
      } = this.config
      const c = mc.start()
      const aModuleOutput = modules.map(p => this.addModule(p, language))
      await Promise.all(aModuleOutput.filter(p => p instanceof Promise))
      this.setEngineModule(this._module)
      const t = mc.stop(c)
      logMud(
        '%d script(s) & %d template(s) & %d string(s) loaded in %s',
        Object.keys(this._scripts.events).length + Object.keys(this._scripts.commands).length + Object.keys(this._scripts.scripts).length,
        this._templateManager.count,
        this.countStrings(this._strings),
        this._renderTime(t)
      )
      await this._i18n.init(this._strings)
      this._engine.events.on('entity.create', ({ entity }) => {
        const oEntity = this._engine.getEntity(entity)
        if (oEntity.blueprint.type === Engine.CONST.ENTITY_TYPES.PLAYER) {
          const idPlayer = oEntity.uid
          logMud('player entity created : user::%s -> %s', idPlayer, oEntity.id)
          this._players[idPlayer].context.pid = oEntity.id
        }
      })
      this._engine.events.on('resolve.textref', oEvent => {
        oEvent.text = this.renderText(oEvent.textref, oEvent.context)
      })
      logMud('initializing modules')
      this._events.emit('init')
      logMud('module initialization done.')
    } catch (e) {
      logMud('[err]', 'something went wrong during events phase')
      throw e
    }
    return this
  }

  declareEventHandlers () {
    // L'engine commande l'exécution d'un script lié à une entité
    this.engine.events.on('entity.event.script', ({
      script,
      payload
    }) => {
      if (!('engine' in payload)) {
        throw new Error('WTF')
      }
      this._dsm.runScript(
        script,
        {
          $event: payload
        }
      )
    })
  }

  output (id, content) {
    this._events.emit('output', {
      id,
      content
    })
  }

  renderText (sString, oVariables) {
    return this._textRenderer.render(sString, oVariables)
  }

  /**
   * Enregistre un player dans le système et lui attribue une Entité et un contexte.
   * @param uid {string} identifiant du player
   * @return {{ context: MUDContext }|null}
   */
  registerPlayer (uid) {
    if (this.isPlayerRegistered(uid)) {
      logMud('failed to register user::%s : already registered !', uid)
      return null
    }
    logMud('user::%s has been registered', uid)
    const text = (sString, oVariables) => this.renderText(sString, oVariables)

    const print = (sString, oVariables) => {
      this.renderText(sString, oVariables)
        .split('\n').forEach(s => this.output(uid, s))
    }

    const getPlayerRoomPlayers = () => this
      ._engine
      .getLocalEntities(this._engine.getEntity(context.pid).location)
      .map(({ entity }) => entity)
      .filter(entity => entity.blueprint.type === this._engine.Const.ENTITY_TYPES.PLAYER)

    const sendPrintToPlayers = (aPlayers, aArguments) => {
      aPlayers
        .map(entity => entity.uid) // ne garder que les uid de chaque entity
        .forEach(uid => this._players[uid].context.print(...aArguments)) // envoyer le message
    }

    print.log = (...aArguments) => logMud(...aArguments)
    print.room = (...aArguments) => {
      const aPlayers = getPlayerRoomPlayers().filter(entity => entity.uid !== uid)
      sendPrintToPlayers(aPlayers, aArguments)
    }
    print.room.all = (...aArguments) => {
      const aPlayers = getPlayerRoomPlayers()
      sendPrintToPlayers(aPlayers, aArguments)
    }
    print.room.except = (aEntitiesId, ...aArguments) => {
      const aPlayers = getPlayerRoomPlayers()
        .filter(entity => !aEntitiesId.includes(entity.id))
      sendPrintToPlayers(aPlayers, aArguments)
    }
    print.to = (id, ...aArguments) => {
      const e = this._engine
      if (e.isEntityExist(id)) {
        /**
         * @type {MUDEntity}
         */
        const oPlayer = e.getEntity(id)
        if (oPlayer.blueprint.type === e.Const.ENTITY_TYPES_PLAYER) {
          const uid = oPlayer.uid
          this._players[uid].context.print(...aArguments)
        } else {
          logMud('attempt to send message to an non-player entity : %s (type: %s)', id, oPlayer.blueprint.type)
        }
      } else {
        logMud('attempt to send message to an unknown player entity : %s', id)
      }
    }
    const engine = this._engine
    const abort = sMessage => this.abort(sMessage)
    const context = {
      system: this,
      engine,
      pid: '*',
      uid,
      print,
      text,
      abort,
      ...this._contextAdditionalProperties
    }
    context.parse = (aArguments, oPatterns) => Parser.parse(context, aArguments, oPatterns)
    context.PARSER_PATTERN = Parser.PARSER_PATTERNS

    this._players[uid] = {
      context
    }

    return this._players[uid]
  }

  /**
   * Renvoie true si le joueur est déja enregistré dans le plasmud
   * @param uid {number|string}
   * @returns {boolean}
   */
  isPlayerRegistered (uid) {
    return uid.toString() in this._players
  }

  /**
   * Supprime un player du système.
   * Ne supprime pas l'entité qui est associée au joueur.
   * @param uid {string} identifiant du player
   */
  unregisterPlayer (uid) {
    if (this.isPlayerRegistered(uid)) {
      logMud('user::%s is unregistered from plasmud.', uid)
      delete this._players[uid]
    } else {
      logMud('failed to remove unregistered user::%s !', uid)
    }
  }

  countStrings (s) {
    switch (getType(s)) {
      case 'string': {
        return 1
      }
      case 'object': {
        return this.countStrings(Object.values(s))
      }
      case 'array': {
        let n = 0
        for (const value of s) {
          n += this.countStrings(value)
        }
        return n
      }
      default: {
        return 0
      }
    }
  }

  /**
   * Prend en charge le fichier de configuration d'un module
   * @param oConfig {{}}
   */
  addModuleConfig (oConfig) {
    if (this._module.data) {
      Object.assign(this._module.data, oConfig)
    } else {
      this._module.data = oConfig
    }
  }

  parseConfigLanguages (lang) {
    switch (getType(lang)) {
      case 'string': {
        return this.parseConfigLanguages(lang.split(',').map(s => s.trim()))
      }

      case 'array': {
        const o = {
          _default: ''
        }
        lang.forEach(s => {
          if (o._default === '') {
            o._default = s
          }
          o[s] = s
        })
        return o
      }

      case 'object': {
        const _default = '_default' in lang
          ? lang._default
          : Object.keys(lang).shift()
        return {
          ...lang,
          _default
        }
      }

      default: {
        return {}
      }
    }
  }

  async addModule (sLocation) {
    const sLang = this._config.language
    const sModuleName = path.basename(sLocation, '.zip')
    logMud('loading module "%s" using language "%s"', sModuleName, sLang === '' ? 'NONE' : sLang)
    const rl = new ResourceLoader()
    const oResources = await rl.load(sLocation)

    const oConfig = oResources['config.json'] || {
      startingLocation: '',
      defaultLanguage: '',
      languages: ''
    }
    const mcp = new ModuleConfigParser(oConfig)
    this.addModuleConfig(oConfig)
    const sLangPath = mcp.getLangPath(sLang)

    const PATH_SEPARATOR = '/'
    const sPathSepLang = sLangPath !== ''
      ? PATH_SEPARATOR + sLangPath + PATH_SEPARATOR
      : PATH_SEPARATOR
    const ASSET_LOCATION = 'assets' + sPathSepLang

    logMud('module %s assets location : %s', sModuleName, ASSET_LOCATION)

    const rFindAsset = new RegExp('^' + ASSET_LOCATION + '(?!templates' + PATH_SEPARATOR + '|strings' + PATH_SEPARATOR + ')([-a-z0-9]+)' + PATH_SEPARATOR)
    const rFindScriptCommand = new RegExp('^(scripts|commands)' + PATH_SEPARATOR + '(.*)\\.js$')
    const rFindEvent = new RegExp('^events' + PATH_SEPARATOR + '(.*)\\.js$')
    const rFindTemplate = new RegExp('^' + ASSET_LOCATION + 'templates' + PATH_SEPARATOR + '(.*)\\.twig$')
    const rFindString = new RegExp('^' + ASSET_LOCATION + 'strings' + PATH_SEPARATOR)

    const tryRegisterEntryAsAsset = (sResourceName, data) => {
      const r = sResourceName.match(rFindAsset)
      if (Array.isArray(r)) {
        const sAssetType = r[1]
        const sId = path.basename(sResourceName, '.json')
        this.defineAsset(sId, sAssetType, data)
      }
    }

    const tryRegisterEntryAsScript = (sResourceName, data) => {
      const r = sResourceName.match(rFindScriptCommand)
      if (Array.isArray(r)) {
        const sType = r[1]
        const sId = r[2]
        this._scripts[sType][sId] = this._dsm.registerScript(sId, data)
      }
    }

    const tryRegisterEntryAsEvent = (sResourceName, data) => {
      const r = sResourceName.match(rFindEvent)
      if (Array.isArray(r)) {
        const sId = path.basename(r[1])
        const oScript = this._dsm.compileScript(sId, data)
        const se = this._scripts.events
        if (!(sId in se)) {
          se[sId] = []
          switch (sId) {
            case 'output':
            case 'command':
            case 'numeric':
            case 'init': {
              this.events.on(sId, event => {
                this.runEventScript(sId, event)
              })
              break
            }

            case 'entity.event.script': {
              this.engine.events.on(sId, ({
                script,
                payload
              }) => {
                this._dsm.runScript(
                  script,
                  {
                    $event: payload
                  }
                )
              })
              break
            }

            case 'entity.destroy':
            case 'entity.create':
            case 'entity.move':
            case 'entity.speak':
            case 'entity.task.attempt':
            case 'item.acquire':
            case 'item.drop':
            case 'item.lose':
            case 'container.unlock':
            case 'container.unlock.failure':
            case 'exit.unlock':
            case 'exit.unlock.failure': {
              this.engine.events.on(sId, event => {
                this.runEventScript(sId, event)
              })
              break
            }
          }
        }
        se[sId].push(oScript)
      }
    }

    const tryRegisterEntryAsStrings = (sResourceName, data) => {
      const r = sResourceName.match(rFindString)
      if (Array.isArray(r)) {
        this.addStrings(data)
      }
    }

    const tryRegisterEntryAsTemplate = (sResourceName, data) => {
      const r = sResourceName.match(rFindTemplate)
      if (Array.isArray(r)) {
        const idTemplate = r[1]
        this._templateManager.defineTemplate(idTemplate, data)
      }
    }

    for (const [sResourceName, data] of Object.entries(oResources)) {
      tryRegisterEntryAsAsset(sResourceName, data)
      tryRegisterEntryAsScript(sResourceName, data)
      tryRegisterEntryAsEvent(sResourceName, data)
      tryRegisterEntryAsStrings(sResourceName, data)
      tryRegisterEntryAsTemplate(sResourceName, data)
    }
  }

  /**
   * Compose un chaine en seconde ou milisecondes en fonction de la grandeur de la valeur passée en paramètre
   * @param nTime {number} durée en ms
   * @return {string} chaine de rendu
   * @private
   */
  _renderTime (nTime) {
    return nTime >= 1000 ? (Math.floor(nTime / 100) / 10).toString() + 's' : nTime.toString() + 'ms'
  }

  /**
   * Déclenche une exception. Cette methode est utilisé par les commandes afin de s'auto-terminer.
   * @param sWhy {string} raison pour laquelle la commande se termine.
   */
  abort (sWhy) {
    throw new AbortError(sWhy)
  }

  /**
   * Détermine si une commande peut être lancée, fait appel à un évènement "command" que les module pourront écouter
   * afin de définir "allowed".
   * @param uid {string} identifiant du player qui lance la commande
   * @param sCommand {string} commande lancée
   * @return {{uid, reason: string, allowed: boolean, command}}
   */
  checkCommandUsability (uid, sCommand) {
    const oData = {
      system: this,
      command: sCommand,
      allowed: true,
      uid,
      reason: ''
    }
    if (!uid) {
      oData.reason = 'admin command'
      return oData
    }
    this._events.emit('command', oData)
    return oData
  }

  isNumericCommand (sCommand) {
    const nCommand = parseInt(sCommand)
    return !isNaN(nCommand)
  }

  /**
   * Renvoie true si la commande spécifiée existe.
   * @param sCommand {string} opcode d ela commande
   * @returns {boolean} true = la commande existe
   */
  isCommandExist (sCommand) {
    if (sCommand in this._scripts.commands) {
      return true
    }
    return !!(this.isNumericCommand(sCommand) && this._acceptNumericCommand)
  }

  /**
   * Renvoie true si la pièce dans laquelle se trouve le joueur possède un hook de la commande spécifiée
   * @param uid {string} identifiant joueur
   * @param sCommand {string} commande envoyée
   * @return {string|undefined}
   */
  getRoomCommandHookScript (uid, sCommand) {
    const { context } = this._players[uid]
    const oPlayer = this._engine.getEntity(context.pid)
    const sRoom = oPlayer.location
    const oRoom = this._engine.getRoom(sRoom)
    const oCommandRegistry = oRoom.commands
    return oCommandRegistry !== undefined && (sCommand in oCommandRegistry)
      ? oCommandRegistry[sCommand]
      : undefined
  }

  /**
   * Lance un script en fonction de la commande.
   * @param uid {string} identifiant du joueur
   * @param sCommand {string} commande
   * @param args {string[]} liste des arguments
   * @throws
   */
  command (uid, sCommand, args) {
    /**
     * @var context {MUDContext}
     */
    const { context } = this._players[uid]
    // Est ce que la pièce dans lequel se trouve le joueur
    // possède un event "command" ?
    const sRoomCommandScript = this.getRoomCommandHookScript(uid, sCommand)
    if (sRoomCommandScript) {
      const oCmdPayload = {
        $context: context,
        $parameters: args
      }
      const symPreventDefault = Symbol('__preventDefault__')
      Object.defineProperty(oCmdPayload, symPreventDefault, {
        enumerable: false,
        configurable: false,
        writable: true,
        value: false
      })
      Object.defineProperty(oCmdPayload, '$preventDefault', {
        enumerable: true,
        configurable: false,
        writable: false,
        value: function (v = true) {
          oCmdPayload[symPreventDefault] = v
        }
      })
      const xResult = this._dsm.runScript(sRoomCommandScript, oCmdPayload)
      if (oCmdPayload[symPreventDefault]) {
        return xResult
      }
    }
    if (this.isCommandExist(sCommand)) {
      // vérifier s'il est possible de lancer cette commande (droit, état)
      const {
        allowed,
        reason
      } = this.checkCommandUsability(uid, sCommand)
      if (allowed) {
        if (this.isNumericCommand(sCommand)) {
          this._events.emit('numeric', {
            $context: context,
            $parameters: args,
            command: parseInt(sCommand)
          })
        } else {
          return this._dsm.runScript(this._scripts.commands[sCommand], {
            $context: context,
            $parameters: args
          })
        }
      } else {
        context.print(reason)
      }
    } else {
      throw new Error('ERR_UNKNOWN_COMMAND')
    }
  }

  defineAsset (id, sCategory, oData) {
    // Checking ID duplicates
    const oDupChecker = this._dupChecker
    if (oDupChecker.has(id)) {
      throw new Error('Asset id duplication : ' + id)
    }
    oDupChecker.add(id)
    const m = this._module
    if (!(sCategory in m)) {
      m[sCategory] = {}
    }
    m[sCategory][id] = oData
  }

  /**
   * Chargement des chaines de caractères
   * @param oStrings {object}
   */
  addStrings (oStrings) {
    if (Array.isArray(oStrings)) {
      oStrings.forEach(s => this.addStrings(s))
    } else {
      const sLang = '_language' in oStrings ? oStrings._language : 'common'
      if (!(sLang in this._strings)) {
        this._strings[sLang] = {}
      }
      deepMerge(this._strings[sLang], oStrings)
    }
  }

  /**
   * Ajoute une propriété au contexte qui sera utilisé dans les commandes
   * @param sProperty {string} nom de la propriété
   * @param value {*} valeur de la propriété
   */
  addContextProperty (sProperty, value) {
    this._contextAdditionalProperties[sProperty] = value
  }

  runEventScript (sEvent, oEvent) {
    const aEventScripts = this._scripts.events[sEvent]
    const eventObject = {
      system: this,
      engine: this._engine,
      ...oEvent
    }
    if (aEventScripts) {
      for (const script of aEventScripts) {
        this._dsm.runScript(
          script,
          {
            $event: eventObject
          }
        )
      }
    }
    return eventObject
  }

  /**
   * Charge l'état initial du MUD dans l'engine.
   * @param oModule {object}
   */
  setEngineModule (oModule) {
    this._engine.setAssets(oModule)
    Object.keys(oModule).forEach(m => {
      const n = Object.values(oModule[m]).length
      logMud('%s: %d item%s loaded', m, n, n > 1 ? 's' : '')
    })
  }

  getAsset (sCategory, sId = undefined) {
    if (sCategory in this._module) {
      if (sId !== undefined) {
        return this._module[sCategory][sId]
      } else {
        return this._module[sCategory]
      }
    } else {
      return {}
    }
  }
}

module.exports = System

const ArgumentParser = require('./argument-parser')
const {
  findLidOrObject,
  findDirection,
  findContainer,
  findItem,
  findActorOrPlayer
} = require('./object-finder')

const PARSER_PATTERNS = {
  ITEM_FROM_CONTAINER: 'ITEM IN CONTAINER', // Identifie un item suivi d'un contenant. L'item est nécessairement dans le contenant
  ITEM_TO_CONTAINER: 'ITEM TO CONTAINER', // identifie un item de l'INVENTAIRE suivit d'un contenant
  ITEM_TO_ENTITY: 'ITEM TO ENTITY', // identifie un item de l'INVENTAIRE suivit d'une entity
  DIRECTION_WITH_ITEM: 'DIRECTION WITH ITEM', // identifie une direction et un item de l'INVENTAIRE
  OBJECT_WITH_ITEM: 'OBJECT WITH ITEM', // identifie un objet de la pièce (uniquement)
  DIRECTION: 'DIRECTION', // identifie une direction
  ENTITY: 'ENTITY', // identifie une entité quelle que soit sa nature, de la pièce ou de l'inventaire
  ITEM: 'ITEM', // identifie un item de l'inventaire uniquement
  NONE: 'NONE', // identifie une ligne de commande vide
  DEFAULT: 'DEFAULT' // identifie la ligne quand toutes les autres échouent
}

const KEYWORDS = {
  TO: ['INTO', 'IN', 'TO'],
  IN: ['IN', 'FROM'],
  WITH: ['WITH', 'USING']
}

function du (x) {
  return x === undefined
    ? 'undefined'
    : x === null
      ? 'null'
      : 'defined'
}

function db (x) {
  return !!x
}

/**
 * A partir des arguments spécifiés, tente de reconnaitre un objet (item) à l'intérieur d'un contenant
 * ce parser est utilisé lorsqu'on souhaite regarder ou prendre un objet qui se trouve dans un contenant
 * @param context
 * @param aArguments
 * @returns {{container: MUDEntity, item: MUDEntity, count: number, keyword: string}|null}
 */
function parseItemInContainer (context, aArguments) {
  const ap = new ArgumentParser()
  ap.keywords = KEYWORDS.IN
  const [oItemSpec, oContSpec] = ap.sliceBetweenKeywords(aArguments)
  // il faut s'attendre à deux éléments
  if (oItemSpec && oContSpec) {
    const sKeyword = oContSpec.keyword
    // Déterminer le contenant
    const { pid, engine } = context
    const oPlayer = engine.getEntity(pid)
    // rechercher le container dans la pièce et dans l'inventaire
    /**
     * @type {{count: number, entity: MUDEntity}|null}
     */
    const oContainer = findContainer(context, oContSpec.arguments, [pid, oPlayer.location])
    if (oContainer) {
      // Container trouvé : chercher l'objet
      const idContainer = oContainer.entity.id
      /**
       * @type {{count: number, entity: MUDEntity}|null}
       */
      const oItem = findItem(context, oItemSpec.arguments, [idContainer])
      if (oItem) {
        // item trouvé
        return {
          keyword: sKeyword,
          container: oContainer.entity,
          item: oItem.entity,
          count: oItem.count
        }
      }
    }
  }
  return null
}

/**
 * Recherche un item de l'inventaire du joueur ainsi qu'un contenant
 * @param context
 * @param aArguments
 * @returns {{container, item, count, keyword: string}}
 */
function parseInvItemToContainer (context, aArguments) {
  const ap = new ArgumentParser()
  ap.keywords = KEYWORDS.TO
  const [oItemSpec, oContSpec] = ap.sliceBetweenKeywords(aArguments)
  if (oItemSpec && oContSpec) {
    const sKeyword = oContSpec.keyword
    // Déterminer le contenant
    const { pid, engine } = context
    const oPlayer = engine.getEntity(pid)
    const oContainer = findContainer(context, oContSpec.arguments, [pid, oPlayer.location])
    const oItem = findItem(context, oItemSpec.arguments, [pid])
    if (oContainer && oItem && oContainer.entity.blueprint.inventory) {
      // item trouvé
      return {
        keyword: sKeyword,
        container: oContainer.entity,
        item: oItem.entity,
        count: oItem.count
      }
    }
  }
}

function parseInvItemToEntity (context, aArguments) {
  const ap = new ArgumentParser()
  ap.keywords = KEYWORDS.TO
  const [oItemSpec, oEntitySpec] = ap.sliceBetweenKeywords(aArguments)
  if (oItemSpec && oEntitySpec) {
    const sKeyword = oEntitySpec.keyword
    // Déterminer le contenant
    const { pid, engine } = context
    const oPlayer = engine.getEntity(pid)
    const oTarget = findLidOrObject(context, oEntitySpec.arguments, [oPlayer.location])
    const oItem = findItem(context, oItemSpec.arguments, [pid])
    if (oTarget && oItem) {
      // item trouvé
      return {
        keyword: sKeyword,
        target: oTarget.entity,
        item: oItem.entity,
        count: oItem.count
      }
    }
  }
}

function parseRoomEntityWithItem (context, aArguments) {
  const { engine, pid } = context
  const ap = new ArgumentParser()
  ap.keywords = KEYWORDS.WITH
  const [oContSpec, oItemSpec] = ap.sliceBetweenKeywords(aArguments)
  const oPlayer = engine.getEntity(pid)
  if (oContSpec && oItemSpec) {
    const oRoomEntity = findLidOrObject(context, oContSpec.arguments, [oPlayer.location])
    const oItem = findItem(context, oItemSpec.arguments, [pid])
    return {
      container: oRoomEntity.entity,
      item: oItem.entity
    }
  }
  return null
}

/**
 * A partir des arguments spécifiés, tente de reconnaitre une entité de la pièce (item, creature, placeable)
 * Ce parser est utilisé lorsqu'on veut agir sur une entité de la pièce
 * @param context
 * @param aArguments
 * @returns {{count: number, entity: MUDEntity}|null}
 */
function parseRoomEntity (context, aArguments) {
  const { pid, engine } = context
  const oPlayer = engine.getEntity(pid)
  return findLidOrObject(context, aArguments, [oPlayer.location])
}

/**
 * A partir des arguments spécifiés, tente de reconnaitre un item présent exclusivement dans l'inventaire du PJ
 * Ce parser est utilisé lorsqu'on veut agir sur un item de l'inventaire
 * @param context
 * @param aArguments
 * @returns {{count: number, item: MUDEntity}|null}
 */
function parseInvItem (context, aArguments) {
  const { pid } = context
  const r = findItem(context, aArguments, [pid])
  if (r) {
    if (r.entity) {
      r.item = r.entity
      delete r.entity
      return r
    } else {
      throw new Error('findLidOrObject could not find : ' + aArguments.toString())
    }
  } else {
    return null
  }
}

/**
 * Ce parser est utilisé lorsqu'on veut agir sur une direction avec un item de l'inventaire.
 * Typiquement ouvrir une porte avec une clé, ou crocheter la serrure d'une porte avec un outil
 * @param context
 * @param aArguments
 */
function parseDirectionWithInvItem (context, aArguments) {
  const ap = new ArgumentParser()
  ap.keywords = KEYWORDS.WITH
  const [oDirSpec, oItemSpec] = ap.sliceBetweenKeywords(aArguments)
  if (oDirSpec && oItemSpec) {
    const oDirection = findDirection(context, oDirSpec.arguments.join(' '))
    if (!oDirection) {
      return null
    }
    const oItem = parseInvItem(context, oItemSpec.arguments)
    return {
      keyword: oItemSpec.keyword,
      direction: oDirection.direction,
      item: oItem.item
    }
  }
  return null
}

/**
 * Fonction de parsing d'une ligne de commande.
 * On défini les types de token suivant :
 * - OBJECT : Une entité (item, creature, placeable) présente dans la pièce
 * - ITEM : Une entité de type "item" uniquement, présent dans un container (le dernier ouvert, ou l'inventaire etc...)
 * - DIRECTION : une direction de la pièce courante.
 * - USING : sert de token de séparation spécifiant que la commande se sert d'un élément particulier pour resoudre
 * son action
 * @param context
 * @param aArguments
 * @param oPatterns
 * @returns {null|*}
 */
function parse (context, aArguments, oPatterns) {
  if (PARSER_PATTERNS.DIRECTION_WITH_ITEM in oPatterns) {
    const fn = oPatterns[PARSER_PATTERNS.DIRECTION_WITH_ITEM]
    const r = parseDirectionWithInvItem(context, aArguments)
    if (r) {
      return fn(r)
    }
  }
  if (PARSER_PATTERNS.OBJECT_WITH_ITEM in oPatterns) {
    const fn = oPatterns[PARSER_PATTERNS.OBJECT_WITH_ITEM]
    const r = parseRoomEntityWithItem(context, aArguments)
    if (r) {
      return fn(r)
    }
  }
  if (PARSER_PATTERNS.ITEM_FROM_CONTAINER in oPatterns) {
    const fn = oPatterns[PARSER_PATTERNS.ITEM_FROM_CONTAINER]
    const r = parseItemInContainer(context, aArguments)
    if (r) {
      return fn(r)
    }
  }
  if (PARSER_PATTERNS.ITEM_TO_CONTAINER in oPatterns) {
    const fn = oPatterns[PARSER_PATTERNS.ITEM_TO_CONTAINER]
    const r = parseInvItemToContainer(context, aArguments)
    if (r) {
      return fn(r)
    }
  }
  if (PARSER_PATTERNS.ITEM_TO_ENTITY in oPatterns) {
    const fn = oPatterns[PARSER_PATTERNS.ITEM_TO_ENTITY]
    const r = parseInvItemToEntity(context, aArguments)
    if (r) {
      return fn(r)
    }
  }
  if (PARSER_PATTERNS.DIRECTION in oPatterns) {
    const fn = oPatterns[PARSER_PATTERNS.DIRECTION]
    const r = findDirection(context, aArguments.join(' '))
    if (r) {
      return fn(r)
    }
  }
  if (PARSER_PATTERNS.ENTITY in oPatterns) {
    const fn = oPatterns[PARSER_PATTERNS.ENTITY]
    const r = parseRoomEntity(context, aArguments)
    if (r) {
      return fn(r)
    }
  }
  if (PARSER_PATTERNS.ITEM in oPatterns) {
    const fn = oPatterns[PARSER_PATTERNS.ITEM]
    const r = parseInvItem(context, aArguments)
    if (r) {
      return fn(r)
    }
  }
  if (PARSER_PATTERNS.NONE in oPatterns) {
    const fn = oPatterns[PARSER_PATTERNS.NONE]
    if (aArguments.length === 0) {
      return fn()
    }
  }
  if (PARSER_PATTERNS.DEFAULT in oPatterns) {
    const fn = oPatterns[PARSER_PATTERNS.DEFAULT]
    return fn()
  }
}

module.exports = {
  parse,
  parseItemInContainer,
  parseRoomEntity,
  parseInvItem,
  parseDirectionWithInvItem,
  PARSER_PATTERNS
}

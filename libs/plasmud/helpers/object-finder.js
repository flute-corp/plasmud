const { suggest } = require('@laboralphy/did-you-mean')
const CONSTS = require('../consts')

let DIRECTION_REGISTRY = null

const REGEXP_LID = /^([a-z][0-9]+)$/i
const REGEXP_N_LID = /^([0-9]+) +([a-z][0-9]+)$/i
const REGEXP_LID_N = /^([a-z][0-9]+) +([0-9]+)$/i
const REGEXP_OBJECT = /^(.+)$/
const REGEXP_N_OBJECT = /^([0-9]+) +(.+)$/
const REGEXP_OBJECT_N = /^(.+) +([0-9]+)$/
const REGEXP_OBJECT_X = /^(.+) *#([0-9]+)$/
const REGEXP_N_OBJECT_X = /^([0-9]+) +(.+) *#([0-9]+)$/
const REGEXP_OBJECT_X_N = /^(.+) *#([0-9]+) +([0-9]+)$/

// Permet de filtrer le type de container auquel on souhaite restraindre la commande
const CONTAINER_TYPES = {
  ROOM: 1,
  INVENTORY: 2,
  CONTAINER: 4,
  ANY: 7
}

/**
 * Fonction de recherche avec preset
 * @param sWord {string} mot qu'on a tapé
 * @param aList {string[]} liste de mots valides
 * @returns {string} mot de la liste le plus proche de ce qu'on a tapé
 */
function presetSuggest (sWord, aList) {
  const a = suggest(sWord, aList, { relevance: 234 })
  return a.shift()
}

/**
 * Construit un registre de reconnaissance des direction à partir des nom de directions locales
 * @param text
 * @returns {*[]}
 */
function buildDirectionRegistry ({ text }) {
  const aDirRegistry = []
  const aDirKeys = ['n', 'e', 'w', 's', 'ne', 'nw', 'se', 'sw', 'u', 'd']
  aDirKeys.forEach(d => {
    const aWords = d.split('').map(w => text('dir.' + w))
    const aMiniWords = aWords.map(w => w.substring(0, 1))
    const sExp = aWords.join('.')
    const sMiniExp = aMiniWords.join('')
    aDirRegistry.push({
      r: new RegExp('^(' + sExp + ')$', 'i'),
      direction: d,
      code: false
    })
    aDirRegistry.push({
      r: new RegExp('^(' + sMiniExp + ')$', 'i'),
      direction: d,
      code: false
    })
    aDirRegistry.push({
      r: new RegExp('^(' + sExp + ') +([0-9]+)$', 'i'),
      direction: d,
      code: true
    })
    aDirRegistry.push({
      r: new RegExp('^(' + sMiniExp + ') +([0-9]+)$', 'i'),
      direction: d,
      code: true
    })
  })
  return aDirRegistry
}

/**
 * Construit une fonction qui va mapper une liste d'objet (issue de la fonction getLocalEntities) en une liste
 * d'objet un peu plus riche
 * @returns {function(*): {lid: *, name: *, containerType: *, entity: *}}
 */
function fObjectMapper () {
  return e => ({
    lid: e.lid, // identifiant local de l'objet
    name: e.entity.name, // nom de l'objet
    entity: e.entity // objet
  })
}

/**
 * Fabrique la liste d'objet contenue dans le container spécifié
 * @param context {*}
 * @param idContainer {string} identifiant du conteneur
 * @returns {{lid: string, name: string, containerType: number, entity: *}[]}
 */
function buildContainerObjectList (context, idContainer) {
  const engine = context.engine
  return engine
    .getLocalEntities(idContainer)
    .map(fObjectMapper())
}

/**
 * Construit la liste des objets avec lesquels il est possible d'intéragir dans la pièce
 */
function buildRoomObjectList (context) {
  const engine = context.engine
  const idRoom = engine.getEntity(context.pid).location
  return buildContainerObjectList(context, idRoom)
}

/**
 * Construit la liste des objets avec lesquels il est possible d'intéragir dans l'inventaire'
 */
function buildInventoryObjectList (context) {
  return buildContainerObjectList(context, context.pid)
}

/**
 * Construit la liste des objet qu'il sera possible de consulter pour tenter de trouver celui spécifié par la dernière
 * commande tapée par le joueur.
 * en fonction du type de container type, la fonction se limitera à un certain type de container.
 * ROOM : on se limite à ce que est possé au sol
 * INVENTORY : on se limite à ce qu'il y a dans l'inventaire du joueur
 * CONTAINER : on se limite à ce qu'il y a dans le "dernier container ouvert"
 * @param context
 * @param aContainers
 * @param fEntityFilter {function}
 * @returns {*[]}
 */
function buildObjectList (context, aContainers, fEntityFilter = undefined) {
  const c1 = aContainers.map(idContainer => buildContainerObjectList(context, idContainer)).flat()
  return (typeof fEntityFilter === 'function') ? c1.filter(({ entity }, i, a) => fEntityFilter(entity, i, a)) : c1
}

/**
 * Parse une direction et renvoie le code-directionnel évalué
 * @param context
 * @param sDirection {string}
 * @returns {{direction: string, code: number}|null}
 */
function findDirection (context, sDirection) {
  if (!DIRECTION_REGISTRY) {
    DIRECTION_REGISTRY = buildDirectionRegistry(context)
  }
  for (const { r, direction, code } of DIRECTION_REGISTRY) {
    const result = sDirection.match(r)
    if (result) {
      return {
        direction,
        code: code ? parseInt(result[2]) : 0
      }
    }
  }
  return null
}

function findObjectByLid (context, lid, nObjectCount, aContainers) {
  const aObjectList = buildObjectList(context, aContainers)
  const oEntity = aObjectList.find(e => e.lid === lid)
  if (oEntity) {
    return {
      entity: oEntity.entity,
      count: parseInt(nObjectCount)
    }
  } else {
    return null
  }
}

/**
 * Tente de reconnaitre un Local ID dans la chaine (ou tableau de chaine) passée en paramètre
 * renvoie null si le paramètre passé ne correspond pas au format d'un LID.
 * @param context
 * @param sString {string|string[]}
 * @param aContainers {number[]}
 * @returns {null|{count: number, entity: *}}
 */
function findLid (context, sString, aContainers) {
  if (Array.isArray(sString)) {
    sString = sString.join(' ')
  }
  let r
  // reconnait "4 i2" pour signifier "4 exemplaires de l'objet désigné par le local-id i2"
  r = sString.match(REGEXP_N_LID)
  if (r) {
    const [dummy, n, lid] = r
    return findObjectByLid(context, lid, n, aContainers)
  }
  // reconnait "i2 4" pour signifier "4 exemplaires de l'objet désigné par le local-id i2"
  r = sString.match(REGEXP_LID_N)
  if (r) {
    const [dummy, lid, n] = r
    return findObjectByLid(context, lid, n, aContainers)
  }
  // reconnait "i2" pour signifier "1 seul exemplaire de l'objet désigné par le local-id i2"
  r = sString.match(REGEXP_LID)
  if (r) {
    const [dummy, lid] = r
    return findObjectByLid(context, lid, 1, aContainers)
  }
  return null
}

/**
 * Trouve un objet par son nom, en fonction du filtre, associe un nombre d'exemplaire
 * @param context {*}
 * @param sName {string}
 * @param nObjectCount {number}
 * @param aContainers {string[]}
 * @param iRank {number}
 * @param fEntityFilter {function} filtre sur les types d'objets
 * @returns {*}
 */
function findObjectByName (context, sName, nObjectCount, aContainers, iRank = 0, fEntityFilter = undefined) {
  if (iRank < 0) {
    throw new Error('ERR_INVALID_LIST_OBJECT_RANK')
  }
  const aObjectList = buildObjectList(context, aContainers, fEntityFilter)
  const aNames = aObjectList.map(({ name }) => name)
  const sSelectedName = presetSuggest(sName, aNames)
  const aEntities = aObjectList.filter(({ name }) => name === sSelectedName)
  const oEntity = aEntities[iRank]
  if (oEntity) {
    return {
      entity: oEntity.entity,
      count: nObjectCount
    }
  } else {
    return null
  }
}

/**
 * Parmis toutes les entités contenus dans l'ensemble des contenants spécifiés, on recherche l'entité dont le nom
 * correspond le mieux à la chaine de caractètre spécifiée
 * @param context {*}
 * @param sString {string|string[]} nom de l'objet recherché (critère de recherche)
 * @param aContainers {string[]} ensemble des identifiant de contenant dans lesquels chercher l'objet
 * @param fEntityFilter {function} filtre sur les types d'entité recherchées
 * @returns {null|*}
 */
function findObject (context, sString, aContainers, fEntityFilter = undefined) {
  if (Array.isArray(sString)) {
    sString = sString.join(' ')
  }

  let r

  // command <N> <OBJECT> <X>
  r = sString.match(REGEXP_N_OBJECT_X)
  if (r) {
    const [dummy, n, sObject, nRank] = r
    return findObjectByName(context, sObject, parseInt(n), aContainers, parseInt(nRank) - 1, fEntityFilter)
  }

  // command <OBJECT> <X> <N>
  r = sString.match(REGEXP_OBJECT_X_N)
  if (r) {
    const [dummy, sObject, nRank, n] = r
    return findObjectByName(context, sObject, parseInt(n), aContainers, parseInt(nRank) - 1, fEntityFilter)
  }

  // command <OBJECT> <X>
  r = sString.match(REGEXP_OBJECT_X)
  if (r) {
    const [dummy, sObject, nRank] = r
    return findObjectByName(context, sObject, 1, aContainers, parseInt(nRank) - 1, fEntityFilter)
  }

  // command <N> <OBJECT>
  r = sString.match(REGEXP_N_OBJECT)
  if (r) {
    const [dummy, n, sObject] = r
    return findObjectByName(context, sObject, parseInt(n), aContainers, 0, fEntityFilter) || null
  }

  // command <OBJECT> <N>
  r = sString.match(REGEXP_OBJECT_N)
  if (r) {
    const [dummy, sObject, n] = r
    return findObjectByName(context, sObject, parseInt(n), aContainers, 0, fEntityFilter) || null
  }

  // command <OBJECT>
  r = sString.match(REGEXP_OBJECT)
  if (r) {
    const [dummy, sObject] = r
    return findObjectByName(context, sObject, 1, aContainers, 0, fEntityFilter) || null
  }

  return null
}

/**
 * La fonction analyse le tableau d'argument et va tenter de reconnaitre :
 * 1) un identifiant local (LID) ainsi qu'un numérateur eventuel et renvoyer un objet avec { entité, numérateur }
 * 2) une chaine spécifiant le nom ainsi qu'un numérateur éventuel et renvoyer un objet avec { entité, numérateur }
 * dans tous les cas la fonction tente d'envoyer un descriptif { entity, numérateur } ou bien NULL si vraiment
 * le tableau d'argument ne correspond à rien (ou s'il n'y a rien dans la pièce)
 * @param context
 * @param aArguments
 * @param aContainers
 * @param fEntityFilter {function} filtre sur les types d'entités
 * @returns {null|{count: number, entity: MUDEntity}}
 */
function findLidOrObject (context, aArguments, aContainers, fEntityFilter = undefined) {
  let r = findLid(context, aArguments, aContainers)
  if (r !== null) {
    return {
      entity: r.entity,
      count: r.count
    }
  }
  r = findObject(context, aArguments, aContainers, fEntityFilter)
  if (r !== null) {
    return {
      entity: r.entity,
      count: r.count
    }
  }
  return null
}

/**
 * Trouve un container dans la liste des location spécifiée
 * @param context
 * @param aArguments
 * @param aLocations
 * @returns {{count: number, entity: *}|null|{count, entity}}
 */
function findContainer (context, aArguments, aLocations) {
  return findLidOrObject(context, aArguments, aLocations, ent => context.engine.isContainer(ent.id))
}

/**
 * Trouve un item transportable dans la liste des locations spécifiée
 * @param context
 * @param aArguments
 * @param aLocations
 * @returns {{count: number, entity: *}|null|{count, entity}}
 */
function findItem (context, aArguments, aLocations) {
  const TYPE = CONSTS.ENTITY_TYPES_ITEM
  return findLidOrObject(context, aArguments, aLocations, ent => ent.blueprint.type === TYPE)
}

function findActorOrPlayer (context, aArguments, aLocations) {
  const TYPE1 = CONSTS.ENTITY_TYPES_ACTOR
  const TYPE2 = CONSTS.ENTITY_TYPES_PLAYER
  return findLidOrObject(context, aArguments, aLocations, ent => {
    const sType = ent.blueprint.type
    return sType === TYPE1 || sType === TYPE2
  })
}

module.exports = {
  findLidOrObject,
  findContainer,
  findItem,
  findActorOrPlayer,
  findDirection,
  CONTAINER_TYPES
}

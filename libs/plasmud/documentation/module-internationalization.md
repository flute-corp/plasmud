# Internationalisation des modules

Tout ce qui peut être internationalisé doit être placé dans le répertoire __assets__.

- blueprints : Les noms, les descriptions
- rooms : Les noms, les descriptions
- sectors : Les noms, les descriptions
- strings : Les chaînes de caractères affichées à destination des joueurs
- templates : Les templates affichés à destination des joueurs

### Exemple d'internationalisation

#### Structure du module internationnalisé :

```
├─ assets
│  ├─ fr
│  │  ├─ blueprints
│  │  ├─ rooms
│  │  ├─ sectors
│  │  ├─ strings
│  │  ├─ dialogs
│  │  ├─ templates
│  │  └─ quests
│  └─ en
│     ├─ blueprints
│     ├─ rooms
│     ├─ sectors
│     ├─ strings
│     ├─ dialogs
│     ├─ templates
│     └─ quests
├─ scripts
├─ commands
└─ config.json
```

#### Contenu de config.json

```json
{
  "startingLocation": "xxxx",
  "defaultLanguage": "fr",
  "languages": ["fr", "en"]
}
```

- startingLocation :
- languages : liste des langues supportées
- defaultLanguage : nom de la langue supportée en cas de non spécification

Si defaultLanguage n'est aps spécifié, alors le système considère que c'est le premier
de la liste qui sera le defaultLanguage.

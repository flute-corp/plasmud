# Evènements déclenchés en combat par les entités

## Quelles entité déclenchent quels évènements ?

Evènement           | actor | player | placeable | item | room | door
--------------------|-------|--------|-----------|------|------|-----
combatRoundEnd      | x     |        |           |      |      |
damaged             | x     |        |           |      |      |
death               | x     |        |           |      |      |
physicallyAttacked  | x     |        |           |      |      |
spellCastAt         | x     |        | x         |      |      | x


## Liste des évènements

### combatRoundEnd
Déclenché lorsqu'une entité est en plein combat, à chaque fois qu'un round se
termine. Cela permet à l'acteur de prendre des décisions quant au déroulement
du combat dans le prochaine round.
- __self__: identifiant de l'entité qui doit décider de l'action à mener.

### damaged
Déclenché lorsqu'une entité est endommagé par une autre entité.
- __self__: Acteur endommagé.
- __damager__: Identifiant de l'entité qui a endommagé l'acteur
- __amount__: Quantité de dégâts encaissés
- __type__: Nature des dégâts encaissés

### death
Déclenché lors de la mort de l'acteur.
- __self__: entité qui a mourru.
- __killer__: identifiant du coupable.

### physicallyAttacked
Déclenché lorsqu'une entité est physiquement agressée par une autre entité.
- __self__: entité agressée
- __attacker__: entité agressive
- __type__: type d'attaque physique perpétrée (tir, coup etc...)

### spellCastAt
Déclenché lorsqu'un "sort" ou une compétence complexe a été lancé sur l'entité
cible.
- __self__: identifiant de l'entité qui est affectée par le sort.
- __harmful__: _true_ si le sort est hostile, _false_ si le sort est inoffensif.
- __spell__: identifiant du sort ou de la compétence lancé sur l'entité.
- __caster__: identifiant de l'entité ayant lancé le sort.

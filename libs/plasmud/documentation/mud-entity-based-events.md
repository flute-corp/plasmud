# Evènements déclenchés par les entités

## Quelles entités déclenchent quels évènements ?

| Evènement      | actor | player | placeable | item | room | door | Testé |
|----------------|-------|--------|-----------|------|------|------|-------|
 | blocked        | x     | x      |           |      |      |      | x     |
 | conversation * | x     |        | x         |      |      |      |       |
 | disturbed      | x     |        | x         |      |      |      | x     |
 | heartbeat *    | x     |        |           |      |      |      |       |
 | perception *   | x     |        |           |      |      |      |       |
| spawn          | x     | x      | x         | x    |      |      | x     |
| despawn        | x     | x      | x         | x    |      |      | x     |
 | locked         |       |        | x         |      |      | x    |       |
 | openFailure    |       |        | x         | x    |      | x    | x     |
 | unlocked       |       |        | x         | x    |      | x    | x     |
 | enter          |       |        |           |      | x    |      | x     |
 | exit           |       |        |           |      | x    |      | x     |
 | command        |       |        |           |      | x    |      |       |
 | acquired       |       |        |           | x    |      |      | x     |
 | dropped        |       |        |           | x    |      |      | x     |
 | equipped *     |       |        |           | x    |      |      |       |
 | unequipped *   |       |        |           | x    |      |      |       |
 | used *         |       |        |           | x    |      |      |       |

* Cet évènement n'est pas codé et pourrait dépendre d'un module externe

## Liste des évènements

### blocked
Déclenché lorsqu'une entité ne peut pas se déplacer d'une pièce à une autre
à cause d'une porte verrouillée.
- __self__: identifiant de l'entité bloquée
- __room__: identifiant de la pièce dans laquelle se trouve l'acteur.
- __direction__: direction de la porte bloquée.

### conversation
Déclenché lorsqu'une entité entend une phrase prononcée par une autre entité.
- __self__: identifiant de l'entité qui réagit à la phrase.
- __pattern__: contenu de la phrase prononcée
- __interactor__: identifiant de l'entité qui à prononcé la phrase
- __volume__: volume de la phrase prononcée (0 = signal silencieux, 1 = chuchotement, 2 = volume normal, 3 = cri, 4 = cri distant (d'une autre pièce))

### disturbed
Déclenché lorsque l'inventaire de l'entité est modifié (on ajoute ou on retire un objet du contenant).
- __self__: entité dont l'inventaire est modifié
- __interactor__: identité de l'entité qui effectue l'action de modifiication de l'inventaire.
- __item__: identité de l'élément de l'inventaire qui a été déplacé
- __type__: 1: élément supprimé, 2: élément ajouté, 3: élément volé.

### heartbeat
Déclenché de manière régulière tous les rounds.
- __self__: entité à laquelle s'applique l'évènement

### perception
Déclenché lorsque la perception d'une entité enver une autre change.
Permet de déterminer si une entité aperçois visuellement ou auditivement
une autre entité.
Permet aussi de déterminer si un entité A perd de vue une autre entité B, si B
fait usage d'invisibilité ou si B quitte la pièce.
- __self__: id entité qui ressent une modification de perception
- __entity__: autre entité qui __self__ percois, olù dont elle pert la perception.
- __types__: { heard: boolean, seen: boolean, vanished: boolean, inaudible: boolean }

### spawn
Déclenché au moment ou l'entité est créée et rejoind le jeu.
- __self__: entité qui a spawné.

### locked
Déclenché lorsqu'un contenant ou une porte est verrouillé.
- __self__: identifiant du contenant.
- __interactor__: identifiant de l'entité qui a verrouillé _self_.

### openFailure
Déclenché lorsqu'un contenant ou une porte n'a pas pu être ouvert(e).

Pour déterminer si c'est une porte : Engine.getExit(self).valid

Pour déterminer si c'est une contenant : Engine.isContainer(self)

- __self__: identifiant du contenant ou de la porte qui n'a pas pu être ouverte.
- __interactor__: identifiant de l'acteur qui n'a pas réussi à ouvrir le contenant

### unlocked
Déclenché lorsqu'un contenant ou une porte initialement verrouillé(e) est déverrouillé(e).
- __self__: identifiant du contenant ou de la porte deverrouillé(e).
- __interactor__: identifiant de l'entité ayant déverrouillé le contenant.

### enter
Déclenché lorsqu'une pièce accueille une nouvelle entité.
- __self__: identifiant de la pièce
- __interactor__: identifiant de l'entité qui entre dans la pièce

### exit
Déclenché lorsqu'une entité quitte la pièce.
- __self__: identifiant de la pièce
- __interactor__: identifiant de l'entité qui quitte dans la pièce

### acquired
Déclenché par un objet qui vien d'être acquis par un acteur.
- __self__: identifiant de l'objet acquis
- __interactor__: identifiant de l'acteur qui a acquis l'objet

### dropped
Déclenché par un objet qui vien d'être abandonné par un acteur, au profit d'un autre.
- __self__: identifiant de l'objet abandonné
- __interactor__: identifiant de l'acteur qui a abandonné l'objet


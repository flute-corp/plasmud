# Composition d'un module

## Dossiers
Chaque module contient un certains nombre de **resources**
de catégories différentes.

Chaque catégorie de ressources est dans son propre dossier.

Chaque dossier peut être organisé en sous-dossiers pour en faciliter l'exploitation.

| Dossier           | Fichiers | Description                                                                                                                            |
|-------------------|----------|----------------------------------------------------------------------------------------------------------------------------------------|
| assets            | json     | Définition du monde du jeu                                                                                                             |
| assets/blueprints | json     | Définition des entités que l'on peut dynamiquement créer (objet d'inventaire, créature, etc...)                                        |
| assets/rooms      | json     | définition des pièces explorables                                                                                                      |
| assets/sectors    | json     | définition des secteurs, regroupant plusieurs pièces, et qui donnent un thème à l'ensemble des pièces regroupées ainsi                 |
| commands          | js       | Scripts des commandes proposées par le module                                                                                          |
| scripts           | js       | Scripts de traitement des opérations qui peuvent intervenir dans le jeu                                                                |
| events            | js       | Scripts qui sont déclenchés lorsque certains évènements dans le jeu se produisent                                                      |
| assets/strings    | json     | Définition des chaines de caractères qui pourront être affichées dans l'aventure.Ce dossier ne contienr qu'un fichier : *strings.json* |
| assets/templates  | twig     | Templates qui peuvent être affichés par les commandes ou les scripts                                                                   |
| assets/dialogs    | json     | Définitions des dialogues                                                                                                              |
| assets/quests     | json     | Définitions des quêtes                                                                                                                 |

### Blueprints
Ces fichiers JSON doivent respecter le schéma libs/mud/schemas/blueprints.json
Ces fichiers décrivent les propriétés des entités avec lesquelles ont peut intéragir, et qui peuvent êtres créées *en série* dans tout le monde du jeu.
- Objets transportables (clé, outils, documents...)
- Objets équipables (armes, vêtements...)
- Créatures (Hostiles, Personnages non-joueurs, Personnages de joueur)

les blueprints peuvent etre organisés dans des sous-dossiers cela importe peu, par contre le nom de fichier devriendra    l'identifiant du blueprint donc il ne faut pas que 2 blueprints aient le même nom de fichier quelque-soient les répertoires dans lesquels ils se trouvent.

### Rooms
Ces fichiers JSON doivent respecter le schémas libs/mud/schemas/rooms.json
Ce sont les définition des pièces, leur nom, leur description, leur secteur d'appartenance, leur contenu, la liste des issues etc...

### Sectors
Ces fichiers JSON doivent respecter le schémas libs/mud/schemas/sectors.json
Ce sont des fichiers définissant un secteur auquel appartient plusieurs pièce afin de leur donner un thème commun. Pour l'instant seule la description du secteur est définissable mais on pourra imaginer un ensemble de propriétés communes à toutes les pièces qui seront définit dans le secteur afin de réduire les copier-coller.

### Commands
Ces scripts JS doivent exporter une fonction *main* principale et doivent respecter la signature suivante :
```application/ecmascript
/**
 * @params context {*} contient tout un tas de fonctions, de valeurs ou d'objets permettant de déterminer le contexte dans lequel cette commande à été lancée
 * @params aArguments {string[]} liste des paramètres tapé à la suite de la commande.
 */

module.exports = function main (context, ...aArguments) {
    // ...
}
```

### Scripts
Ces scripts JS doivent exporter une fonction *main* principale.
Pour l'instant cette partie du Mud n'est pas parfaitement éprouvée et la présente documentation sera mise à jour quand ce sera le cas.
```application/ecmascript
/**
 * @params event {*} objet évènement
 */

module.exports = function main (event) {
    // ...
}
```
### Strings
Un dossier contenant un unique fichier *string.jsonù contenant toutes les chaine de caractère affichables.
Cet objet sera utilisé par les fonction *print* et *text* du contexte passé en paramètre des scripts de commande.

### Templates
Un dossier contenant tous les templates. Ces templates sont utilisés par les fonctions *print* du contexte passé en paramètre des scripts de commande.

### Dialogs
Le système de dialogue n'est pas enccore finalisé, la documentation sera mise à jour quand ce sera le cas.

## Détails de la création des ressources

### Créer une pièce, ou un secteur.
Les pièces et les secteurs sont des ressources statiques.

| Ressource | Schémas |
|---------|-------|
| Pièce | libs/mud/schemas/rooms.json |
| Secteur | libs/mud/schemas/sectors.json |

### Blueprints
Les blueprints son plus complexes à créer etant donnée leur grande diversité.

Les blueprints doivent cependant respecter le schemas **libs/mud/schemas/blueprints.json**

Les sections suivantes détaillent la création de tous les types de blueprints

#### Cas n°1 : importer un objet transportable depuis ADVD205E
Le module ADVD205E possède de nombreux objets transportable de base. On va importer l'un d'entre eux pour le rendre dispo dans le MUD, sans modification de l'objet de base.

Exemple : Armure de cuir
L'armure de cuir simple est disponible dans ADVD205E sous l'identifiant *arm-leather*.

1) créer un fichier *blueprints/items/arm-leather.json*
2) remplir le json comme ceci
```json
{
  "tag": "arm-leather",
  "type": "item",
  "subtype": "armor",
  "name": "Armure de cuir",
  "desc": [
    "La cuirasse et les protecteurs d'épaules de cette armure sont faits de cuir qui a été durci en étant bouilli dans de l'huile. Le reste de l'armure est fait de matériaux plus tendres et plus souples."
  ],
  "weight": 0,
  "stackable": false,
  "inventory": false
}
```

**tag:** {string} Les tags sont des chaines d'identification modifiables des entités. En général les scripts et les commandes permettent d'identifier les objets transportables grace au tag. Pour une quête qui réclame de rapporter un objet précis, pour une porte qui réclame une clé précise pour être ouverte, le script de vérification correspondant va comparer le tag de l'entité avec une valeur attendue.

**type:** {string} catégorie d'entité avec lequel on peut interagir.Valeur à choisir parmis les suivantes :

| type | description |
|----|-----------|
| item | Objet transportable, possédant un *subtype* (voir liste ci-après) |
| placeable | Un élément du décor, un meuble. Généralement ce sont des contenants (bureau, coffre) mais cela peut etre un objet interressant à examiner. En tout cas ce n'est ni un objet tranportable, ni une créature |
| actor | Personnage non joueur, hostile, amical, marchand, etc... |
| player | Personnage appartenant à un joueur (cette catégorie n'est jamais utilisée dans la définition des pièces ou des contenus) |

**subtype:** {string} valeur à choisir parmis les suivantes :

| subtype | description |
| ---- | ----------- |
| weapon | Arme équipable |
| armor | Armure équipable |
| shield | Bouclier équipable |
| equipment | Bottes, gants, ceinture, anneaux, amulette, toute autre pièce d'équipement qui peut rentrer dans un slot d'équipement |
| key | Clé, utilisée pour ouvrir des portes |
| valuable | Objet précieux normalement destiné à être revendu |
| money | unité monétaire (pièce d'or, billets etc...) |
| container | objet transportable qui peut lui-même contenir d'autre objets |
| tool | outil transportable comme par exemple, trousse de soin, outils de cambrioleur... |
| consumable | objet consommable, comme les provisions, potions, etc... |
| book | livres ou documents, lettre, etc... |
| plot | objet nécessaire à une quête |
| material | objet utilisé dans une recette artisanale |
| misc | tout objet transportable qui ne correspond à aucune autre catégorie |

**name:** {string} Nom de l'objet. Pour un objet transportable c'est le nom de la version *identifiée*.

**desc:** {string[]} Description de l'objet dans sa version *identifiée* (chaque élément du tableau est une ligne de texte.)

**weight:** Poids indicatif. Concernant les objets transportables (*type* = item), le poids est calculé grâce au système de règle (on peut donc mettre la valeur 0)

**stackable:** {boolean} Pour les objets transportables, cette propriété (si elle est a la valeur *vrai*) indique que l'objet est suffisament *fin* pour être empilé. C'est généralement le cas d'objets petits que l'on peut avoir en de nombreux exemplaires (pièces de monnaie, munitions, gemmes, parchemin, certains outils consommables).

**inventory:** {boolean} Si cette propriété est vrai, l'objet possède un inventaire.

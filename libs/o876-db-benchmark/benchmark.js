const debug = require('debug')
const db = require('../o876-db')
const NG = require('../name-generator')
const fs = require('fs')
const path = require('path')
const os = require('os')
const ng = new NG()

log = debug('bm')
debug.enable('bm')
const TMP_DIR = fs.mkdtempSync(path.join(os.tmpdir(), 'o876dbBenchMark-'))

async function createManager () {
  log('creating db manager')
  const dbm = new db.Manager()
  await dbm.init({
    path: TMP_DIR,
    collections: ['c1']
  })
  return dbm
}

async function create10000Documents (collection) {
  log('creating 100000 documents')
  for (let i = 0; i < 10000; ++i) {
    const id = i + 1000
    const bWitness = id === 4444
    const lastname = bWitness ? 'glaaki0' : ng.generateLastName()
    const firstname = bWitness ? 'glouke0' : ng.generateFirstName()
    const name = firstname + ' ' + lastname
    const age = Math.floor(Math.random() * 100) + 18
    const d = {
      id,
      name,
      firstname,
      lastname,
      age
    }
    await collection.save(d)
  }
  log('all document created')
}

async function benchmark1 () {
  const dbm = await createManager()
  const c = dbm.collections.c1
  await create10000Documents(c)
  log('now benchmarking with no index')
  log('start querying name glaaki0')
  console.time('query')
  const aDocs = await c.find({ name: 'glouke0 glaaki0' })
  console.timeEnd('query')
  log('%d document(s) found', aDocs.length)
  const { id, name, age } = aDocs[0]
  log('found - id: %s, name: %s, age: %d', id, name, age)
  log('end querying')
  log('dropping collection c1')
  await c.drop()
  log('collection dropped')
}

async function benchmark2 () {
  const dbm = await createManager()
  const c = dbm.collections.c1
  await create10000Documents(c)
  log('create index on name : normal index')
  console.time('index creation')
  await c.createIndex('name')
  console.timeEnd('index creation')
  log('now benchmarking with index')
  log('start querying name glaaki0')
  console.time('query')
  const aDocs = await c.find({ name: 'glouke0 glaaki0' })
  console.timeEnd('query')
  log('%d document(s) found', aDocs.length)
  const { id, name, age } = aDocs[0]
  log('found - id: %s, name: %s, age: %d', id, name, age)
  log('end querying')
  log('dropping collection c1')
  await c.drop()
  log('collection dropped')
}

async function benchmark3 () {
  const dbm = await createManager()
  const c = dbm.collections.c1
  await create10000Documents(c)
  log('create index on name : normal index')
  console.time('index creation')
  await c.createIndex('name', {
    matchCase: true,
    hash: {
      type: 'crc16'
    }
  })
  console.timeEnd('index creation')
  log('now benchmarking with index crc 16')
  log('start querying name glaaki0')
  console.time('query')
  const aDocs = await c.find({ name: 'glouke0 glaaki0' })
  console.timeEnd('query')
  log('%d document(s) found', aDocs.length)
  const { id, name, age } = aDocs[0]
  log('found - id: %s, name: %s, age: %d', id, name, age)
  log('end querying')
  log('dropping collection c1')
  await c.drop()
  log('collection dropped')
}

async function benchmark4 () {
  const dbm = await createManager()
  const c = dbm.collections.c1
  await create10000Documents(c)
  log('create index on name : normal index')
  console.time('index creation')
  await c.createIndex('name', {
    matchCase: true,
    hash: {
      type: 'crc32'
    }
  })
  console.timeEnd('index creation')
  log('now benchmarking with index crc 32')
  log('start querying name glaaki0')
  console.time('query')
  const aDocs = await c.find({ name: 'glouke0 glaaki0' })
  console.timeEnd('query')
  log('%d document(s) found', aDocs.length)
  const { id, name, age } = aDocs[0]
  log('found - id: %s, name: %s, age: %d', id, name, age)
  log('end querying')
  log('dropping collection c1')
  await c.drop()
  log('collection dropped')
}

async function run () {
  await benchmark1()
  log('-------------------')
  await benchmark2()
  log('-------------------')
  await benchmark3()
  log('-------------------')
  await benchmark4()
}

run()

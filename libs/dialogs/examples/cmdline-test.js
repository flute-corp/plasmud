const readline = require('readline')
const fs = require('fs')
const { Context } = require('../sync')

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})
/*
rl.question('What is your name ? ', function (name) {
  rl.question('Where do you live ? ', function (country) {
    console.log(`${name}, is a citizen of ${country}`)
    rl.close()
  })
})
*/

rl.on('close', function () {
  console.log('\nFin de dialogue')
  process.exit(0)
})

const oContext = new Context()
let bExit = false

function question (sQuestion) {
  return new Promise(resolve => {
    rl.question(sQuestion, answer => {
      resolve(answer)
    })
  })
}

async function loadDialogFile (sLocation) {
  const sDialogContent = fs.readFileSync(sLocation)
  const oDialogContent = JSON.parse(sDialogContent)
  await oContext.load(oDialogContent)
  oContext.events.on('dialog.condition', async oEvent => {
    const b = await question('La condition : "' + oEvent.condition + '" est vrai (o/n) ? ')
    const b2 = b.toLowerCase() === 'o'
    console.log('[', oEvent.condition, '=', b2, ']')
    oEvent.result(b2)
  })
  oContext.events.on('dialog.screen', ({ id, text, options }) => {
    console.log('\n\n---', id, '---------------')
    console.log(text)
    console.log(' ')
    options.forEach((o, i) => {
      const si = (i + 1).toString()
      const sOptions = '[' + si + ']'
      console.log(sOptions, o.text)
    })
  })
  oContext.events.on('dialog.end', () => {
    console.log('dialog.end')
    bExit = true
  })
}

async function run () {
  await oContext.reset()
  await oContext.displayCurrentScreen()
  while (!bExit) {
    const s = await question('>')
    const n = parseInt(s)
    try {
      await oContext.selectOption(n - 1)
    } catch (e) {
      console.warn('Option non proposée.')
    }
  }
  rl.close()
}

async function main () {
  const sDialogLocation = process.argv.slice(2).shift()
  await loadDialogFile(sDialogLocation)
  await run()
}

main().then(() => {
  console.log('fin du programme')
})

const EventEmitter = require('events')
const Node = require('./Node')
const deepClone = require('../../../deep-clone')

class Context {
  constructor () {
    this._screen = null
    this._dialog = {}
    this._options = null
    this._nodeIds = []
    this._events = new EventEmitter()
    this._actor = null // acteur auquel le dialogue est assigné
    this._player = null // joueur qui a activé le dialogue
    this._variables = {}
  }

  get variables () {
    return this._variables
  }

  set actor (value) {
    this._actor = value
  }

  get actor () {
    return this._actor
  }

  set player (value) {
    this._player = value
  }

  get player () {
    return this._player
  }

  /**
   * Chargement d'un dialogue à partir de sa definition JSON
   * @param oStructure
   */
  load (oStructure) {
    const { chapters, variables } = oStructure
    if (chapters.length <= 0) {
      return
    }
    this._variables = deepClone(variables)
    this._nodeIds = chapters.map(({ id }) => id)
    const oDialog = Node.createFromArrayStructure(chapters)
    this._dialog = oDialog
    this.reset()
    for (const [sDialog, d] of Object.entries(oDialog)) {
      d.events.on('dialog.resolve.condition', oEvent => {
        const oCtxEvent = {
          condition: oEvent.condition,
          result: oEvent.result,
          actor: this._actor,
          player: this._player,
          variables: this._variables
        }
        this._events.emit('dialog.condition', oCtxEvent)
        oEvent.result = oCtxEvent.result
      })
    }
  }

  reset () {
    if (this._nodeIds.length > 0) {
      // trouver le premier node valide
      const sId = this._nodeIds.find(id => this._dialog[id].isDisplayed())
      if (sId) {
        this._screen = this._dialog[sId]
      } else {
        this._screen = null
      }
    }
  }

  get screen () {
    return this._screen
  }

  set screen (value) {
    if (value === null) {
      this._screen = null
      this._events.emit('dialog.end')
    } else if (value instanceof Node) {
      this._screen = value
      this.displayCurrentScreen()
    } else {
      console.error(value)
      throw new TypeError('Only Dialog Node instance may be assigned to "screen" dialog Context property')
    }
  }

  get events () {
    return this._events
  }

  displayCurrentScreen () {
    if (this.screen.scripts.action) {
      this._events.emit('dialog.action', {
        action: this.screen.scripts.action,
        variables: this._variables,
        actor: this._actor,
        player: this._player
      })
    }
    this.fetchOptions()
    this.triggerDialogScreenEvent()
  }

  triggerDialogScreenEvent () {
    this._events.emit('dialog.screen', {
      id: this.screen.id,
      text: this.screen.text,
      variables: this._variables,
      actor: this._actor,
      player: this._player,
      options: this._options.map(
        opt => ({
          id: opt.id,
          text: opt.render(this._variables)
        })
      )
    })
  }

  fetchOptions () {
    this._options = this.screen ? this.screen.getDisplayedChildren() : []
  }

  /**
   * Cette action séléection une option du dialogue
   * Un évènement dialog.action est lancé
   * utilise :
   *  - this._options
   * modifie
   *  - this._options
   *  - this._screen
   * @param id {number|string}
   */
  selectOption (id) {
    const sId = id.toString()
    let oOption = this._options.find(o => o.id === sId)
    if (!oOption && (typeof id === 'number')) {
      oOption = this._options[id]
    }
    if (oOption) {
      // lancer le script de l'action
      if (oOption.scripts.action) {
        this._events.emit('dialog.action', {
          action: oOption.scripts.action,
          variables: this._variables,
          actor: this._actor,
          player: this._player
        })
      }
      // Choisir le nouvel écran
      this.screen = oOption.getFirstValidChild()
    } else {
      throw new Error('ERR_DIALOG_OPTION_NOT_FOUND')
    }
  }
}

module.exports = Context

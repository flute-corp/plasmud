const EventEmitter = require('events')
const HandleBars = require('handlebars')
const validateSchema = require('../../../schema-validation')
const JSON_SCHEMA_DIALOG_NODE = require('../../schemas/dialog.json')

/**
 * @typedef DialogText {string[]|string}
 */

/**
 * L'option contient un référence vers un script conditionnel, un strref, un script d'action'
 */
class Node {
  constructor (id) {
    /**
     * @type {string}
     * @private
     */
    this._id = id
    this._events = new EventEmitter()
    this._scripts = {
      condition: '',
      action: ''
    }
    /**
     * @type {DialogText}
     * @private
     */
    this._text = ''
    this._template = null
    /**
     * @type {Node[]}
     * @private
     */
    this._children = []
  }

  get children () {
    return this._children
  }

  get id () {
    return this._id
  }

  get events () {
    return this._events
  }

  render (oTokens) {
    return this._template(oTokens)
  }

  set text (value) {
    this._text = Array.isArray(value) ? value.join('\n') : value
    this._template = HandleBars.compile(this._text)
  }

  get text () {
    return this._text
  }

  get scripts () {
    return this._scripts
  }

  /**
   * Lance le script de condition pour déterminer si le dialog node peut etre affiché
   * @returns {boolean}
   */
  isDisplayed () {
    if (this._scripts.condition) {
      let bResult = true
      const result = b => {
        bResult = b
      }
      const oEvent = { condition: this._scripts.condition, result }
      this._events.emit('dialog.resolve.condition', oEvent)
      return bResult
    } else {
      return true
    }
  }

  /**
   * @typedef DialogExport {object}
   * @property action {string} nom du script à lancer lorsque le node est activé (affiché ou cliqué)
   * @property id {string} identifiant du node
   * @property text {DialogText} texte de dialogue à afficher
   *
   * Renvoie une structure exportable du node
   * @returns {DialogExport}
   */
  export () {
    return this
  }

  /**
   * Liste des dialog nodes qui sont affichables
   * @returns {DialogExport[]}
   */
  getDisplayedChildren () {
    const aChildren = this._children.filter(o => o.isDisplayed())
    return aChildren.map(child => child.export())
  }

  /**
   * Trouve le premier childnode dont le script de condition répond true (ce sera ce node qui sera affiché en tant qu'écran de dialogue)
   * @returns {DialogExport|null}
   */
  getFirstValidChild () {
    const child = this._children.find(o => o.isDisplayed())
    if (child) {
      return child.export()
    } else {
      return null
    }
  }

  static createFromArrayStructure (aStructures) {
    validateSchema(aStructures, JSON_SCHEMA_DIALOG_NODE)
    const oRegistry = {}
    aStructures.forEach(s => {
      oRegistry[s.id] = new Node(s.id)
    })
    aStructures.forEach(s => {
      const d = oRegistry[s.id]
      d.import(s, oRegistry)
      return d
    })
    return oRegistry
  }

  import (oStruct, oRegistry) {
    this.text = oStruct.text
    this._scripts.action = 'action' in oStruct ? oStruct.action : ''
    this._scripts.condition = 'condition' in oStruct ? oStruct.condition : ''
    this._children = 'options' in oStruct
      ? oStruct.options.map(nid => {
        if (nid in oRegistry) {
          return oRegistry[nid]
        } else {
          throw new Error('Node ' + nid + ' is not in registry.')
        }
      })
      : []
  }
}

module.exports = Node

/**
 * target at melee range
 * Renvoie true si la cible du sujet est à portée de son arme de mélée
 * @param subject {EntityState} sujet pour lequel on effectue le test
 * @param target {EntityState} cible actuelle du sujet
 * @param rules {Rules} système
 * @param weapon {EntityState} arme impliquée dans l'attaque
 * @returns {boolean}
 */
function main ({ rules, subject, target, weapon }) {
  rules.checkActor(subject)
  rules.checkActor(target)
  return rules.getDistance(subject, target) <= rules.getMeleeWeaponRange(subject, weapon)
}

module.exports = main

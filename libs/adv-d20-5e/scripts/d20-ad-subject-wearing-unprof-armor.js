/**
 * Returns true is subject is not proficient with worn armor
 * @param subject {EntityState} sujet pour lequel on effectue le test
 * @param rules {Rules} système
 * @returns {boolean}
 */
function main ({ subject, rules }) {
  rules.checkActor(subject)
  /**
   * @var rules {Rules}
   * @var subject {EntityState}
   */
  const oArmor = rules.getEquippedItem(subject, rules.getItemBaseTypeSlot(rules.CONST.ITEM_BASE_TYPE_ARMOR))
  return oArmor
    ? !rules.isProficient(subject, oArmor)
    : true
}

module.exports = main

/**
 * target unseen
 * Renvoie true si la cible du sujet n'est pas visible
 * @param subject {EntityState} sujet pour lequel on effectue le test
 * @param target {EntityState} cible actuelle du sujet
 * @param rules {Rules} système
 * @returns {boolean}
 */
function main ({ subject, target, rules }) {
  return rules.runScript('d20-ad-subject-undetected-by-target', { subject: target, target: subject })
}

module.exports = main

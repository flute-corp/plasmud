/**
 * déterminer si le sujet est trop encombré
 * @param subject {EntityState} sujet pour lequel on effectue le test
 * @param rules {Rules} système
 * @returns {boolean}
 */
function main ({ subject, rules }) {
  rules.checkActor(subject)
  const encumbrance = rules.getEncumbrance(subject)
  return encumbrance === rules.CONST.ENCUMBRANCE_HEAVY || encumbrance === rules.CONST.ENCUMBRANCE_FULL
}

module.exports = main

/**
 * Consomme les buffer de résistance aux dégats pour le type de dégâts spécifié
 * @param subject {EntityState} sujet dont on modifie les buffers
 * @param rules {Rules} système
 * @param type {string} type de dégâts
 * @param value {number} valeur de réduction
 * @returns {object}
 */
function main ({ subject, rules, type, value }) {
  const aBufferEffects = rules
    .getActiveEffects(subject, eff => eff.tag === rules.CONST.EFFECT_DAMAGE_RESISTANCE && eff.types.includes(type) && eff.buffer !== null)
  if (aBufferEffects.length === 0) {
    return null
  }
  let nMaxBuffer = 0
  aBufferEffects
    .forEach(eff => {
      eff.buffer -= Math.min(value, eff.amp)
      nMaxBuffer = Math.max(nMaxBuffer, eff.buffer)
    })
  return nMaxBuffer
}

module.exports = main

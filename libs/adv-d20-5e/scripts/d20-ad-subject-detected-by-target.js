/**
 * Déterminer si le sujet est détecté par la cible
 * @param subject {EntityState} sujet pour lequel on effectue le test
 * @param target {EntityState} cible actuelle du sujet
 * @param rules {Rules} système
 * @returns {boolean}
 */
function main ({ subject, target, rules }) {
  rules.checkActor(subject)
  rules.checkActor(target)
  const bSubjectHaveInvis = rules.hasEffect(subject, rules.CONST.EFFECT_INVISIBILITY)
  const bTargetHaveSeeInvis = rules.hasEffect(target, rules.CONST.EFFECT_SEE_INVISIBILITY)
  // TODO check blindsight and true sight
  return !bSubjectHaveInvis || bTargetHaveSeeInvis
}

module.exports = main

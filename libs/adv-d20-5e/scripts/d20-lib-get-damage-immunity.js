/**
 * Déterminer l'immunité au dégâts du sujet
 * @param subject {EntityState} sujet pour lequel on effectue le test
 * @param rules {Rules} système
 * @param type {string} type de dégâts dont on cherche à déterminer la résistance
 * @returns {boolean}
 */
function main ({ subject, rules, type }) {
  // filter de type de dégâts
  const f = x => x.types.includes(type)

  // déterminer les résistance aux dégats
  return Math.max(0, rules.aggregateModifiers(subject, [
    rules.CONST.EFFECT_DAMAGE_IMMUNITY,
    rules.CONST.ITEM_PROPERTY_DAMAGE_IMMUNITY
  ], {
    effectFilter: f,
    propFilter: f
  }).max)
}

module.exports = main

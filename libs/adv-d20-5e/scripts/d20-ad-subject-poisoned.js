/**
 * déterminer si le sujet est de petite taille
 * @param subject {EntityState} sujet pour lequel on effectue le test
 * @param rules {Rules} système
 * @returns {boolean}
 */
function main ({ subject, rules }) {
  rules.checkActor(subject)
  return rules.hasEffect(subject, rules.CONST.EFFECT_POISON)
}

module.exports = main

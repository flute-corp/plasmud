/**
 * déterminer si le sujet est de petite taille
 * @param subject {EntityState} sujet pour lequel on effectue le test
 * @param rules {Rules} système
 * @returns {boolean}
 */
function main ({ subject, rules }) {
  rules.checkActor(subject)
  const size = subject.size
  return size === rules.CONST.CREATURE_SIZE_SMALL || size === rules.CONST.CREATURE_SIZE_TINY
}

module.exports = main

/**
 * (Recharge 6). The mephit exhales a 15-foot cone of fire.
 * Each creature in that area must make a DC 11 Dexterity saving throw, taking 7 (2d6) fire damage on a failed save,
 * or half as much damage on a successful one.
 * @param rules {Rules}
 * @param spell {SpellContext} contexte du sort
 */
function main ({ rules, spell }) {
  const { subject, target, dc } = spell
  // va cibler target mais aussi toute créature à proximité sauf le subject dans un rayon de 15
  // application de dégats de feu
  // 1-déterminer les cibles
  const aTargets = rules
    .getNearestEntities(target, 15)
    .filter(e => e !== subject)
  // 2-pour toutes les cibles, appliquer 2d6 de degat de feu (divisé par deux en cas de JS)
  // déterminer le DC
  const ability = rules.CONST.ABILITY_DEXTERITY
  aTargets
    .filter(t => !rules.isSpellResisted(spell, t))
    .forEach(t => {
      // déterminer si la créature réussi son jet de sauvegarde.
      const saved = rules.rollSavingThrow(t, ability, dc, rules.CONST.SAVE_TYPE_FIRE, subject)
      // calculer les dégâts
      const nDamageAmount = rules.runScript('d20-lib-reduce-savable-damage', {
        subject,
        ability,
        saved,
        damage: rules.dice.roll(6, 2)
      })
      // appliquer effet de degats
      const eDam = rules.createEffect(rules.CONST.EFFECT_DAMAGE, {
        value: nDamageAmount,
        type: rules.CONST.DAMAGE_TYPE_FIRE
      })
      rules.applyEffect(eDam, t, subject)
    })
}

module.exports = main

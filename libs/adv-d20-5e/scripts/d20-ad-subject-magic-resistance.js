/**
 * Indique si la cible poosède un effet "magic resistance" ou une item property "magic resistance"
 * @param subject {EntityState}
 * @param rules {Rules}
 * @return {boolean}
 */
function main ({ subject, rules }) {
  rules.checkActor(subject)
  const aCharmEffects = rules.getActiveEffects(subject, ({ tag }) => tag ===  rules.CONST.EFFECT_CHARM)
  // parmis les effets de charm affectés à target, y en a t il dont la source est le sujet ?
  return aCharmEffects.some(({ source }) => source === subject)
}

module.exports = main

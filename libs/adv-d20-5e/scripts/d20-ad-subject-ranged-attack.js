/**
 * Renvoie true si le spell est valid et considéré comme attack à distance
 * @param spell {object}
 * @returns {boolean}
 */
function isRangedSpell (spell) {
  return spell && spell.projectile
}

/**
 * Renvoie true si l'arme est valide et à distance (arc, arbalètre etc...)
 * @param weapon {EntityState}
 * @returns {boolean}
 */
function isRangedWeapon (weapon) {
  return weapon && weapon.ranged
}

/**
 * déterminer si le sujet porte une arme à distance ou bien lance un sort
 * @param subject {EntityState} sujet pour lequel on effectue le test
 * @param rules {Rules} système
 * @param weapon {EntityState}
 * @param spell {object}
 * @returns {boolean}
 */
function main ({ subject, rules, weapon, spell }) {
  rules.checkActor(subject)
  return isRangedSpell(spell) || isRangedWeapon(weapon)
}

module.exports = main

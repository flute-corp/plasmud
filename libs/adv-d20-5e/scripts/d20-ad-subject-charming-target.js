/**
 * Indique si la cible est charmée par le sujet
 * @param subject {EntityState}
 * @param target {EntityState}
 * @param rules {Rules}
 * @return {boolean}
 */
function main ({ subject, target, rules }) {
  rules.checkActor(subject)
  rules.checkActor(target)
  const aCharmEffects = rules.getActiveEffects(target, ({ tag }) => tag === rules.CONST.EFFECT_CHARM)
  // parmis les effets de charm affectés à target, y en a t il dont la source est le sujet ?
  return aCharmEffects.some(({ source }) => source === subject)
}

module.exports = main

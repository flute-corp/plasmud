/**
 * déterminer si le sujet n'est pas détecté par la cible
 * @param subject {EntityState} sujet pour lequel on effectue le test
 * @param target {EntityState} cible actuelle du sujet
 * @param rules {Rules} système
 * @returns {boolean}
 */
function main ({ subject, target, rules }) {
  rules.checkActor(subject)
  rules.checkActor(target)
  return !rules.runScript('d20-ad-subject-detected-by-target', { subject, target })
}

module.exports = main

/**
 * Renvoie true si le sujet porte une armure qui n'est pas taggée STEALTH
 * @param subject {EntityState} sujet pour lequel on effectue le test
 * @param rules {Rules} système
 * @returns {boolean}
 */
function main ({ subject, rules }) {
  rules.checkActor(subject)
  const oArmor = rules.getEquippedItem(subject, rules.getItemBaseTypeSlot(rules.CONST.ITEM_BASE_TYPE_ARMOR))
  return oArmor.disadvantages.includes[rules.CONST.SKILL_STEALTH]
}

module.exports = main

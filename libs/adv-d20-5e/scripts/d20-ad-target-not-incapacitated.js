/**
 * Renvoie true si la cible est capable de combattre
 * @param subject {EntityState} sujet pour lequel on effectue le test
 * @param target {EntityState} target pour lequel on effectue le test
 * @param rules {Rules} système
 * @returns {boolean}
 */
function main ({ subject, target, rules }) {
  return !rules.runScript('d20-ad-target-incapacitated', { subject, target })
}

module.exports = main

function isWeaponHeavy (oWeapon, rules) {
  return oWeapon && oWeapon.itemBaseType === rules.CONST.ITEM_BASE_TYPE_WEAPON
    ? oWeapon.attributes.includes(rules.CONST.WEAPON_ATTRIBUTE_HEAVY)
    : false
}

/**
 * déterminer si le sujet porte une arme lourde
 * @param subject {EntityState} sujet pour lequel on effectue le test
 * @param rules {Rules} système
 * @returns {boolean}
 */
function main ({ subject, rules }) {
  rules.checkActor(subject)
  const oWeaponM = rules.getEquippedItem(subject, rules.CONST.EQUIPMENT_SLOT_WEAPON_MELEE)
  const oWeaponR = rules.getEquippedItem(subject, rules.CONST.EQUIPMENT_SLOT_WEAPON_RANGED)
  return isWeaponHeavy(oWeaponM, rules) || isWeaponHeavy(oWeaponR, rules)
}

module.exports = main

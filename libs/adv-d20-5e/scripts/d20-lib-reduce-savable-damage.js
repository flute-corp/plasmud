/**
 * Réduit les dégats d'une attaque pour laquelle on à réussi un jet de sauvegarde.
 * Normalement les degat sont réduits de moitié mais certain talents de rogue permettent de potentiellement eviter
 * tous les dégats si le jet de sauvegarde (dex) est réussi
 */

/**
 * Renvoie true si le personnage dispose du feat EVASION et que le jet de sauvegarde est un jet de DEXTERITY
 * @param oRules {Rules}
 * @param oActor {EntityState}
 * @param ability {String}
 * @returns {boolean}
 */
function canBeEvaded (oRules, oActor, ability) {
  return ability === oRules.CONST.ABILITY_DEXTERITY && oActor.feats.includes(oRules.CONST.FEAT_EVASION)
}

function main ({ rules, subject, saved, ability, damage }) {
  rules.checkActor(subject)
  // cas des roublards et de l'EVASION
  if (canBeEvaded(rules, subject, ability)) {
    return saved ? 0 : damage >>> 1
  }
  return saved ? damage >>> 1 : damage
}

module.exports = main

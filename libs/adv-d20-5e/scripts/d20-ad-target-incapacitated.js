/**
 * Renvoie true si la cible n'est pas capable de combattre
 * @param subject {EntityState} sujet pour lequel on effectue le test
 * @param target {EntityState} target pour lequel on effectue le test
 * @param rules {Rules} système
 * @returns {boolean}
 */
function main ({ subject, target, rules }) {
  rules.checkActor(target)
  // TODO dresser la liste des effets incapacitants
  return false
}

module.exports = main

/**
 * target too close
 * Renvoie true si la cible du sujet est trop proche pour utiliser efficacement une arme de distance
 * @param subject {EntityState} sujet pour lequel on effectue le test
 * @param target {EntityState} cible actuelle du sujet
 * @param rules {Rules} système
 * @returns {boolean}
 */
function main ({ subject, target, rules }) {
  return rules.runScript('d20-ad-target-at-melee-range', { subject: target, target: subject })
}

module.exports = main

/**
 * déterminer si le sujet porte une arme à distance
 * @param subject {EntityState} sujet pour lequel on effectue le test
 * @param rules {Rules} système
 * @returns {boolean}
 */
function main ({ subject, rules }) {
  rules.checkActor(subject)
  const oWeapon = subject.getLast.weaponUsed
  return oWeapon && oWeapon.ranged
}

module.exports = main

/**
 * Indique si la cible est effrayée par le sujet
 * @param subject {EntityState}
 * @param target {EntityState}
 * @param rules {Rules}
 * @return {boolean}
 */
function main ({ subject, target, rules }) {
  rules.checkActor(subject)
  rules.checkActor(target)
  const aFearEffects = rules.getActiveEffects(target, ({ tag }) => tag ===  rules.CONST.EFFECT_FEAR)
  // parmis les effets de charm affectés à target, y en a t il dont la source est le sujet ?
  return aFearEffects.some(({ source }) => source === subject)
}

module.exports = main

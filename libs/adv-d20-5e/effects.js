// ./effects (generated)
module.exports = {
  EFFECT_ABILITY_MODIFIER: require('./effects/ability-modifier.js'),
  EFFECT_AC_MODIFIER: require('./effects/ac-modifier.js'),
  EFFECT_ATTACK_MODIFIER: require('./effects/attack-modifier.js'),
  EFFECT_BLINDNESS: require('./effects/blindness.js'),
  EFFECT_CHARM: require('./effects/charm.js'),
  EFFECT_CONCEALMENT: require('./effects/concealment.js'),
  EFFECT_DAMAGE_IMMUNITY: require('./effects/damage-immunity.js'),
  EFFECT_DAMAGE_MODIFIER: require('./effects/damage-modifier.js'),
  EFFECT_DAMAGE_RESISTANCE: require('./effects/damage-resistance.js'),
  EFFECT_DAMAGE: require('./effects/damage.js'),
  EFFECT_DEAFNESS: require('./effects/deafness.js'),
  EFFECT_EFFECT_IMMUNITY: require('./effects/effect-immunity.js'),
  EFFECT_ENHANCEMENT: require('./effects/enhancement.js'),
  EFFECT_FEAR: require('./effects/fear.js'),
  EFFECT_GROUP: require('./effects/group.js'),
  EFFECT_INVISIBILITY: require('./effects/invisibility.js'),
  EFFECT_POISON: require('./effects/poison.js'),
  EFFECT_SAVING_THROW_MODIFIER: require('./effects/saving-throw-modifier.js'),
  EFFECT_SEE_INVISIBILITY: require('./effects/see-invisibility.js'),
  EFFECT_SPELL_IMMUNITY: require('./effects/spell-immunity.js'),
  EFFECT_WEAPON_RESISTANCE: require('./effects/weapon-resistance.js')
}

// ./item-properties (generated)
module.exports = {
  ITEM_PROPERTY_ABILITY_MODIFIER: require('./item-properties/ability-modifier.js'),
  ITEM_PROPERTY_AC_MODIFIER: require('./item-properties/ac-modifier.js'),
  ITEM_PROPERTY_ATTACK_MODIFIER: require('./item-properties/attack-modifier.js'),
  ITEM_PROPERTY_CONCEALMENT: require('./item-properties/concealment.js'),
  ITEM_PROPERTY_DAMAGE_IMMUNITY: require('./item-properties/damage-immunity.js'),
  ITEM_PROPERTY_DAMAGE_MODIFIER: require('./item-properties/damage-modifier.js'),
  ITEM_PROPERTY_DAMAGE_RESISTANCE: require('./item-properties/damage-resistance.js'),
  ITEM_PROPERTY_ENHANCEMENT: require('./item-properties/enhancement.js'),
  ITEM_PROPERTY_IMMUNE_CRITICAL: require('./item-properties/immune-critical.js'),
  ITEM_PROPERTY_INCREASE_HITPOINTS: require('./item-properties/increase-hitpoints.js'),
  ITEM_PROPERTY_ON_HIT_POISON: require('./item-properties/on-hit-poison.js'),
  ITEM_PROPERTY_SAVING_THROW_MODIFIER: require('./item-properties/saving-throw-modifier.js'),
  ITEM_PROPERTY_VAMPYRE: require('./item-properties/vampyre.js'),
  ITEM_PROPERTY_WEAPON_RESISTANCE: require('./item-properties/weapon-resistance.js')
}

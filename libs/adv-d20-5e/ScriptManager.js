const Scriptorium = require('../scriptorium')
const Scripts = require('./scripts')

class ScriptManager extends Scriptorium {
  constructor (oRules) {
    super()
    this.routes = {
      ...Scripts
    }
    this.defaultContext = {
      rules: oRules
    }
  }
}

module.exports = ScriptManager

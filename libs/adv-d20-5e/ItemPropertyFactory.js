const ITEM_PROPERTIES = require('./item-properties')
/**
 * @typedef ItemProperty {object}
 * @param property {string} identifiant de l'item property
 * @param value {number|string} valeur numérique ou chaîne
 */

class ItemPropertyFactory {
  constructor () {
    this._properties = ITEM_PROPERTIES
  }

  /**
   * Creation de la structure de base de l'item property
   * @param sId
   * @param payload
   * @returns {ItemProperty}
   */
  create (sId, payload) {
    const PROPS = this._properties
    if (sId in PROPS) {
      const oModule = PROPS[sId]
      const oIP = oModule.create(payload)
      if (!('tag' in oIP)) {
        oIP.tag = ''
      }
      return oIP
    } else {
      throw new Error('Item property script not found : "' + sId + '"')
    }
  }
}

module.exports = ItemPropertyFactory

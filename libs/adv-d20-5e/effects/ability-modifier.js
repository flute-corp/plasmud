const Effect = require('./abstract/Effect')

/**
 * @class AbilityModifier
 * la creature gagne ou perd un certain nombre de point d'abilité
 */
class AbilityModifier extends Effect {
  /**
   * Initialisateur
   * @param value {number} +1, +2, +3, etc... ou bien -1, -2 ...
   * @param ability {string} ABILITY_*
   */
  constructor ({ value, ability }) {
    super()
    this.amp = value
    this.ability = ability
  }
}

module.exports = AbilityModifier

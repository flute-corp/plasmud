const { EffectInitError } = require('../../errors')
const CONST = require('../../consts')

/**
 * Cette classe est une classe de base qui ne fait que mémoriser un état
 */
class Effect {
  constructor () {
    this._against = {
      morality: CONST.ALIGNMENT_MORALITY_ANY,
      entropy: CONST.ALIGNMENT_ENTROPY_ANY,
      damage: CONST.DAMAGE_TYPE_ANY,
      specie: CONST.SPECIE_ANY
    }
    this._tag = ''
    this._amp = 0
    this._source = null
    this._target = null
    this._terminated = false
    this._duration = 0
    this._elapsed = 0
    this._data = {}
  }

  get tag () {
    return this._tag
  }

  set tag (value) {
    this._tag = value
  }

  get state () {
    return {
      amp: this.amp,
      tag: this.tag,
      terminated: this._terminated,
      duration: this._duration,
      elapsed: this._elapsed,
      data: this._data,
      source: this._source ? this._source.id : null,
      target: this._target ? this._target.id : null
    }
  }

  get amp () {
    return this._amp
  }

  set amp (value) {
    this._amp = value
  }

  get source () {
    return this._source
  }

  set source (value) {
    this._source = value
  }

  get target () {
    return this._target
  }

  set target (value) {
    this._target = value
  }

  get data () {
    return this._data
  }

  get elapsed () {
    return this._elapsed
  }

  get duration () {
    this._terminated = this._duration <= 0
    return this._duration
  }

  set duration (value) {
    this._duration = value
  }

  /**
   * Permet de rendre le fonctionnement de l'effet conditionnel à la nature de l'aggression
   * @param against {AgainstStruct}
   */
  set against (against) {
    this._against = {
      ...this._against,
      ...against
    }
  }

  get against () {
    return this._against
  }

  initError (oEffectState, sReason) {
    return new EffectInitError(oEffectState, sReason)
  }

  get expired () {
    return this._terminated || this._duration === null || this._duration <= 0
  }

  /**
   * Termine immédiatement l'effet
   */
  terminate () {
    if (!this._terminated) {
      this._terminated = true
      this.beforeTerminate()
    }
    this._duration = 0
  }

  beforeTerminate () {
    // ce qu'il se passe lorsque l'effet se termine
  }

  /**
   * Les effets passifs on une methode process qui ne fait rien
   * @param oRules {Rules}
   */
  process (oRules) {
    ++this._elapsed
    --this._duration
  }
}

module.exports = Effect

const Effect = require('./abstract/Effect')

/**
 * @class DamageImmunity
 * Immunité partielle ou totale aux dégats
 * Cet effet est appliqué à une créature
 */
class DamageImmunity extends Effect {
  /**
   * Initialisateur
   * @param value {number} nombre flottant entre -1 et -Infinity, correspond à la fraction de dégâts ignorée
   * ou si le nombre est négatif le facteur d'augmentation des dégâts
   * une valeur de 1 indique que la créature est immunisée contre le type de dégâts spécifié
   * une valeur de 0.5 indique que la créature ne prend que 50% des dégats
   * une valeur de 0 est neutre, les dégâts sont appliqué normalement
   * une valeur de -1 indique que les dégâts sont doublés
   * une valeur de -2 indique que les dégâts sont triplés
   * @param type {string[]} liste des types de dégâts
   */
  constructor ({ value, types }) {
    super()
    // amp doit etre inférieur ou égal à 1
    this.amp = Math.min(1, value)
    this.types = Array.isArray(types) ? types : [types]
  }
}

module.exports = DamageImmunity

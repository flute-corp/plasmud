const Effect = require('./abstract/Effect')

/**
 * @class Enhancement
 * l'arme sur laquelle est appliqué l'effet gagne +x en attaque et +x en dégats
 * @remark AGAINSTABLE
 */
class Enhancement extends Effect {
  /**
   * Initialisateur
   * @param value {number} +1, +2, +3, etc... ou bien -1, -2 ...
   * @param against {AgainstStruct}
   */
  constructor ({ value, against = {} }) {
    super()
    this.amp = value
    this.against = against
  }
}

module.exports = Enhancement

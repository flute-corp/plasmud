const Effect = require('./abstract/Effect')

/**
 * @class Deafness
 * la creature est assourdie
 * A deafened creature can’t hear and automatically fails any ability check that requires hearing. (perception)
 */
class Deafness extends Effect {
}

module.exports = Deafness

const Effect = require('./abstract/Effect')
const Dice = require('../includes/Dice')

/**
 * @class DamageModifier
 * Augmentation ou réduction des dégats d'un certain type
 * Cet effet est appliqué à une arme
 * @remark AGAINSTABLE
 */
class DamageModifier extends Effect {
  /**
   * Initialisateur
   * @param value {number}
   * @param type {string[]} liste des types de dégâts
   * @param against {AgainstStruct}
   */
  constructor ({ value, type, against = {} }) {
    super()
    this.formula = Dice.xdy(value)
    this.type = type
    this.against = against
  }
}

module.exports = DamageModifier

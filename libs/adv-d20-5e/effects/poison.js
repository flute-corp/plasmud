const Effect = require('./abstract/Effect')

/**
 * @class Poison
 * la creature est empoisonnée
 * Poisoned entites have disadvantage on attack rolls and ability checks.
 */
class Poison extends Effect {
  /**
   * Initialisateur
   * @param type {string} POISON_*
   */
  constructor ({ type }) {
    super()
    this.type = type
  }
}

module.exports = Poison

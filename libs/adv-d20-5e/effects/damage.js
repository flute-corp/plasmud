const Effect = require('./abstract/Effect')
const Dice = require('../includes/Dice')
const CONST = require('../consts')

const dice = new Dice()

/**
 * @class Damage
 * The damage effect lowers target's HP
 */
class Damage extends Effect {
  /**
   * Initialisateur
   * @param value {number|string} damage amount. "xdy + z" notation will be evaluated
   * @param type {string} DAMAGE_TYPE_*
   * @param weapon {EntityState} arme utilisé (pour résoudre le problème de vampyre)
   * @param ammo {EntityState} munition utilisée
   */
  constructor ({ value, type, weapon, ammo }) {
    super()
    if (value === undefined || value === null) {
      throw new Error('"value" is required in this effect')
    }
    this.formula = Dice.xdy(value)
    if (weapon && !type) {
      type = weapon.damageType
    }
    if (!type) {
      throw new Error('invalid damage type')
    }
    this.type = type
    this.weapon = weapon
    this.ammo = ammo
    this.info = null
  }

  getUnresistedDamageAmount (amount, resistance) {
    return resistance < amount
      ? amount - resistance
      : 0
  }

  getResistedDamageAmount (amount, resistance) {
    return Math.min(resistance, amount)
  }

  getUnimmunizedDamageAmount (amount, immunity) {
    return Math.floor(amount * Math.max(0, 1 - immunity))
  }

  getImmunizedDamageAmount (amount, immunity) {
    return Math.floor(amount * Math.max(0, immunity))
  }

  getAmplifiedDamageAmount (amount, vulnerability) {
    return Math.floor(amount * Math.max(0, 1 + vulnerability))
  }

  /**
   * Applique un effet vampire lorsqu'une arme vampirique inflige des dégâts
   * @param oRules {Rules}
   * @param oVampyre {EffectState} Entité qui utilise l'arme vampirique
   * @param nAmount {number} quantité de dégâts initiaux de l'attaque
   */
  applyVampyreEffect (oRules, oVampyre, nAmount) {
    // L'arme est elle vampirique ?
    const oWeapon = this.weapon
    let nWeaponVampyre = 0
    if (oWeapon) {
      nWeaponVampyre = oRules.aggregateModifiers(oWeapon, [CONST.ITEM_PROPERTY_VAMPYRE]).max
      if (oWeapon.ranged) {
        const oAmmo = this.ammo
        const nAmmoVampyre = oAmmo
          ? oRules.aggregateModifiers(oWeapon, [CONST.ITEM_PROPERTY_VAMPYRE]).max
          : 0
        nWeaponVampyre = Math.max(nWeaponVampyre, nAmmoVampyre)
      }
      oRules.modifyHitPoints(oVampyre, -nWeaponVampyre)
    }
    return nWeaponVampyre
  }

  getWeaponResistance (oRules) {
    const oActor = this.target
    const weapon = this.ammo || this.weapon
    if (!weapon) {
      return 0
    }
    const bMagicalWeapon = weapon.magical
    if (bMagicalWeapon) {
      return 0
    }
    return oRules.aggregateModifiers(
      oActor,
      [
        oRules.CONST.EFFECT_WEAPON_RESISTANCE,
        oRules.CONST.ITEM_PROPERTY_WEAPON_RESISTANCE
      ]).max
  }

  process (oRules) {
    super.process(oRules)
    const target = this.target
    const amount = dice.evaluate(this.formula)
    const type = this.type

    const oDamageInfo = {
      type,
      damage: {
        initial: amount,
        resisted: 0,
        immunized: 0,
        amplified: 0,
        final: amount
      },
      resistance: Math.max(
        oRules.runScript('d20-lib-get-damage-resistance', { subject: target, type }),
        this.getWeaponResistance(oRules)
      ),
      immunity: oRules.runScript('d20-lib-get-damage-immunity', { subject: target, type }),
      vulnerability: oRules.runScript('d20-lib-get-damage-vulnerability', { subject: target, type }),
      balance: 0,
      protection: 0,
      vampyre: 0
    }
    oDamageInfo.balance = oDamageInfo.immunity - oDamageInfo.vulnerability
    oDamageInfo.damage.resisted = this.getResistedDamageAmount(amount, oDamageInfo.resistance)
    if (oDamageInfo.balance >= 0) {
      oDamageInfo.damage.immunized =
        this.getImmunizedDamageAmount(amount - oDamageInfo.damage.resisted, oDamageInfo.immunity)
      oDamageInfo.damage.final = amount - oDamageInfo.damage.resisted - oDamageInfo.damage.immunized
    } else {
      oDamageInfo.damage.final =
        oDamageInfo.damage.amplified =
          this.getAmplifiedDamageAmount(amount - oDamageInfo.damage.resisted, oDamageInfo.vulnerability)
    }
    oDamageInfo.protection = oRules.runScript('d20-lib-dec-damage-resistance-buffer', { subject: target, type, value: oDamageInfo.damage.final })
    oDamageInfo.vampyre = this.applyVampyreEffect(oRules, this.source, oDamageInfo.damage.final)
    this.info = oDamageInfo
    oRules.modifyHitPoints(target, -oDamageInfo.damage.final)
    target.getLast.damager = this.source
    if (target.hp <= 0) {
      target.getLast.killer = this.source
    }
  }
}

module.exports = Damage

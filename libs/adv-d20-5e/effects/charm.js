const Effect = require('./abstract/Effect')

/**
 * @class Charm
 * A charmed creature can’t attack the charmer or target the charmer with harmful abilities or magical effects.
 * The charmer has advantage on any ability check to interact socially (charisma) with the creature.
 */
class Charm extends Effect {
}

module.exports = Charm

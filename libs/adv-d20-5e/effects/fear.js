const Effect = require('./abstract/Effect')

/**
 * @class Fear
 *
 * A frightened creature has disadvantage on ability checks and attack rolls while the source of its fear is within line of sight.
 * The creature can’t willingly move closer to the source of its fear.
 */
class Fear extends Effect {
}

module.exports = Fear

const Effect = require('./abstract/Effect')
const CONST = require('../consts')

/**
 * @class SavingThrowModifier
 * la creature gagne ou perd un certain nombre de point de bonus d'attaque
 */
class SavingThrowModifier extends Effect {
  /**
   * Initialisateur
   * @param value {number} +1, +2, +3, etc... ou bien -1, -2 ...
   * @param ability {string} ABILITY_*
   * @param type {string} SAVING_THROW_*
   * @param against {AgainstStruct}
   * @remark AGAINSTABLE
   */
  constructor ({ value, ability, type = CONST.SAVE_TYPE_ALL, against = {} }) {
    super()
    this.amp = value
    this.ability = ability
    this.type = type
    this.against = against
  }
}

module.exports = SavingThrowModifier

const Effect = require('./abstract/Effect')

/**
 * @class AcModifier
 * la creature gagne ou perd un certain nombre de point de classe d'armure d'un certain type
 */
class AcModifier extends Effect {
  /**
   * Initialisateur
   * @param value {number} +1, +2, +3, etc... ou bien -1, -2 ...
   * @param type {string} AC_*
   * @param against {AgainstStruct}
   *
   * @remark AGAINSTABLE
   */
  constructor ({ value, against = {} }) {
    super()
    this.amp = value
    this.against = against
  }
}

module.exports = AcModifier

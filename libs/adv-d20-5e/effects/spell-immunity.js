const Effect = require('./abstract/Effect')

/**
 * @class Poison
 * la creature est immunisée contre les sorts de niveau inférieur ou égal à l'amp
 */
class SpellImmunity extends Effect {
  /**
   * initialisation
   * @param level {number}
   * @param school {string} SPELL_SCHOOL
   * @param spell {string} identifiant sort spécifique
   * @param category {string} SPELL_CATEGORY
   * @param buffer {number} nombre de niveaux de sort absorbable avant dissipation
   */
  constructor ({ level = -1, school = '', spell = '', category = '', buffer = Infinity }) {
    super()
    this.amp = level
    this.school = school
    this.spell = spell
    this.category = category
    this.buffer = buffer
  }

  get expired () {
    return super.expired || this.buffer <= 0
  }

  canProtectFromSpell (oSpellContext) {
    const bSchoolResisted = this.school === '' || oSpellContext.data.school === this.school
    const bLevelResisted = oSpellContext.level.innate <= this.amp
    const bSpellResisted = this.spell === '' || oSpellContext.id === this.spell
    const bCategoryResisted = this.category === '' || oSpellContext.data.categories.includes(this.category)
    return bSchoolResisted && bLevelResisted && bSpellResisted && bCategoryResisted
  }

  reduceBuffer (oSpellContext) {
    this.buffer -= oSpellContext.level.innate
  }
}

module.exports = SpellImmunity

const Effect = require('./abstract/Effect')

/**
 * @class WeaponResistance
 * Résistance aux armes normales
 */
class WeaponResistance extends Effect {
  /**
   * Initialisateur
   * @param value {number} nombre de points de dégât résisté à chaque attaque
   */
  constructor ({ value }) {
    super()
    this.amp = value
  }
}

module.exports = WeaponResistance

const Effect = require('./abstract/Effect')

/**
 * @class Concealment
 * Camouflage total ou partiel
 * Cet effet est appliqué à une créature
 * @remark AGAINSTABLE
 * Exploité dans Rules::computeAttackRoll
 */
class Concealment extends Effect {
  /**
   * Initialisateur
   * @param value {number} nombre flottant entre 0 et 1, correspond à la probabilité d'ignorer une attaque visée
   * @param against {AgainstStruct}
   */
  constructor ({ value, against = {} }) {
    super()
    // amp doit etre inférieur ou égal à 1
    this.amp = Math.min(1, Math.max(0, value))
    this.against = against
  }
}

module.exports = Concealment

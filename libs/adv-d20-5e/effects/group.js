const Effect = require('./abstract/Effect')
const { EffectInitError } = require('../errors')

/**
 * @class Group
 *
 * Effet technique permettant de grouper plusieur effets en un seul groupe
 * - terminer un EFFECT_GROUP provoque la terminaison de tous les effets groupés
 * - l'indicateur d'expiration est a true lorsque les indicateurs d'expiration de tous les effets sont à true
 */
class Group extends Effect {
  constructor ({ group, tag }) {
    super()
    if (!Array.isArray(group)) {
      throw this.initError('"effects" must be an array of EffectState')
    }
    this.group = group
    this.tag = tag
  }

  get duration () {
    return this.group.reduce((prev, curr) => Math.max(curr.duration, prev), 0)
  }

  set duration (value) {
    super.duration = value
  }

  against (against) {
    this.group.forEach(e => {
      e.against = against
    })
  }

  get expired () {
    return this.group.every(e => e.expired)
  }

  terminate () {
    this.group.forEach(e => e.terminate())
  }
}

module.exports = Group

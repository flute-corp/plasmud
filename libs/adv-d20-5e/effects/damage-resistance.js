const Effect = require('./abstract/Effect')

/**
 * @class DamageResistance
 * Résistance à un certain type de dégât
 * Parfois un damage buffer sert de compteurs de dégât : lors que ce buffer tombe à zero, l'effet est dissipé
 */
class DamageResistance extends Effect {
  /**
   * Initialisateur
   * @param value {number} nombre de points de dégât résisté à chaque attaque
   * @param type {string} liste de type de dégâts résisté par l'effet
   * @param buffer {number} nombre de point de dégât max encaissable avant dissipation de l'effet
   */
  constructor ({ value, types, buffer = null }) {
    super()
    this.amp = value
    this.types = Array.isArray(types) ? types : [types]
    this.buffer = buffer
  }

  isBufferDepleted () {
    return (typeof this.buffer === 'number') ? this.buffer <= 0 : false
  }

  get expired () {
    return super.expired || this.isBufferDepleted()
  }
}

module.exports = DamageResistance

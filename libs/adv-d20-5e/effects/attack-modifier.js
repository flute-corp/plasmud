const Effect = require('./abstract/Effect')

/**
 * @class AttackModifier
 * la creature gagne ou perd un certain nombre de point de bonus d'attaque
 */
class AttackModifier extends Effect {
  /**
   * Initialisateur
   * @param value {number} +1, +2, +3, etc... ou bien -1, -2 ...
   * @param against {AgainstStruct}
   * @remark AGAINSTABLE
   */
  constructor ({ value, against = {} }) {
    super()
    this.amp = value
    this.against = against
  }
}

module.exports = AttackModifier

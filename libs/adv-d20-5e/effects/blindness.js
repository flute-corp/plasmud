const Effect = require('./abstract/Effect')

/**
 * @class Blindness
 * la creature est aveuglée
 * A blinded creature can’t see and automatically fails any ability check that requires sight. (perception)
 * Attack rolls against the creature have advantage, and the creature’s attack rolls have disadvantage.
 */
class Blindness extends Effect {
}

module.exports = Blindness

const Effect = require('./abstract/Effect')

/**
 * @class Immunity
 * la creature est immunisée contre un certain type d'effets
 */
class EffectImmunity extends Effect {
  /**
   * Initialisateur
   * @param effect {string} tag de l'effet immunisé
   */
  constructor ({ effect }) {
    super()
    this.which = effect
  }
}

module.exports = EffectImmunity

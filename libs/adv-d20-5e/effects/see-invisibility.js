const Effect = require('./abstract/Effect')

/**
 * @class SeeInvisibility
 * Permet de voir les creature invisible
 */
class SeeInvisibility extends Effect {
}

module.exports = SeeInvisibility

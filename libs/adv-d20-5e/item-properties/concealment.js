const CONST = require('../consts')
const againstable = require('../includes/againstable')

/**
 *
 * @param value
 * @param against
 * @returns {{against: ((*&{specie: string, damage: string, entropy: string, morality: string})|{specie: string, damage: string, entropy: string, morality: string}), property: string, value}}
 * @remark AGAINSTABLE
 */
function create ({ value, against = {} }) {
  return {
    property: CONST.ITEM_PROPERTY_CONCEALMENT,
    value,
    ...againstable(against)
  }
}

module.exports = { create }

const CONST = require('../consts')

/**
 * Octroie une immunité partielle ou totale à un type de dégats
 * Cela permet d'ignorer un certain pourcentage de dégat
 * La valeur est un nombre flottant entre -1 et 1
 * si la valeur est négative cela devien une vulnérabilité qui augmente les degat recu
 * 0: pas d'immunité
 * 0.5 : semi invulnérabilité (50% de dégats ignoré)
 * 1: totale immunité (100% de dégats ignoré)
 * -1: totale vulnerabilité (+100% de degat en plus)
 * @param type {string} type de dégâts
 * @param value {number} valeur de l'immunité (positive ou négative)
 * @returns {{property: string, type, value}}
 */
function create ({ types, value }) {
  return {
    property: CONST.ITEM_PROPERTY_ABILITY_MODIFIER,
    types,
    value
  }
}

module.exports = { create }

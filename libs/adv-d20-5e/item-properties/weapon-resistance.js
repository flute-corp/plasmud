const CONST = require('../consts')

/**
 * L'objet possédant cette propriété octroie une resistance à un type d'arme
 * @param value {number} valeur de la résistance
 * @returns {{property: string, type, value}}
 */
function create ({ value }) {
  return {
    property: CONST.ITEM_PROPERTY_WEAPON_RESISTANCE,
    value
  }
}

module.exports = { create }

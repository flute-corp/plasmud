const CONST = require('../consts')

function create ({ value }) {
  return {
    property: CONST.ITEM_INCREASE_HITPOINTS,
    value: value
  }
}

module.exports = { create }

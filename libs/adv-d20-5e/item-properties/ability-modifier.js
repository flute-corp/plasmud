const CONST = require('../consts')

/**
 * Bonus ou malus d'un attribut d'une entité
 * @param ability {string} nom de l'attribut
 * @param value {number} valeur (positive ou négative)
 * @returns {{property: string, ability, value}}
 */
function create ({ ability, value }) {
  return {
    property: CONST.ITEM_PROPERTY_ABILITY_MODIFIER,
    ability,
    value
  }
}

module.exports = { create }

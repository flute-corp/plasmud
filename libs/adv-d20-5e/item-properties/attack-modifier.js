const CONST = require('../consts')
const againstable = require('../includes/againstable')

/**
 * Modifie le bonus d'attaque octroyé par l'objet
 * @param value {number}
 * @param against {AgainstStruct}
 * @returns {{against: {damage: string, entropy: string, specie: string, morality: string}, property: string, value}}
 * @remark AGAINSTABLE
 */
function create ({ value, against = {} }) {
  return {
    property: CONST.ITEM_PROPERTY_ATTACK_MODIFIER,
    value,
    ...againstable(against)
  }
}

module.exports = { create }

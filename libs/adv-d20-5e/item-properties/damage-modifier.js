const CONST = require('../consts')
const Dice = require('../includes/Dice')
const againstable = require('../includes/againstable')

/**
 *
 * @param value
 * @param type
 * @param against
 * @returns {{against: ((*&{specie: string, damage: string, entropy: string, morality: string})|{specie: string, damage: string, entropy: string, morality: string}), property: string, type: undefined, value: {count: number, sides: number}}}
 * @remark AGAINSTABLE
 */
function create ({ value, type = undefined, against = {} }) {
  return {
    property: CONST.ITEM_PROPERTY_DAMAGE_MODIFIER,
    value: Dice.xdy(value), // { sides: 6, count: 1 }
    type,
    ...againstable(against)
  }
}

module.exports = { create }

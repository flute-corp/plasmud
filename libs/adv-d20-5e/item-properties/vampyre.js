const CONST = require('../consts')

/**
 * Appliquée à une arme, cette propriété restitue des points de vie au porteur de l'arme
 * @param value {number} quantité max de pv vampirisé pour chaque coup
 * @returns {{property: string, value}}
 */
function create ({ value }) {
  return {
    property: CONST.ITEM_PROPERTY_VAMPYRE,
    value: value
  }
}

module.exports = { create }

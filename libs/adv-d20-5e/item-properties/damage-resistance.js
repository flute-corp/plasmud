const CONST = require('../consts')

/**
 * L'objet possédant cette propriété octroie une resistance à un type de dégât particulier
 * @param types {array} DAMAGE_TYPE_*
 * @param value {number} valeur de la résistance
 * @returns {{property: string, type, value}}
 */
function create ({ types, value }) {
  return {
    property: CONST.ITEM_PROPERTY_DAMAGE_RESISTANCE,
    types,
    value,
    buffer: null
  }
}

module.exports = { create }

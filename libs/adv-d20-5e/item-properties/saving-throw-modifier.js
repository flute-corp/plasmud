const CONST = require('../consts')

/**
 * Bonus ou malus d'un attribut d'une entité
 * @param ability {string} nom de l'attribut
 * @param value {number} valeur (positive ou négative)
 * @param type {string} SAVE_TYPE_*
 * @returns {{property: string, ability, value, type}}
 */
function create ({ ability, type, value }) {
  return {
    property: CONST.ITEM_PROPERTY_ABILITY_MODIFIER,
    ability,
    type,
    value
  }
}

module.exports = { create }

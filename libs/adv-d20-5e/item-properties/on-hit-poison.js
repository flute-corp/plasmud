const CONST = require('../consts')

/**
 * Appliquée à une arme ou une munition, cette propriété applique un effet de poison pour chaque coup porté
 * @param type {string} type de poison utilisé par l'arme ou la munition
 * @returns {{property: string, value}}
 */
function create ({ type }) {
  return {
    property: CONST.ITEM_PROPERTY_ON_HIT_POISON,
    type
  }
}

module.exports = { create }

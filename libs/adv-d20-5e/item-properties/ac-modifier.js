const CONST = require('../consts')
const againstable = require('../includes/againstable')

/**
 * Modificateur de classe d'armure, généralement utilisé pour des bonus.
 * Dans le cas de malus, utiliser AC_UNARMORED
 * @param value {number} modificateur numérique de la classe d'armure
 * @param type {string} type de classe d'armure AC_*
 * @param against {AgainstStruct} Contre quel type d'ennemi ce bonus fonctionne
 * @returns {{against: {damage: string, entropy: string, specie: string, morality: string}, property: string, type, value}}
 * @remark AGAINSTABLE
 */
function create ({ value, against = {} }) {
  return {
    property: CONST.ITEM_PROPERTY_AC_MODIFIER,
    value,
    ...againstable(against)
  }
}

module.exports = { create }

const CONST = require('../consts')

/**
 * Les coup critique sont ignore (non-multiplié) et considéré comme coup normaux
 * @returns {{property: string}}
 */
function create () {
  return {
    property: CONST.ITEM_PROPERTY_IMMUNE_CRITICAL
  }
}

module.exports = { create }

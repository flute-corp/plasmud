class DataStructureError extends Error {
  constructor (oEntity, sKey) {
    super('EntityState "' + oEntity.id + '" data structure corrupted: could not be found "' + sKey + '" property.')
    this.name = 'DataStructureError'
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, DataStructureError)
    }
  }
}

class BadEntityTypeError extends Error {
  constructor (oEntity, sExpected) {
    super('EntityState data structure invalid: expected ' + sExpected + ' - ' + oEntity.entityType + ' given.')
    this.name = 'BadEntityTypeError'
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, BadEntityTypeError)
    }
  }
}

class EntityPropertyMissingError extends Error {
  constructor (sProperty) {
    super('EntityState property missing : ' + sProperty)
    this.name = 'EntityPropertyMissingError'
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, EntityPropertyMissingError)
    }
  }
}

class EffectInitError extends Error {
  constructor (oEffectState, sReason) {
    super('Effect events error : [' + oEffectState.tag + ']: ' + sReason)
    this.name = 'EffectInitError'
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, EffectInitError)
    }
  }
}

class KeyNotFoundError extends Error {
  constructor (sKey, sCollection) {
    super('Key "' + sKey + '" could not be found in collection "' + sCollection + '"')
    this.name = 'KeyNotFoundError'
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, KeyNotFoundError)
    }
  }
}

module.exports = {
  DataStructureError,
  BadEntityTypeError,
  EntityPropertyMissingError,
  EffectInitError,
  KeyNotFoundError
}

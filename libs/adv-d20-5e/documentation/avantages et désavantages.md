# Avantages et désavantage

## Lexique

Pour alléger le texte, au lieu de dire à chaque fois *"avantage ou désavantage"*
on dira *"circonstance"*

## Mécanismes

Chaque fois qu'un jet de dé est effectué pour certaines actions
(jet de sauvegarde, jet d'attaque, jet de compétence)
on calcule les *circonstances* associées à ce jet.
Pour calculer ces *circonstances* on se base sur un
fichier de définition json qui contient la liste des scripts
à lancer.
Si tous les scripts d'une *circonstance* renvoie la valeur *true*
alors on concidère que la créature bénéficie de la *circonstance*
pour ce type de jet.

## Créer un avantage ou un désavantage

### editer le fichier correspondant

Soit **data/advantages.json** pour les avantages
soit **data/disadvantages.json** pour les désavantages.

### Ajouter une entrée structurée dans *advantages.json*

#### propriété : description
Un texte libre décrivant les conditions dans lesquelle
s'appliquent l'avantage, le type de jet de dé, la
caractéristique impliquée...

#### propriété : type
l'une des deux valeurs suivantes :
- ADVANTAGE
- DISADVANTAGE

#### propriété : ability
Un tableau dont chaque élément est un des constantes de caractéristiques.
Ce tableau restreint les caractéristiques qui peuvent activer
cet avantage.

Un tableau vide désactive l'avantage car aucune caractéristique
soumise ne pourra correspondre à un filtre vide.

Si l'avantage s'applique à n'importe quelle caractéristique
on peut soit toutes les énnumérer, soit mettre la propriété
à la valeur **null**

#### propriété : skills
Un tableau de constantes de compétence permettant de filtrer
les jet de test de compétence pour lesquels cet avantage
s'applique.

#### propriété : rollTypes
Un tableau de filtrage des type de jet de dés.

Chaque élément peut etre une des valeurs suivantes :
- ROLL_TYPE_ATTACK
- ROLL_TYPE_SAVE
- ROLL_TYPE_CHECK

#### propriété : saveTypes
Un tableau enumérant les type de jet de sauvegarde
géré par cet avantage.

#### propriété : conditions
Liste des scripts qui seront lancé pour déterminer si l'avantage
s'applique, si un script renvoie FALSE l'avantage ne s'applique pas.


### Scripts
Ne pas oublier de créer les scripts qu'on a défini dans l'avantage.
Sinon ça plante.

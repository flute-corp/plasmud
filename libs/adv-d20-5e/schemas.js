// ./schemas (generated)
module.exports = {
  CREATURE_BLUEPRINT: require('./schemas/creature-blueprint.json'),
  ITEM_BLUEPRINT: require('./schemas/item-blueprint.json')
}

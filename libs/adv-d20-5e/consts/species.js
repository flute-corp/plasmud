module.exports = {
  SPECIE_ABERRATION: 'SPECIE_ABERRATION', // abérrations, aliens, et horreurs cosmiques : tyranoeil, aboleth, mimic, devoreur mental, aliens, créature pseudo naturelles...
  SPECIE_BEAST: 'SPECIE_BEAST', // animaux normaux ou primitifs : loup, chauve souris, rat, tigre, ours, dinosaures...
  SPECIE_CELESTIAL: 'SPECIE_CELESTIAL', // créatures natives des plans célestes (anges, pégases, planétars, solaires...)
  SPECIE_CONSTRUCT: 'SPECIE_CONSTRUCT', // créature mécaniques et artificielles : golems, robots, horreur casquée, épées animées
  SPECIE_DRAGON: 'SPECIE_DRAGON', // dragons
  SPECIE_ELEMENTAL: 'SPECIE_ELEMENTAL', // élémentaires de feu, glace, eau, djinn, assassin invisible
  SPECIE_FEY: 'SPECIE_FEY', // créature connectée à la nature : féé, dryade, nymphe, grig, nixe, satyre,
  SPECIE_FIENDS: 'SPECIE_FIELDS', // créatures natives des plans infernaux (démons, diable, succubes, rakshasa, yugoloth, chiens de l'enfer...)
  SPECIE_GIANT: 'SPECIE_GIANT', // humanoides géants : troll, ogres, géants, ettins...
  SPECIE_HUMANOID: 'SPECIE_HUMANOID', // humain, elfe, nain, gnome, orque, gobelin, gobelours, hobgobelin
  SPECIE_MAGICAL_BEAST: 'SPECIE_MAGICAL_BEAST', // animaux créés par magie : chien éclipsant, licorne
  SPECIE_MONSTROSITY: 'SPECIE_MONSTROSITY', // créatures monstrueuses issues d'expériences, de malédictions, de mutations, (loup-garou, minotaure, yuan-ti)
  SPECIE_OOZE: 'SPECIE_OOZE', // créatures sans consistence solide : blobs, gelée, cube gélatineux
  SPECIE_PLANT: 'SPECIE_PLANT', // végétaux doués d'animation : myconide
  SPECIE_UNDEAD: 'SPECIE_UNDEAD', // mort vivants en tout genre : zombie, vampire, liche, fantôme, goules, squelettes
  SPECIE_VERMIN: 'SPECIE_VERMIN' // divers arthropodes et vers : insectes, vers, araignées, charognard
}

module.exports = {
  DAMAGE_TYPE_CRUSHING: 'DAMAGE_TYPE_CRUSHING', // dégâts écrasants (massues, gourdins, marteaux...)
  DAMAGE_TYPE_PIERCING: 'DAMAGE_TYPE_PIERCING', // dégâts perforants (piques, dagues...)
  DAMAGE_TYPE_SLASHING: 'DAMAGE_TYPE_SLASHING', // dégâts tranchants (épée, haches...)
  DAMAGE_TYPE_FIRE: 'DAMAGE_TYPE_FIRE', // dégâts de feu
  DAMAGE_TYPE_COLD: 'DAMAGE_TYPE_COLD', // dégâts de froid
  DAMAGE_TYPE_ACID: 'DAMAGE_TYPE_ACID', // dégâts d'acide
  DAMAGE_TYPE_ELECTRICITY: 'DAMAGE_TYPE_ELECTRICITY', // dégâts d'électricité
  DAMAGE_TYPE_FORCE: 'DAMAGE_TYPE_FORCE', // dégâts de force
  DAMAGE_TYPE_WITHERING: 'DAMAGE_TYPE_WITHERING', // dégâts d'énergie négative
  DAMAGE_TYPE_POISON: 'DAMAGE_TYPE_POISON', // dégâts empoisonnés
  DAMAGE_TYPE_PSYCHIC: 'DAMAGE_TYPE_PSYCHIC', // dégâts psychiques
  DAMAGE_TYPE_RADIANT: 'DAMAGE_TYPE_RADIANT'
}

const {
  ABILITIES,
  MORALITY,
  ENTROPY,
  SPECIES,
  DAMAGE_TYPES,
  SAVE_TYPES,
  ENTITY_TYPES,
  ITEM_BASE_TYPES,
  WEAPON_TYPES,
  ARMOR_TYPES,
  AMMO_TYPES,
  SHIELD_TYPES
} = require('./enums')

const AGAINST_DEF = {
  property: 'against',
  type: 'object',
  properties: [
    {
      property: 'entropy',
      type: 'enum',
      enum: ENTROPY,
      required: false
    },
    {
      property: 'morality',
      type: 'enum',
      enum: MORALITY,
      required: false
    },
    {
      property: 'specie',
      type: 'enum',
      enum: SPECIES,
      required: false
    },
    {
      property: 'damage',
      type: 'enum',
      enum: DAMAGE_TYPES,
      required: false
    }
  ],
  required: false
}

/*
type:
  enum : Le champ du formulaire accepter une valeur parmis une énumération.
  integer : Le champ accepte une valeur numérique entière (positive ou négative)
  float : Le champ accepte une valeur numérique flottante
  string : Le champ accepte une chaine de caractères
  enum[] : Le champ accepte un tableau dont chaque éléments est une valeur choisie parmis une énumération
  depending-on-property : La définition de cette propriété dépend de la valeur d\'une autre propriété.
*/

module.exports = {
  type: 'object',
  properties: [
    {
      property: 'entityType',
      type: 'enum',
      remark: 'Doit absolument être ENTITY_TYPE_ITEM... sinon, ce n\'est pas un item et nous ne sommes pas dans le bon fichier',
      enum: ENTITY_TYPES,
      required: true
    },
    {
      property: 'itemBaseType',
      type: 'enum',
      enum: ITEM_BASE_TYPES,
      required: true
    },
    {
      property: 'itemSubType',
      type: 'enum-depending-on',
      dependency: 'itemBaseType',
      remark: 'les valeurs sélectionnables de la propriété itemSubType dépendent de la valeur de la propriété itemBaseType',
      enum: {
        ITEM_BASE_TYPE_WEAPON: WEAPON_TYPES,
        ITEM_BASE_TYPE_ARMOR: ARMOR_TYPES,
        ITEM_BASE_TYPE_SHIELD: SHIELD_TYPES,
        ITEM_BASE_TYPE_AMMO: AMMO_TYPES
      }
    },
    {
      property: 'properties',
      type: 'collection',
      remark: [
        'Une collection contient plusieurs items',
        'Dans ce cas précis la sélection d\'une valeur pour un item provoque l\'apparition de nouveaux champs'
      ],
      items: {
        ITEM_PROPERTY_ABILITY_MODIFIER: [
          {
            property: 'ability',
            type: 'enum',
            enum: ABILITIES,
            required: true
          },
          {
            property: 'value',
            type: 'integer',
            range: [-20, 20],
            required: true
          }
        ],
        ITEM_PROPERTY_AC_MODIFIER: [
          {
            property: 'value',
            type: 'integer',
            range: [-20, 20],
            required: true
          },
          AGAINST_DEF
        ],
        ITEM_PROPERTY_ATTACK_MODIFIER: [
          {
            property: 'value',
            type: 'integer',
            range: [-20, 20],
            required: true
          },
          AGAINST_DEF
        ],
        ITEM_PROPERTY_CONCEALMENT: [
          {
            property: 'value',
            type: 'float',
            range: [0, 1],
            required: true
          },
          AGAINST_DEF
        ],
        ITEM_PROPERTY_DAMAGE_IMMUNITY: [
          {
            property: 'types',
            type: 'enum[]',
            enum: DAMAGE_TYPES,
            required: true
          },
          {
            property: 'value',
            type: 'float',
            range: [-1, 1],
            required: true
          }
        ],
        ITEM_PROPERTY_DAMAGE_MODIFIER: [
          {
            property: 'value',
            type: 'number|dice',
            range: [-20, 20],
            examples: [
              1,
              5,
              -2,
              '1d6',
              '1d8',
              '2d4+2'
            ],
            required: true
          },
          {
            property: 'type',
            type: 'enum',
            enum: DAMAGE_TYPES,
            required: true
          },
          AGAINST_DEF
        ],
        ITEM_PROPERTY_DAMAGE_RESISTANCE: [
          {
            property: 'types',
            type: 'enum[]',
            enum: DAMAGE_TYPES,
            required: true
          },
          {
            property: 'value',
            type: 'integer',
            range: [1, 50],
            required: true
          }
        ],
        ITEM_PROPERTY_ENHANCEMENT: [
          {
            property: 'value',
            type: 'integer',
            range: [-5, 5],
            required: true
          },
          AGAINST_DEF
        ],
        ITEM_PROPERTY_IMMUNE_CRITICAL: [],
        ITEM_PROPERTY_INCREASE_HITPOINTS: [
          {
            property: 'value',
            type: 'integer',
            range: [1, 1000],
            required: true
          }
        ],
        ITEM_PROPERTY_ON_HIT_POISON: [
          {
            property: 'type',
            type: 'string',
            required: true
          }
        ],
        ITEM_PROPERTY_SAVING_THROW_MODIFIER: [
          {
            property: 'ability',
            type: 'enum',
            enum: ABILITIES,
            required: true
          },
          {
            property: 'type',
            type: 'enum',
            enum: SAVE_TYPES,
            required: true
          },
          {
            property: 'value',
            type: 'integer',
            range: [-20, 20],
            required: true
          }
        ],
        ITEM_PROPERTY_VAMPYRE: [
          {
            property: 'value',
            type: 'integer',
            range: [1, 6],
            required: true
          }
        ],
        ITEM_PROPERTY_WEAPON_RESISTANCE: [
          {
            property: 'value',
            type: 'float',
            range: [0, 1],
            required: true
          }
        ]
      }
    }
  ]
}

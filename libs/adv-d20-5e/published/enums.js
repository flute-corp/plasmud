const CONSTS = Object.values(require('../consts'))
const ABILITIES = CONSTS.filter(c => c.startsWith('ABILITY_'))
const MORALITY = CONSTS.filter(c => c.startsWith('ALIGNMENT_MORALITY_'))
const ENTROPY = CONSTS.filter(c => c.startsWith('ALIGNMENT_ENTROPY_'))
const SPECIES = CONSTS.filter(c => c.startsWith('SPECIE_'))
const ALIGNMENTS = CONSTS.filter(c => c.startsWith('ALIGNMENT_') && !(c.startsWith('ALIGNMENT_MORALITY_') || c.startsWith('ALIGNMENT_ENTROPY_')))
const DAMAGE_TYPES = CONSTS.filter(c => c.startsWith('DAMAGE_TYPE_'))
const SAVE_TYPES = CONSTS.filter(c => c.startsWith('SAVE_TYPE_'))
const ENTITY_TYPES = Object.values(require('../consts/entity-types'))
const ITEM_BASE_TYPES = Object.values(require('../consts/item-base-types'))
const WEAPON_TYPES = Object.values(require('../consts/weapon-types'))
const ARMOR_TYPES = Object.values(require('../consts/armor-types'))
const AMMO_TYPES = Object.values(require('../consts/ammo-types'))
const SHIELD_TYPES = Object.values(require('../consts/shield-types'))
const GENDERS = Object.values(require('../consts/genders'))
const SIZES = Object.values(require('../consts/sizes'))
const FEATS = Object.values(require('../consts/feats'))
const SKILLS = Object.values(require('../consts/skills'))
const CLASSES = Object.keys(require('../data/classes.json'))
const PROFICIENCIES = Object.values(require('../consts/proficiencies'))
const EQUIPMENT_SLOTS = CONSTS.filter(c => c.startsWith('EQUIPMENT_SLOT_'))

module.exports = {
  ABILITIES,
  MORALITY,
  ENTROPY,
  SPECIES,
  DAMAGE_TYPES,
  SAVE_TYPES,
  ENTITY_TYPES,
  ITEM_BASE_TYPES,
  WEAPON_TYPES,
  ARMOR_TYPES,
  AMMO_TYPES,
  SHIELD_TYPES,
  GENDERS,
  SIZES,
  FEATS,
  SKILLS,
  CLASSES,
  PROFICIENCIES,
  ALIGNMENTS,
  EQUIPMENT_SLOTS
}

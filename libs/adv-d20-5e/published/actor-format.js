const {
  ABILITIES,
  MORALITY,
  ENTROPY,
  SPECIES,
  DAMAGE_TYPES,
  SAVE_TYPES,
  ENTITY_TYPES,
  GENDERS,
  SIZES,
  FEATS,
  CLASSES,
  PROFICIENCIES,
  ALIGNMENTS,
  EQUIPMENT_SLOTS
} = require('./enums')

module.exports = {
  type: 'object',
  properties: [
    {
      property: 'entityType',
      type: 'enum',
      remark: 'Doit absolument être ENTITY_TYPE_ACTOR... sinon, ce n\'est pas un actor et nous ne sommes pas dans le bon fichier',
      enum: ENTITY_TYPES,
      required: true
    },
    {
      property: 'specie',
      type: 'enum',
      enum: SPECIES,
      required: true
    },
    {
      property: 'gender',
      type: 'enum',
      enum: GENDERS,
      required: true,
      default: 'GENDER_NONE'
    },
    {
      property: 'size',
      type: 'enum',
      enum: SIZES,
      required: true,
      default: 'CREATURE_SIZE_MEDIUM'
    },
    {
      property: 'speed',
      type: 'integer',
      range: [0, 100],
      required: true,
      default: 30
    },
    {
      property: 'ac',
      type: 'integer',
      range: [10, 20],
      required: true,
      default: 10
    },
    {
      property: 'feats',
      type: 'enum[]',
      enum: FEATS,
      required: false
    },
    {
      property: 'skills',
      type: 'array',
      items: {
        type: 'object',
        properties: [
          {
            property: 'skill',
            type: 'enum',
            enum: SKILLS,
            required: true
          },
          {
            property: 'value',
            type: 'number',
            range: [-20, 20],
            required: true
          }
        ]
      }
    },
    {
      property: 'classes',
      type: 'array',
      items: {
        type: 'object',
        properties: [
          {
            property: 'class',
            type: 'enum',
            enum: CLASSES,
            required: true
          },
          {
            property: 'level',
            type: 'number',
            range: [1, 20],
            required: true
          }
        ]
      }
    },
    {
      property: 'abilities',
      type: 'array',
      items: {
        type: 'object',
        properties: [
          {
            property: 'ability',
            type: 'enum',
            enum: ABILITIES,
            required: true
          },
          {
            property: 'value',
            type: 'number',
            range: [3, 25],
            required: true
          }
        ]
      }
    },
    {
      property: 'proficiencies',
      type: 'enum[]',
      enum: PROFICIENCIES,
      required: false
    },
    {
      property: 'alignment',
      type: 'enum',
      enum: ALIGNMENTS,
      required: false
    },
    {
      property: 'equipment',
      type: 'array',
      items: {
        type: 'object',
        properties: [
          {
            property: 'slot',
            type: 'enum',
            enum: EQUIPMENT_SLOTS,
            required: true
          },
          {
            property: 'item',
            type: 'string',
            required: true,
            remark: 'référence d\'un item equipable'
          }
        ]
      }
    }
  ]
}

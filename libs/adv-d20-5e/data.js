// ./data (generated)
module.exports = {
  ADV_DIS_REPOSITORY: require('./data/adv-dis-repository.json'),
  ADVANTAGES: require('./data/advantages.json'),
  AMMO_TYPES: require('./data/ammo-types.json'),
  ARMOR_TYPES: require('./data/armor-types.json'),
  CLASSES: require('./data/classes.json'),
  DISADVANTAGES: require('./data/disadvantages.json'),
  FEATS: require('./data/feats.json'),
  ITEM_BASE_TYPES: require('./data/item-base-types.json'),
  SHIELD_TYPES: require('./data/shield-types.json'),
  SIZES: require('./data/sizes.json'),
  SKILLS: require('./data/skills.json'),
  SPELLS: require('./data/spells.json'),
  VARIABLES: require('./data/variables.json'),
  WEAPON_TYPES: require('./data/weapon-types.json')
}

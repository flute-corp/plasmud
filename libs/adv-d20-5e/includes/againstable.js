const CONST = require('../consts')

function main (against) {
  const oDefault = {
    morality: CONST.ALIGNMENT_MORALITY_ANY,
    entropy: CONST.ALIGNMENT_ENTROPY_ANY,
    damage: CONST.DAMAGE_TYPE_ANY,
    specie: CONST.SPECIE_ANY
  }
  return against ? {
    against: {
      ...oDefault,
      ...against
    }
  } : {
    against: {
      ...oDefault
    }
  }
}

module.exports = main

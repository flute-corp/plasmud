const EventManager = require('events')
const CONST = require('./consts')
const BlueprintFactory = require('./BlueprintFactory')
const ItemPropertyFactory = require('./ItemPropertyFactory')
const EffectProcessor = require('./EffectProcessor')
const ERRORS = require('./errors')
const Dice = require('./includes/Dice')
const DATA = require('./data')
const ScriptManager = require('./ScriptManager')

/**
 * @typedef ItemProperty {object}
 * @property property {string} identifiant de l'item property
 * @property value {number|string} valeur numérique ou chaîne
 *
 * @typedef EffectState {object}
 * @property source {EntityState}
 * @property target {EntityState}
 * @property effect {string}
 * @property amp {number}
 * @property duration {number}
 * @property elapsed {number}
 * @property data {object}
 * @property terminated {boolean}
 *
 * @typedef EntityState {object} Structure de mémorisation des entité de l'ADV20
 * @property ref {string} nom du blueprint ayant servit à créé le entity state
 * @property entityType {string} type de l'entity ENTITY_TYPE_*
 * @property itemBaseType {string} type de base de l'item ITEM_BASE_TYPE_*
 * @property itemSubType {string} item: type d'armure, de bouclier, ou d'arme
 * @property properties {ItemProperty[]} item: liste des propriété de l'entité
 * @property effects {array} item & creature: liste des effets appliqués à l'item
 * @property combatStyle {string} creature: style de combat actuellement adopté
 * @property classes {string[]} creature: liste des classes prises par la creature
 * @property abilities {object} creature: liste des attribut de la creature
 * @property specie {string} creature : specie de la creature
 * @property entropy {number} creature : entropie de la créature entre -1 (chaos) et +1 (loi)
 * @property morality {string} creature : moralité de la créature entre -1 (mal) et +1 (bien)
 * @property feats {string[]} creature : liste des dons
 * @property gender {string} creature: GENDER_* const
 * @property proficiencies {string[]} creature : liste de PROFICIENCY_*
 * @property proficiency {string} item : PROFICIENCY_*
 * @property specie {string} creature : specie SPECIE_*
 * @property size {number} creature : taille
 * @property hp {number} creature : nombre de pv actuel
 * @property ac {number} creature : classe d'armure naturelle (peau, écailles, cuir)
 * @property attackCount {number} creature : nombre d'attaque effectuée ce round
 * @property encumbrance {number} : creature : masse totale de l'équipement et des objet de l'inventaire
 * @property getLast {object} : flags de mémoire des dernières actions réalisée
 *
 * @typedef ItemData {object}
 * @property cost {number} cout indicatif
 * @property ac {number} classe d'armure dans le cas d'une armure ou d'un bouclier
 * @property category {string} catégory supplémentaire de l'item
 * @property maxDexterityModifier {number} limitateur de bonus de dexterité
 * @property requiredStrength {number} force requise pour equipper l'objet sans désavantage
 * @property stealth {string} avantage ou désavantage de furtivité à porter cet item
 * @property weigth {number} poids indicatif
 * @property damage {string} formule de calcul des dégats
 * @property damageVersatile {string|null} formule de calcul quand l'arme est prise à deux mains
 * @property damageType {string} type de déga délivré par l'arme
 * @property ranged {boolean} pour les arme : arme à distance
 * @property attributes {string[]} liste des attributs de l'arme
 *
 * @typedef AgainstStruct {object}
 * @property entropy {string} alignement entropique (CONST.ALIGNMENT_ENTROPY_*)
 * @property morality {string} alignement moral (CONST.ALIGNMENT_MORALITY_*)
 * @property specie {string} type racial (CONST.SPECIE_*)
 * @property damage {string} type de dégât (CONST.DAMAGE_TYPE_*)
 *
 * @typedef AggressionStruct {object}
 * @property alignment {string} alignement (CONST.ALIGNMENT_*)
 * @property specie {string} type racial (CONST.SPECIE_*)
 * @property damage {string} type de dégât (CONST.DAMAGE_TYPE_*)
 *
 * @typedef DamageStruct {object}
 * @property diceCount {number}
 * @property diceSides {number}
 * @property critThreatMin {number}
 * @property critThreatMax {number}
 * @property critMult {number}
 * @property damTypes {string[]}
 *
 *
 * @typedef DamageOutput {object}
 * @property amount {number}
 * @property types {string[]}
 *
 * @typedef AttackOutcomeBonus {object}
 * @property ability {number} attack bonus provided by ability
 * @property weapon {number} attack bonus provided by weapon
 * @property proficiency {number} attack bonus provided by proficiency
 *
 * @typedef AttackOutcome {object}
 * @property roll {number} base roll (1-20)
 * @property bonus {number} attack bonuses sum
 * @property bonusDetail {AttackOutcomeBonus} attack bonuses details
 * @property advantage {boolean}
 * @property disadvantage {boolean}
 * @property advantages {string[]}
 * @property disadvantages {string[]}
 * @property attack {number} final attack roll value
 * @property ability {string} ability being used during attack
 * @property critical {boolean} critical hit
 * @property hit {boolean} if true then target has been hit
 * @property miss {string} if not hit then explains why : CONST.ATTACK_MISS_*
 * @property weapon {EntityState} weapon used during this attack
 * @property ammo {EntityState} ammo used during this attack
 * @property spell {string} identifiant du sort qui a déclenché cette attaque
 * @property damages {DamageOutput[]} dégat infligés par types de dégat
 * @property damageSources {DamageOutput[]} dégat infligés par sources de dégat
 * @property ac {number} valeur de la classe d'armure à percer
 * @property effects {array}
 * @property distance {number} distance beetween target and subject
 *
 * @typedef SpellContext {object}
 * @property fail {boolean} indicates whereas the spell failed or not
 * @property cause {string} description of what caused the spell to fail
 * @property id {string} spell identifier
 * @property subject {EntityState} the actor casting the spell
 * @property target {EntityState} the spell primary target
 * @property ability {string} ability involved in the casting process (saving throw, attack throw...)
 * @property data {object} original blueprint data
 * @property dc {number} spell difficulty class
 * @property attack {AttackOutcome} if the spell requires an attack throw : this is the attack outcome
 * @property level {number} level at which this spell is cast
 */

/**
 * @class Rules
 */
class Rules {
  constructor () {
    this._blueprintFactory = new BlueprintFactory()
    this._itemPropertyFactory = new ItemPropertyFactory()
    this._effectProcessor = new EffectProcessor()
    this._dice = new Dice()
    this._lastEntityId = 0
    this._scriptManager = new ScriptManager(this)
    this._events = new EventManager()
    this._entityRegistry = {}
  }

  getNextId () {
    const n = ++this._lastEntityId
    return 'adventity#' + n.toString()
  }

  get blueprintFactory () {
    return this._blueprintFactory
  }

  get entities () {
    return this._entityRegistry
  }

  get DATA () {
    return DATA
  }

  get CONST () {
    return CONST
  }

  get events () {
    return this._events
  }

  get dice () {
    return this._dice
  }

  get blueprints () {
    return this._blueprintFactory.getBlueprints()
  }

  get scripts () {
    return this._scriptManager.routes
  }

  get scriptContext () {
    return this._scriptManager.defaultContext
  }

  get effectProcessor () {
    return this._effectProcessor
  }

  getPropertyEnumValues (sProperty) {
    switch (sProperty) {
      case 'entityType': {
        return [
          this.CONST.ENTITY_TYPE_ACTOR,
          this.CONST.ENTITY_TYPE_ITEM
        ]
      }
      case 'itemBaseType': {
        return [

        ]
      }
    }
  }

  // section : technique
  //  _            _
  // | |_ ___  ___| |__
  // | __/ _ \/ __| '_ \
  // | ||  __/ (__| | | |
  //  \__\___|\___|_| |_|
  //
  // fonctions techniques

  runScript (sScript, context) {
    return this._scriptManager.runScriptSync(sScript, context)
  }

  /**
   * Renvoie le blueprint correspondant à la resref spécifiée
   * @param resref {string} resource reference
   * @returns {object}
   */
  getBlueprint (resref) {
    return this._blueprintFactory.getBlueprint(resref)
  }

  isBlueprintExists (resref) {
    return resref in this.blueprintFactory.getBlueprints()
  }

  // section : check types
  //        _             _      _
  //    ___| | _____  ___| | __ | |_ _   _ _ __   ___  ___
  //   / __| |/ / _ \/ __| |/ / | __| | | | '_ \ / _ \/ __|
  //  | (__|   <  __/ (__|   <  | |_| |_| | |_) |  __/\__ \
  //   \___|_|\_\___|\___|_|\_\  \__|\__, | .__/ \___||___/
  //                                 |___/|_|
  // fonctions de verification des types d'entité ou d'objets

  /**
   * Renvoie le type de l'entity
   * cale peut etre l'une des constante ENTITY_TYPE_*
   * nommément ENTITY_TYPE_ACTOR ou ENTITY_TYPE_ITEM
   * @param oEntity {EntityState}
   * @returns {string} ENTITY_TYPE_*
   */
  getEntityType (oEntity) {
    return oEntity.entityType
  }

  /**
   * Indique si la structure EntityState spécifiée fait référence à un objet.
   * @param oEntity {EntityState}
   * @returns {boolean}
   */
  isItem (oEntity) {
    return oEntity &&
      this.getEntityType(oEntity) === CONST.ENTITY_TYPE_ITEM &&
      oEntity.itemBaseType !== undefined
  }

  /**
   * Indique si la structure EntityState spécifiée fait référence à une créature.
   * @param oEntity {EntityState}
   * @returns {boolean}
   */
  isActor (oEntity) {
    return oEntity && (this.getEntityType(oEntity) === 'player' || this.getEntityType(oEntity) === CONST.ENTITY_TYPE_ACTOR)
  }

  /**
   * Renvoie true si l'entité spécifiée est une arme
   * @param oEntity {EntityState}
   * @return {boolean}
   */
  isWeapon (oEntity) {
    return oEntity &&
      this.getEntityType(oEntity) === CONST.ENTITY_TYPE_ITEM &&
      oEntity.itemBaseType === CONST.ITEM_BASE_TYPE_WEAPON
  }

  /**
   * Renvoie true si l'entité spécifiée est un bouclier
   * @param oEntity {EntityState}
   * @return {boolean}
   */
  isShield (oEntity) {
    return oEntity &&
      this.getEntityType(oEntity) === CONST.ENTITY_TYPE_ITEM &&
      oEntity.itemBaseType === CONST.ITEM_BASE_TYPE_SHIELD
  }

  /**
   * verifie que l'entité spécifiée soit bien un objet equippable, sinon : provoque une erreur
   * @param oEntity {EntityState}
   */
  checkItem (oEntity) {
    if (!this.isItem(oEntity)) {
      if (!oEntity) {
        throw new Error('This entity is null or undefined')
      }
      const sNature = this.getEntityType(oEntity)
      throw new Error('Entity is not a valid item. It is a "' + sNature + '" of base type "' + oEntity.itemBaseType + '"')
    }
  }

  /**
   * verifie que l'entité spécifiée soit bien un acteur, sinon : provoque une erreur
   * @param oEntity {EntityState}
   */
  checkActor (oEntity) {
    if (!this.isActor(oEntity)) {
      throw new ERRORS.BadEntityTypeError(oEntity, CONST.ENTITY_TYPE_ACTOR)
    }
  }

  /**
   * Vérifie que l'entité spécifée est bien un item d'un certain type
   * provoque une erreur sinon.
   * @param oEntity {EntityState}
   * @param sItemBaseType {string} ITEM_BASE_TYPE_*
   */
  checkItemBaseType (oEntity, sItemBaseType) {
    this.checkItem(oEntity)
    const ibt = this.getEntityType(oEntity)
    if (ibt !== sItemBaseType) {
      throw new Error(`bad item type : "${ibt}" - expected : "${sItemBaseType}"`)
    }
  }

  /**
   * Calcul du bonus
   * @param nAbility {number} valeur de la caractéristique
   * @returns {number}
   */
  computeAbilityBonus (nAbility) {
    return Math.floor((nAbility - 10) / 2)
  }

  // section : entity creation
  //             _   _ _                               _   _
  //   ___ _ __ | |_(_) |_ _   _    ___ _ __ ___  __ _| |_(_) ___  _ __
  //  / _ \ '_ \| __| | __| | | |  / __| '__/ _ \/ _` | __| |/ _ \| '_ \
  // |  __/ | | | |_| | |_| |_| | | (__| | |  __/ (_| | |_| | (_) | | | |
  //  \___|_| |_|\__|_|\__|\__, |  \___|_|  \___|\__,_|\__|_|\___/|_| |_|
  //                       |___/
  // creation des entités item et actor

  /**
   * Construit un Item et l'integre au store
   * @param resref {string} identifiant de la ressource permettant de créer l'item
   * @param [id] {string} identifiant de l'entité
   * @return {EntityState}
   */
  _createEntityItem (resref, id = '') {
    const bp = this.getBlueprint(resref)
    if (bp.entityType !== CONST.ENTITY_TYPE_ITEM) {
      throw new Error('this blueprint is not an item : "' + resref + '"')
    }
    const oItemBaseData = this.DATA.ITEM_BASE_TYPES[bp.itemBaseType]
    const extraData = 'dataType' in oItemBaseData
      ? this.DATA[oItemBaseData.dataType][bp.itemSubType]
      : {}
    if (id === '') {
      id = this.getNextId()
    }
    const oItem = {
      id,
      ref: resref,
      entityType: CONST.ENTITY_TYPE_ITEM,
      itemBaseType: bp.itemBaseType,
      itemSubType: bp.itemSubType,
      properties: [],
      effects: [],
      weight: oItemBaseData.weight,
      attributes: [],
      damage: bp.damage,
      damageType: bp.damageType,
      ...extraData
    }
    bp.properties.forEach(payload => {
      const ip = this.createItemProperty(payload.property, payload)
      this.addItemProperty(oItem, ip)
    })
    this.registerEntity(oItem)
    return oItem
  }

  /**
   * Construit l'état d'une creature
   * @param resref {string} identifiant de la ressource permettant de créer l'acteur
   * @param [id] {string} identifiant de l'entité
   * @returns {EntityState}
   */
  _createEntityActor (resref, id = '') {
    const bp = this.getBlueprint(resref)
    if (bp.entityType !== CONST.ENTITY_TYPE_ACTOR) {
      throw new Error('this blueprint is not an actor : "' + resref + '"')
    }
    if (id === '') {
      id = this.getNextId()
    }
    const oActor = {
      id,
      ref: resref,
      entityType: CONST.ENTITY_TYPE_ACTOR,
      effects: [],
      combatStyle: null,
      classes: {},
      abilities: {
        [CONST.ABILITY_STRENGTH]: 10,
        [CONST.ABILITY_CONSTITUTION]: 10,
        [CONST.ABILITY_DEXTERITY]: 10,
        [CONST.ABILITY_INTELLIGENCE]: 10,
        [CONST.ABILITY_WISDOM]: 10,
        [CONST.ABILITY_CHARISMA]: 10
      },
      proficiencies: [],
      skills: {},
      equip: {
        [CONST.EQUIPMENT_SLOT_WEAPON_MELEE]: null,
        [CONST.EQUIPMENT_SLOT_WEAPON_2ND_MELEE]: null,
        [CONST.EQUIPMENT_SLOT_WEAPON_RANGED]: null,
        [CONST.EQUIPMENT_SLOT_SHIELD]: null,
        [CONST.EQUIPMENT_SLOT_RIGHTCHARM]: null,
        [CONST.EQUIPMENT_SLOT_LEFTCHARM]: null,
        [CONST.EQUIPMENT_SLOT_RIGHTRING]: null,
        [CONST.EQUIPMENT_SLOT_LEFTRING]: null,
        [CONST.EQUIPMENT_SLOT_HEAD]: null,
        [CONST.EQUIPMENT_SLOT_NECK]: null,
        [CONST.EQUIPMENT_SLOT_CHEST]: null,
        [CONST.EQUIPMENT_SLOT_CLOAK]: null,
        [CONST.EQUIPMENT_SLOT_ARMS]: null,
        [CONST.EQUIPMENT_SLOT_BELT]: null,
        [CONST.EQUIPMENT_SLOT_AMMO]: null,
        [CONST.EQUIPMENT_SLOT_BOOTS]: null,
        [CONST.EQUIPMENT_SLOT_CARMOR]: null,
        [CONST.EQUIPMENT_SLOT_CWEAPON_B]: null,
        [CONST.EQUIPMENT_SLOT_CWEAPON_L]: null,
        [CONST.EQUIPMENT_SLOT_CWEAPON_R]: null
      },
      entropy: 0,
      morality: 0,
      feats: [],
      gender: bp.gender,
      specie: bp.specie,
      size: bp.size,
      hp: 0,
      ac: bp.ac,
      speed: bp.speed,
      attackCount: 0,
      encumbrance: 0,
      getLast: {
        weaponUsed: null,
        attacker: null,
        spellContext: null
      }
    }
    this.initAlignment(oActor, bp.alignment)
    bp.classes.forEach(a => {
      this.addClass(oActor, a.class, a.level)
    })
    bp.abilities.forEach(a => {
      oActor.abilities[a.ability] = a.value
    })
    bp.proficiencies.forEach(p => {
      oActor.proficiencies.push(p)
    })
    bp.skills.forEach(a => {
      oActor.skills[a.skill] = a.value
    })
    bp.feats.forEach(a => {
      oActor.feats.push(a)
    })
    this.registerEntity(oActor)
    if (bp.equipment && bp.equipment.length > 0) {
      const aEquipmentCopy = bp.equipment.slice(0)
      const oEvent = {
        entity: oActor,
        equipment: aEquipmentCopy
      }
      this._events.emit('entity.equipment', oEvent)
      aEquipmentCopy.forEach(a => {
        const oItem = this.createEntity(a.item)
        this.equipItem(oActor, oItem, a.slot)
      })
    }
    oActor.hp = this.getMaxHitPoints(oActor)
    return oActor
  }

  registerEntity (oEntity) {
    if (oEntity.id in this._entityRegistry) {
      throw new Error('this entity id is already registered')
    } else {
      this._entityRegistry[oEntity.id] = oEntity
      this.events.emit('entity.created', { entity: oEntity })
    }
  }

  /**
   * Creation d'une entité
   * @param resref {string} identifiant du blueprint servant a creer l'entité
   * @param id {string} identifiant de l'entité
   * @returns {EntityState}
   */
  createEntity (resref, id = '') {
    const bp = this.getBlueprint(resref)
    let oEntity
    switch (bp.entityType) {
      case CONST.ENTITY_TYPE_ACTOR:
        oEntity = this._createEntityActor(resref, id)
        break

      case CONST.ENTITY_TYPE_ITEM:
        oEntity = this._createEntityItem(resref, id)
        break

      default:
        throw new ERRORS.KeyNotFoundError(resref, 'blueprints')
    }
    return oEntity
  }

  /**
   * Renvoie true si l'item est equippé par l'actor
   * @param oItem {EntityState}
   * @param oActor {EntityState}
   * @return {boolean}
   */
  isItemEquippedOnActor (oItem, oActor) {
    // quel sont les slots utilisé oar l'item ?
    this.checkItem(oItem)
    const { equipSlots } = this.getItemData(oItem.ref)
    return equipSlots.some(es => this.getEquippedItem(oActor, es) === oItem)
  }

  /**
   * Détruire toutes les entité-items equippée par l'entité-actor
   * @param oEntity {EntityState}
   */
  destroyEntity (oEntity) {
    if (this.isActor(oEntity)) {
      for (const item of Object.values(oEntity.equip)) {
        this.destroyEntity(item)
      }
    }
    // détruire l'entité si elle est équipée
    // Rechercher l'entité dans l'inventaire des actors

    if (this.isItem(oEntity)) {
      const oOwnerFound = Object
        .values(this.entities)
        .find(e => this.isActor(e) && this.isItemEquippedOnActor(oEntity, e))
      if (oOwnerFound) {
        this.removeEquippedItem(oOwnerFound, oEntity)
      }
    }
    delete this._entityRegistry[oEntity.id]
    this.events.emit('entity.destroyed', { entity: oEntity })
  }

  // section : entity data
  //             _   _ _               _       _
  //   ___ _ __ | |_(_) |_ _   _    __| | __ _| |_ __ _
  //  / _ \ '_ \| __| | __| | | |  / _` |/ _` | __/ _` |
  // |  __/ | | | |_| | |_| |_| | | (_| | (_| | || (_| |
  //  \___|_| |_|\__|_|\__|\__, |  \__,_|\__,_|\__\__,_|
  //                       |___/
  // obtention d'information de l'état des entités

  /**
   * Déterminer le bonus associé à une abilité
   * @param oEntity {EntityState} entité concernée
   * @param sAbility {string} nom de l'abilité
   */
  getAbility (oEntity, sAbility) {
    this.checkActor(oEntity)
    const nAbility = oEntity.abilities[sAbility]
    const nEffMod = this
      .getEquipmentActiveEffects(oEntity)
      .filter(eff => eff.tag === CONST.EFFECT_ABILITY_MODIFIER &&
        eff.ability === sAbility
      )
      .reduce((prev, curr) => prev + curr.amp, 0)
    const nIPMod = this
      .getEquipmentItemProperties(oEntity)
      .filter(ip => ip.property === CONST.ITEM_PROPERTY_ABILITY_MODIFIER &&
        ip.ability === sAbility
      )
      .reduce((prev, curr) => prev + curr.value, 0)
    return nAbility + nEffMod + nIPMod
  }

  /**
   * Renvoie la valeur cumulée du bonus de l'ability spécifié
   * @param oEntity {EntityState}
   * @param sAbility {string}
   * @return {number}
   */
  getAbilityBonus (oEntity, sAbility) {
    return this.computeAbilityBonus(this.getAbility(oEntity, sAbility))
  }

  /**
   * Renvoie le niveau effectif de la creature
   * @param oActor {EntityState}
   * @returns {number}
   */
  getLevel (oActor) {
    this.checkActor(oActor)
    return Object.values(oActor.classes).reduce((prev, curr) => prev + curr, 0)
  }

  /**
   * Renvoie le Hit Die (nombre de point de vie par niveau d'une classe
   * @param sClass {string} CLASS_*
   * @param sSize {string} CREATURE_SIZE_*
   * @return {number}
   */
  getClassHitDie (sClass, sSize) {
    return sClass === CONST.CLASS_MONSTER
      ? this.DATA.SIZES[sSize].hitDie
      : this.DATA.CLASSES[sClass].hitDie
  }

  /**
   * Renvoie le nombre maximum de HP qu'une entité peut avoir
   * @param oActor {EntityState}
   * @return {number}
   */
  getMaxHitPoints (oActor) {
    this.checkActor(oActor)
    let nHP = 0
    const sSize = oActor.size // ca peut servir surtout pour les monstres
    for (const [sClass, nLevel] of Object.entries(oActor.classes)) {
      nHP += this.getClassHitDie(sClass, sSize) * nLevel
    }
    // ajouter les modifiers
    const nBonusHP = this.aggregateModifiers(oActor, [
      CONST.EFFECT_TEMPORARY_HITPOINTS,
      CONST.ITEM_PROPERTY_INCREASE_HITPOINTS
    ]).sum
    return nHP + nBonusHP
  }

  /**
   * Change les point de vie courant de la créature spécifiée
   * Si les points de vie de la creature tombe en dessous de zéro...
   * TODO: système d'évènement comme 'death', 'revive' etc...
   * @param oActor {EntityState}
   * @param nDelta {number}
   */
  modifyHitPoints (oActor, nDelta) {
    oActor.hp = Math.max(0, Math.min(this.getMaxHitPoints(oActor), oActor.hp + nDelta))
  }

  /**
   * Renvoie le proficiency bonus de la créature, basé sur son niveau
   * Sert pour les attack roll, save et skillcheck
   * @param oActor {EntityState}
   * @return {number}
   */
  getProficiencyBonus (oActor) {
    return Math.floor((this.getLevel(oActor) - 1) / 4) + 2
  }

  /**
   * Renvoie la proficiency necessaire pour utiliser correctement l'item
   * renvoie '' vide si l'item n'a pas besoin de proficiency
   * @param oItem {EntityState}
   * @returns {string}
   */
  getItemRequiredProficiency (oItem) {
    this.checkItem(oItem)
    return 'proficiency' in oItem ? oItem.proficiency : ''
  }

  /**
   * Indique si la créature est capable d'utiliser correctement l'objet spécifié
   * @param oActor {EntityState}
   * @param oItem {EntityState}
   * @return {boolean}
   */
  isProficient (oActor, oItem) {
    this.checkActor(oActor)
    const sProficiency = oItem
      ? this.getItemRequiredProficiency(oItem)
      : ''
    return sProficiency === '' || oActor.proficiencies.includes(sProficiency)
  }

  // section : item properties
  //  _ _                                                   _   _
  // (_) |_ ___ _ __ ___    _ __  _ __ ___  _ __   ___ _ __| |_(_) ___  ___
  // | | __/ _ \ '_ ` _ \  | '_ \| '__/ _ \| '_ \ / _ \ '__| __| |/ _ \/ __|
  // | | ||  __/ | | | | | | |_) | | | (_) | |_) |  __/ |  | |_| |  __/\__ \
  // |_|\__\___|_| |_| |_| | .__/|_|  \___/| .__/ \___|_|   \__|_|\___||___/
  //                       |_|             |_|
  // gestion des items properties (creation, ajout, lecture)

  /**
   * ajoute une propriété à un item
   * @param item {EntityState} item auquel on ajoute une propriété
   * @param property {ItemProperty} propriété à ajouter
   */
  addItemProperty (item, property) {
    item.properties.push(property)
  }

  /**
   * supprime la propriété spécifié d'un item
   * @param item {EntityState} item auquel on supprime une propriété
   * @param property {ItemProperty} propriété à supprimer
   */
  removeItemProperty (item, property) {
    const iProp = item.properties.indexOf(property)
    if (iProp >= 0) {
      item.properties.splice(iProp, 1)
    }
  }

  /**
   * Creation d'une item property
   * @param sProperty {string} identifiant ITEM_PROPERTY_*
   * @param payload {object} donnée concernant l'item property
   * @returns {ItemProperty} nouvelle propriété d'item
   */
  createItemProperty (sProperty, payload) {
    return this._itemPropertyFactory.create(sProperty, payload)
  }

  /**
   * Renvoie les item properties d'un item
   * @param oEntity {EntityState} item
   * @returns {ItemProperty[]}
   */
  getItemProperties (oEntity) {
    this.checkItem(oEntity)
    if ('properties' in oEntity) {
      return oEntity.properties
    }
    return []
  }

  /**
   * Renvoie les item properties des items equippé par la creature spécifiée
   * @param oEntity {EntityState} créature
   * @returns {ItemProperty[]} liste des items properties
   */
  getEquipmentItemProperties (oEntity) {
    const aItemProperties = []
    Object
      .values(oEntity.equip)
      .filter(oItem => !!oItem)
      .forEach(oItem => {
        aItemProperties.push(...this.getItemProperties(oItem))
      })
    return aItemProperties
  }

  // section : actor methods
  //     _        _                              _   _               _
  //    / \   ___| |_ ___  _ __   _ __ ___   ___| |_| |__   ___   __| |___
  //   / _ \ / __| __/ _ \| '__| | '_ ` _ \ / _ \ __| '_ \ / _ \ / _` / __|
  //  / ___ \ (__| || (_) | |    | | | | | |  __/ |_| | | | (_) | (_| \__ \
  // /_/   \_\___|\__\___/|_|    |_| |_| |_|\___|\__|_| |_|\___/ \__,_|___/
  // methodes de mutation des actors

  /**
   *
   * @param oActor {EntityState}
   * @param sClass {string}
   * @param nLevel {number}
   */
  addClass (oActor, sClass, nLevel = 1) {
    this.checkActor(oActor)
    if (sClass in oActor.classes) {
      oActor.classes[sClass] += nLevel
    } else {
      oActor.classes[sClass] = nLevel
    }
  }

  // section : effects
  //  _____  __  __           _
  // | ____|/ _|/ _| ___  ___| |_ ___
  // |  _| | |_| |_ / _ \/ __| __/ __|
  // | |___|  _|  _|  __/ (__| |_\__ \
  // |_____|_| |_|  \___|\___|\__|___/
  // gestion des effets

  /**
   * Proxy vers createEffect
   * @param sEffect {string}
   * @param payload {object}
   * @return {EffectState}
   */
  createEffect (sEffect, payload) {
    return this._effectProcessor.createEffect(sEffect, payload)
  }

  applyEffect (oEffect, oTarget, oSource, nDuration) {
    this._effectProcessor.applyEffect(oEffect, this, oTarget, oSource, nDuration)
    this.events.emit('effect.applied', { effect: oEffect })
    return oEffect
  }

  /**
   * Renvoie la liste des effets actifs sur la créature sp&cifiée
   * @param oEntity {EntityState}
   * @param [pFilter] {function} filtre supplémentaires
   * @return {EffectState[]}
   */
  getActiveEffects (oEntity, pFilter = null) {
    if (pFilter) {
      return oEntity.effects.filter(pFilter)
    } else {
      return oEntity.effects
    }
  }

  /**
   * Renvoie true si l'entité spécifiée possède l'effet
   * @param oEntity {EntityState}
   * @param sEffect {string}
   * @returns {boolean}
   */
  hasEffect (oEntity, sEffect) {
    const ae = this.getActiveEffects(oEntity)
    if (ae.length === 0) {
      return false
    }
    return ae.some(eff => eff.tag === sEffect)
  }

  /**
   * Renvoi la liste des effets actifs d'une entité, incluant les effets dégagés par l'équipement
   * @param oEntity {EntityState}
   * @returns {EffectState[]}
   */
  getEquipmentActiveEffects (oEntity) {
    this.checkActor(oEntity)
    const aItems = Object.values(oEntity.equip).filter(e => !!e)
    const aEffects = []
    aItems.forEach(oItem => {
      aEffects.push(...this.getActiveEffects(oItem))
    })
    return aEffects.concat(this.getActiveEffects(oEntity))
  }

  //  _____            _                            _
  // | ____|__ _ _   _(_)_ __  _ __ ___   ___ _ __ | |_
  // |  _| / _` | | | | | '_ \| '_ ` _ \ / _ \ '_ \| __|
  // | |__| (_| | |_| | | |_) | | | | | |  __/ | | | |_
  // |_____\__, |\__,_|_| .__/|_| |_| |_|\___|_| |_|\__|
  //          |_|       |_|

  unequipItem (actor, slot) {
    this.checkEquipmentSlot(slot)
    this.checkActor(actor)
    const item = actor.equip[slot]
    actor.equip[slot] = null
    this.events.emit('entity.unequip', { entity: actor, item, slot })
  }

  /**
   * Place un item dans le slote d'équippement spécifié
   * @param actor {EntityState} créature qui s'équippe
   * @param [slot] {string} identifiant du slot : EQUIPMENT_SLOT_*
   * @param item {EntityState} item dont la créature s'équippe
   * @returns {EntityState} précédent item equipé
   */
  equipItem (actor, item, slot = undefined) {
    if (slot !== undefined) {
      this.checkEquipmentSlot(slot)
    }
    this.checkActor(actor)
    this.checkItem(item)
    const oItemData = this.getItemData(item.ref)
    if ('equipSlots' in oItemData) {
      const equipSlots = oItemData.equipSlots
      // determiner le slot par defaut si celui ci n'est pas spécifié
      if (!slot && equipSlots.length > 0) {
        slot = equipSlots[0]
      }
      const aSlots = new Set(equipSlots)
      // vérifier si le slot spécifié est ok
      if (aSlots.has(slot)) {
        // vérifier existence d'un objet déja équipé dans le slot
        const oEquippedItem = this.getEquippedItem(actor, slot)
        actor.equip[slot] = item
        this.events.emit('entity.equip', { entity: actor, item, slot })
        return oEquippedItem
      } else {
        throw new Error('Cannot equip base item "' + oItemData.itemBaseType + '" in " slot "' + slot + '". Must be equip in : ' + equipSlots.join(', ') + '.')
      }
    } else {
      throw new Error('Cannot equip base item "' + sItemBase + '" in " slot "' + slot + '" : This base item has no favored equipment slot.')
    }
  }

  /**
   * Renvoie true si le slot existe
   * @param sSlot {string} EQUIPEMENT_SLOT_*
   * @returns {boolean}
   */
  isEquipementSlotValid (sSlot) {
    return sSlot === CONST.EQUIPMENT_SLOT_AMMO ||
      sSlot === CONST.EQUIPMENT_SLOT_ARMS ||
      sSlot === CONST.EQUIPMENT_SLOT_BELT ||
      sSlot === CONST.EQUIPMENT_SLOT_BOOTS ||
      sSlot === CONST.EQUIPMENT_SLOT_CARMOR ||
      sSlot === CONST.EQUIPMENT_SLOT_CHEST ||
      sSlot === CONST.EQUIPMENT_SLOT_CLOAK ||
      sSlot === CONST.EQUIPMENT_SLOT_CWEAPON_B ||
      sSlot === CONST.EQUIPMENT_SLOT_CWEAPON_L ||
      sSlot === CONST.EQUIPMENT_SLOT_CWEAPON_R ||
      sSlot === CONST.EQUIPMENT_SLOT_HEAD ||
      sSlot === CONST.EQUIPMENT_SLOT_LEFTCHARM ||
      sSlot === CONST.EQUIPMENT_SLOT_SHIELD ||
      sSlot === CONST.EQUIPMENT_SLOT_LEFTRING ||
      sSlot === CONST.EQUIPMENT_SLOT_NECK ||
      sSlot === CONST.EQUIPMENT_SLOT_RIGHTCHARM ||
      sSlot === CONST.EQUIPMENT_SLOT_WEAPON_MELEE ||
      sSlot === CONST.EQUIPMENT_SLOT_WEAPON_2ND_MELEE ||
      sSlot === CONST.EQUIPMENT_SLOT_WEAPON_RANGED ||
      sSlot === CONST.EQUIPMENT_SLOT_RIGHTRING
  }

  /**
   * renvoi true si le slot spécifié est un slot d'arme
   * @param sSlot {string}
   * @returns {boolean}
   */
  isEquipmentSlotWeapon (sSlot) {
    return sSlot === CONST.EQUIPMENT_SLOT_AMMO ||
      sSlot === CONST.EQUIPMENT_SLOT_CWEAPON_B ||
      sSlot === CONST.EQUIPMENT_SLOT_CWEAPON_L ||
      sSlot === CONST.EQUIPMENT_SLOT_CWEAPON_R ||
      sSlot === CONST.EQUIPMENT_SLOT_WEAPON_RANGED ||
      sSlot === CONST.EQUIPMENT_SLOT_WEAPON_MELEE ||
      sSlot === CONST.EQUIPMENT_SLOT_WEAPON_2ND_MELEE
  }

  /**
   * verifie si le slot d'equipement spécifié existe; provoque une erreur sinon.
   * @param sSlot {string} EQUIPEMENT_SLOT_*
   */
  checkEquipmentSlot (sSlot) {
    if (!this.isEquipementSlotValid(sSlot)) {
      throw new Error('Invalid equipment slot : "' + sSlot + '"')
    }
  }

  /**
   * Renvoie l'item equippé dans le slot spécifié ou null si rien d'equippé à cet endroit
   * @param oActor {EntityState}
   * @param sSlot {string} EQUIPMENT_SLOT_*
   * @returns {EntityState|null}
   */
  getEquippedItem (oActor, sSlot) {
    this.checkEquipmentSlot(sSlot)
    if (this.isActor(oActor)) {
      return oActor.equip[sSlot] || null
    }
    return null
  }

  removeEquippedItem (oActor, oItem) {
    return this
      .getItemBaseTypeData(oItem.itemBaseType)
      .equipSlots
      .forEach(s => {
        if (this.getEquippedItem(oActor, s) === oItem) {
          this.unequipItem(oActor, s)
        }
      })
  }

  //     _    _ _                                  _
  //    / \  | (_) __ _ _ __  _ __ ___   ___ _ __ | |_
  //   / _ \ | | |/ _` | '_ \| '_ ` _ \ / _ \ '_ \| __|
  //  / ___ \| | | (_| | | | | | | | | |  __/ | | | |_
  // /_/   \_\_|_|\__, |_| |_|_| |_| |_|\___|_| |_|\__|
  //              |___/

  /**
   * Renvoie la valeur d'entropie (valeur sur l'axe loi-chaos) d'un alignement.
   * chaotique-mauvais -> chaotique
   * chaotique-bon -> chaotique
   * loyal neutre -> loyal
   *
   * @param sAlignment {string}
   * @returns {string} CONST.ALIGNMENT_ENTROPY_*
   */
  getAlignmentEntropy (sAlignment) {
    switch (sAlignment) {
      case CONST.ALIGNMENT_LAWFUL_GOOD:
      case CONST.ALIGNMENT_LAWFUL_NEUTRAL:
      case CONST.ALIGNMENT_LAWFUL_EVIL:
        return CONST.ALIGNMENT_ENTROPY_LAWFUL

      case CONST.ALIGNMENT_CHAOTIC_GOOD:
      case CONST.ALIGNMENT_CHAOTIC_NEUTRAL:
      case CONST.ALIGNMENT_CHAOTIC_EVIL:
        return CONST.ALIGNMENT_ENTROPY_CHAOTIC

      case CONST.ALIGNMENT_NEUTRAL_GOOD:
      case CONST.ALIGNMENT_TRUE_NEUTRAL:
      case CONST.ALIGNMENT_NEUTRAL_EVIL:
        return CONST.ALIGNMENT_ENTROPY_NEUTRAL

      default:
        return CONST.ALIGNMENT_ENTROPY_ANY
    }
  }

  /**
   * Renvoie la valeur de moralité d'un alignement
   * @param sAlignment
   * @returns {string} CONST.ALIGNMENT_MORALITY_*
   */
  getAlignmentMorality (sAlignment) {
    switch (sAlignment) {
      case CONST.ALIGNMENT_LAWFUL_GOOD:
      case CONST.ALIGNMENT_CHAOTIC_GOOD:
      case CONST.ALIGNMENT_NEUTRAL_GOOD:
        return CONST.ALIGNMENT_MORALITY_GOOD

      case CONST.ALIGNMENT_LAWFUL_NEUTRAL:
      case CONST.ALIGNMENT_TRUE_NEUTRAL:
      case CONST.ALIGNMENT_CHAOTIC_NEUTRAL:
        return CONST.ALIGNMENT_MORALITY_NEUTRAL

      case CONST.ALIGNMENT_LAWFUL_EVIL:
      case CONST.ALIGNMENT_NEUTRAL_EVIL:
      case CONST.ALIGNMENT_CHAOTIC_EVIL:
        return CONST.ALIGNMENT_MORALITY_EVIL

      default:
        return CONST.ALIGNMENT_MORALITY_ANY
    }
  }

  /**
   * renvoie l'aligment d'une creature
   * @param oActor {EntityState}
   * @returns {string} ALIGNMENT_*
   */
  getAlignment (oActor) {
    this.checkActor(oActor)
    const { morality, entropy } = oActor
    const m = Math.round(1.5 * morality) + 2
    const e = Math.round(1.5 * entropy) + 2
    const x = e * 10 + m
    switch (x) {
      case 30:
      case 31:
      case 40:
      case 41:
        return CONST.ALIGNMENT_CHAOTIC_EVIL

      case 20:
      case 21:
        return CONST.ALIGNMENT_NEUTRAL_EVIL

      case 0:
      case 1:
      case 10:
      case 11:
        return CONST.ALIGNMENT_LAWFUL_EVIL

      case 32:
      case 42:
        return CONST.ALIGNMENT_CHAOTIC_NEUTRAL

      case 22:
        return CONST.ALIGNMENT_TRUE_NEUTRAL

      case 2:
      case 12:
        return CONST.ALIGNMENT_LAWFUL_NEUTRAL

      case 33:
      case 34:
      case 43:
      case 44:
        return CONST.ALIGNMENT_CHAOTIC_GOOD

      case 23:
      case 24:
        return CONST.ALIGNMENT_NEUTRAL_GOOD

      case 3:
      case 4:
      case 13:
      case 14:
        return CONST.ALIGNMENT_LAWFUL_GOOD
    }
  }

  shiftAlignment (oActor, sToward, amount = 0.02) {
    switch (sToward) {
      case CONST.ALIGNMENT_ENTROPY_CHAOTIC:
        oActor.entropy += amount
        break

      case CONST.ALIGNMENT_ENTROPY_LAWFUL:
        oActor.entropy -= amount
        break

      case CONST.ALIGNMENT_ENTROPY_NEUTRAL:
        oActor.entropy -= Math.sign(oActor.entropy) * amount
        break

      case CONST.ALIGNMENT_MORALITY_GOOD:
        oActor.morality += amount
        break

      case CONST.ALIGNMENT_MORALITY_EVIL:
        oActor.morality -= amount
        break

      case CONST.ALIGNMENT_MORALITY_NEUTRAL:
        oActor.morality -= Math.sign(oActor.morality) * amount
        break
    }
    oActor.entropy = Math.max(-1, Math.min(1, oActor.entropy))
    oActor.morality = Math.max(-1, Math.min(1, oActor.morality))
  }

  initAlignment (oActor, sAlignment) {
    switch (this.getAlignmentEntropy(sAlignment)) {
      case CONST.ALIGNMENT_ENTROPY_LAWFUL:
        oActor.entropy = -1
        break

      case CONST.ALIGNMENT_ENTROPY_CHAOTIC:
        oActor.entropy = 1
        break

      case CONST.ALIGNMENT_ENTROPY_NEUTRAL:
        oActor.entropy = 0
        break
    }
    switch (this.getAlignmentMorality(sAlignment)) {
      case CONST.ALIGNMENT_MORALITY_GOOD:
        oActor.morality = 1
        break

      case CONST.ALIGNMENT_MORALITY_EVIL:
        oActor.morality = -1
        break

      case CONST.ALIGNMENT_MORALITY_NEUTRAL:
        oActor.morality = 0
        break
    }
  }

  //     _               _           _            _                   _
  //    / \   __ _  __ _(_)_ __  ___| |_      ___| |_ _ __ _   _  ___| |_
  //   / _ \ / _` |/ _` | | '_ \/ __| __|____/ __| __| '__| | | |/ __| __|
  //  / ___ \ (_| | (_| | | | | \__ \ ||_____\__ \ |_| |  | |_| | (__| |_
  // /_/   \_\__, |\__,_|_|_| |_|___/\__|    |___/\__|_|   \__,_|\___|\__|
  //         |___/

  /**
   * Créée une structure Against à partir de la créature spécifiée, la structure against permettra de filtrer
   * les propriété et les effets qui fonctionne contre la créature spécifiée
   * @param oActor {EntityState}
   * @param [oWeapon] {EntityState} arme impliqué dans l'action
   * @returns {AgainstStruct}
   * @private
   */
  buildAgainstStruct (oActor, oWeapon = undefined) {
    this.checkActor(oActor)
    if (oActor) {
      const damage = oWeapon
        ? oWeapon.damageType
        : CONST.DAMAGE_TYPE_CRUSHING
      const sAlignment = this.getAlignment(oActor)
      return {
        entropy: this.getAlignmentEntropy(sAlignment),
        morality: this.getAlignmentMorality(sAlignment),
        damage,
        specie: oActor.specie
      }
    } else {
      return {
        entropy: CONST.ALIGNMENT_ENTROPY_ANY,
        morality: CONST.ALIGNMENT_MORALITY_ANY,
        damage: CONST.DAMAGE_TYPE_ANY,
        specie: CONST.SPECIE_ANY
      }
    }
  }

  /**
   * Renvoie un indicateur de compatibilité d'alignement.
   * @param e1 {string} entropie 1
   * @param e2 {string} entropie 2
   * @param m1 {string} moralité 1
   * @param m2 {string} moralité 2
   * @returns {boolean}
   * @private
   */
  getAlignmentCompatibility (e1, e2, m1, m2) {
    const bAllDefined = !!e1 && !!e2 && !!m1 && !!m2
    const bEntropyComp = e1 === CONST.ALIGNMENT_ENTROPY_ANY ||
      e2 === CONST.ALIGNMENT_ENTROPY_ANY ||
      e1 === e2
    const bMoralityComp = m1 === CONST.ALIGNMENT_MORALITY_ANY ||
      m2 === CONST.ALIGNMENT_MORALITY_ANY ||
      m1 === m2
    return bAllDefined && bEntropyComp && bMoralityComp
  }

  /**
   * Renvoie un indicateur de compatibilité de type de dégât
   * @param d1 {string}
   * @param d2 {string}
   * @returns {boolean}
   * @private
   */
  getDamageTypeCompatibility (d1, d2) {
    return d1 !== undefined &&
      d2 !== undefined &&
      (
        d1 === CONST.DAMAGE_TYPE_ANY ||
        d2 === CONST.DAMAGE_TYPE_ANY ||
        d1 === d2
      )
  }

  /**
   * Renvoie un indicateur de compatibilité.
   * Les deux species sont compatible si l'une des deux species spécifiée est ANY ou si les deux species sont identique
   * @param r1 {string}
   * @param r2 {string}
   * @returns {boolean}
   * @private
   */
  getSpecieCompatibility (r1, r2) {
    return r1 !== undefined &&
      r2 !== undefined &&
      (
        r1 === CONST.SPECIE_ANY ||
        r2 === CONST.SPECIE_ANY ||
        r1 === r2
      )
  }

  /**
   * Indique si l'effet ou propriété spécifiée peut agir selon la nature de l'attaque
   * Une attaque est caractérisée par une alignment (celui de la créature qui attaque)
   * un specie (celle de la creature qui attaque)
   * un type de dégat (celui de l'arme qui sert à l'attaque)
   * @param oPoE {object}
   * @param oAgainst {AgainstStruct}
   * @returns {boolean}
   * @private
   */
  isItemPropertyOrEffectCompatibleAgainst (oPoE, oAgainst) {
    if (('against' in oPoE) && oPoE.against !== null) {
      const oPoEAgainst = oPoE.against
      const bAlignmentsCompatible = this.getAlignmentCompatibility(oPoEAgainst.entropy, oAgainst.entropy, oPoEAgainst.morality, oAgainst.morality)
      const bDamageTypesCompatible = this.getDamageTypeCompatibility(oPoEAgainst.damage, oAgainst.damage)
      const bSpeciesCompatible = this.getSpecieCompatibility(oPoEAgainst.specie, oAgainst.specie)
      return Boolean(bAlignmentsCompatible && bDamageTypesCompatible && bSpeciesCompatible)
    } else {
      // pas de structure against
      return true
    }
  }

  /**
   * Indique si l'effet ou l'item property ne peut agir que selon certaines attaques
   * Si c'est le cas , la fonction renvoie FALSE
   * si l'effet agit indépendament de la nature de l'attque (alignment, specie, type de dégat)
   * la fonction renvoie true.
   * @param oPoE
   * @returns {boolean}
   */
  isItemPropertyOrEffectCompatibleWithAll (oPoE) {
    if (('against' in oPoE) && oPoE.against !== null) {
      const { entropy, morality, specie, damage } = oPoE.against
      return entropy === CONST.ALIGNMENT_ENTROPY_ANY &&
        morality === CONST.ALIGNMENT_MORALITY_ANY &&
        specie === CONST.SPECIE_ANY &&
        damage === CONST.DAMAGE_TYPE_ANY
    } else {
      // pas de structure against
      return true
    }
  }

  /**
   * Renvoie les données associées à un resref
   * @param resref {string}
   * @returns {*}
   */
  getItemData (resref) {
    const bp = this.getBlueprint(resref)
    const sItemBaseType = bp.itemBaseType
    const sItemSubType = bp.itemSubType
    const d1 = this.getItemBaseTypeData(sItemBaseType)
    const d2 = sItemSubType !== undefined
      ? this.getItemSubTypeData(sItemBaseType, bp.itemSubType)
      : {}
    return {
      itemBaseType: sItemBaseType,
      itemSubType: sItemSubType,
      ...d1,
      ...d2
    }
  }

  getItemBaseTypeData (sItemBaseType) {
    const ibt = this.DATA.ITEM_BASE_TYPES
    if (!(sItemBaseType in ibt)) {
      throw new Error('this item base type does not exist : ' + sItemBaseType)
    }
    return ibt[sItemBaseType]
  }

  getItemSubTypeData (sItemBaseType, sItemSubType) {
    const ibts = this.getItemBaseTypeData(sItemBaseType)
    if ('dataType' in ibts) {
      if (ibts.dataType in this.DATA) {
        const istdt = this.DATA[ibts.dataType]
        if (sItemSubType in istdt) {
          return istdt[sItemSubType]
        } else {
          throw new Error('this item sub type does not exist : ' + sItemSubType + ' in data type ' + ibts.dataType)
        }
      } else {
        throw new Error('this item data type does not exist : ' + ibts.dataType + ' in base type ' + sItemBaseType)
      }
    } else {
      throw new Error('this item base type has no sub type : ' + sItemBaseType)
    }
  }

  /**
   * Renvoie le premier slot d'equipement d'un certain type de base d'item
   * @param sItemBaseType {string}
   * @returns {string}
   */
  getItemBaseTypeSlot (sItemBaseType) {
    return this.getItemBaseTypeData(sItemBaseType).equipSlots[0]
  }

  /**
   * Calcule la classe d'armure d'une créature face à une attaque
   * @param oDefender {EntityState}
   * @param oAttacker {EntityState}
   * @param oWeapon {EntityState}
   * @returns {number}
   */
  getAC (oDefender, oAttacker, oWeapon) {
    const nDexterityBonus = this.getAbilityBonus(oDefender, CONST.ABILITY_DEXTERITY)
    const sArmorSlot = this.getItemBaseTypeSlot(CONST.ITEM_BASE_TYPE_ARMOR)
    const oArmor = this.getEquippedItem(oDefender, sArmorSlot)
    const sShieldSlot = this.getItemBaseTypeSlot(CONST.ITEM_BASE_TYPE_SHIELD)
    const oLeftHand = this.getEquippedItem(oDefender, sShieldSlot)
    const oShield = this.isShield(oLeftHand)
      ? oLeftHand
      : null
    let nArmorDexterityBonusLimit = oArmor ? oArmor.maxDexterityModifier : null
    if (nArmorDexterityBonusLimit === null) {
      nArmorDexterityBonusLimit = Infinity
    }
    const nArmorAC = (oArmor ? oArmor.ac : oDefender.ac) + Math.min(nArmorDexterityBonusLimit, nDexterityBonus)
    const nShieldAC = oShield ? oShield.ac : 0
    const nBaseAC = nArmorAC + nShieldAC
    const oAgainst = oAttacker
      ? this.buildAgainstStruct(oAttacker, oWeapon)
      : null

    const nACBonus = this.aggregateModifiers(oDefender, [
      CONST.ITEM_PROPERTY_AC_MODIFIER,
      CONST.EFFECT_AC_MODIFIER
    ], { against: oAgainst }).sum
    return nBaseAC + nACBonus
  }

  /**
   * Déterminer la capacité maximale de portage d'une creature
   * @param oActor {EntityState}
   * @returns {number} poids maximum transportable
   */
  getCarryingCapacity (oActor) {
    return Math.floor(DATA.SIZES[oActor.size].carryingCapacity * this.getAbility(oActor, CONST.ABILITY_STRENGTH) * DATA.VARIABLES.CARRYING_CAPACITY_FACTOR)
  }

  /**
   * Déterminer l'encombrement d'une creature
   * @param oActor {EntityState}
   * @returns {string} encombrement ENCUMBRANCE_*
   */
  getEncumbrance (oActor) {
    const nEncumbrance = oActor.encumbrance
    const nFullyEncumbred = this.getCarryingCapacity(oActor)
    const nLightlyEncumbred = Math.floor(nFullyEncumbred * DATA.VARIABLES.LIGHTLY_ENCUMBERED_LIMIT)
    const nHeavilyEncumbred = Math.floor(nFullyEncumbred * DATA.VARIABLES.HEAVILY_ENCUMBERED_LIMIT)
    if (nEncumbrance > nFullyEncumbred) {
      return CONST.ENCUMBRANCE_FULL
    }
    if (nEncumbrance > nHeavilyEncumbred) {
      return CONST.ENCUMBRANCE_HEAVY
    }
    if (nEncumbrance > nLightlyEncumbred) {
      return CONST.ENCUMBRANCE_LIGHT
    }
    return CONST.ENCUMBRANCE_NONE
  }

  //     _       _                  _
  //    / \   __| |_   ____ _ _ __ | |_ __ _  __ _  ___
  //   / _ \ / _` \ \ / / _` | '_ \| __/ _` |/ _` |/ _ \
  //  / ___ \ (_| |\ V / (_| | | | | || (_| | (_| |  __/
  // /_/   \_\__,_| \_/ \__,_|_| |_|\__\__,_|\__, |\___|
  //                                         |___/

  /**
   * Déterminer la caractéristique associée à la compétence
   * @param sSkill {string}
   * @return {string}
   */
  getSkillAbility (sSkill) {
    return DATA.SKILLS[sSkill].ability
  }

  /**
   * Trouver les avantages et désavantages qui s'appliquent au jet de dé spécifié
   * Le jet est spécifié par :
   * - un type de jet (save, check, attack)
   * - une caractéristique peut etre déduite de la compétence
   * - de la compétence spécifiée (pour le jet de compétence)
   * @param oActor {EntityState} Acteur qui effectue l'action
   * @param oTarget {EntityState} Cible de l'action
   * @param sRollType {string} type de jet de dé : ROLL_TYPE_* (sauvegarde, attaque, compétence)
   * @param sAbility {string} caractéristique en jeu pour le jet de dé
   * @param oExtra {{skill: string, saveType: string, spell: object, weapon: EntityState}}
   * @return {{ advantages: string[], disadvantages: string[] }}
   */
  getRollCircumstances (oActor, oTarget, sRollType, sAbility, oExtra = {}) {
    const {
      skill = '',
      saveType = '',
      weapon = undefined,
      spell = undefined
    } = oExtra
    // completer la caractéristique si la compétence est spécifiée
    if (sRollType === CONST.ROLL_TYPE_CHECK && skill && !sAbility) {
      sAbility = this.getSkillAbility(skill)
    }

    const pFilter = ([key, value]) => {
      // filtrer les avantages qui ont le rolltype, ability ou skill requis
      const { abilities = null, rollTypes, skills = null, saveTypes = null, conditions } = value
      if (!rollTypes.includes(sRollType)) {
        // le jet de dé n'est pas du type supporté par cette circonstance
        return false
      }
      if (abilities && !abilities.includes(sAbility)) {
        // le jet de dé n'est pas de la caractéristique supportée par cette circonstance
        return false
      }
      if (sRollType === CONST.ROLL_TYPE_CHECK && skills && !skills.includes(skill)) {
        // le jet de dé ne fait pas intervenir un skill géré par cette circonstance
        return false
      }
      if (sRollType === CONST.ROLL_TYPE_SAVE && saveTypes && !saveTypes.includes(saveType)) {
        // Le jet de dé n'est pas un jet de sauvegarde lié à cette circonstance
        return false
      }
      // tester par le script
      return conditions.every(sc => this._scriptManager.runScriptSync(sc, {
        subject: oActor,
        target: oTarget,
        weapon,
        spell
      }))
    }

    return {
      advantages: Object
        .entries(DATA.ADVANTAGES)
        .filter(pFilter)
        .map(([key, value]) => key),
      disadvantages: Object
        .entries(DATA.DISADVANTAGES)
        .filter(pFilter)
        .map(([key, value]) => key)
    }
  }

  /**
   * Lance un D20 applique des avantages ou desavantage si necessaire
   * @param bAdvantaged {boolean} indique si le dé doit etre avantagé
   * @param bDisadvantaged {boolean} indique si le dé doit etre désavantagé
   * @returns {number} nombre aléatoire entre 1 et 20
   */
  d20 (bAdvantaged = false, bDisadvantaged = false) {
    const n = (bAdvantaged ? 1 : 0) + (bDisadvantaged ? -1 : 0)
    switch (n) {
      case -1: // disadvantage
        return Math.min(this.d20(), this.d20())

      case 1: // advantage
        return Math.max(this.d20(), this.d20())

      default:
        return this._dice.roll(20)
    }
  }

  /**
   * Aggreger les amplitudes et valeurs des effets et propriétés affectant une entité en tenant compte d'une
   * éventuelle structure Against passé en paramètre
   * @param oEntity {EntityState|EntityState[]}
   * @param aPropEffects {array}
   * @param against {AgainstStruct}
   * @param effectFilter {function}
   * @param propFilter {function}
   * @return {{ effects: array, properties: array, sum: number, max: number, min: number }}
   */
  aggregateModifiers (oEntity, aPropEffects, { effectFilter = null, propFilter = null, against = null } = {}) {
    const oSetPropEffects = new Set(
      Array.isArray(aPropEffects)
        ? aPropEffects
        : [aPropEffects]
    )
    const bActor = this.isActor(oEntity) // on va aggreger les items equipé par l'entité
    const bItem = this.isItem(oEntity) // il s'agit d'une simple entité
    // là il s'agit d'un array d'item probablement. mais on vérifié
    const bArrayOfItems = Array.isArray(oEntity) && oEntity.every(e => this.isItem(e))
    if (!bActor && !bItem && !bArrayOfItems) {
      console.error(oEntity)
      throw new Error('given parameter is neither Actor, nor Item, nor Item[]...')
    }
    // ITEM PROPERTIES
    const aAllIP = bActor
      ? this.getEquipmentItemProperties(oEntity)
      : bItem
        ? this.getItemProperties(oEntity)
        : bArrayOfItems
          ? oEntity.map(item => this.getItemProperties(item)).flat()
          : []
    // filtrer par nom de propriété et par against-compliance
    const aFilteredIP = aAllIP.filter(ip =>
      oSetPropEffects.has(ip.property) &&
      (propFilter ? propFilter(ip) : true) &&
      (
        against
          ? this.isItemPropertyOrEffectCompatibleAgainst(ip, against)
          : this.isItemPropertyOrEffectCompatibleWithAll(ip)
      )
    )
    // réduire
    const nIPAcc = aFilteredIP.reduce((prev, curr) => prev + curr.value, 0)
    const nIPMax = aFilteredIP.reduce((prev, curr) => Math.max(prev, curr.value), 0)
    const nIPMin = aFilteredIP.reduce((prev, curr) => Math.min(prev, curr.value), nIPMax)
    // Pareil pour les EFFETS
    const aAllEffects = bActor
      ? this.getEquipmentActiveEffects(oEntity)
      : bItem
        ? this.getActiveEffects(oEntity)
        : bArrayOfItems
          ? oEntity.map(item => this.getActiveEffects(item)).flat()
          : []
    const aFilteredEffects = aAllEffects.filter(eff =>
      oSetPropEffects.has(eff.tag) &&
      (effectFilter ? effectFilter(eff) : true) &&
      (
        against
          ? this.isItemPropertyOrEffectCompatibleAgainst(eff, against)
          : this.isItemPropertyOrEffectCompatibleWithAll(eff)
      )
    )
    const nEffAcc = aFilteredEffects.reduce((prev, curr) => prev + curr.amp, 0)
    const nEffMax = aFilteredEffects.reduce((prev, curr) => Math.max(prev, curr.amp), 0)
    const nEffMin = aFilteredEffects.reduce((prev, curr) => Math.min(prev, curr.amp), nEffMax)
    return {
      properties: aFilteredIP,
      effects: aFilteredEffects,
      sum: nIPAcc + nEffAcc,
      max: Math.max(nIPMax, nEffMax),
      min: Math.min(nIPMin, nEffMin)
    }
  }

  /**
   * Renvoie l'equipement défensif equippé d'un acteur
   * Cela comprend touts les objet equipé sauf les armes
   * @param oActor {EntityState}
   * @return {EntityState[]}
   */
  getEquippedDefensiveGears (oActor) {
    this.checkActor(oActor)
    const aGear = []
    const aeq = oActor.equip
    for (const [slot, oItem] of Object.entries(aeq)) {
      const bt = oItem
        ? oItem.itemBaseType
        : ''
      // un equippement défensif est un item qui n'est pas une arme ni une munition
      if (
        bt !== '' &&
        bt !== CONST.ITEM_BASE_TYPE_WEAPON &&
        bt !== CONST.ITEM_BASE_TYPE_AMMO
      ) {
        aGear.push(oItem)
      }
    }
    return aGear
  }

  /**
   *
   * @param oSource
   * @returns {AttackOutcome}
   */
  buildAttackOutcome (oSource = {}) {
    return {
      roll: 0,
      bonus: 0,
      bonusDetail: {
        ability: 0,
        weapon: 0,
        proficiency: 0
      },
      advantage: false,
      disadvantage: false,
      advantages: [],
      disadvantages: [],
      ability: '',
      critical: false,
      hit: false,
      miss: '',
      weapon: null,
      ammo: null,
      spell: null,
      damages: [],
      damageSources: [],
      ac: 0,
      effects: [],
      distance: 0,
      ...oSource
    }
  }

  /**
   * Détermine la caractéristique d'attaque idéale.
   * @param oActor {EntityState} entité qui attaque
   * @param oWeapon {EntityState} arme utilisée
   * @param thrown {Boolean} arme lancée
   * @returns {{bonus: number, ability: (string)}}
   */
  getSuitableAttackAbility (oActor, oWeapon, thrown) {
    const bRangedWeapon = oWeapon && oWeapon.ranged
    // arme utilisée
    const bSuccessfullyThrown = thrown && oWeapon.attributes.includes(CONST.WEAPON_ATTRIBUTE_THROWN)
    const bWeaponFinesse = oWeapon
      ? oWeapon.attributes.includes(CONST.WEAPON_ATTRIBUTE_FINESSE)
      : true
    const nStrBonus = this.getAbilityBonus(oActor, CONST.ABILITY_STRENGTH)
    const nDexBonus = this.getAbilityBonus(oActor, CONST.ABILITY_DEXTERITY)
    const bDexGtStr = nDexBonus >= nStrBonus
    const bWillUseDex = bRangedWeapon || (bWeaponFinesse && bDexGtStr) || bSuccessfullyThrown
    // TODO prévoir le coup d'utiliser la sagesse plutot que la dex pour viser avec une arme à distance
    const sAtkAbility = bWillUseDex ? CONST.ABILITY_DEXTERITY : CONST.ABILITY_STRENGTH
    const nAbilityBonus = bWillUseDex ? nDexBonus : nStrBonus
    return {
      ability: sAtkAbility,
      bonus: nAbilityBonus
    }
  }

  /**
   * effectue un jet d'attaque avec tous les bonus associés
   * @param oAttacker {EntityState} actor qui attaque
   * @param oDefender {EntityState} actor qui est attaqué
   * @param weapon {EntityState} arme utilisé lors de l'attaque
   * @param ammo {EntityState} munition utilisé lors de l'attaque
   * @param ability {string} dans le cas ou l'attaque est un sort, ce paramètre permet de choisir l'atk ability.
   * @param spell {object} donnée de spell
   * @param thrown {boolean} si le mode thrown est activé, alors l'arme est lancée
   * @returns {{critical: boolean, attack: *, roll: number, ability: (string), miss: boolean}}
   */
  computeAttackRoll (oAttacker, oDefender, {
    weapon,
    ammo = undefined,
    ability = '',
    spell = null,
    thrown = false
  } = {}) {
    this.checkActor(oAttacker)
    oAttacker.getLast.weaponUsed = weapon
    oDefender.getLast.attacker = oAttacker
    const oAgainst = oDefender
      ? this.buildAgainstStruct(oDefender, weapon)
      : null
    // avant toute chose : concealment
    const nConcealment = this.aggregateModifiers(oDefender, [
      CONST.EFFECT_CONCEALMENT,
      CONST.ITEM_PROPERTY_CONCEALMENT
    ], {
      against: oAgainst
    }).max
    if (nConcealment > 0 && Dice.random() >= nConcealment) {
      return this.buildAttackOutcome({ miss: CONST.ATTACK_MISS_CONCEALMENT })
    }
    const bRangedWeapon = weapon && (weapon.ranged || thrown)
    // arme de mélée, mais la cible est trop loin
    if (!bRangedWeapon && this.getDistance(oAttacker, oDefender) > this.getMeleeWeaponRange(oAttacker, weapon)) {
      return this.buildAttackOutcome({ miss: CONST.ATTACK_MISS_OUT_OF_MELEE_RANGE })
    }
    // arme utilisée
    let nAbilityBonus
    let sAtkAbility
    if (ability === '') {
      const saa = this.getSuitableAttackAbility(oAttacker, weapon, thrown)
      nAbilityBonus = saa.bonus
      sAtkAbility = saa.ability
    } else {
      sAtkAbility = ability
      nAbilityBonus = this.getAbilityBonus(oAttacker, sAtkAbility)
    }
    // dé 20
    const { advantages, disadvantages } = this.getRollCircumstances(oAttacker, oDefender, CONST.ROLL_TYPE_ATTACK, sAtkAbility, {
      weapon,
      spell
    })
    const bRollDisadvantage = disadvantages.length > 0
    const bRollAdvantage = advantages.length > 0
    const nRoll = this.d20(bRollAdvantage, bRollDisadvantage)
    // bonus additionnel à l'attaque contre le defender
    // Pour les bonus : On va prendre en compte tout l'équipement
    // SAUF les armes... à l'expection de l'arme implquée dans l'attaque
    // ainsi si l'attaquant a deux armes, une dans chaque main
    // il n'y aura que l'arme qui frappe que l'on va compter pour l'attaque.
    const aGear = this.getEquippedDefensiveGears(oAttacker)
    if (weapon) {
      aGear.push(weapon)
    }
    if (ammo) {
      aGear.push(ammo)
    }
    const nAtkBonusWeaponPropEffects = this.aggregateModifiers(aGear, [
      CONST.ITEM_PROPERTY_ATTACK_MODIFIER,
      CONST.ITEM_PROPERTY_ENHANCEMENT,
      CONST.EFFECT_ATTACK_MODIFIER,
      CONST.EFFECT_ENHANCEMENT
    ], { against: oAgainst }).sum

    // Proficiency par niveau
    const nProfBonus = this.isProficient(oAttacker, weapon)
      ? this.getProficiencyBonus(oAttacker)
      : 0

    const bonus = nAbilityBonus + nAtkBonusWeaponPropEffects + nProfBonus

    // hit
    const nRollBonus = nRoll + bonus
    let miss = ''
    const critical = nRoll >= DATA.VARIABLES.CRITICAL_THREAT_RANGE
    const fumble = nRoll <= 1
    const ac = this.getAC(oDefender, oAttacker, weapon)
    const bAtkGtAC = nRollBonus >= ac
    const hit = !fumble && (bAtkGtAC || critical)
    if (!hit) {
      miss = fumble ? CONST.ATTACK_MISS_FUMBLE : CONST.ATTACK_MISS_AC
    }

    return this.buildAttackOutcome({
      roll: nRoll,
      bonus,
      bonusDetail: {
        ability: nAbilityBonus,
        weapon: nAtkBonusWeaponPropEffects,
        proficiency: nProfBonus
      },
      advantage: bRollAdvantage,
      disadvantage: bRollDisadvantage,
      advantages,
      disadvantages,
      ability: sAtkAbility,
      ac,
      critical,
      hit,
      miss,
      weapon,
      ammo,
      spell
    })
  }

  /**
   * Permet d'aggreger une structure de dégâts
   * @param aDamages {DamageOutput[]}
   * @param aAggregator {DamageOutput[]}
   * @private
   */
  aggregateDamages (aDamages, aAggregator) {
    aDamages.forEach(({ amount, type }) => {
      // rechercher s'il existe deja ce type de dégâts
      const oSuitableDmg = aAggregator.find(d => d.type === type)
      if (oSuitableDmg) {
        oSuitableDmg.amount += amount
      } else {
        aAggregator.push({
          amount,
          type
        })
      }
    })
  }

  /**
   * Renvoie true si l'arme spécifiée est equippée par l'acteur en tant qu'arme principale
   * @param oActor {EntityState}
   * @param oWeapon {EntityState}
   * @returns {boolean}
   */
  isMainEquippedWeapon (oActor, oWeapon) {
    const oEquWeapons = [
      this.getEquippedItem(oActor, CONST.EQUIPMENT_SLOT_WEAPON_RANGED),
      this.getEquippedItem(oActor, CONST.EQUIPMENT_SLOT_WEAPON_MELEE),
      this.getEquippedItem(oActor, CONST.EQUIPMENT_SLOT_CWEAPON_R),
      this.getEquippedItem(oActor, CONST.EQUIPMENT_SLOT_CWEAPON_L),
      this.getEquippedItem(oActor, CONST.EQUIPMENT_SLOT_CWEAPON_B)
    ]
    return oEquWeapons.includes(oWeapon)
  }

  /**
   * Renvoie true si l'arme spécifiée est equippée par l'acteur en tant qu'arme secondaire
   * @param oActor {EntityState}
   * @param oWeapon {EntityState}
   * @returns {boolean}
   */
  isSecondaryEquippedWeapon (oActor, oWeapon) {
    const oEquWeapons = [
      this.getEquippedItem(oActor, CONST.EQUIPMENT_SLOT_WEAPON_2ND_MELEE)
    ]
    return oEquWeapons.includes(oWeapon)
  }

  /**
   * Calcule le détails des dégâts infligé lors d'une attaque
   * @param oAttacker {EntityState}
   * @param oDefender {EntityState}
   * @param oAttackOutcome {AttackOutcome}
   * @returns {AttackOutcome}
   */
  computeAttackDamage (oAttacker, oDefender, oAttackOutcome) {
    // si l'arme n'est pas celle équippée dans la main directrice, on n'ajoute pas le bonus str/dex
    const oWeapon = oAttackOutcome.weapon
    const oAmmo = oAttackOutcome.ammo
    const oShield = this.getEquippedItem(oAttacker, CONST.EQUIPMENT_SLOT_SHIELD)
    const against = this.buildAgainstStruct(oDefender)

    // si l'arme est off-hand, pas de bonus de carac aux dégâts
    const bOffHand = this.isSecondaryEquippedWeapon(oAttacker, oWeapon)
    const bCanVersatile = !bOffHand && !oShield
    const bIsWeapon = this.isWeapon(oWeapon)
    const sAbility = oAttackOutcome.ability
    const nAbilityBonus = bOffHand
      ? Math.min(0, this.getAbilityBonus(oAttacker, sAbility))
      : this.getAbilityBonus(oAttacker, sAbility)
    const bVersatile = bCanVersatile && oWeapon.attributes.includes(CONST.WEAPON_ATTRIBUTE_VERSATILE)
    const sDamageFormula = bIsWeapon
      ? bVersatile
        ? oWeapon.damageVersatile
        : oWeapon.damage
      : '1'
    const sDamageType = bIsWeapon
      ? oWeapon.damageType
      : CONST.DAMAGE_TYPE_CRUSHING
    const nBaseDamage = this._dice.evaluate(sDamageFormula)
    // déterminer les bonus aux dégâts
    // alteration
    // la liste des items pris en compte :
    const aGear = this.getEquippedDefensiveGears(oAttacker)
    if (bIsWeapon) {
      aGear.push(oWeapon)
    }
    if (oAmmo) {
      aGear.push(oAmmo)
    }
    const nEnhBonus = this.aggregateModifiers(aGear, [
      CONST.ITEM_PROPERTY_ENHANCEMENT,
      CONST.EFFECT_ENHANCEMENT
    ], {
      against
    }).sum
    const aDamModifiers = this.aggregateModifiers(aGear, [
      CONST.ITEM_PROPERTY_DAMAGE_MODIFIER,
      CONST.EFFECT_DAMAGE_MODIFIER
    ], {
      against
    })
    // trier et aggreger par type de dégats
    const aDamages = aDamModifiers
      .effects
      .concat(aDamModifiers.properties)
      .map(dm => ({
        amount: this._dice.evaluate(dm.value),
        type: dm.type
      }))
    aDamages.push({
      desc: 'base',
      amount: nBaseDamage,
      type: sDamageType
    }, {
      desc: 'ability',
      amount: nAbilityBonus,
      type: sDamageType
    })
    if (nEnhBonus) {
      aDamages.push({
        desc: 'enhancement',
        amount: nEnhBonus,
        type: sDamageType
      })
    }
    const aDamageRegistry = []
    this.aggregateDamages(aDamages, aDamageRegistry)
    // doubler les dégats en cas de crit
    if (oAttackOutcome.critical) {
      const nCritFactor = this.DATA.VARIABLES.CRITICAL_FACTOR
      aDamageRegistry.forEach(d => {
        d.amount *= nCritFactor
      })
    }
    oAttackOutcome.damages = aDamageRegistry
    oAttackOutcome.damageSources = aDamages
    return oAttackOutcome
  }

  /**
   * Applique des effet de dégâts à la cible de l'attaque outcome
   * @param oAttacker {EntityState}
   * @param oDefender {EntityState}
   * @param oAttackOutcome {AttackOutcome}
   */
  applyAttackDamage (oAttacker, oDefender, oAttackOutcome) {
    const weapon = oAttackOutcome.weapon
    const ammo = oAttackOutcome.ammo
    return oAttackOutcome.damages.map(({ amount, type }) => {
      const eDam = this.createEffect(this.CONST.EFFECT_DAMAGE, {
        value: amount,
        type,
        weapon,
        ammo
      })
      return this.applyEffect(eDam, oDefender, oAttacker)
    })
  }

  /**
   * Renvoie la portée de l'arme de la créature spécifée
   * @param oActor {EntityState}
   * @param oWeapon {EntityState}
   * @returns {number}
   */
  getMeleeWeaponRange (oActor, oWeapon) {
    const nNaturalRange = DATA.SIZES[oActor.size].space
    const bRealWeapon = this.isItem(oWeapon) && oWeapon.itemBaseType === CONST.ITEM_BASE_TYPE_WEAPON
    if (!bRealWeapon) {
      return nNaturalRange
    }
    if (oWeapon.ranged) {
      return Infinity
    }
    const bReach = oWeapon
      ? oWeapon.attributes.includes(CONST.WEAPON_ATTRIBUTE_REACH)
      : false
    return nNaturalRange + (bReach ? DATA.VARIABLES.MELEE_RANGE : 0)
  }

  /**
   * Passe au round suivant
   */
  nextRound () {
    // résolution des effets pour tous le monde
    // filtrer les entité qui ont des effets
    // Pour chaque effet, lancer le processeur d'effet,
    // retirer les effets expirés, et emmettre un evènement pour chaque effets expirés
    Object
      .values(this._entityRegistry)
      .filter(e => this.isActor(e) && e.effects.length > 0)
      .forEach(e => {
        const aRemovedEffects = this._effectProcessor.processEffects(e, this)
        aRemovedEffects.forEach(effect => {
          this.events.emit('effect.expired', { effect })
        })
      })
  }

  /**
   * Calcule la distance entre deux entités
   * @param oEntity1 {EntityState}
   * @param oEntity2 {EntityState}
   * @return {number}
   */
  getDistance (oEntity1, oEntity2) {
    if (oEntity1 === oEntity2) {
      return 0
    }
    const event = {
      entities: [oEntity1, oEntity2],
      distance: null
    }
    this.events.emit('request.distance', event)
    return event.distance || 0
  }

  /**
   * Renvoie la liste des entités les plus proches d'une origine.
   * @param oOriginEntity {EntityState} entité d'origine
   * @param nRadius {number} distance maximale requetée
   * @return {EntityState[]}
   */
  getNearestEntities (oOriginEntity, nRadius) {
    const event = {
      origin: oOriginEntity,
      entities: []
    }
    this.events.emit('request.nearest-entities', event)
    return event.entities
  }

  /**
   * Permet de déterminer le dé de difficulté du sort
   * @param oCaster {EntityState} Spell Caster
   * @param sAbility {string} caractéristique employée
   */
  getSpellDC (oCaster, sAbility) {
    // 8 + your spellcasting ability modifier + your proficiency bonus
    const nBase = 8
    const nProficiency = this.getProficiencyBonus(oCaster)
    const nAbility = this.getAbilityBonus(oCaster, sAbility)
    return nBase + nProficiency + nAbility
  }

  /**
   * Permet de lancer un jet de sauvegarde
   * @param oActor {EntityState} entité qui tente un jet de sauvegarde
   * @param sAbility {string} caractéristique impliquée dans le jet de sauvegarde
   * @param dc {number} difficulté
   * @param oAdversary {EntityState} créature contre laquelle on fait le jet de sauvegarde (pour le calcule des advantage et disadvantages)
   * @param type {string} type de jet de sauvegarde
   * @returns {boolean}
   */
  rollSavingThrow (oActor, sAbility, dc, type = CONST.SAVE_TYPE_ALL, oAdversary = null) {
    const { advantages, disadvantages } = this.getRollCircumstances(oActor, oAdversary, CONST.ROLL_TYPE_SAVE, { saveType: type })
    const nRoll = this.d20(advantages.length > 0, disadvantages.length > 0)
    if (nRoll >= DATA.VARIABLES.ROLL_AUTO_SUCCESS) {
      return true
    }
    if (nRoll <= DATA.VARIABLES.ROLL_AUTO_FAIL) {
      return false
    }
    const against = oAdversary
      ? this.buildAgainstStruct(oAdversary, null)
      : null
    const am = this.aggregateModifiers(oActor, [
      CONST.EFFECT_SAVING_THROW_MODIFIER,
      CONST.ITEM_PROPERTY_SAVING_THROW_MODIFIER
    ], {
      effectFilter: eff => eff.ability === sAbility && (eff.type === CONST.SAVE_TYPE_ALL ? true : eff.type === type),
      propFilter: prop => prop.ability === sAbility && (prop.type === CONST.SAVE_TYPE_ALL ? true : prop.type === type),
      against
    })
    return nRoll + am.sum >= dc
  }

  /**
   * Lance un sort en se servant ded la caractéristique spécifiée
   * @param oCaster {EntityState} lanceur de sort
   * @param idSpell {string} identifiant du sort
   * @param nLevel {number} niveau du sort
   * @param sAbility {string} caractéristique servant a calculer le DC
   * @param [oTarget] {EntityState} cible eventuelle du sort
   * @return {SpellContext}
   */
  castSpell (oCaster, idSpell, nLevel, sAbility, oTarget = undefined) {
    // TODO si le caster n'a qu'une classe ca va
    // mais s'il a plusieurs spellcasting class on fait comment pour déterminer la carac primaire ?
    const oSpellData = DATA.SPELLS[idSpell]
    const bNeedAttackRoll = oSpellData.projectile &&
      oSpellData.targetType.includes(CONST.TARGET_TYPE_CREATURE) &&
      oTarget !== null &&
      this.isActor(oTarget)
    const oSpellContext = {
      fail: false,
      cause: '',
      id: idSpell,
      subject: oCaster,
      target: oTarget,
      ability: sAbility,
      data: oSpellData,
      level: {
        innate: oSpellData.level,
        slot: Math.max(oSpellData.level, nLevel)
      },
      dc: this.getSpellDC(oCaster, sAbility), // difficulty class de base calculée
      attack: null,
      effects: []
    }
    oSpellContext.attack = bNeedAttackRoll
      ? this.computeAttackRoll(
        oCaster,
        oTarget,
        {
          weapon: this.getEquippedItem(oCaster, CONST.EQUIPMENT_SLOT_WEAPON_MELEE),
          ammo: null,
          ability: sAbility,
          spell: oSpellData,
          thrown: oSpellData.projectile
        }
      )
      : null
    // verifier si la portée du sort est suffisante
    const nDistance = this.getDistance(oCaster, oTarget)
    const nSpellRange = oSpellData.range
    const bInRange = nDistance <= nSpellRange
    if (!bInRange) {
      oSpellContext.fail = true
      oSpellContext.cause = CONST.SPELL_FAILURE_CAUSE_OUT_OF_RANGE
    }
    // déterminer le script à utiliser
    this._events.emit('spell.cast', { spell: oSpellContext })
    if (!oSpellContext.fail) {
      this.runScript(oSpellData.script, { spell: oSpellContext })
    }
    if (oSpellData.concentration) {
      this.terminateActiveSpell(oCaster)
      oCaster.getLast.activeSpell = oSpellContext
    }
    return oSpellContext
  }

  /**
   * check if a spell is resisted
   * @param oSpellContext {SpellContext}
   * @param oTarget {EntityState}
   * @returns {boolean}
   */
  isSpellResisted (oSpellContext, oTarget) {
    // si target possède certaines protection, spell-mantle, invulnerability-globe, immunity... il peut resister
    // Aggreger tous les effets SPELL_IMMUNITY
    const aProtectors = this
      .getActiveEffects(oTarget, eff => eff.tag === CONST.EFFECT_SPELL_IMMUNITY)
      .filter(eff => eff.canProtectFromSpell(oSpellContext))
    if (aProtectors.length > 0) {
      aProtectors.forEach(eff => {
        eff.reduceBuffer(oSpellContext)
      })
      this.events.emit('spell.deflected', { spell: oSpellContext })
      return true
    } else {
      return false
    }
  }

  /**
   * Termine le sort actuellement maintenu par un acteur
   * @param oActor {EntityState}
   */
  terminateActiveSpell (oActor) {
    const oSpellContext = oActor.getLast.activeSpell
    if (oSpellContext) {
      this.abortSpell(oSpellContext)
      oActor.getLast.activeSpell = null
    }
  }

  /**
   * Termine un sort actuellement maintenu en concentration
   * @param oSpellContext {SpellContext}
   */
  abortSpell (oSpellContext) {
    // tous les effets sont dissipés
    oSpellContext.effects.forEach(e => {
      if (!e.expired) {
        e.terminate()
      }
    })
    oSpellContext.effects = []
  }
}

module.exports = Rules

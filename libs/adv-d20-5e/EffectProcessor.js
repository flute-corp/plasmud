const EFFECTS = require('./effects')
const CONST = require('./consts')

const ROLL_TYPE_METHOD_PARTS = {
  [CONST.ROLL_TYPE_ATTACK]: 'Attack',
  [CONST.ROLL_TYPE_SAVE]: 'Save',
  [CONST.ROLL_TYPE_CHECK]: 'Check'
}

/**
 * @typedef EffectState {object}
 * effect {string} identifiant effet,
 * amp {number} amplitude numérique de l'effet
 * target {string} identifiant cible
 * source {string} identifiant de la source
 * duration {number} durée de l'effet
 * elapsed {number} durée écoulée
 * data {object}
 */

class EffectProcessor {
  constructor () {
    this._Effects = EFFECTS
  }

  getEffectClass (sEffect) {
    if (!(sEffect in this._Effects)) {
      throw new Error(`unknown effect ${sEffect}.`)
    }
    return this._Effects[sEffect]
  }

  /**
   * Creation d'une structure d'effet
   * @param sEffect
   * @param payload
   * @returns {Effect}
   */
  createEffect (sEffect, payload) {
    const PEffect = this.getEffectClass(sEffect)
    const oEffect = new PEffect(payload)
    oEffect.tag = sEffect
    return oEffect
  }

  applyEffect (oEffect, oRules, oTarget, oSource, nDuration) {
    oEffect.duration = nDuration
    oEffect.source = oSource
    oEffect.target = oTarget
    oEffect.process(oRules)
    if (oEffect.duration > 0) {
      oTarget.effects.push(oEffect)
    }
    return oEffect
  }

  removeExpiredEffects (oCreature) {
    const aTerminated = []
    const aEffects = oCreature.effects
    let i = aEffects.length - 1
    while (i >= 0) {
      const eff = aEffects[i]
      if (eff.expired) {
        eff.terminate()
        aEffects.splice(i, 1)
        aTerminated.push(eff)
      }
      --i
    }
    return aTerminated
  }

  /**
   * Procède à l'exécution de tous les effets d'une créature
   * @param oCreature {EntityState}
   * @param oRules {Rules}
   * @returns {Effect[]}
   */
  processEffects (oCreature, oRules) {
    // TODO Vider le cache des effets ici
    oCreature.effects.forEach(eff => {
      eff.process(oRules)
    })
    return this.removeExpiredEffects(oCreature)
  }
}

module.exports = EffectProcessor

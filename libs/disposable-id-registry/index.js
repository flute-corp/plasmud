/**
 * This class provides prefixed auto-incremental identifiers.
 * There is a method to pick an id : getId()
 * and a methode to dispose an id : disposeId()
 * disposing an id makes this id available for future use.
 * @example
 * const dr = new DiscarableRegistry();
 * dr.getId() --> 1
 * dr.getId() --> 2
 * dr.getId() --> 3
 * dr.disposeId(2) // we don't need id "2" any longer
 * dr.getId() --> 2 again,
 * dr.getId() --> 4
 * dr.disposeId(1)
 * dr.disposeId(3)
 * dr.getId() --> 1
 * dr.getId() --> 3
 * dr.getId() --> 5
 */
class DisposableIdRegistry {
  constructor (sPrefix = null) {
    this._registry = []
    this._lastId = 0
    this._prefix = sPrefix
    this._bufferSize = 0
  }

  create (state) {
    const oInstance = DisposableIdRegistry()
    oInstance.state = state
    return oInstance
  }

  get state () {
    return {
      registry: this._registry,
      lastId: this._lastId,
      prefix: this._prefix,
      bufferSize: this.bufferSize
    }
  }

  set state ({ registry, lastId, prefix, bufferSize }) {
    this._registry = registry
    this._lastId = lastId
    this._prefix = prefix
    this._bufferSize = bufferSize
  }

  get bufferSize () {
    return this._bufferSize
  }

  set bufferSize (value) {
    this._bufferSize = value
  }

  get prefix () {
    return this._prefix
  }

  set prefix (value) {
    this._prefix = value
  }

  getId () {
    if (this._registry.length > this._bufferSize) {
      return this._registry.shift()
    } else {
      ++this._lastId
      return this._prefix ? (this._prefix + this._lastId) : this._lastId
    }
  }

  disposeId (id) {
    this._registry.push(id)
  }
}

module.exports = DisposableIdRegistry


function isPrime (n) {
  // Corner case
  if (n <= 1) {
    return false
  }

  // Check from 2 to n-1
  const n2 = Math.floor(n / 2)
  for (let i = 2; i < n2; ++i) {
    if (n % i === 0) {
      return false
    }
  }
  return true
}

function getNthPrimeBelowValue (nth, value) {
  for (let i = 0; i < nth; ++i) {
    if (i > 0) {
      --value
    }
    while (!isPrime(value)) {
      --value
    }
    if (value <= 2) {
      break
    }
  }
  return value
}

function getNthPrimeAboveValue (nth, value) {
  for (let i = 0; i < nth; ++i) {
    if (i > 0) {
      ++value
    }
    while (!isPrime(value)) {
      ++value
    }
  }
  return value
}

function getPrimesBetween (n1, n2, nMaxCount = Infinity) {
  const aPrimes = []
  if (nMaxCount === 0) {
    return aPrimes
  }
  for (let n = n1; n <= n2; ++n) {
    if (isPrime(n)) {
      aPrimes.push(n)
      if (aPrimes.length >= nMaxCount) {
        break
      }
    }
  }
  return aPrimes
}

function getPrimesBetweenDesc (n1, n2, nMaxCount = Infinity) {
  const aPrimes = []
  if (nMaxCount === 0) {
    return aPrimes
  }
  for (let n = n2; n >= n1; --n) {
    if (isPrime(n)) {
      aPrimes.push(n)
      if (aPrimes.length >= nMaxCount) {
        break
      }
    }
  }
  return aPrimes.sort()
}

module.exports = {
  isPrime,
  getNthPrimeAboveValue,
  getNthPrimeBelowValue,
  getPrimesBetween,
  getPrimesBetweenDesc
}
